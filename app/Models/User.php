<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable 
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

  public $table = 'users';

    public $fillable = ['company_id','profile_pic','gender','fullname','position','birthdate','email','personal_email','contact','emergency_contact','address','info','role','activation_code','temp_photo','confirm_code','password','note','deactive_date','joining_date','resign_date','serving_date','is_verified','device_id','pay_code'];

    protected $hidden = ['remember_token','created_at','updated_at'];

    protected $dates = ['deleted_at'];

  public function getBirthdateAttribute($value)
  {
      return date("d-m-Y", strtotime($value));
  }
  public function setBirthdateAttribute($value)
  {
    $this->attributes['birthdate'] = date('Y-m-d',strtotime($value));
  }
  public function setDeactiveDateAttribute($value)
  {
    $this->attributes['deactive_date'] = date('Y-m-d',strtotime($value));
  }

  public function getJoiningDateAttribute($value)
  {
      return date("d-m-Y", strtotime($value));
  }
  public function setJoiningDateAttribute($value)
  {
    $this->attributes['joining_date'] = date('Y-m-d',strtotime($value));
  }
  public function getResignDateAttribute($value)
  {
      return (!empty($value)) ? date("d-m-Y", strtotime($value)) : ""; 
  }
  public function setResignDateAttribute($value)
  {
      if (!empty($value)) {
        $this->attributes['resign_date'] = date('Y-m-d',strtotime($value));
      }else{
        $this->attributes['resign_date'] = NULL;
      }
  }
  public function getServingDateAttribute($value)
  {
      return (!empty($value)) ? date("d-m-Y", strtotime($value)) : ""; 
  }
  public function setServingDateAttribute($value)
  {
      if (!empty($value)) {
        $this->attributes['serving_date'] = date('Y-m-d',strtotime($value));
      }else{
        $this->attributes['serving_date'] = NULL;
      }
  }

  // public function getProfilePicAttribute($value)
  // {
  //     return url("upload").'/'.$value;
  // }
  public function countPending() {
    return $this->hasMany('App\Models\Leave','user_id','id');
  }

  public function countApprove() {
    return $this->hasMany('App\Models\Leave','user_id','id');
  }

  public function countDisApprove() {
    return $this->hasMany('App\Models\Leave','user_id','id');
  }
  public function userPosition(){
      return $this->hasOne('App\Models\Position','id','position');
  }
}
