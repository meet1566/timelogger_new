<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ATSInstantId extends Model{

	public $table = 'ATS_instantid';
	
	public $fillable = ['instant_id'];
	
}