<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssetsService extends Model
{
    //
    public $table = 'assets_service';

    public $fillable = ['user_id','assets_id','device_type','serial_number','notes'];
}
