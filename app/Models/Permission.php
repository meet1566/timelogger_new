<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model{

	public $table = 'permission';
	public $fillable = ['route','section','description'];
	public $timestamps = false;

	
}