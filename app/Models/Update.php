<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Update extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'updates';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['type','type_id','message','success'];

}
