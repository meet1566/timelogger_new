<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserQuizAnswer extends Model
{

	public $table = 'user_quiz_answers';

	public $fillable = ['user_id','quiz_id','quiz_option_id'];

}
