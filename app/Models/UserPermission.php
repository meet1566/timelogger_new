<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserPermission extends Model{

	public $table = 'user_permission';

	public $fillable = ['user_id','permission_id'];

	public $timestamps = false;
	
}
