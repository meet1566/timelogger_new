<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    public $table = 'positions';

    public $fillable = ['id','position_name'];
}
