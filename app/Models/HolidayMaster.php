<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HolidayMaster extends Model
{
    public $table = 'holiday_master';

	public $fillable = ['id','title'];
}
