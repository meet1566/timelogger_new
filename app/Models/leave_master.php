<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class leave_master extends Model
{
    //
    public $table = 'leave_master';
    public $fillable = ['casual_leave','sick_leave','privilege_leave','status'];
    public $timestamps = true;
}
