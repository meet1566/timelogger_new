<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model{

	public $table = 'notification';
	
	public $fillable = ['id','user_id','title', 'is_read','is_view'];
	
}