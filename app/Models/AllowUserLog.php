<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AllowUserLog extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'allow_user_logs';
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','log_date','from_time','to_time','status','type','reason','user'];
}
