<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model{

	public $table = 'companies';
	
	public $fillable = ['name','slogan','logo', 'website','working_hours', 'theme','domain'];
	
}