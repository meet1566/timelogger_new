<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WorkingDays extends Model
{
	public $table = 'working_days';

	public $fillable = ['id','month','year','working_date','working_hours'];

	public function setWorkingDateAttribute($value) {
		$this->attributes['working_date'] = date('Y-m-d',strtotime($value));
	}
	public function getWorkingDateAttribute($value) {
		return date('d-m-Y',strtotime($value));
	}

}
