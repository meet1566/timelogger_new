<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Holiday extends Model
{

	public $table = 'holidays';

	public $fillable = ['day','date','holiday'];

	public function getDateAttribute($value)
    {
        return date('d-m-Y', strtotime($value));

    }
    public function setDateAttribute($value)
	{
	    $this->attributes['date'] = date('Y-m-d',strtotime($value));
	}
}
