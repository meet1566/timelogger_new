<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuizOption extends Model
{

	public $table = 'quiz_options';

	public $fillable = ['option','quiz_id','is_true'];

}
