<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssetLog extends Model
{
    //
    public $table = 'assets_log';
    public $fillable = ['id','assets_master_id','status'];

    public $timestamps = true;
}
