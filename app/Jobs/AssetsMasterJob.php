<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\AssetsMaster;
use App\helpers\FileHelp;
use Illuminate\Http\Request;

class AssetsMasterJob
{
    use SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $assets_master;
    public function __construct($assets_master)
    {
        $this->assets_master = $assets_master;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {
        $assets_master = $this->assets_master;

        $id = \Request::segment(3);
        $save_assets_master = AssetsMaster::firstOrNew(['id'=>$id]);
        $save_assets_master->fill($assets_master);
        $save_assets_master->save();
        return;
    }
}
