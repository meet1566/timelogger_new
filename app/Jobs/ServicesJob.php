<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;
use App\Models\Services;
use Illuminate\Support\Facades\Input;

class ServicesJob
{
    use SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $services;
    public function __construct($services)
    {
        $this->services = $services;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {
        $services = $this->services;
        $phone_num = implode(',',$services['phone_num']);
        $id = \Request::segment(2);
        $save_services = Services::firstOrNew(['id'=>$id]);
        $save_services->fill($services);
        $save_services->phone_num = $phone_num;
        $save_services->save();
        return;
    }
}
