<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;
use App\Models\HolidayYear;
use Illuminate\Support\Facades\Input;
use App\helpers\FileHelp;

class HolidayYearJob 
{
    use SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $holiday;
    public function __construct($holiday)
    {
        $this->holiday = $holiday;
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {
        $holiday = $this->holiday;

         $id = \Request::segment(2);
        $save_holiday = HolidayYear::firstOrNew(['id'=>$id]);
        $save_holiday->fill($holiday);
        $save_holiday->save();
        return;
    }
}
