<?php
namespace App\Utility;
use Auth,Request;
use App\Models\User;
use App\Models\UserPermission;
use App\Models\Permission;

class CheckPermission
{
	public static function isPermitted($routeName)
	{
		$permitted = false;

		$user = Auth::user();
		$permission_id = Permission::select('id')->where('route', $routeName)->first();

		$check_user_permission = UserPermission::where('user_id',$user->id)->where('permission_id',$permission_id['id'])->count();

    if ( 1 <= $check_user_permission ){
    	$permitted = true;
    }

		return $permitted;
	}
}
