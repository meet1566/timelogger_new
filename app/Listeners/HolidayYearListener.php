<?php

namespace App\Listeners;

use App\Events\HolidayYearEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\HolidayYearJob;
use Illuminate\Foundation\Bus\DispatchesJobs;
class HolidayYearListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    use DispatchesJobs;
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  HolidayYearEvent  $event
     * @return void
     */
    public function handle(HolidayYearEvent $event)
    {
        $holiday = $event->holiday;
        
        $save_data = $this->dispatch(new HolidayYearJob($holiday));
    }
}
