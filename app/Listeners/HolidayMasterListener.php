<?php

namespace App\Listeners;

use App\Events\HolidayMastersEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\HolidayMasterJob;
use Illuminate\Foundation\Bus\DispatchesJobs;
class HolidayMasterListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    use DispatchesJobs;
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  HolidayMasterEvent  $event
     * @return void
     */
    public function handle(HolidayMastersEvent $event)
    {
        $holiday = $event->holiday;

        $save_data = $this->dispatch(new HoliDayMasterJob($holiday));
    }
}
