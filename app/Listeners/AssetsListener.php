<?php

namespace App\Listeners;

use App\Events\AssetsEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\AssetsJob;
use Illuminate\Foundation\Bus\DispatchesJobs;
class AssetsListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
     use DispatchesJobs;
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AssetsLaptopEvent  $event
     * @return void
     */
    public function handle(AssetsEvent $event)
    {
        $laptop_assets = $event->laptop_assets;
        $save_data = $this->dispatch(new AssetsJob($laptop_assets));
    }
}
