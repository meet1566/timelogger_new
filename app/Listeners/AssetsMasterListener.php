<?php

namespace App\Listeners;

use App\Events\AssetsMasterEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\AssetsMasterJob;
use Illuminate\Foundation\Bus\DispatchesJobs;
class AssetsMasterListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    use DispatchesJobs;
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AssetsMasterEvent  $event
     * @return void
     */
    public function handle(AssetsMasterEvent $event)
    {
        $assets_master = $event->assets_master;
        $save_data = $this->dispatch(new AssetsMasterJob($assets_master));
    }
}
