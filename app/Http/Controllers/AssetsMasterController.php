<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Assets;
use App\Models\AssetLog;
use App\Models\AssetsMaster;
use App\Models\AssetsMasterHistory;
use App\Models\User;
use App\Models\AssetsService;
use App\Events\AssetsMasterEvent;
use App\Http\Requests;
use Event;
use DB;

class AssetsMasterController extends Controller
{
   public function index(Request $request)
   {
   		if($request->ajax()){

            $where_str = '1 = ?';
            $where_params = [1];

            if ($request->has('sSearch')) {
                $search = $request->get('sSearch');
                $where_str .= " and (assets.main_title like \"%{$search}%\""
                            ." or users.fullname like \"%{$search}%\""
                            ." or assets.device_type like \"%{$search}%\""
                            ." or DATE_FORMAT(assets_master.start_date, '%d-%m-%Y') like \"%{$search}%\""
                            ." or DATE_FORMAT(assets_master.end_date, '%d-%m-%Y') like \"%{$search}%\""
                             .")";           
            }
            $columns = ['assets_master.id','assets.main_title as title','users.fullname as fullname','assets_master.start_date as startdate','assets_master.status as status','assets_master.end_date as enddate','assets.device_type'];

            $laptop_count = AssetsMaster::select($columns)
                        ->leftjoin('assets','assets.id','=','assets_master.assets_id')
                        ->leftjoin('users','users.id','=','assets_master.user_id')
                        ->whereRaw($where_str,$where_params)
                        ->count();
            $laptops = AssetsMaster::select($columns)
                       ->leftjoin('assets','assets.id','=','assets_master.assets_id')
                       ->leftjoin('users','users.id','=','assets_master.user_id')
                    ->whereRaw($where_str,$where_params);
           

            if ($request->has('iDisplayStart') && $request->get('iDisplayLength') != '-1') {
                $laptops = $laptops->take($request->get('iDisplayLength'))->skip($request->get('iDisplayStart'));
            }

            if ($request->has('iSortCol_0')) {
                for ($i = 0; $i < $request->get('iSortingCols'); $i++) {
                    $column = $columns[$request->get('iSortCol_' . $i)];
                    if (false !== ($index = strpos($column, ' as '))) {
                        $column = substr($column, 0, $index);
                    }
                    $laptops = $laptops->orderBy($column, $request->get('sSortDir_' . $i));
                }
            }
            
            $laptops = $laptops->get();
            $response['iTotalDisplayRecords'] = $laptop_count;
            $response['iTotalRecords'] = $laptop_count;

            $response['sEcho'] = intval($request->get('sEcho'));

            $response['aaData'] = $laptops;
            // print_r($response);
            return $response;
        }

        return view('assetsMaster.index');
   }

   public function ShowHistory(Request $request)
   {
        if($request->ajax()){

            $where_str = '1 = ?';
            $where_params = [1];

            if ($request->has('sSearch')) {
                $search = $request->get('sSearch');
                $where_str .= " and (assets.main_title like \"%{$search}%\""
                            ." or users.fullname like \"%{$search}%\""
                            ." or DATE_FORMAT(assets_master_history.start_date, '%d-%m-%Y') like \"%{$search}%\""
                            ." or DATE_FORMAT(assets_master_history.end_date, '%d-%m-%Y') like \"%{$search}%\""
                             .")";           
            }
            $columns = ['assets_master_history.id','assets.main_title as title','users.fullname as fullname','assets_master_history.start_date as startdate','assets_master_history.end_date as enddate','assets.device_type'];

            $laptop_count = AssetsMasterHistory::select($columns)
                        ->leftjoin('assets','assets.id','=','assets_master_history.assets_id')
                        ->leftjoin('users','users.id','=','assets_master_history.user_id')
                        ->whereRaw($where_str,$where_params)
                        ->count();
            $laptops = AssetsMasterHistory::select($columns)
                       ->leftjoin('assets','assets.id','=','assets_master_history.assets_id')
                       ->leftjoin('users','users.id','=','assets_master_history.user_id')
                       ->whereRaw($where_str,$where_params);

           
            if ($request->has('iDisplayStart') && $request->get('iDisplayLength') != '-1') {
                $laptops = $laptops->take($request->get('iDisplayLength'))->skip($request->get('iDisplayStart'));
            }

            if ($request->has('iSortCol_0')) {
                for ($i = 0; $i < $request->get('iSortingCols'); $i++) {
                    $column = $columns[$request->get('iSortCol_' . $i)];
                    if (false !== ($index = strpos($column, ' as '))) {
                        $column = substr($column, 0, $index);
                    }
                    $laptops = $laptops->orderBy($column, $request->get('sSortDir_' . $i));
                }
            }
            
            $laptops = $laptops->get();
            // dd($laptops);
            $response['iTotalDisplayRecords'] = $laptop_count;
            $response['iTotalRecords'] = $laptop_count;

            $response['sEcho'] = intval($request->get('sEcho'));

            $response['aaData'] = $laptops;
            // print_r($response);
            return $response;
        }

        return view('assetsMaster.assets_history');
    }

    public function AssetsStatus($id)
    {
        $assets_master = Assets::select('service_status')->where('id',$id)->first();
        $assets_service = AssetsService::select('notes')->where('assets_id',$id)->where('status','InProcess')->first();
        if ($assets_service == null) {
            return response()->json(['message'=>false,'message_type'=>'error','msg'=>'Please First Make Device In InProcess']);
        }
        // dd($assets_master);
        if($assets_master['service_status'] == "outward")
        {
            $status = "inward";
            $assets_status = Assets::find($id);
            $assets_status->service_status = $status;
            $assets_status->update();

            $assets_service = AssetsService::where('assets_id',$id)->where('status','InProcess')->first();
            $assets_service->status = "Resolved";
            $assets_service->save(); 

            $assets_log = new AssetLog();
            $assets_log->assets_master_id = $id;
            $assets_log->notes = "Issue Resolved" .' : '. $assets_service['notes'];
            $assets_log->status = $status;
            $assets_log->save();
        }else
        {
            $status = "outward";
            $assets_status = Assets::find($id);
            $assets_status->service_status = $status;
            $assets_status->update();

            $assets_log = new AssetLog();
            $assets_log->assets_master_id = $id;
            $assets_log->notes = $assets_service['notes'];
            $assets_log->status = $status;
            $assets_log->save();
        }
        return response()->json(['message'=>true,'message_type'=>'success','msg'=>'Status Change successfully']);
    }

    public function ShowAssetsHistory($id)
    {   

        // DB::enableQueryLog();
        $assets_history = AssetsMaster::select('id','assets_id','user_id','status')
        ->with(['assetUser' => function($query){
            $query->select('id','fullname');
        }])
        ->with(['assetHistory' => function($query) {
            $query->select('id','device_type','serial_number');
        }])
        ->with(['assetLog' => function($query) {
            $query->select('id','created_at','status','assets_master_id')->orderBy('id','DESC')->limit(5)->get();
        }])
       ->where('id',$id)->first()->toArray(); 

        return response()->json($assets_history);
    }

    public function create()
    {
    		// $user = User::all()->pluck('fullname','id')->toArray();
    		// $assets = Assets::all()->pluck('main_title','id')->toArray();
        $users = DB::table('users')
                 ->selectRaw('id, fullname')
                 ->orderBy('fullname')->get()->toArray(); 
        
        $user = array_column($users, 'fullname', 'id');
 
        $asset = DB::table('assets')
                  ->selectRaw('main_title, id')->where('status','!=','scrap')->get()->toArray(); 
 
        $assets = array_column($asset, 'main_title', 'id');
     		
     	return view('assetsMaster.create',compact('user','assets'));
    }
   public function store(Request $request)
    {
        $this->validate($request,[

            'assets_id' => 'required',
            'user_id' => 'required',  
            'start_date' => 'required',  
        ],[
            'assets_id.required' => 'Please Select Assets',
            'user_id.required' => 'Please Select User',
            'start_date.required' => 'Please Start Date',
        ]);

    	$assets_master = $request->all();
        $assets_status = Assets::find($assets_master['assets_id']);
        $assets_status->status = "Allocated";
        $assets_status->save();
    	// dd($assets_master['assets_id']);
        event(new AssetsMasterEvent($assets_master));
    	if($assets_master['save_button'] == "save_new"){
            return redirect()->back()->with('message','Record Added Successfully')->with('message_type','success');
        }
        return redirect()->route('assets.master.index')
                         ->with('message','Record Added Successfully.')
                         ->with('message_type','success'); 
    }
    public function edit($id)
    {
        $id = base64_decode($id);
        
        $assets_master = AssetsMaster::where('id',$id)->first();
        // $user = User::all()->pluck('fullname','id')->toArray();
        $users = DB::table('users')
                ->selectRaw('id, fullname')
                ->orderBy('fullname')->get()->toArray(); 
        $user = array_column($users, 'fullname', 'id');
   		$assets = Assets::all()->pluck('main_title','id')->toArray();
        
        return view('assetsMaster.edit',compact('assets_master','user','assets'));
    }
    public function update(Request $request)
    {
        $this->validate($request,[

            'assets_id' => 'required',
            'user_id' => 'required',  
            'start_date' => 'required',  
        ],[
            'assets_id.required' => 'Please Select Assets',
            'user_id.required' => 'Please Select User',
            'start_date.required' => 'Please Start Date',
        ]);
        $assets_master = $request->all();

        Event::fire(new AssetsMasterEvent($assets_master));
        if($assets_master['save_button'] == "save"){
            return redirect()->back()
                         ->with('message','Record Updated successfully')
                         ->with('message_type','success');    
        }
        return redirect()->route('assets.master.index')
                         ->with('message','Record Updated successfully')
                         ->with('message_type','success');
    }
    public function delete(Request $request)
    {
        $id = $request->get('id');
        $assets_master = AssetsMaster::where('id', $id)->first();
        AssetsMaster::where('id', $id)->delete();
        $assets_master_history = AssetsMasterHistory::where('assets_id',$assets_master['assets_id'])->first();
        $assets_master_history->end_date = date('d-m-Y'); 
        $assets_master_history->update();
        $Assets = Assets::find($assets_master['assets_id']);
        $Assets->status = "scrap";
        $Assets->update();
        return back()->with('message', 'Record Deleted Successfully.')
            ->with('message_type', 'success');
    }
}
