<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Log;
use App\Models\User;
use Input,DB,Auth;
use App\Helpers\TimeCounter;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
// use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class LogController extends Controller {

    public function index(Request $request) {

        // if($request->ajax()){
            $where_str = "1 = ?";
            $where_param = array(1);

            if($request->has('search.value')) {
                $search = $request->get('search.value');

                 $where_str .= " and DATE_FORMAT(time, '%d-%m-%Y') like '%{$search}%'";

            }
            if(auth()->user()->is_admin){      
                // $users = Log::selectRaw("logs.id,TIME_FORMAT(MAX(check_out), '%h:%i:%s')  AS check_out,TIME_FORMAT(MIN(check_in), '%h:%i:%s') AS check_in,users.fullname as fullname,logs.time,SEC_TO_TIME(SUM(TIME_TO_SEC(time_diff))) AS time_diff,logs.checkin_device_model,logs.checkout_device_model,logs.checkin_imei_no,logs.checkout_imei_no")
                //        /* ->orderBy('time', 'DESC')*/
                //         ->groupby(DB::raw('logs.user_id'))
                //         ->leftjoin('users', 'users.id', '=', 'logs.user_id')
                //         ->whereRaw($where_str,$where_param);
                $users = Log::selectRaw("logs.id,TIME_FORMAT(check_out, '%h:%i:%s')  AS check_out,TIME_FORMAT(check_in, '%h:%i:%s') AS check_in,users.fullname as fullname,logs.time,logs.time_diff,logs.user_id as log_user,logs.checkin_device_id,logs.checkout_device_id")
                        ->where('time',date('Y-m-d'))
                        ->orderBy('logs.check_in', 'asc')
                        // ->groupby(DB::raw('logs.user_id'))
                        ->leftjoin('users', 'users.id', '=', 'logs.user_id')
                        ->whereRaw($where_str,$where_param);
            }else{
                // if($request->has('month')){
                // $month = intval( $request->get('month') );
                // $where_str .= " and MONTH(`time`) = $month";
                // }

                // if($request->has('year')){
                // $year = intval( $request->get('year') );
                //     $where_str .= " and YEAR(`time`) = $year";

                // }
                $month = date('m');
                $year = date('Y');
                $where_str .= " and MONTH(`time`) = $month";
                $where_str .= " and YEAR(`time`) = $year";
                
                $columns = array('logs.id','logs.time','logs.check_in','logs.check_out','logs.time_diff');
                $users_count_c = Log::select('logs.id')
                            ->where('user_id',Auth::id())
                            ->groupby(DB::raw('logs.time'))
                            ->whereRaw($where_str,$where_param)->get();
                $users_count = count($users_count_c);
                $users = Log::selectRaw("logs.id,TIME_FORMAT(MAX(check_out),'%h:%i:%s') as check_out,TIME_FORMAT(MIN(check_in),'%h:%i:%s') as check_in,logs.time,SEC_TO_TIME(SUM(TIME_TO_SEC(time_diff))) as time_diff,logs.time_diff,logs.user_id as log_user")
                        ->where('user_id',Auth::id())
                        ->groupby(DB::raw('logs.time'))
                        ->whereRaw($where_str,$where_param);
                        
            }
            $user = $users->get();
            $users = [];
            $users_times = [];
            $final_arr_users = [];
            $c = 0;
            foreach ($user->toArray() as $key => $value) {
                if(auth()->user()->is_admin){
                    $users[$value['log_user']][$c] = $value;
                    $user_name = null;
                    if($value['checkin_device_id'] != null){
                        $checkin_user_count = User::where('device_id',$value['checkin_device_id'])->count();
                        if($checkin_user_count > 0){
                            $user_names = User::where('device_id',$users[$value['log_user']][$c]['checkin_device_id'])->first()->toArray();
                            $user_name = $user_names['fullname'];
                        }
                    }
                    $users[$value['log_user']][$c]['checkin_user'] = $user_name;

                    $check_out_user_name = null;
                    if($value['checkout_device_id'] != null){
                        $checkout_user_count = User::where('device_id',$value['checkout_device_id'])->count();
                        if($checkout_user_count > 0){
                            $checkout_user_names = User::where('device_id',$users[$value['log_user']][$c]['checkout_device_id'])->first()->toArray();
                            $check_out_user_name = $checkout_user_names['fullname'];
                        }
                    }
                    $users[$value['log_user']][$c]['checkout_user'] = $check_out_user_name;

                    $c++;
                }else{
                    $users[$value['time']][$c] = $value;
                }
                if($value['time_diff'] == null){
                    $users_times[$value['log_user']]['time_diff'][] = "00:00:00";
                }else{
                    $users_times[$value['log_user']]['time_diff'][] = $value['time_diff'];
                }

            }
            // dd($users);
            foreach ($users_times as $key => $value) {
                $counter = new TimeCounter($value['time_diff']);
                $total_time = $counter->get_total_time();
                $users_times[$key]['total'] = $total_time;
            }
            // dd($users_times);

        $all_users = User::where('is_verified','1')->where('users.is_admin','0')->orderBy('fullname')->pluck('fullname','id')->toArray();

        $user_id = User::select('id', 'fullname')
                        ->where('is_verified','1')
                        ->get();
        return view('logs.index',compact('all_users','users','users_times','user_id'));
    }
    
    public function getLogs(Request $request){
        $where_str = "1 = ?";
        $where_param = array(1);
        
        $data = $request->all();
        if($data['log_date'] != null){
            $val = date('Y-m-d',strtotime($data['log_date']));
            $where_str .= " and time = '$val'";
        }
        if($data['user'] != null){
            $val = $data['user'];
            $where_str .= " and logs.user_id = '$val'";
        }
        $users = Log::selectRaw("logs.id,TIME_FORMAT(check_out, '%h:%i:%s')  AS check_out,TIME_FORMAT(check_in, '%h:%i:%s') AS check_in,users.fullname as fullname,logs.time,logs.checkin_device_id,logs.checkout_device_id,logs.time_diff,logs.user_id as log_user")
                        ->orderBy('logs.check_in', 'asc')
                        //->where('is_verified',1)
                        //->groupby(DB::raw('logs.user_id'))
                        ->leftjoin('users', 'users.id', '=', 'logs.user_id')
                        ->where('users.is_admin','0')
                        ->whereRaw($where_str,$where_param);
        $user = $users->get();
        $users = [];
        $users_times = [];
        $final_arr_users = [];
        $c = 0;
        foreach ($user->toArray() as $key => $value) {
            if(auth()->user()->is_admin){
                $users[$value['log_user']][$c] = $value;
                $user_name = null;
                if($value['checkin_device_id'] != null){
                    $checkin_user_count = User::where('device_id',$value['checkin_device_id'])->count();
                    if($checkin_user_count > 0){
                        $user_names = User::where('device_id',$users[$value['log_user']][$c]['checkin_device_id'])->first()->toArray();
                        $user_name = $user_names['fullname'];
                    }
                }
                $users[$value['log_user']][$c]['checkin_user'] = $user_name;

                $check_out_user_name = null;
                if($value['checkout_device_id'] != null){
                    $checkout_user_count = User::where('device_id',$value['checkout_device_id'])->count();
                    if($checkout_user_count > 0){
                        $checkout_user_names = User::where('device_id',$users[$value['log_user']][$c]['checkout_device_id'])->first()->toArray();
                        $check_out_user_name = $checkout_user_names['fullname'];
                    }
                }
                $users[$value['log_user']][$c]['checkout_user'] = $check_out_user_name;

                $c++;
            }else{
                $users[$value['time']][$c] = $value;
            }
            if($value['time_diff'] == null){
                $users_times[$value['log_user']]['time_diff'][] = "00:00:00";
            }else{
                $users_times[$value['log_user']]['time_diff'][] = $value['time_diff'];
            }

        }
        foreach ($users_times as $key => $value) {
            $counter = new TimeCounter($value['time_diff']);
            $total_time = $counter->get_total_time();
            $users_times[$key]['total'] = $total_time;
        }             

        $res_html = view('logs.index_partial',compact('users','users_times'))->render();

        return response()->json(['success' => true,'response_html' => $res_html,'count'=>count($users)], 200);
    }
    public function userCreate($userId)
    {
        return view('logs.user-create',compact('userId'));
    }

    public function userStore(Request $request, $userId)
    {
        $data = $request->all();

        $rules = [
            'date' => 'required',
            'check_in' => 'required',
            'check_out' => 'required'
        ];

        $this->validate($request,$rules);
    }

    public function importlogs(Request $request)
    {        
        $name =$request->excel->getClientOriginalName();   
        $request->excel->move(public_path(),$name);
        $readerData = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xlsx');
        $readerData->setReadDataOnly(TRUE);
        $spreadsheetData = $readerData->load($name); 
        
        $worksheet = $spreadsheetData->getActiveSheet();         
        $highestRow=$worksheet->getHighestRow();
        $highestColumn = $worksheet->getHighestColumn();
        $date = $worksheet->getCellByColumnAndRow(5,4)->getValue();
        $logDate = explode(":", $date);
        $logDate = $logDate[1];
        $logDate = str_replace('/', '-', $logDate);
        $logDate=date('Y-m-d', strtotime($logDate));
        $rowData = $worksheet->rangeToArray('A6:'.$highestColumn.$highestRow,NULL,true,false);
        foreach($rowData as $key => $value){
            $userId = User::where('pay_code',$value[1])->value('id');            
            if(isset($userId))
            {
                Log::updateOrCreate(['time' => $logDate, 'pay_code' => $value[1],'user_id' => $userId],[
                    'pay_code' =>  $value[1],
                    'user_id' => $userId,
                    'lat' =>  "",
                    'long' => "",
                    'time' =>  $logDate,
                    'check_in' => $value[7],
                    'check_out' =>  $value[10],
                    'time_diff' => $value[11],
                    'checkin_device_id' =>  "",
                    'checkout_device_id' => "",
                    'status' =>  $value[12]
                ]);
            }
        }      
        unlink(public_path($name)); 
        return response()->json(['status'=>true,'message'=>'Your File Has Been Uploaded Successfully','message_type'=>'success']);
    }
}
