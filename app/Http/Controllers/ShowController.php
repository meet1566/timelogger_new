<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Position;
use App\Models\User;
use App\Models\Log;
use App\Models\Leave;
use App\Models\leave_master;
use App\Models\WorkingDays;
use App\Models\HolidayYear;
use App\Models\HolidayMaster;
use App\Events\HolidayYearEvent;
use Event;
use Input,Debugbar,Auth,URL,Validator,Redirect;
use App\Utility\ImageResize;
use File,DB,PDF,Mpdf;

class ShowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function log(Request $request,$id)
    {  
        
        
        $month_count =  intval($request['month'])+1;
        
        
        $list_pdf = DB::table("logs")
            ->selectRaw("*,TIME_FORMAT(MAX(check_out),'%h:%i:%s') as check_out,TIME_FORMAT(MIN(check_in),'%h:%i:%s') as check_in,DATE_FORMAT(time, '%d-%m-%Y') as time,SEC_TO_TIME( SUM(TIME_TO_SEC(time_diff))) as time_diff,SUM(TIME_TO_SEC(time_diff)) as time_diff_sec")
            ->whereRaw('MONTH(time) = ?',[$month_count])
            ->whereRaw('YEAR(time) = ?',[$request['year']])
            ->where('user_id',$id)
            ->groupby('time')
            // ->groupby('check_out')
            // ->groupby('check_in')
            // ->groupby('time_diff')
            // ->groupby('time_diff_sec')
            ->leftjoin('users','users.id','=','logs.user_id')
            ->get();
        $array_data = json_decode(json_encode($list_pdf),true);      
            // dd($array_data);
        $count = 0;
        $late_coming = 0;
        foreach ($array_data as $key => $value) {
            if($array_data[0]['user_id'] == 74 || $array_data[0]['user_id'] == 69 || $array_data[0]['user_id'] == 116 ){

                if(($value['time_diff']) < '08:00:00' && '06:00:00' < ($value['time_diff'])){
                    $count++;
                }
            
            }else{

                if(($value['time_diff']) < '09:00:00' && '06:00:00' < ($value['time_diff'])){
                    $count++;
                }
                if ($value['check_in'] > '10:10:00' && $value['check_in'] != '12:00:00'){
                    $late_coming++;
                }
            }
        }
        // dd($late_coming);
        $logged_user_id = Auth::id();
        // dd($request['month']);
        if($request->ajax()){
            ini_set('memory_limit', '-1');
            $dates=[];
            $day_name = [];
            $data = $request->all();
            if($data['month'] == "")
            {
                $data['month'] = date('n')-1;
                $data['year'] = date('Y');
            }
            $data['month'] = $data['month']+1;
            $main_dates = [];

            //calculate day of given month and year
            $count_days = cal_days_in_month(CAL_GREGORIAN,$data['month'], $data['year']);

            for ($i = 1; $i <= $count_days; $i++) {
                $date = $data['year'].'/'.$data['month'].'/'.$i; 
                $dates['dayname'] = date('l', strtotime($date));
                $dates['dates'] = str_pad($i, 2, '0', STR_PAD_LEFT) . "-" .str_pad($data['month'], 2, "0", STR_PAD_LEFT). "-" . $data['year'];
                $dates['holiday_name']='';
                $main_dates[str_pad($i, 2, '0', STR_PAD_LEFT) . "-" .str_pad($data['month'], 2, "0", STR_PAD_LEFT). "-" . $data['year']]=$dates;
            }
             /*===working days====*/
            $mainDates = $main_dates;
            $holiDay = DB::table('holiday_year')
                    ->selectRaw('holiday_date')
                    ->whereMonth('holiday_date','=',$data['month'])
                    ->whereYear('holiday_date','=',$data['year'])
                    ->get()->toArray(); 
            // dd($holiDay);
            $holiDays = array_column($holiDay, 'holiday_date');
            $currentMonthDates = [];
            $c = 0;
            $c1 = 0;

            foreach ($mainDates as $mainDates_key1 => $mainDates_value1) {
                $mainDates_value4 = date("Y-m-d", strtotime($mainDates_key1)); 
            foreach ($holiDays as $mainDates_key3 => $mainDates_value3)
                {
                    if($mainDates_value3 == $mainDates_value4){
                        unset($mainDates[$mainDates_key1]);
                    }           
                }
            }
            foreach ($mainDates as $mainDates_key2 => $mainDatesvalue2) {

                if($mainDatesvalue2 ['dayname'] != "Sunday"){
                    if($mainDatesvalue2 ['dayname'] != "Saturday"){

                    array_push($currentMonthDates, $mainDates_key2);
                    }
                  if($mainDatesvalue2 ['dayname'] == "Saturday"){
                      $c++;
                      if($c==1 || $c==3 || $c==5){

                      array_push($currentMonthDates, $mainDates_key2);
             
                      }
                  }
                }                
            }
            $count_working_hours = count($currentMonthDates)*9;   
            /*=====working days====*/
            
            $current_month_dates = [];
            foreach ($main_dates as $main_dates_key => $main_dates_value) {
              if(strtotime($main_dates_key) < strtotime('now')){
                   array_push($current_month_dates, $main_dates_key);
                }
            }
            $where_str = "1 = ?";
            $where_param = array(1);
            
            if($request->has('search.value')){
                $search = $request->get('search');
                $search = $search['value'];
                $where_str .= " and (DATE_FORMAT(time, '%d-%m-%Y') like '%{$search}%'"
                 ." or check_in like '%{$search}%'"
                ." or check_out like '%{$search}%'"
                ." or time_diff like '%{$search}%'" .")";
            }           

            $month = date('n');
            $year = date('Y');

            if($request->has('month')){
                $month = intval( $request->get('month') );
                $months = $month+1;
                $where_str .= " and MONTH(`time`) = $months";
            }

            if($request->has('year')){
                $year = intval( $request->get('year') );
                $where_str .= " and YEAR(`time`) = $year";
            }

            $columns = array('id','time','check_in','check_out',DB::raw("SEC_TO_TIME( SUM(TIME_TO_SEC(time_diff))) as time_diff"));
            //$columns = array('id','time','check_in','check_out','time_diff');

            $sort_columns = array('id','time','check_in','check_out','time_diff');
            
            $Log = Log::selectRaw("SEC_TO_TIME( SUM(TIME_TO_SEC(time_diff))) as time_diff")->get(); 
            //$Log = Log::select('time_diff')->raw("time_diff")->get();  

            $users_count = Log::select('id')
                        ->where('user_id',$id)
                        ->groupby('user_id','time')
                        ->whereRaw($where_str,$where_param)
                        ->count();

            $users = Log::select($columns)
                    ->where('user_id',$id)
                    ->groupby('user_id','time')
                   ->whereRaw($where_str,$where_param);
            
            if($request->has('start') && $request->get('length') !='-1'){
                $users = $users->take($request->get('length'))
                               ->skip($request->get('start'));
            }
            if($request->has('order'))
            {
                $sql_order='';
                for ( $i = 0; $i < $request->input('order.0.column'); $i++ )
                {
                    $column = $sort_columns[$i];
                    if(false !== ($index = strpos($column, ' as '))){
                        $column = substr($column, 0, $index);
                    }
                    $users = $users->orderBy($column,$request->input('order.'.$i.'.dir'));
                }
            }
            $users = $users->get()->toArray();

            $user_leaves = Leave::select('status','date as leavedate','leave_time')
                            ->where('user_id',$id)
                            ->whereMonth('date','=',$data['month'])
                            ->whereYear('date','=',$year)
                            ->get()->toArray();

            $holiday_date = HolidayYear::select('holiday_date','day')
                               ->whereMonth('holiday_date','=',$data['month'])
                               ->whereYear('holiday_date','=',$year)
                               //->where('day','!=','Sunday')
                               ->get()->toArray();
            $holidayDate = array_column($holiday_date, 'holiday_date');                   

            $working_hr = Log::selectRaw("SEC_TO_TIME(SUM(TIME_TO_SEC(time_diff)))as working_hr")
                                      ->where('user_id',$id)
                                      ->whereMonth('time','=',$data['month'])
                                       ->whereYear('time','=',$year)
                                       ->get()
                                      ->toArray();
            $working_hr = $working_hr[0]['working_hr'];

            $leaves_count = Leave::select('user_id')
                               ->where('user_id',$id)
                               ->where('status','=','a')
                               ->whereMonth('date','=',$data['month'])
                               ->whereYear('date','=',$year)
                               ->count('user_id');

            $leave_hours = ($leaves_count)*9;
           
            if($working_hr != "")
            {
                $working_hr = explode(":", $working_hr);
                $working_hr[0]=$working_hr[0] + $leave_hours;
                $working_hr = implode(":", $working_hr);
            }
            
            $work = "";
            if($working_hr != "")
            {
                $working_hr=$working_hr;
            }else{
                $working_hr=$work;
            }
            $set_date_array = [];
            foreach ($current_month_dates as $key => $value) 
            {
                foreach ($users as $key1 => $value1) 
                {
                    if($value == $value1['time']){

                        $set_date_array[$key]['current_date'] = $value;
                        $set_date_array[$key]['day'] = date('l', strtotime($value));
                        $set_date_array[$key]['id'] = $value1['id'];
                        $set_date_array[$key]['time'] = $value1['time'];
                        $set_date_array[$key]['check_in'] = $value1['check_in'];
                        $set_date_array[$key]['check_out'] = $value1['check_out'];
                        $set_date_array[$key]['time_diff'] = $value1['time_diff'];
                        $set_date_array[$key]['count_working_hours'] = $count_working_hours;
                        $set_date_array[$key]['working_hr'] = $working_hr;
                        $set_date_array[$key]['holidayDate'] = $holidayDate;
                    }else{
                        if(!isset($set_date_array[$key])){

                            $set_date_array[$key]['id'] = "";
                            $set_date_array[$key]['time'] = "";
                            $set_date_array[$key]['check_in'] = "";
                            $set_date_array[$key]['check_out'] = "";
                            $set_date_array[$key]['time_diff'] = "";
                        }
                        $set_date_array[$key]['current_date'] = $value;
                        $set_date_array[$key]['day'] = date('l', strtotime($value));
                        $set_date_array[$key]['count_working_hours'] = $count_working_hours;
                        $set_date_array[$key]['working_hr'] = $working_hr;
                        $set_date_array[$key]['holidayDate'] = $holidayDate;
                        $set_date_array[$key]['count_test'] = $count;
                        $set_date_array[$key]['latecoming_count'] = $late_coming;

                        foreach ($user_leaves as $key3 => $value3) 
                        {
                            $leavedate = date("d-m-Y", strtotime($user_leaves[$key3]['leavedate']));
                            if( $leavedate== $value)
                            {

                                $set_date_array[$key]['userleave'] = $leavedate;
                                $set_date_array[$key]['status'] = $user_leaves[$key3]['status'];
                                $set_date_array[$key]['leave_time'] = $user_leaves[$key3]['leave_time'];
                            }else{

                            }

                        }
                           
                    }
                }
            }
            
            $response['iTotalDisplayRecords'] = $users_count;
            $response['iTotalRecords'] = $users_count;
            $response['sEcho'] = intval($request->get('sEcho'));
            $response['aaData'] = $set_date_array;
            $response['count_working_hours'] = $count_working_hours;
            $response['holidayDate'] = $holidayDate;
            $response['working_hr'] = $working_hr;
            $response['holidayDate'] = $holidayDate;
            $response['count_test'] = $count;
            $response['latecoming_count'] = $late_coming;

            return $response;
        }

        $name = User::select('fullname')->where('id',$id)->first(); 
        $all_users = '';
        if(auth()->user()->is_admin){
            $logged_user_id = '';
        }
        
        return view('show.logs',['id'=>$id,'name'=>$name,'all_users'=>$all_users,'logged_user_id'=>$logged_user_id]);
    }

    public function userWorkingHours($id, $month, $year)
    {
        $logs = Log::select('time','check_in','check_out','time_diff')
                               ->where('user_id',$id)
                               ->whereMonth('time','=',$month)
                               ->whereYear('time','=',$year)
                               ->distinct('time')
                               ->get()->toArray();

        $working_hr = Log::selectRaw("SEC_TO_TIME(SUM(TIME_TO_SEC(time_diff)))as working_hr")
                              ->where('user_id',$id)
                              ->whereMonth('time','=',$month)
                               ->whereYear('time','=',$year)
                               ->get()
                              ->toArray();
        $working_hr = $working_hr[0]['working_hr'];

        $leaves_count = Leave::select('user_id')
                               ->where('user_id',$id)
                               ->where('status','=','a')
                               ->whereMonth('date','=',$month)
                               ->whereYear('date','=',$year)
                               ->count('user_id');

        $leaves_date = Leave::select('date')
                               ->where('user_id',$id)
                               ->where('status','=','a')
                               ->whereMonth('date','=',$month)
                               ->whereYear('date','=',$year)->get()->toArray();
        $leaves_dates = array_column($leaves_date, 'date');

        $leave_hours = ($leaves_count)*9;

        $name = User::select('fullname')->where('id',$id)->first(); 

        return view('show.logs-user-working-hours',compact('id','name','logs','working_hr','leave_hours','leaves_dates'));
    }

    public function userLeaves(Request $request)
    {  

         if($request->ajax()){
                $where_str = "1 = ?";
                $where_param = array(1);
                if($request->has('sSearch')){
                    $search = $request->get('sSearch');

                    $where_str .= " and (DATE_FORMAT(leaves.date, '%d-%m-%Y') like \"%{$search}%\""
                                ." or leaves.leave_type like \"%{$search}%\""
                                ." or leaves.leave_time like \"%{$search}%\""
                                ." or leaves.status like \"%{$search}%\""
                                ." or leaves.leave_purpose like \"%{$search}%\""
                                .")";
                }
                $user_id = Auth::id();
                $where_str .= " and user_id = '$user_id'";

                if($request->has('leavetype')){
                    $leavetype = $request->get('leavetype');
                    if($leavetype != null){
                        $where_str .= " and leave_type = '$leavetype'";
                    }
                }
                //dd($where_str);
                if($request->has('year')){
                    $year = $request->get('year');
                    $where_str .= " and YEAR(`date`) = '$year'";
                }

                //=========
                                $sick_leave = Leave::where('user_id',$user_id)
                                    // ->where('status','a')
                                    ->whereIn('status',array('a','u'))
                                    ->where('leave_type','Sick Leave')
                                    ->where('leave_time','full day')
                                    ->whereRaw($where_str,$where_param)
                                    ->count();
                $sick_half_leaves = Leave::where('user_id',$user_id)
                                    // ->where('status','a')
                                    ->whereIn('status',array('a','u'))
                                    ->where('leave_type','Sick Leave')
                                    ->where('leave_time','half day')
                                    ->whereRaw($where_str,$where_param)
                                    ->count();

                $sick_leave = $sick_leave+($sick_half_leaves/2);
                            
                $privilege_leave = Leave::where('user_id',$user_id)
                                    // ->where('status','a')
                                    ->whereIn('status',array('a','u'))
                                    ->where('leave_type','Privilege Leave')
                                    ->where('leave_time','full day')
                                    ->whereRaw($where_str,$where_param)
                                    ->count();

                $priviledge_half_leaves = Leave::where('user_id',$user_id)
                                    // ->where('status','a')
                                    ->whereIn('status',array('a','u'))
                                    ->where('leave_type','Privilege Leave')
                                    ->where('leave_time','half day')
                                    ->whereRaw($where_str,$where_param)
                                    ->count();
                $privilege_leave = $privilege_leave+($priviledge_half_leaves/2);

                $casual_leave = Leave::where('user_id',$user_id)
                                    // ->where('status','a')
                                    ->whereIn('status',array('a','u'))
                                    ->where('leave_type','Casual Leave')
                                    ->where('leave_time','full day')
                                    ->whereRaw($where_str,$where_param)
                                    ->count();
                $casual_half_leaves = Leave::where('user_id',$user_id)
                                    // ->where('status','a')
                                    ->whereIn('status',array('a','u'))
                                    ->where('leave_type','Casual Leave')
                                    ->where('leave_time','half day')
                                    ->whereRaw($where_str,$where_param)
                                    ->count();

                $casual_leave = $casual_leave+($casual_half_leaves/2);

                // $leave_master = leave_master::select('sick_leave','casual_leave','privilege_leave')->where('user_id',$user_id)->first();

                $res_html = view('leaves.leavelist',['sick_leave'=>$sick_leave,'privilege_leave'=>$privilege_leave,'casual_leave'=>$casual_leave])->render();
                //===========

                $columns = array('id','date','leave_type','leave_time','leave_purpose','status','from_hour','to_hour');
                $leaves_count = Leave::select('user_id')
                            ->whereRaw($where_str,$where_param)
                            ->count();
                if($request->get('leavetype') == null){
                    $leaves = Leave::select($columns);
                }else{
                    
                    $leaves = Leave::select($columns)
                            ->whereRaw($where_str,$where_param);
                }            
                        
                if ($request->has('iDisplayStart') && $request->get('iDisplayLength') != '-1') {
                    $leaves = $leaves->take($request->get('iDisplayLength'))->skip($request->get('iDisplayStart'));
                }

                if ($request->has('iSortCol_0')) {
                    for ($i = 0; $i < $request->get('iSortingCols'); $i++) {
                        $column = $columns[$request->get('iSortCol_' . $i)];
                        if (false !== ($index = strpos($column, ' as '))) {
                            $column = substr($column, 0, $index);
                        }
                        $leaves = $leaves->orderBy($column, $request->get('sSortDir_' . $i));
                    }
                }
                $leaves = $leaves->get();
                foreach($leaves as $key =>$value)
                {

                    $leaves[$key]['day'] = date('l', strtotime($value['date']));

                }
                // $leaves = $leaves->get();
                $response['iTotalDisplayRecords'] = $leaves_count;
                $response['iTotalRecords'] = $leaves_count;
                $response['sEcho'] = intval($request->get('sEcho'));
                $response['aaData'] = $leaves;
                $response['res_html'] = $res_html;
                return $response;
        }
        return view('show.user-leaves');
    }

    //remaning_leave
    public function remaningleave()
    {
        $user_data = User::select('id as user_id','joining_date')->where('is_admin',0)->where('is_verified',1)->get();
        $user_arr = $user_data->toArray();
        $from = date('2021-01-01');
        $to = date('2021-12-31');
        foreach($user_arr as $userid )
        {
            $leave_data_full = Leave::select('user_id','leave_type')->where('leave_type','Privilege Leave')->               where('leave_time','full day')->where('user_id',$userid['user_id'])->                  where('status','a')->whereBetween('date', [$from, $to])->get();

            $leave_data_half = Leave::select('user_id','leave_type')->where('leave_type','Privilege Leave')->               where('leave_time','half day')->where('user_id',$userid['user_id'])->                  where('status','a')->whereBetween('date', [$from, $to])->get();
            
            $full_count = count($leave_data_full);
            $half_count = count($leave_data_half)/2;
            $total_count = 6 - ($full_count+$half_count);

            $year = date('y', strtotime($userid['joining_date']));
            if($year !=  date('y')){

                $divid_leave = 6;

            }else{
                $day = date('d', strtotime($userid['joining_date']));
                
                $month = date('m', strtotime($userid['joining_date']));

                if ($day >= 16) {
                    
                    $month += 1;

                }
                $total_month = 12;
                $finalmonth = $total_month - $month +1;
                $total_leave = $finalmonth * 1.5;
                $divid_leave = $total_leave/3;
                $total_count = 0;
            }
            $user_id = $userid['user_id'];
            $remaning_leave = leave_master::updateOrCreate(['user_id'=> $user_id],
                ['casual_leave'=> $divid_leave,'sick_leave'=> $divid_leave,'privilege_leave'=> $total_count + $divid_leave,'status'=> 0]);
        }
    }

    public function paycount()
    {
        $id = Auth::user()->id;
        $from = date('2021-01-01');
        $to = date('2021-12-31');
        $leave_data_full = Leave::select('user_id','leave_type')->where('leave_type','Privilege Leave')->               where('leave_time','full day')->where('user_id',$id)->where('status','a')->               whereBetween('date', [$from, $to])->get();

        $leave_data_half = Leave::select('user_id','leave_type')->where('leave_type','Privilege Leave')->               where('leave_time','half day')->where('user_id',$id)->where('status','a')->               whereBetween('date', [$from, $to])->get();
            
        $full_count = count($leave_data_full);
        $half_count = count($leave_data_half)/2;
        $total_count = 6 - ($full_count+$half_count);

        $paycount = leave_master::select('user_id','privilege_leave')->where('user_id',$id)->first();
        $privilege_leave = $paycount['privilege_leave'];
        if ($privilege_leave >6) {
            
        $leave_master = leave_master::where('user_id',$id)->update(['privilege_leave'=>$privilege_leave-$total_count,'pay_count'=>$total_count,'status'=>1]);
         return redirect()->back()->with('status', 'your request sent successfully'); 
        }else{
       return redirect()->back()->with('status', 'you can not send request'); 
        }
        // echo "<pre>";
        // print_r($leave_master);
        // exit();
       
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function leave(Request $request,$id)
    {
        if($request->ajax()){

            $where_str = "1=?";

            $where_param =array(1);

            if($request->has('search.value')){

                $search = $request->get('search.value');

                $where_str .= " and DATE_FORMAT(date, '%d-%m-%Y') like \"%{$search}%\""
                            ." or leave_type like \"%{$search}%\""
                            ." or leave_purpose like \"%{$search}%\""
                            .")";
            }
            $columns = array('id', 'date','leave_type','leave_time','leave_purpose','from_hour','to_hour','status');
            $leaves_count = Leave::select('id')
                        ->where('user_id',$id)
                        ->whereRaw($where_str,$where_param)
                        ->count();

            $leaves = Leave::select($columns)
                        ->where('user_id',$id)
                        ->whereRaw($where_str,$where_param)
                        ->orderBy('date','DESC');

            if($request->has('start') && $request->get('length') !='-1'){
            $leaves = $leaves->take($request->get('length'))
                                    ->skip($request->get('start'));
            }

            if($request->has('order')){
                $sql_order='';
                for ( $i = 0; $i < $request->input('order.0.column'); $i++ )
                {
                    $column = $columns[$i];
                    if(false !== ($index = strpos($column, ' as '))){
                        $column = substr($column, 0, $index);
                    }
                    $leaves = $leaves->orderBy($column,$request->input('order.'.$i.'.dir'));
                }
            }
            $leaves = $leaves->get()->toArray();
            foreach($leaves as $key =>$value)
            {

                $leaves[$key]['day'] = date('l', strtotime($value['date']));

            }
            $response['iTotalDisplayRecords'] =$leaves_count;
            $response['iTotalRecords'] = $leaves_count;

            $response['sEcho'] = intval($request->get('sEcho'));

            $response['aaData'] = $leaves;

            return $response;
        }
        
        $username = User::select('fullname')->where('id',$id)->first()->toArray();

        return view('show.leaves',compact('id','username'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function profile(){

        $profile_details = User::select('users.position','positions.position_name as position','joining_date')
                ->leftjoin('positions','positions.id','=','users.position')
                ->where('users.id', '=', Auth::user()->id)
                ->first();    
        $aadhar_array = User::select("aadhar_img")->where('id',Auth::user()->id)->get()->toArray(); 
        $split_img = explode(',',$aadhar_array[0]['aadhar_img']);       
        return view('show.user',compact('profile_details','split_img'));
    }

    public function user_profile(Request $request, $id)
    {
        if($request->ajax()){       
            ini_set('memory_limit', '-1');
            $dates=[];
            $day_name = [];
            $data = $request->all();
            if($data['month'] == "")
            {
                $data['month'] = date('n')-1;
                $data['year'] = date('Y');
            }
            $data['month'] = $data['month']+1;
            $main_dates = [];

            //calculate day of given month and year
            $count_days = cal_days_in_month(CAL_GREGORIAN,$data['month'], $data['year']);

            for ($i = 1; $i <= $count_days; $i++) {
                $date = $data['year'].'/'.$data['month'].'/'.$i; 
                $dates['dayname'] = date('l', strtotime($date));
               $dates['dates'] = str_pad($i, 2, '0', STR_PAD_LEFT) . "-" .str_pad($data['month'], 2, "0", STR_PAD_LEFT). "-" . $data['year'];
               $dates['holiday_name']='';
                $main_dates[str_pad($i, 2, '0', STR_PAD_LEFT) . "-" .str_pad($data['month'], 2, "0", STR_PAD_LEFT). "-" . $data['year']]=$dates;
            }
            $current_month_dates = [];
            foreach ($main_dates as $main_dates_key => $main_dates_value) {
                if(strtotime($main_dates_key) < strtotime('now')){
                   array_push($current_month_dates, $main_dates_key);
                }
            }
            $where_str = "1 = ?";
            $where_param = array(1);
            
            if($request->has('search.value')){
                $search = $request->get('search');
                $search = $search['value'];
                $where_str .= " and (DATE_FORMAT(time, '%d-%m-%Y') like '%{$search}%'"
                 ." or check_in like '%{$search}%'"
                ." or check_out like '%{$search}%'"
                ." or time_diff like '%{$search}%'" .")";
            }           

            $month = date('n');
            $year = date('Y');
            if($request->has('month')){
                $month = intval( $request->get('month') );
                $months = $month+1;
                               
                $where_str .= " and MONTH(`time`) = $months";
            }

            if($request->has('year')){
                $year = intval( $request->get('year') );
                $where_str .= " and YEAR(`time`) = $year";
            }

            $columns = array('id','time','check_in','check_out',DB::raw("SEC_TO_TIME( SUM(TIME_TO_SEC(time_diff))) as time_diff"));
            
            $sort_columns = array('id','time','check_in','check_out','time_diff');
            
            $Log = Log::select('time_diff')->raw("SEC_TO_TIME( SUM(TIME_TO_SEC(time_diff))) as time_diff")->get();  
        
            $users_count = Log::select('id')
                        ->where('user_id',$id)
                        ->groupby('user_id','time')
                        ->whereRaw($where_str,$where_param)
                        ->count();

            $users = Log::select($columns)
                    ->where('user_id',$id)
                    ->groupby('user_id','time')
                   ->whereRaw($where_str,$where_param);
            
            if($request->has('start') && $request->get('length') !='-1'){
                $users = $users->take($request->get('length'))
                               ->skip($request->get('start'));
            }
            if($request->has('order'))
            {
                $sql_order='';
                for ( $i = 0; $i < $request->input('order.0.column'); $i++ )
                {
                    $column = $sort_columns[$i];
                    if(false !== ($index = strpos($column, ' as '))){
                        $column = substr($column, 0, $index);
                    }
                    $users = $users->orderBy($column,$request->input('order.'.$i.'.dir'));
                }
            }
            $users = $users->get()->toArray();
            
            $user_leaves = Leave::select('status','date as leavedate')
                            ->where('user_id',$id)
                            ->whereMonth('date','=',$data['month'])
                            ->whereYear('date','=',$year)
                            ->get()->toArray();               
            $holiday_date = HolidayYear::select('holiday_date','day')
                               ->whereMonth('holiday_date','=',$data['month'])
                               ->whereYear('holiday_date','=',$year)
                               //->where('day','!=','Sunday')
                               ->get()->toArray();
            $holidayDate = array_column($holiday_date, 'holiday_date');    

            $set_date_array = [];
            foreach ($current_month_dates as $key => $value) 
            {
                foreach ($users as $key1 => $value1) 
                {
                    if($value == $value1['time']){

                        $set_date_array[$key]['current_date'] = $value;
                        $set_date_array[$key]['day'] = date('l', strtotime($value));
                        $set_date_array[$key]['id'] = $value1['id'];
                        $set_date_array[$key]['time'] = $value1['time'];
                        $set_date_array[$key]['check_in'] = $value1['check_in'];
                        $set_date_array[$key]['check_out'] = $value1['check_out'];
                        $set_date_array[$key]['time_diff'] = $value1['time_diff'];
                        $set_date_array[$key]['holidayDate'] = $holidayDate;
                    }else{
                        if(!isset($set_date_array[$key])){

                            $set_date_array[$key]['id'] = "";
                            $set_date_array[$key]['time'] = "";
                            $set_date_array[$key]['check_in'] = "";
                            $set_date_array[$key]['check_out'] = "";
                            $set_date_array[$key]['time_diff'] = "";
                        }
                        $set_date_array[$key]['current_date'] = $value;
                        $set_date_array[$key]['day'] = date('l', strtotime($value));
                        $set_date_array[$key]['holidayDate'] = $holidayDate;

                        foreach ($user_leaves as $key3 => $value3) 
                        {
                            $leavedate = date("d-m-Y", strtotime($user_leaves[$key3]['leavedate']));
                            if( $leavedate== $value)
                            {

                                $set_date_array[$key]['userleave'] = $leavedate;
                                $set_date_array[$key]['status'] = $user_leaves[$key3]['status'];
                            }else{

                            }

                        }
                           
                    }
                }
            }  
                
            $response['iTotalDisplayRecords'] = $users_count;
            $response['iTotalRecords'] = $users_count;
            $response['sEcho'] = intval($request->get('sEcho'));
            $response['aaData'] = $set_date_array;
            $response['holidayDate'] = $holidayDate;

            return $response;
        }

        $user_details = User::select('users.*','positions.position_name as position')
                    ->leftjoin('positions','positions.id','=','users.position')
                    ->where('users.id', '=', $id)
                    ->first();    

        $total_leaves = Leave::where('user_id',$id)->get()->count();

        $current_month_leaves = Leave::selectRaw("user_id, date")
                        ->whereMonth('date','=',date('m'))
                        ->whereYear('date','=',date('Y'))
                        ->where('user_id','=',$id)
                        ->get()
                        ->count();      

        //logs graph
        $leaves_log = DB::table("leaves")
            ->selectRaw('COUNT(*) as count,user_id,users.fullname as name,users.is_verified,YEAR(date) as year')
            ->join('users','users.id','=','leaves.user_id')
            ->where('users.is_verified','=','1')
            ->where('users.deleted_at','=',NULL)
           // ->Where('users.deactive_date','=', NULL)
           // ->whereYear('date','=',date('Y'))
            ->groupby(DB::raw('user_id'))
            ->get();  

        $leaves_logs = json_decode(json_encode($leaves_log),true);
        foreach ($leaves_logs as $leaves_log_key => $leaves_log_value) {
            $leaves_count_logs [] = $leaves_log_value['count'];
            $fullname [] = $leaves_log_value['name'];
            $words = explode(" ", $leaves_log_value['name']);
            $username_leaves = "";
            $count = 0;
            foreach ($words as $w) {
                if($count<2){
                    if(isset($w[0])){
                        $username_leaves .= $w[0];
                    }
                }
                $count++;
            }
            $username[] = '"'.$username_leaves.' '.$leaves_log_value['user_id'].'"';
        }
        $leaves_count_logs = implode(',',$leaves_count_logs);
        $username = implode(',',$username); 
        $aadhar_array = User::select("aadhar_img")->where('id',$id)->get()->toArray();                        
        $split_img = explode(',',$aadhar_array[0]['aadhar_img']);           
        return view('user.user-profile',compact('id','user_details','total_leaves','current_month_leaves','leaves_count_logs','username','fullname','split_img'));
    }

    public function edit(Request $request)
    {
        $position = ['NEW'=>"Add New Position"]+Position::orderBy('position_name','asc')->pluck('position_name', 'id')->toArray();
        $user = User::find(Auth::id());

        $profile = User::find(Auth::id());

        return view('show.edit',compact('user','profile','position'));
    }

    public function update(Request $request)
    {
        // $user = User::find(Auth::id());

        // $this->validate($request,[

        //     'fullname' => 'required|regex:/^[a-zA-Z .]*$/',
        //     'position'      => 'required',
        //     'contact'       => 'required|min:10|max:11',
        // ]);
        // $user->fill($request->all());

        // $user->save();

        $this->validate($request,[
            'gender' => 'required',
            'fullname' => 'required|regex:/^[a-zA-Z .]*$/',
            'position' => 'required',
            'birthdate' => 'required',
            'personal_email' => 'required|regex:/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@(gmail.com)$/',
            'contact' => 'required|regex:/^[- +()]*[0-9][- +()0-9]*$/|max:15|min:10',
            'emergency_contact'=> 'required|regex:/^[- +()]*[0-9][- +()0-9]*$/|max:15|min:10|different:contact',
            'address' => 'required',
            'hobby' => 'required'
        ]);
        $user = user::find(Auth::id());   
        $user->gender = $request->gender;
        $user->fullname=$request->fullname;
        $user->position=$request->position;
        $user->birthdate=$request->birthdate;
        $user->joining_date=$request->joining_date;
        $user->contact=$request->contact;
        $user->personal_email=$request->personal_email;
        $user->emergency_contact=$request->emergency_contact;
        $user->address=$request->address;
        $user->hobby=$request->hobby;
        $user->update();

        return redirect()->route('show.user')
                     ->with('message','Your Profile Has Been Updated Successfully')
                     ->with('message_type','success');
    }

    public function getUploadProfilePhoto()
    {
        return view('profile.upload-photo');
    }

    public function postUploadProfilePhoto(Request $request)
    {
        $profile = User::find(Auth::id());

        $rules = ['profile_pic' => 'required|mimes:jpeg,jpg,png|image|max:4096'];

        $validator = Validator::make($request->all(), $rules);

        if ( $validator->passes() )
        {
            $microtime = microtime();
            $search = array('.',' ');
            $microtime = str_replace($search, "_", $microtime);
            $photo = $microtime . '.' . $request->file('profile_pic')->getClientOriginalExtension();

            $path = public_path()."/upload/";

            $old_file = $path . $profile->photo;

            list($width, $height, $type, $attr) = getimagesize($request->file('profile_pic'));

            $save_w = $width;
            $save_h = $height;

            $request->file('profile_pic')->move($path, $photo);

            if ( $save_w >= 500 || $save_h >= 500 )
            {
                $resizeObj = new ImageResize($path . $photo);
                $resizeObj->resizeImage(300, 200, 0);
                $resizeObj->saveImage($path . $photo, 100);
            }
            $profile->temp_photo = $photo;

            $profile->save();

            return redirect()->route('profile.photo.crop.get');
        }
        else
        {
            return back()->withErrors($validator);
        }
    }

    public function getCropProfilePhoto(Request $request)
    {
        $profile = User::find(Auth::id());

        return view('profile.crop-photo',compact('profile'));
    }

    public function postCropProfilePhoto(Request $request) {

        $profile = User::find(Auth::id());

        $photo = $profile->temp_photo;

        $old_photo = $profile->profile_pic;

        $public_path = str_replace("\\","/", public_path());

        $save_folder =  $public_path . '/upload/';
        $photo_file = $save_folder . '/' . $photo;

        if ($old_photo != NULL AND $old_photo != '') {
            $old_photo_path = $save_folder . '/' . $old_photo;

            @unlink($old_photo_path);
        }

        list($width, $height, $type, $attr) = getimagesize($photo_file);

        $save_w = $width;
        $save_h = $height;

        $crop_x = $request->get('x');
        $crop_y = $request->get('y');
        $crop_w = $request->get('w');
        $crop_h = $request->get('h');

        $resizeObj = new ImageResize($photo_file);
        $resizeObj->cropCustomized($crop_x, $crop_y,$crop_w, $crop_h, $crop_w, $crop_h);
        $outputfile = $save_folder . $photo;

        $resizeObj->saveImage($outputfile, 100);

        $profile->profile_pic = $photo;
        $profile->save();

        return Redirect::route('show.edit')->with('message','Your Profile Photo Updated Successfully')
                                           ->with('message_type','success');
    }
    public function exportlogs($id,$month,$year){
       
        $list_pdf = DB::table("logs")
            ->selectRaw("*,TIME_FORMAT(MAX(check_out),'%h:%i:%s') as check_out,TIME_FORMAT(MIN(check_in),'%h:%i:%s') as check_in,DATE_FORMAT(time, '%d-%m-%Y') as time,SEC_TO_TIME( SUM(TIME_TO_SEC(time_diff))) as time_diff,SUM(TIME_TO_SEC(time_diff)) as time_diff_sec")
            ->whereRaw('MONTH(time) = ?',[$month])
            ->whereRaw('YEAR(time) = ?',[$year])
            ->where('user_id',$id)
            ->groupby('time')
            ->leftjoin('users','users.id','=','logs.user_id')
            ->get();
        $array_data = json_decode(json_encode($list_pdf),true);

        $start_date = "01-".$month."-".$year;
        $start_time = strtotime($start_date);
        $end_time = strtotime("+1 month", $start_time);
        $count = -1;
        for($i=$start_time; $i<$end_time; $i+=86400)
        {
            $count++;
           $list[$count]['time'] = date('d-m-Y', $i);
           $list[$count]['check_in']  = "00:00:00"; 
           $list[$count]['check_out'] = "00:00:00";
           $list[$count]['time_diff'] = "00:00:00";
        }
        $arr = [];
        foreach($array_data as $key => $value)
        {
            $arr[$key]['time'] = $value['time'];
            $arr[$key]['check_in'] = $value['check_in'];     
            $arr[$key]['check_out'] = $value['check_out'];     
            $arr[$key]['time_diff'] = $value['time_diff'];    
        }
        
        $merge_array =  array_merge($arr, $list);
        $final = array_reverse(array_values(array_column(array_reverse($merge_array),null,'time')));
        $leave_count_disapprove = leave::select('user_id','DATE_FORMAT(date, "%d-%m-%Y") as date','status')
            ->whereRaw('MONTH(date) = ?',[$month])
            ->whereRaw('YEAR(date) = ?',[$year])
            ->whereIn('status',array('d','u'))
            ->where('user_id',$id)->count();
        $leave = leave::select('user_id','date','status')
            ->whereRaw('MONTH(date) = ?',[$month])
            ->whereRaw('YEAR(date) = ?',[$year])
            ->where('user_id',$id)->get()->toArray();
        $cnt = 0; 
        $leave_status = array_column($leave,'status');
        
        $improper_count = 0;
        $works_hours =[];
        foreach ($array_data as $key => $value) {

            $works_hours[] = $value['time_diff_sec'];
            if(($value['time_diff']) < '09:00:00'){
                $improper_count++;
            }
        }
        $dateObj   = \DateTime::createFromFormat('!m', $month);
        $monthName = $dateObj->format('F'); 
        $works_hours = array_sum($works_hours);
        $w_h = intval($works_hours/3600);
        $r= $works_hours%3600;
        $w_m = intval($r/60);
        
        $total_working_hours =  WorkingDays::selectRaw("SEC_TO_TIME(SUM(TIME_TO_SEC(working_hours))) as working_hours")
                                ->whereRaw('MONTH(working_date) = ?',[$month])
                                ->whereRaw('YEAR(working_date) = ?',[$year])
                                ->get()
                                ->toArray();

        $leave_count_approve = leave::select('user_id','DATE_FORMAT(date, "%d-%m-%Y") as date','status')
            ->whereRaw('MONTH(date) = ?',[$month])
            ->whereRaw('YEAR(date) = ?',[$year])
            ->where('status','a')
            ->where('user_id',$id)->count();
         $leave_count_disapprove = leave::select('user_id','DATE_FORMAT(date, "%d-%m-%Y") as date','status')
            ->whereRaw('MONTH(date) = ?',[$month])
            ->whereRaw('YEAR(date) = ?',[$year])
            ->whereIn('status',array('d','u'))
            ->where('user_id',$id)->count();
            $leave = leave::select('user_id','date','status')
            ->whereRaw('MONTH(date) = ?',[$month])
            ->whereRaw('YEAR(date) = ?',[$year])
            ->where('user_id',$id)->get()->toArray();
            $cnt = 0; 
            $leave_status = array_column($leave, 'status');
        $mpdf = new \Mpdf\Mpdf();
        $mpdf -> SetFont('eurostyle');
        // $header= '<table width="100%"><tr>  
        //     <td width="80%"><b>'.ucwords($array_data[0]['fullname']).' - '.$monthName .' '.$year.' '.'</b>Summary</td>
        //     <td width="20%"><h1>THINK TANKER</h1></td>
        //     </tr></table><br>';
        // $mpdf->SetHTMLHeader($header);
        $mpdf->setFooter("Page {PAGENO} of {nb}");
        $mpdf->autoPageBreak = true;
        $mpdf->WriteHTML(\View::make('show.exportlogs',['pdf_data'=>$final,'improper_count'=>$improper_count,'leave_count_approve'=>$leave_count_approve,'cnt'=>$cnt,'leave_status'=>$leave_status,'leave_count_disapprove'=>$leave_count_disapprove,'works_hours'=>$w_h.':'.$w_m,'month_name'=>$monthName,'year'=>$year,'total_working_hours'=>$total_working_hours,'name'=>$array_data[0]['fullname']])->render());
        $filename = $array_data[0]['fullname'].$month.'-'.$year.'.pdf';
        $filepath = public_path() ."/uploads/" . $filename;
        $mpdf->output($filepath);

        return response()->download($filepath);
    }
    public function allUserExportLogs($id,$month,$year){
        ini_set('memory_limit','523M');
        
        $list_pdf = DB::table("logs")
            ->selectRaw("user_id,TIME_FORMAT(MAX(check_out),'%h:%i:%s') as check_out,TIME_FORMAT(MIN(check_in),'%h:%i:%s') as check_in,SEC_TO_TIME( SUM(TIME_TO_SEC(time_diff))) AS time_diff,time AS Date,users.fullname")
            ->leftjoin('users','users.id','=','logs.user_id')
            ->whereRaw('MONTH(time) = ?',[$month])
            ->whereRaw('YEAR(time) = ?',[$year])
            ->groupby('time','user_id')
            ->get();

        $array_data = json_decode(json_encode($list_pdf),true);
        foreach ($array_data as $key => $value) {
            $unique_date[] = $value['Date'];
            $unique_name[] = $value['fullname'];
        }

        $data = [];
        $unique_date = array_unique($unique_date);
        $unique_name = array_unique($unique_name);
        $index = 0;
        foreach ($unique_date as $date_key => $date_value) {
            foreach ($unique_name as $name_key => $name_value) {
                $data[$index]['date'] = $date_value;
                $data[$index]['name'] = $name_value;
                $index++;
            }
        }

        $logs_data = [];
        $logs_data1 = [];
        $count = 0;
        $flag = true;
        foreach ($data as $data_key => $data_value) {
            foreach($array_data as $single_key => $single_value) {
                if(($data_value['date'] == $single_value['Date']) && ($data_value['name'] == $single_value['fullname'])){
                    $logs_data[$count]['user_id'] = $single_value['user_id'];
                    $logs_data[$count]['check_out'] = $single_value['check_out'];
                    $logs_data[$count]['check_in'] = $single_value['check_in'];
                    $logs_data[$count]['time_diff'] = $single_value['time_diff'];
                    $logs_data[$count]['Date'] = $single_value['Date'];
                    $logs_data[$count]['fullname'] = $single_value['fullname'];
                }else{
                    $flag = false;
                }
            }
            if($flag == false){
                $logs_data1[$count]['user_id'] = 1;
                $logs_data1[$count]['check_out'] = "";
                $logs_data1[$count]['check_in'] = "";
                $logs_data1[$count]['time_diff'] = "";
                $logs_data1[$count]['Date'] = $data_value['date'];
                $logs_data1[$count]['fullname'] = $data_value['name'];
            }
                $count++;
        }

        $main_pdf_arr = array_replace_recursive($logs_data1, $logs_data);
        
        $total_working_hours =  WorkingDays::selectRaw("SEC_TO_TIME(SUM(TIME_TO_SEC(working_hours))) as working_hours")
                                ->whereRaw('MONTH(working_date) = ?',[$month])
                                ->whereRaw('YEAR(working_date) = ?',[$year])
                                 ->get()
                                 ->toArray();
        $exp_total_hours = explode(':',$total_working_hours[0]['working_hours']);                         
        $work_hours = Log::selectRaw("SEC_TO_TIME(SUM(TIME_TO_SEC(time_diff))) as work_hour,user_id")
                            ->whereRaw('MONTH(time) = ?',[$month])
                            ->whereRaw('YEAR(time) = ?',[$year])
                            ->groupby('user_id')
                            ->get()
                            ->toArray();
        // dd($work_hours);
        $improper_count = 0;
        foreach ($array_data as $key => $value) {
            if(($value['time_diff']) < '09:00:00'){
                $improper_count++;
            }
        }
        $dateObj   = \DateTime::createFromFormat('!m', $month);
        $monthName = $dateObj->format('F'); 
        // return view('show.all-user-exportlogs',['pdf_data'=>$array_data,'improper_count'=>$improper_count,'month_name'=>$monthName,'year'=>$year]);
        //$mpdf = new \mPDF('c', 'A4-L','','','15','15','25','18');
        $mpdf = new \Mpdf\Mpdf();
        ini_set("pcre.backtrack_limit", "10000000");
        $mpdf -> SetFont('eurostyle');
        $header= '<table width="100%"><tr>  
            <td width="80%">'.'<h4>Employee Logs'." ".$monthName." ".$year.'</h4></td>
            <td width="20%"><h1>thinkTANKER</h1></td>
            </tr></table>';
        $mpdf->SetHTMLHeader($header);
        $mpdf->setFooter("Page {PAGENO} of {nb}");
        $mpdf->autoPageBreak = true;
        $mpdf->WriteHTML(\View::make('show.all-user-exportlogs',['pdf_data'=>$main_pdf_arr,'improper_count'=>$improper_count,'month_name'=>$monthName,'year'=>$year,'work_hours'=>$work_hours,'exp_total_hours'=>$exp_total_hours])->render());
        $filename = 'pdfview.pdf';
        $filepath = public_path() ."/uploads/" . $filename;
        $mpdf->output($filepath);
        
        return response()->download($filepath);
        
    }
}
