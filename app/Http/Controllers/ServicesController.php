<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Events\ServicesEvent;
use Event,Input;
use App\Models\Services;
use App\Models\Category;

class ServicesController extends Controller
{
    public function index(Request $request)
   {
   		if($request->ajax()){

            $where_str = '1 = ?';
            $where_params = [1];

            if ($request->has('sSearch')) {
                $search = $request->get('sSearch');
                $where_str .= " and (services.phone_num like \"%{$search}%\""
                            ." or services.person_name like \"%{$search}%\""
                            ." or category.category_name like \"%{$search}%\""
                             .")";           
            }
            $columns = ['services.id','services.person_name as person_name','services.phone_num as phone_num','category.category_name as category'];

            $services_count = Services::select($columns)
                        ->leftjoin('category','category.id','=','services.category')
                        ->whereRaw($where_str,$where_params)
                        ->count();
            $services = Services::select($columns)
                        ->leftjoin('category','category.id','=','services.category')
                       ->whereRaw($where_str,$where_params);

            if ($request->has('iDisplayStart') && $request->get('iDisplayLength') != '-1') {
                $services = $services->take($request->get('iDisplayLength'))->skip($request->get('iDisplayStart'));
            }

            if ($request->has('iSortCol_0')) {
                for ($i = 0; $i < $request->get('iSortingCols'); $i++) {
                    $column = $columns[$request->get('iSortCol_' . $i)];
                    if (false !== ($index = strpos($column, ' as '))) {
                        $column = substr($column, 0, $index);
                    }
                    $services = $services->orderBy($column, $request->get('sSortDir_' . $i));
                }
            }
            
            $services = $services->get();
            $response['iTotalDisplayRecords'] = $services_count;
            $response['iTotalRecords'] = $services_count;

            $response['sEcho'] = intval($request->get('sEcho'));

            $response['aaData'] = $services;

            return $response;
        }

        return view('services.index');
   }
   public function create()
   {
        $category = ['NEW'=>"Add New Category"]+Category::orderBy('category_name','asc')->pluck('category_name', 'id')->toArray();
   		return view('services.create',['category'=>$category]);
   }
   public function store(Request $request)
    {
        $services_master = $request->all();

        $rules = [

            'person_name' => 'required',
            'category' => 'required',  
        ];
        $messages = [
            'person_name.required' => 'Person Name is Required',
            'category.required' => 'Category is Required',
        ];
        $this->validate($request,$rules,$messages);
    	
    	event(new ServicesEvent($services_master));
    	if($services_master['save_button'] == "save_new"){
            return redirect()->back()->with('message','Record Added Successfully')->with('message_type','success');
        }
        return redirect()->route('services.index')
                         ->with('message','Record Added Successfully.')
                         ->with('message_type','success'); 
    }
    public function edit($id)
    {
        $category = ['NEW'=>"Add New Category"]+Category::orderBy('category_name','asc')->pluck('category_name', 'id')->toArray();
        $service_master = Services::where('id',$id)->first();
        
        return view('services.edit',compact('service_master','category'));
    }
    public function update(Request $request)
    {
        $this->validate($request,[

            'person_name' => 'required',
            'category' => 'required',  
        ],[
            'person_name.required' => 'Person Name is Required',
            'category.required' => 'Category is Required',
        ]);
    	$services_master = $request->all();

        event(new ServicesEvent($services_master));

        if($services_master['save_button'] == "save"){
            return redirect()->back()
                         ->with('message','Record Updated successfully')
                         ->with('message_type','success');    
        }
        return redirect()->route('services.index')
                         ->with('message','Record Updated successfully')
                         ->with('message_type','success');
    }
    public function delete(Request $request)
    {
        $id = $request->get('id');
        $services_master = Services::where('id', $id)->first();
        Services::where('id', $id)->delete();

        return back()->with('message', 'Record Deleted Successfully.')
            ->with('message_type', 'success');
    }
    public function categoryStore(Request $request){
        $category_data = $request->all();
        $cat_value = $category_data['newval'];
        $category = new Category();
        $category->category_name = $cat_value;
        $category->save();
        $cat_id = $category->id;
        return $cat_id;
    }
}
