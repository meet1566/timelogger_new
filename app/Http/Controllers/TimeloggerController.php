<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Position;
use App\Models\Log;
use App\Models\leave_master;
use App\Models\AssetsService;
use App\Models\AssetsMaster;
use App\Models\AssetLog;
use App\Models\Leave;
use App\Models\HolidayYear;
use App\Models\WorkingDays;
use App\Utility\ImageResize;
use Mail,Auth,DB,Validator,Redirect,Carbon,Artisan,Cache,Hash;


class TimeloggerController extends Controller
{
    public function index(Request $request)
    {
      $type = $request->get('type');
          // dd($type);
       if($request->ajax()){

            $where_str = '1 = ?';
            $where_params = [1];

            if ($request->has('sSearch')) {
                $search = $request->get('sSearch');
                  $where_str .= " and (users.fullname like \"%{$search}%\""
                            // ." or positions.position_name like \"%{$search}%\""
                            ." or users.email like \"%{$search}%\""
                            .")";
            }

            $columns = array('users.id','users.profile_pic','users.fullname','positions.position_name as position','users.email','users.pay_code'); 


            $users_count = User::select('id')
                        ->whereRaw($where_str,$where_params);
                        
            $users = User::select($columns)
                    ->leftjoin('positions','positions.id','=','users.position');

            if($type == 'currentemp'){
                $is_verified = '1';
            }else{
                $is_verified = '0';
            } 
             
            $users_count = $users_count->where('is_verified',$is_verified)
                            ->count();
            
            $users = $users->where('is_verified',$is_verified)
                            ->where('users.is_admin','0')
                            ->whereRaw($where_str,$where_params)
                            ->orderBy('fullname');
                  

            if ($request->has('iDisplayStart') && $request->get('iDisplayLength') != '-1') {
                $users = $users->take($request->get('iDisplayLength'))->skip($request->get('iDisplayStart'));
            }

            if ($request->has('iSortCol_0')) {
                for ($i = 0; $i < $request->get('iSortingCols'); $i++) {
                    $column = $columns[$request->get('iSortCol_' . $i)];
                    if (false !== ($index = strpos($column, ' as '))) {
                        $column = substr($column, 0, $index);
                    }
                    $users = $users->orderBy($column, $request->get('sSortDir_' . $i));
                }
            }
            
            $users = $users->get();
            $response['iTotalDisplayRecords'] = $users_count;
            $response['iTotalRecords'] = $users_count;

            $response['sEcho'] = intval($request->get('sEcho'));

            $response['aaData'] = $users;
            // dd($users);
            return $response;
        }

        return view('user.index');
    }

    public function create()
    {
        $position = ['NEW'=>"Add New Position"]+Position::orderBy('position_name','asc')->pluck('position_name', 'id')->toArray();
        return view('user.create',['position'=>$position]);
    }

    public function positionStore(Request $request)
    {
        $position_data = $request->all();
        $postion_value = $position_data['newval'];
        $position = new Position();
        $position->position_name = $postion_value;
        $position->save();
        $postion_id = $position->id;
        return $postion_id;
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $btn_id = $data['id'];

        $this->validate($request,[
            'gender' => 'required',
            'fullname' => 'required|regex:/^[a-zA-Z .]*$/',
            'position' => 'required',
            'birthdate' => 'required',
             //'email' => 'required|regex:/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]{5,}+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/|unique:users,email',
            'email' => 'required|regex:/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@(thinktanker.in)$/|unique:users,email',
            'contact' => 'required|regex:/^[- +()]*[0-9][- +()             0-9]*$/|max:15|min:10',
            'emergency_contact' => 'required|regex:/^[- +()]*[0-9][-+()0-9]*$/|max:15|min:10',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
            'address' => 'required',
            'hobby' => 'required',
            'joining_date'=>'required',
            'code'=>'required'
        ]);
        if(\Session::has('image_name')){
            $path = public_path()."/upload/".trim(\Session::get('image_name'));
            $temp_path = public_path()."/temp_image/".trim(\Session::get('image_name'));
            \File::move($temp_path,$path);
        }

        $user = new User($data);
        $user->profile_pic = trim(\Session::get('image_name'));
        $user->password = \Hash::make($data['password']);
        $activation_code = str_random('20');
        $user->confirm_code = $activation_code;
        $user->is_verified = 1;
        $user->save();
        $email_id = $user->email;
        $user_id = $user->id;
        
        $day = date('d', strtotime($data['joining_date']));
        $month = date('m', strtotime($data['joining_date']));

        if ($day >= 16) {

            $month += 1;

        }
        $total_month = 12;
        $finalmonth = $total_month - $month +1;
        $total_leave = $finalmonth * 1.5;
        $divid_leave = $total_leave/3;

        $remaning_leave = new leave_master;
        $remaning_leave->user_id = $user_id;
        $remaning_leave->casual_leave = $divid_leave;
        $remaning_leave->sick_leave = $divid_leave;
        $remaning_leave->privilege_leave = $divid_leave;
        $remaning_leave->save();

        if ($user->save())
        {
            $data = array(
                'fullname' => $user->fullname,
                'code'     => $activation_code);

            Mail::send('notification', $data, function($message) use ($email_id)
            {
                $message->from('donotreply@thinktanker.in');
                $message->to($email_id)->subject('Please activate your account');
            });
        }
        if ($btn_id == 'submit') {
            return response()->json('new');
        }else{
            \Session::forget('image_name');
            return response()->json('exit');
        }
    }

    public function admin()
    {
       
       $total_working_hours =  WorkingDays::selectRaw("SEC_TO_TIME(SUM(TIME_TO_SEC(working_hours))) as working_hours")->whereMonth('working_date','=',date('m'))
                                 ->where('year','=',date('Y'))
                                 ->get()
                                 ->toArray();
        $exp_total_hours = explode(':',$total_working_hours[0]['working_hours']);                         
        $work_hours = Log::selectRaw("SEC_TO_TIME(SUM(TIME_TO_SEC(time_diff))) as work_hour")
                            ->whereMonth('time','=',date('m'))
                            ->whereYear('time','=',date('Y'))
                            ->where('user_id','=',Auth::id())
                            ->get()
                            ->toArray();

        $exp_work_hours = explode(':',$work_hours[0]['work_hour']);
       
        $time_differance = Log::selectRaw("TIME_FORMAT(MIN(check_in), '%h:%i:%s') as check_in,TIME_FORMAT(MAX(check_out), '%h:%i:%s') as check_out,SEC_TO_TIME(SUM(TIME_TO_SEC(time_diff))) as time_difference")
                ->where('time','=',date('Y-m-d'))
                ->where('user_id','=',Auth::id())
                ->get()
                ->toArray();       
               
        $logs = Log::select(DB::raw("DATE_FORMAT(check_in,'%h:%i %p') as check_in") , DB::raw("DATE_FORMAT(check_out,'%h:%i %p') as check_out") , 'time')->where('user_id', Auth::id())
            ->where('time', date('Y-m-d'))
            ->orderBy('id', 'DESC')
            ->first();
        $count_log_row = Log::where('time','=',date('Y-m-d'))
                                       ->where('user_id','=',Auth::id())
                                       ->count(); 
        $total_leaves = Leave::where('user_id',Auth::id())
                        ->whereYear('date','=',date('Y'))
                        // ->where('status','=','a')
                        ->whereIn('status',array('a','u'))
                        ->limit(5)
                        ->orderBy('id','desc')
                        ->get()
                        ->toArray();   
        $casual_leaves = Leave::where('leave_type','=','Casual Leave') 
                        ->where('user_id',Auth::id())
                        // ->where('status','a')
                        ->whereIn('status',array('a','u'))
                        ->where('leave_time','full day')
                        ->whereYear('date','=',date('Y'))
                        ->count(); 

        $casual_half_leaves = Leave::where('leave_type','=','Casual Leave') 
                        ->where('user_id',Auth::id())
                        // ->where('status','a')
                        ->whereIn('status',array('a','u'))
                        ->where('leave_time','half day')
                        ->whereYear('date','=',date('Y'))
                        ->count();
        $casual_leaves = $casual_leaves+($casual_half_leaves/2);
        
        $sick_leaves = Leave::where('leave_type','=','Sick Leave') 
                        ->where('user_id',Auth::id())
                        // ->where('status','a')
                        ->whereIn('status',array('a','u'))
                        ->where('leave_time','full day')
                        ->whereYear('date','=',date('Y'))
                        ->count(); 
        $sick_half_leaves = Leave::where('leave_type','=','Sick Leave') 
                        ->where('user_id',Auth::id())
                        // ->where('status','a')
                        ->whereIn('status',array('a','u'))
                        ->where('leave_time','half day')
                        ->whereYear('date','=',date('Y'))
                        ->count(); 

        $sick_leaves = $sick_leaves+($sick_half_leaves/2);

        $priviledge_leaves = Leave::where('leave_type','=','Privilege Leave') 
                        ->where('user_id',Auth::id())
                        // ->where('status','a')
                        ->whereIn('status',array('a','u'))
                        ->where('leave_time','full day')
                        ->whereYear('date','=',date('Y'))
                        ->count();

        $priviledge_half_leaves = Leave::where('leave_type','=','Privilege Leave') 
                        ->where('user_id',Auth::id())
                        // ->where('status','a')
                        ->whereIn('status',array('a','u'))
                        ->where('leave_time','half day')
                        ->whereYear('date','=',date('Y'))
                        ->count();
        $priviledge_leaves = $priviledge_leaves+($priviledge_half_leaves/2);
        $checkin_time = Log::where('time','=',date('Y-m-d'))
                        ->where('user_id','=',Auth::id())
                        ->orderBy('check_in','desc')
                        ->first();
        $checkin_empty = Log::where('time','=',date('Y-m-d'))
                        ->where('user_id','=',Auth::id())
                        ->where('check_in','<>','')
                        ->where('check_out','=',NULL)
                        ->orderBy('check_in','desc')
                        ->first();
         $pending_leave = Leave::where('user_id',Auth::id())
                        ->whereYear('date','=',date('Y'))
                        ->where('status','=','p')
                        ->limit(3)
                        ->orderBy('id','desc')
                        ->get()
                        ->toArray();                           
        
        $holiday = HolidayYear::where('holiday_year.holiday_date','>=',date('Y-m-d'))
                        ->leftjoin('holiday_master','holiday_master.id','=','holiday_year.holiday_id')   
                        ->orderBy('holiday_year.holiday_date','asc')
                        ->limit(3)
                        ->get()
                        ->toArray();
        $month_back_date = date('Y-m-d',strtotime('-30 days'));   
        $new_employee = User::where('created_at','>=',$month_back_date)->get()->toArray();
        
        $current_date = date('Y-m-d');
        $date = new Carbon\Carbon;
        $today = $date->format('d');
        $month = $date->format('m');
        $birthday_members = User::select('position', 'fullname','birthdate','profile_pic','gender')
                        ->where('is_verified','1')
                        //->whereDay('birthdate', '>=', date('m-d'))
                        ->orderBy(DB::raw('CONCAT(SUBSTR(`birthdate`,6) < SUBSTR(CURDATE(),6), SUBSTR(`birthdate`,6))'))
                        ->limit(3)
                        ->get()
                        ->toArray();
        //logs and leaves graph  
         //$missing_logs = DB::select(DB::raw("SELECT SUM(q.timeSum) AS count,q.month_main,q.year,q.month,q.user_id FROM (SELECT (CASE WHEN IFNULL(SUM( TIME_TO_SEC( time_diff ) ),0) < 32400 THEN 1 ELSE 0 END)  AS timeSum ,DATE_FORMAT(`time`,'%Y-%m') AS month_main,YEAR(`time`) AS YEAR,MONTH(TIME) AS MONTH,user_id AS user_Id FROM `logs` WHERE user_id = ".Auth::id()." GROUP BY  `time`) q GROUP BY q.month_main  ORDER BY q.month_main DESC limit 6"));
        if (Auth::id() == 74 || Auth::id() == 69 || Auth::id() == 113) {
            $users_log = DB::table("logs")
                        ->selectRaw("COUNT(*) as count,DATE_FORMAT(`time`,'%m-%Y') AS month_main,YEAR(time) as year,MONTH(time) as month,logs.user_id as user_id")
                        ->where('user_id',Auth::id())
                        ->where('time_diff','<','08:00:00')
                        ->where('time_diff','>','05:00:00')
                        ->groupby(DB::raw('MONTH(time)'))
                        ->groupby(DB::raw('Year(time)'))
                        ->orderBy(DB::raw('Year(time)'), 'desc')->take(6)
                        ->orderBy(DB::raw('MONTH(time)'), 'desc')->take(6)
                        ->get();
            // dd($users_log);
        }else{

            $users_log = DB::table("logs")
                        ->selectRaw("COUNT(*) as count,DATE_FORMAT(`time`,'%m-%Y') AS month_main,YEAR(time) as year,MONTH(time) as month,logs.user_id as user_id")
                        ->where('user_id',Auth::id())
                        ->where('time_diff','<','09:00:00')
                        ->groupby(DB::raw('MONTH(time)'))
                        ->groupby(DB::raw('Year(time)'))
                        ->orderBy(DB::raw('Year(time)'), 'desc')->take(6)
                        ->orderBy(DB::raw('MONTH(time)'), 'desc')->take(6)
                        ->get();
        }

        $users_logs = json_decode(json_encode($users_log),true);
        $date = (new Carbon\Carbon)->submonths(11);
        $years = $date->format('Y-m-d');
        $users_leave = DB::table("leaves")
                        ->selectRaw("COUNT(*) as count,DATE_FORMAT(`date`,'%m-%Y') AS month_main,YEAR(date) as year,MONTH(date) as month,leaves.user_id as user_id")
                        ->where('user_id',Auth::id())
                        ->where('status','=','a')
                        ->groupby(DB::raw('MONTH(date)'))
                        ->groupby(DB::raw('Year(date)'))
                        ->get();

        if (Auth::id() != 74 && Auth::id() != 69 && Auth::id() != 113) {               
            $users_leaves = json_decode(json_encode($users_leave),true);
            foreach ($users_logs as $key1 => $value1) {
                   foreach ($users_leaves as $key => $value) {
                        if($value1['month_main'] == $value['month_main']){
                           
                                $users_logs[$key1]['count'] = $value1['count']-$value['count'];
                                $users_logs[$key1]['month_main'] = $value1['month_main'];
                                $users_logs[$key1]['year'] = $value1['year'];
                                $users_logs[$key1]['month'] = $value1['month'];
                                $users_logs[$key1]['user_id'] = $value1['user_id'];
                            }
                            else{
                               
                            }
                    }
            }
        }

        // $leaves_data = DB::table("leaves")
        //                 ->selectRaw('COUNT(*) as count,user_id,MONTH(date) as month,YEAR(date) as year')
        //                 ->where('user_id','=',Auth::id())
        //                 ->groupby(DB::raw('MONTH(date)'))
        //                 ->orderBy(DB::raw('MONTH(date)'), 'desc')->take(6)
        //                 ->get();  

        $leaves_data = DB::table("leaves")
                            ->selectRaw('COUNT(*) as count,user_id,MONTH(date) as month,YEAR(date) as year')
                            ->where('user_id','=',Auth::id())
                            ->where('status','=','a')
                            ->groupby(DB::raw('MONTH(date)'))
                            ->groupby(DB::raw('Year(date)'))
                            ->orderBy(DB::raw('Year(date)'), 'desc')->take(6)
                            ->orderBy(DB::raw('MONTH(date)'), 'desc')->take(6)
                            ->get();

        $leaves_disapprove_data = DB::table("leaves")
                            ->selectRaw('COUNT(*) as count,user_id,MONTH(date) as month,YEAR(date) as year')
                            ->where('user_id','=',Auth::id())
                            ->where('status','=','d')
                            ->groupby(DB::raw('MONTH(date)'))
                            ->groupby(DB::raw('Year(date)'))
                            ->orderBy(DB::raw('Year(date)'), 'desc')->take(6)
                            ->orderBy(DB::raw('MONTH(date)'), 'desc')->take(6)
                            ->get();   

        $logs_data = json_decode(json_encode($users_logs),true);
        $leaves_data = json_decode(json_encode($leaves_data),true);
        $leaves_disapprove_data = json_decode(json_encode($leaves_disapprove_data),true);
        $count_logs = [];
        $month = [];

        $last_six_months = [];
        $old = [];
        $new = [];
        $leaves_new = [];
        $leaves_old = [];
        $leaves_disapprove_new = [];
        $leaves_disapprove_old = [];
        $count_disapprove_leaves = [];
        $count_logs = [];
        $count_leaves = [];

        // for ($i = 0; $i <= 5; $i++) {
        //   $last_six_months[date('m', strtotime("-$i month"))] = date('Y', strtotime("-$i month"));
        //   // $last_six_months[][date(', Y', strtotime("-$i month"))] = date(', Y', strtotime("-$i month"));
        // }
        for ($i = 0; $i <= 5; $i++) 
        {
           $last_six_months[date("m", strtotime( date( 'Y-m-01' )." -$i months"))] = date("Y", strtotime( date( 'Y-m-01' )." -$i months"));
        }
        $count = 0;
        foreach ($last_six_months as $last_six_months_key => $last_six_months_value) {
            foreach ($logs_data as $array_data_key => $array_data_value) {
                if($last_six_months_key == $array_data_value['month'])
                {
                    $old[$count]['count'] = $array_data_value['count'];
                    $old[$count]['user_id'] = $array_data_value['user_id'];
                    $old[$count]['month'] = $array_data_value['month'];
                    $old[$count]['year'] = substr($array_data_value['year'],-2);
                }else{

                    $new[$count]['count'] = 0;
                    $new[$count]['user_id'] = $array_data_value['user_id'];
                    $new[$count]['month'] = $last_six_months_key;
                    $new[$count]['year'] =  substr($last_six_months_value,-2);
                }
            }
            foreach ($leaves_data as $array_data_key => $array_data_value) {
                if($last_six_months_key == $array_data_value['month']){
                   if($last_six_months_value == $array_data_value['year']){
                      $leaves_old[$count]['count'] = $array_data_value['count'];
                    }
                }else{
                    $leaves_new[$count]['count'] = 0;
                }
            }
            foreach ($leaves_disapprove_data as $array_data_key1 => $array_data_value1) {
                if($last_six_months_key == $array_data_value1['month']){
                   if($last_six_months_value == $array_data_value1['year']){
                      $leaves_disapprove_old[$count]['count'] = $array_data_value1['count'];
                    }
                }else{
                    $leaves_disapprove_new[$count]['count'] = 0;
                }
            }
            $count++;
        }

        $logs_data = array_replace_recursive($new,$old);
        $leaves_data = array_replace_recursive($leaves_new,$leaves_old);
        $leaves_disapprove_data = array_replace_recursive($leaves_disapprove_new,$leaves_disapprove_old);

        krsort($logs_data);
        krsort($leaves_data);
        krsort($leaves_disapprove_data);
        foreach ($logs_data as $logs_key => $logs_value) {
            $count_logs[] = $logs_value['count'];
            $dateObj = \DateTime::createFromFormat('!m', $logs_value['month']);
            $month[] = "'".substr($dateObj->format('F'),0,3).' '.$logs_value['year']."'";
        }
        foreach ($leaves_data as $logs_key => $logs_value) {
            $count_leaves[] = $logs_value['count'];
        }
        foreach ($leaves_disapprove_data as $logs_key1 => $logs_value1) {
            $count_disapprove_leaves[] = $logs_value1['count'];
        }
        $count_logs = implode(',',$count_logs);
        $month = implode(',',$month);
        $count_leaves = implode(',',$count_leaves);
        $count_disapprove_leaves = implode(',',$count_disapprove_leaves);

        //logs graph
        // $leaves_log = DB::table("leaves")
        //     ->selectRaw('COUNT(*) as count,user_id,users.fullname as name,users.is_verified,YEAR(date) as year')
        //     ->join('users','users.id','=','leaves.user_id')
        //     ->where('users.is_verified','=','1')
        //     ->where('users.deleted_at','=',NULL)
        //     // ->Where('users.deactive_date','=', NULL)
        //     // ->whereYear('date','=',date('Y'))
        //     ->groupby(DB::raw('user_id'))
        //     ->get();

        $leaves_log = DB::table("leaves")
            ->selectRaw('COUNT(*) as count,user_id,users.fullname as name,users.is_verified,YEAR(date) as year')
            ->join('users','users.id','=','leaves.user_id')
            ->where('users.is_verified','=','1')
            ->where('users.deleted_at','=',NULL)
            ->where('status','=','a')
            ->groupby(DB::raw('user_id'))
            ->orderBy('name')
            ->get();
              
        $leaves_logs = json_decode(json_encode($leaves_log),true);

        //$leaves_log1 = json_decode(json_encode($leaves_log),true);
        foreach ($leaves_logs as $leaves_log_key => $leaves_log_value) {
            $leaves_count_logs [] = $leaves_log_value['count'];
            $fullname [] = ucwords($leaves_log_value['name']);
            $words = explode(" ", ucwords($leaves_log_value['name']));
            $username_leaves = "";
            $count = 0;
            foreach ($words as $w) {
                if($count<2){
                    if(isset($w[0])){
                        $username_leaves .= $w[0];
                    }
                }
                $count++;
            }
            $username[] = '"'.$username_leaves.' '.$leaves_log_value['user_id'].'"';
        }
        $leaves_count_logs = implode(',',$leaves_count_logs);
        $username = implode(',',$username);
        $leaves = "";
        $birthday_data = "";

        /*working hours*/
        $dates=[];
        $day_name = [];
        $main_dates = [];
        $current_month = date('m');
        $current_year = date('Y');
        //calculate day of given month and year
        $count_days = cal_days_in_month(CAL_GREGORIAN,$current_month, $current_year);
        for ($i = 1; $i <= $count_days; $i++) {
            $date = date($current_year).'/'.date($current_month).'/'.$i;
   
            $dates['dayname'] = date('l', strtotime($date));
            $dates['dates'] = str_pad($i, 2, '0', STR_PAD_LEFT) . "-" .str_pad($current_month, 2, "0", STR_PAD_LEFT). "-" . $current_year;
             $dates['holiday_name']='';
            $main_dates[str_pad($i, 2, '0', STR_PAD_LEFT) . "-" .str_pad($current_month, 2, "0", STR_PAD_LEFT). "-" . $current_year]=$dates;
        }
        $d =[];
        $holi = DB::table('holiday_year')
                ->selectRaw('holiday_date')
                ->whereMonth('holiday_date','=',$current_month)
                ->whereYear('holiday_date','=',$current_year)
                ->get()->toArray(); 
        $holidays = array_column($holi, 'holiday_date');

        $current_month_dates = [];
        $m;
        $c = 0;
        $c1 = 0;

        foreach ($main_dates as $key1 => $value1) {
            $value4 = date("Y-m-d", strtotime($key1)); 
        foreach ($holidays as $key3 => $value3)
            {
                if($value3 == $value4){
                    unset($main_dates[$key1]);
                }           
            }
        }
        foreach ($main_dates as $main_dates_key => $main_dates_value) 
        {
            if($main_dates_value ['dayname'] != "Sunday")
            {
                if($main_dates_value ['dayname'] != "Saturday")
                {
                array_push($current_month_dates, $main_dates_key);
                }
                    if($main_dates_value ['dayname'] == "Saturday")
                    {
                      $c++;
                      if($c==1 || $c==3 || $c==5)
                      {
                         array_push($current_month_dates, $main_dates_key);        
                      }
                    }
            }
        }
        $count_hours = count($current_month_dates)*9;
        /*working hours*/
        /*12 months logs*/
        $date = (new Carbon\Carbon)->submonths(11);
        $years = $date->format('Y-m-d');

        $users_log_count = DB::table("logs")
                        ->selectRaw("COUNT(*) as count,logs.user_id,users.fullname as name")
                        ->join('users','users.id','=','logs.user_id')
                        ->where('logs.time_diff','<','09:00:00')
                        ->where('users.is_verified','=','1')
                        ->where('users.deleted_at','=',NULL)
                        ->where('users.is_admin','!=','1')
                        ->where('logs.time','>=',$years)
                        ->groupby(DB::raw('user_id'))
                        ->orderBy('name')
                        ->get();


        $users_log_counts = json_decode(json_encode($users_log_count),true);
        
        // $users_leave_count = DB::table("leaves")
        //                     ->selectRaw("COUNT(*) as count,leaves.user_id as user_id,users.fullname as name")
        //                     ->join('users','users.id','=','leaves.user_id')
        //                     ->where('leaves.status','=','a')
        //                     ->where('leaves.date','>=',$years)
        //                     ->where('users.is_verified','=','1')
        //                     ->where('users.deleted_at','=',NULL)
        //                     ->groupby(DB::raw('leaves.user_id'))
        //                     ->get();

        // $users_leave_counts = json_decode(json_encode($users_leave_count),true);

        // foreach ($users_log_counts as $key1 => $value1) {
        //        foreach ($users_leave_counts as $key => $value) {
        //         if($value1['user_id'] == $value['user_id']){
        //             $users_log_counts[$key1]['count'] = $value1['count']-$value['count'];
        //             $users_log_counts[$key1]['name'] = ucwords($value1['name']);
        //             $users_log_counts[$key1]['user_id'] = $value1['user_id'];
        //         }else{
                    
        //         }

        //         }
        // }
      
        //users_log_counts
        foreach ($users_log_counts as $users_log_counts_key => $users_log_counts_value) {
            $users_count_logs [] = $users_log_counts_value['count'];
            $fullname_logs [] = ucwords($users_log_counts_value['name']);
            $user_words = explode(" ", ucwords($users_log_counts_value['name']));
            $username_logs = "";
            $count = 0;
            foreach ($user_words as $w1) {
                if($count<2){
                    if(isset($w1[0])){
                        $username_logs .= $w1[0];
                    }
                }
                $count++;
            }
            $usernameLogs[] = '"'.$username_logs.' '.$users_log_counts_value['user_id'].'"';
        }
        $users_count_logs = implode(',',$users_count_logs);
        $usernameLogs = implode(',',$usernameLogs);
        /*end 12 months logs*/
        $last_thirtydays = (new Carbon\Carbon)->subDays(30);
        $all_pending_leave = DB::table("leaves")
                    ->selectRaw("leaves.user_id as user_id,users.fullname,MONTH(date) as month,YEAR(date) as year, date, leave_type, leave_purpose")
                    ->join('users','users.id','=','leaves.user_id')
                    ->where('leaves.status','=','p')
                    ->whereDate('date', '>', $last_thirtydays)
                    ->get();
        $all_pending_leaves = json_decode(json_encode($all_pending_leave),true);
        $today_leaves = Leave::select('users.fullname as name', 'leaves.leave_time as day')
                        ->leftjoin('users','users.id','=','leaves.user_id')
                        ->where('date','=',date('Y-m-d'))
                        ->where('status','=','a')
                        ->get()
                        ->toArray(); 
        $assets_service = AssetsService::select('user_name as name','device_type','notes')->where(DB::raw('DATE(`created_at`)'),date('Y-m-d'))->where('status','Pending')->get()->toArray();
        $assets_master  = AssetsMaster::with(['assetUser' => function($query){
            $query->select('id','fullname');
        }])
        ->with(['assetHistory' => function($query) {
            $query->select('id','device_type','serial_number');
        }])
        ->with(['assetLog' => function($query) {
            $query->select('id','created_at','assets_master_id');
        }])
        ->where('status','outward')->get()->toArray();
        $holiday_array = HolidayYear::select("holiday_date")->get()->pluck('holiday_date')->toArray();
        return view('dashboard.admin', compact('assets_master','logs', 'leaves', 'holiday', 'birthday_data','new_employee','casual_leaves','sick_leaves','priviledge_leaves','total_leaves','checkin_time','checkin_empty','count_log_row','time_differance','pending_leave','count_logs','month','count_leaves','leaves_count_logs','username','fullname','birthday_members','exp_total_hours','exp_work_hours','count_hours','all_pending_leaves','count_disapprove_leaves','users_count_logs','fullname_logs','usernameLogs','today_leaves','assets_service','holiday_array'));
    }
    public function getLogsChart(Request $request){
        $data = $request->all();
        // dd($data);
        $user_id = $data['id'];
        $username = User::where('id',$user_id)->first();
        $fullname = $username['fullname'];
        $date = (new Carbon\Carbon)->submonths(11);;
        $years = $date->format('Y-m-d');
        // if($user_id == 74){
        //     $logs_data = DB::table("logs")
        //             ->selectRaw('COUNT(*) as count,user_id,MONTH(time) as month,YEAR(time) as year,DATE_FORMAT(`time`,"%m-%Y") AS month_main')
        //             ->where('user_id',$user_id)
        //             ->where('logs.time_diff','<','08:00:00')
        //             ->where('logs.time','>=',$years)
        //             ->groupby(DB::raw('MONTH(time)'))
        //             ->groupby(DB::raw('Year(time)'))
        //             ->orderBy(DB::raw('Year(time)'))
        //             ->orderBy(DB::raw('MONTH(time)'))
        //             ->get();  
        // }else{

            $logs_data = DB::table("logs")
                    ->selectRaw('COUNT(*) as count,user_id,MONTH(time) as month,YEAR(time) as year,DATE_FORMAT(`time`,"%m-%Y") AS month_main')
                    ->where('user_id',$user_id)
                    ->where('logs.time','>=',$years)
                    ->groupby(DB::raw('MONTH(time)'))
                    ->groupby(DB::raw('Year(time)'))
                    ->orderBy(DB::raw('Year(time)'))
                    ->orderBy(DB::raw('MONTH(time)'));
            $users_get_all_id = User::select('id')->where("fullname","!=",null)->get()->toArray();
            $single_array = $array = array_column($users_get_all_id, 'id');
            foreach($single_array as $val){
                if ($val == 74 || $val == 69) {
                    // dd($val);
                    $logs_data->where('logs.time_diff','<','08:00:00');
                }else{
                    $logs_data->where('logs.time_diff','<','09:00:00');
                }
            }
            $result_of_count = $logs_data->get();  
            $logs_data = json_decode(json_encode($result_of_count),true);
        
        $users_leave_count = DB::table("leaves")
                            ->selectRaw("COUNT(*) as count,leaves.user_id as user_id,MONTH(date) as month,YEAR(date) as year,DATE_FORMAT(date,'%m-%Y') AS month_main")
                            ->join('users','users.id','=','leaves.user_id')
                            ->where('user_id',$user_id)
                            ->where('leaves.status','=','a')
                            ->where('leaves.date','>=',$years)
                            ->where('users.is_verified','=','1')
                            ->where('users.deleted_at','=',NULL)
                            ->groupby(DB::raw('MONTH(date)'))
                            ->groupby(DB::raw('Year(date)'))
                            ->orderBy(DB::raw('Year(date)'))
                            ->orderBy(DB::raw('MONTH(date)'))
                            ->get();

        $users_leave_counts = json_decode(json_encode($users_leave_count),true);
        foreach ($logs_data as $key1 => $value1) {
            foreach ($users_leave_counts as $key => $value) {
                if($value1['month_main'] == $value['month_main']){
                    $logs_data[$key1]['count'] = $value1['count']-$value['count'];
                    $logs_data[$key1]['month_main'] = ucwords($value1['month_main']);
                    $logs_data[$key1]['user_id'] = $value1['user_id'];
                    $logs_data[$key1]['month'] = $value1['month'];
                    $logs_data[$key1]['year'] = $value1['year'];
                }else{
                    
                }
            }
        }

        for ($i = 0; $i <= 11; $i++) {
          $last_six_months[] = date('m-Y', strtotime("-$i month"));
        }
        $count = 0;
        foreach ($last_six_months as $last_six_months_key => $last_six_months_value) {
                foreach ($logs_data as $array_data_key => $array_data_value) {
                    if($last_six_months_value == $array_data_value['month_main']){
                        $leaves_old[$count]['count'] = $array_data_value['count'];
                        $leaves_old[$count]['month'] = $last_six_months_value;
                        $leaves_old[$count]['year'] = $last_six_months_value;
                    }else{
                        $leaves_new[$count]['count'] = 0;
                        $leaves_old[$count]['month'] = $last_six_months_value;
                        $leaves_old[$count]['year'] = $last_six_months_value;
                    }
                }
            
            $count++;
        }
        $logs_data = array_replace_recursive($leaves_new,$leaves_old);
        foreach ($logs_data as $leaves_key => $leaves_value) {
            $count_leaves[] = $leaves_value['count'];
            $month_main[] = $leaves_value['month'];
        }

        $username = User::select('fullname')->where('id',$user_id)->first()->toArray();
        $username = ucwords($username['fullname']);

        return response()->json(['count_leaves'=>$count_leaves,'month_main'=>$month_main,'fullname'=>$username]);
    }
    public function getChart(Request $request){
        $data = $request->all();
        $user_id = $data['id'];
        $username = User::where('id',$user_id)->first();
        $fullname = $username['fullname'];
        $leaves_data = DB::table("leaves")
            ->selectRaw('COUNT(*) as count,user_id,YEAR(date) as year')
            ->where('leaves.status','=','a')
            ->where('user_id',$user_id)
            ->groupby(DB::raw('YEAR(date)'))
            ->orderBy(DB::raw('YEAR(date)'), 'desc')->take(6)
            ->get();  
        $leaves_data = json_decode(json_encode($leaves_data),true);
        for ($i = 0; $i <= 5; $i++) {
          $last_six_years[] = date('Y', strtotime("-$i year"));
        }

        foreach ($last_six_years as $last_six_years_key => $last_six_years_value) {
            foreach ($leaves_data as $array_data_key => $array_data_value) {
                    if($last_six_years_value == $array_data_value['year']){
                        $leaves_old[$last_six_years_key]['count'] = $array_data_value['count'];
                        $leaves_old[$last_six_years_key]['user_id'] = $array_data_value['user_id'];
                        $leaves_old[$last_six_years_key]['year'] = $array_data_value['year'];
                    }else{
                        $leaves_new[$last_six_years_key]['count'] = 0;
                        $leaves_new[$last_six_years_key]['user_id'] = $user_id;
                        $leaves_new[$last_six_years_key]['year'] = $last_six_years_value;
                    }
            }
        }

        $leaves_data = array_replace_recursive($leaves_new,$leaves_old);
        ksort($leaves_data);
        
        foreach ($leaves_data as $leaves_key => $leaves_value) {
            $count_leaves[] = $leaves_value['count'];
            $year[] = (string) $leaves_value['year'];
        }
        // dd($year);
        return response()->json(['count_leaves'=>$count_leaves,'year'=>$year,'fullname'=>$fullname]);
    }
    public function getSecondChart(Request $request){
        $data = $request->all();
        $leaves_data = DB::table("leaves")
            ->selectRaw('COUNT(*) as count,user_id,MONTH(date) as month,YEAR(date) as year')
            ->where('user_id',$data['id'])
            ->where('leaves.status','=','a')
            ->where(DB::raw('YEAR(date)'),$data['year'])
            ->groupby(DB::raw('MONTH(date)'))
            ->get();   
        $leaves_data = json_decode(json_encode($leaves_data),true);

        for ($i = 1; $i <= 12; $i++) {
          $last_six_months[] = $i;
        }
        // dd($last_six_months);
        $count = 0;
        foreach ($last_six_months as $last_six_months_key => $last_six_months_value) {
            foreach ($leaves_data as $array_data_key => $array_data_value) {
                if($last_six_months_value == $array_data_value['month']){
                    $leaves_old[$count]['count'] = $array_data_value['count'];
                    $leaves_old[$count]['month'] = $last_six_months_value;
                    $leaves_old[$count]['year'] = $data['year'];
                }else{
                    $leaves_new[$count]['count'] = 0;
                    $leaves_old[$count]['month'] = $last_six_months_value;
                    $leaves_old[$count]['year'] = $data['year'];
                }
            }
            $count++;
        }
        $leaves_data = array_replace_recursive($leaves_new,$leaves_old);

        foreach ($leaves_data as $leaves_key => $leaves_value) {
            $count_leaves[] = $leaves_value['count'];
            $dateObj = \DateTime::createFromFormat('!m', $leaves_value['month']);
            $month[] = substr($dateObj->format('F'),0,3).' '.substr($leaves_value['year'],-2);
        }
        $username = User::select('fullname')->where('id',$data['id'])->first()->toArray();
        $username = $username['fullname'];

        return response()->json(['count_leaves'=>$count_leaves,'month'=>$month,'fullname'=>$username]);

    }
    public function edit($id)
    {
        $position = ['NEW'=>"Add New Position"]+Position::orderBy('position_name','asc')->pluck('position_name', 'id')->toArray();
        
        $todayDate = date('d-m-Y');
        $users = User::find($id);
        $aadhar_array = User::select("aadhar_img")->where('id',$id)->get()->toArray(); 
        $split_img = explode(',',$aadhar_array[0]['aadhar_img']);
        $last_logs_date = Log::select('time')->where('user_id',$id)->orderby('time','desc')->first();
        return view('user.edit',compact('users','last_logs_date','todayDate','position','split_img'));
    }

    public function update(Request $request, $id)
    {
        $users = User::find($id);
        $data = $request->all();
        $this->validate($request,[
            'profile_pic' => 'image',
            'gender' => 'required',
            'fullname' => 'required|regex:/^[a-zA-Z .]*$/',
            'position' => 'required',
            'birthdate' => 'required',
            'pay_code' => 'required|unique:users,pay_code,'. $id,
             // 'personal_email' => 'required|email',
            'personal_email' => 'required|regex:/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@(gmail.com)$/',
            'contact' => 'required|regex:/^[- +()]*[0-9][- +()             0-9]*$/|max:15',
            'emergency_contact' => 'required|regex:/^[- +()]*[0-9][-+()0-9]*$/|max:15',
            'address' => 'required',
            'joining_date'=>'required'
        ]);
        // dd($request->all());

        $users->fill($request->all());
        if(!empty($data['serving_date'])){
            $users->is_verified = 0;
        }else{
            $users->is_verified = 1;   
        }
        $users->remarks = $request->remarks;
        $users->passbook_username = $request->passbook_username;
        $users->bank_name = $request->bank_name;
        $users->account_no = $request->account_no;
        $users->ifs_code = $request->ifs_code;
        $users->aadhar_no = $request->aadhar_no;
        $users->pancard_no = $request->pancard_no;
        if($request->aadhar_img != Null){
            $path = public_path()."/document/";
            $img_array = array();
            foreach($request['aadhar_img'] as $aadhar)
            {
                $name = $users['fullname'] . '_' . rand() . '.' . $aadhar->getClientOriginalExtension();
                $img_array[] = $name;  
                $aadhar->move(public_path()."/document/", $name);  
            }
            $users->aadhar_img =implode(',',$img_array);
            
        }
        if($request->pancard_img != Null)
        {
            $pancard = $users['fullname'] . '_' . rand() . '.' . $request->file('pancard_img')->getClientOriginalExtension();
            $users->pancard_img = $pancard;
            $path = public_path()."/document/";
            $request->pancard_img->move($path,$pancard);
        }
        $users->save();
        //dd($users);
        if($data['save_button'] == "save_exit"){
            return redirect()->route('user.index',['type'=>'currentemp'])->with('message_type','success')
                     ->with('message','User Updated Successfully');
        }
        return back()->with('message_type','success')
                     ->with('message','User Updated Successfully');

    }

    public function trash(Request $request){
        $id = $request->get('id');
        $user = User::where('id',$id)->forceDelete();
        
        return back()->with('message','User Deleted Successfully')
                     ->with('message_type','success');
    }

    public function restore(Request $request,$id){

        User::withTrashed()->where('id',$id)->restore();

        return back()->with('message_type','success')
                     ->with('message','User Successfully Restore');
    }
    public function destroy(Request $request,$id)
    {
       
        User::withTrashed()->where('id',$id)->forceDelete();

        return back()->with('message','User Successfully Deleted')
                     ->with('message_type','success');
    }
    public function recyclebin(Request $request){

        if($request->ajax()){
            $where_str = "1 =?";
            $where_param =array('1');

                if($request->has('search.value')){
                     $search = $request->get('search.value');
                     $where_str .= "and (fullname like \"%{$search}%\""
                                ." or profile_pic like \"%{$search}%\""
                                ." or position like \"%{ $search}%\""
                                .")";
                }
            $columns = array('id','fullname','profile_pic','position','email');

            $sort_columns = array('id','fullname','position');

            $users_count = User::select('id')
                        ->onlyTrashed()->whereRaw($where_str,$where_param)
                        ->count();

            $users = User::select($columns)
                    ->onlyTrashed()->whereRaw($where_str,$where_param);
            if($request->has('iDisplayStart') && $request->get('iDisplayLenghth') !='-1')
            {
                $users = $users ->take($request->get('iDisplayLenghth'))
                                ->skip($request->get('iDisplayStart'));
            }
            if($request->has('iSortCol_0'))
            {
                $sql_order = '';
                for ( $i = 0 ; $i < $request->get('iSortingCols') ; $i++) {
                    $column = $columns[$request->get('iSortCol_' . $i)];
                        if(false !== ($index = strpos($column, ' as ')))
                        {
                            $column = substr($column, 0, $index);
                        }
                        $users = $users->orderBy($columns,$request->get('sSortDir_'.$i));
                }
            }
            $users = $users->get();

            $response['iTotalDisplayRecords'] =$users_count;
            $response['iTotalRecords'] = $users_count;

            $response['sEcho'] = intval($request->get('sEcho'));

            $response['aaData'] = $users->toArray();

            return $response;
        }

        return view('user.recycle');
    }
    //for upload photo
    public function getUploadProfilePhoto(Request $request)
    {
        // dd($request->id);
        $id = '';
        if(isset($request->id)){
            $id = $request->id;
        }
        return view('user.upload-photo',['id'=>$id]);
    }

    public function postUploadProfilePhoto(Request $request)
    {
        $data = $request->all();
        dd($data['id']);
        $profile = User::findornew($data['id']);

        $rules = ['profile_pic' => 'required|mimes:jpeg,jpg,png|image|max:4096'];

        $validator = Validator::make($request->all(), $rules);

        if ( $validator->passes() )
        {
            $microtime = microtime();
            $search = array('.',' ');
            $microtime = str_replace($search, "_", $microtime);
            $photo = $microtime . '.' . $request->file('profile_pic')->getClientOriginalExtension();
            if(!empty($data['id'])){
                $path = public_path()."/upload/";
            }else{
                $path = public_path()."/temp_image/";
            }

            $old_file = $path . $profile->photo;

            list($width, $height, $type, $attr) = getimagesize($request->file('profile_pic'));

            $save_w = $width;
            $save_h = $height;
            $request->file('profile_pic')->move($path, $photo);

            if ( $save_w >= 500 || $save_h >= 500 )
            {
                $resizeObj = new ImageResize($path . $photo);
                $resizeObj->resizeImage(300, 200, 0);
                $resizeObj->saveImage($path . $photo, 100);
            }
            if(!empty($data['id'])){
                $profile->temp_photo = $photo;
                $profile->save();
            }else{
                $profile = $photo;
            }

            return redirect()->route('user.profile.photo.crop.get',['id'=>$data['id'],'profile'=>$profile]);
        }
        else
        {
            return back()->withErrors($validator);
        }
    }

    public function getCropProfilePhoto(Request $request)
    {
            
        $data = $request->all();

        if($data['id'] == ''){
            $profile['temp_photo'] = $data['profile'];
        }else{
            $profile = User::find($data['id']);
        }

        return view('user.crop-photo',['profile'=>$profile,'id'=>$data['id']]);
    }

    public function postCropProfilePhoto(Request $request) {
        
        $data = $request->all();

        if(!empty($data['id'])){

            $profile = User::find($data['id']);
            $photo = $profile->temp_photo;
            $old_photo = $profile->profile_pic;
            $public_path = str_replace("\\","/", public_path());
            $save_folder =  $public_path . '/upload/';
            $photo_file = $save_folder . '/' . $photo;
            if ($old_photo != NULL AND $old_photo != '') {
                $old_photo_path = $save_folder . '/' . $old_photo;

                @unlink($old_photo_path);
            }

            list($width, $height, $type, $attr) = getimagesize($photo_file);

            $save_w = $width;
            $save_h = $height;
            $crop_x = $request->get('x');
            $crop_y = $request->get('y');
            $crop_w = $request->get('w');
            $crop_h = $request->get('h');
            $resizeObj = new ImageResize($photo_file);
            $resizeObj->cropCustomized($crop_x, $crop_y,$crop_w, $crop_h, $crop_w, $crop_h);
            $outputfile = $save_folder . $photo;
            $resizeObj->saveImage($outputfile, 100);
            $profile->profile_pic = $photo;
            $profile->save();

            return Redirect::route('show.edit')->with('message','Your Profile Photo Updated Successfully')
                                               ->with('message_type','success');
        }else{
            if(\Session::has('image_name')){
                $save_folder =  public_path() . '/temp_image/';
                $old_photo_path = $save_folder.trim(\Session::get('image_name'));
                @unlink($old_photo_path);
            }

            $profile = $data['image_name'];
            $public_path = str_replace("\\","/", public_path());
            $save_folder =  $public_path . '/temp_image/';
            $photo_file = $save_folder .trim($profile);
            $crop_x = $data['x'];
            $crop_y = $data['y'];
            $crop_w = $data['w'];
            $crop_h = $data['h'];
            $resizeObj = new ImageResize($photo_file);
            $resizeObj->cropCustomized($crop_x, $crop_y,$crop_w, $crop_h, $crop_w, $crop_h);
            $outputfile = $save_folder . trim($profile);
            $resizeObj->saveImage($outputfile, 100);

            \Session::put('image_name',$profile);

            $position = ['NEW'=>"Add New Position"]+Position::orderBy('position_name','asc')->pluck('position_name', 'id')->toArray();
    
            return view('user.create',['position'=>$position]);
        }   
    }

    public function changePasswordIndex()
    {
        return view('profile.change-password');
    }

    public function changePassword(Request $request)
    {
        $rules = [
           'old_password' => 'required',
            'new_password' => 'required',
            'confirm_password' => 'required|same:new_password',
        ];

        $customMessages = [
            'required' => 'The :attribute  is required.',
             'same' => 'The :attribute  must be same as New Password.'
        ];
        $this->validate($request, $rules, $customMessages);
        $user_id = Auth::id();
        $old_password = User::select('password')->where('id',$user_id)->value('password');
        if (Hash::check($request->old_password,$old_password)) {
            $user = User::find($user_id);
            $user->password = Hash::make($request->new_password);
            $user->update();
            $this->passwordChangeMail();
           
            return response()->json(["success"=>true]);
        }else{

            return response()->json(["success"=>false]);
        }
        return redirect('profile')->with('success', 'Server stored');
    }

    public function addBankDetails(Request $request)
    {
        
        $rules = [
            'passbook_username' => 'required',
            'bank_name' => 'required',
            'account_no' => 'required',
            'ifs_code' => 'required',
        ];

        $customMessages = [
            'required' => 'The :attribute  is required.',
        ];
        $this->validate($request, $rules, $customMessages);
        
        $id = Auth::id();
        $user = User::where('id',$id)->first();
        $user->passbook_username = $request->passbook_username;
        $user->bank_name = $request->bank_name;
        $user->account_no = $request->account_no;
        $user->ifs_code = $request->ifs_code;
        $user->save();
       
         return response()->json(["success"=>true]);
       
    }

    public function addDocumnetDetails(Request $request)
    {   
        $rules = [
            'aadhar_img' => 'required',
            'aadhar_no' => 'required',
            'pancard_no' => 'required',
            'pancard_img' => 'required|max:500',
        ];

        $customMessages = [
            'required' => 'The :attribute  is required.',
            'max' => 'The :attribute  is may not be greater than :max.'
        ];
        $this->validate($request, $rules, $customMessages);
        $id = Auth::id();
       
        $pancard = Auth::user()->fullname . '_' . rand() . '.' . $request->file('pancard_img')->getClientOriginalExtension();
        $user = User::where('id',$id)->first();
        $user->aadhar_no = $request->aadhar_no;
        $path = public_path()."/document/";
        $img_array = array();
        foreach($request['aadhar_img'] as $aadhar)
        {
            $name = Auth::user()->fullname . '_' . rand() . '.' . $aadhar->getClientOriginalExtension();
            $img_array[] = $name;  
            $aadhar->move(public_path()."/document/", $name);  
        }
        $user->aadhar_img =implode(',',$img_array);
        $user->pancard_no = $request->pancard_no;
        $user->pancard_img = $pancard;
        $user->save(); 
        $request->file('pancard_img')->move($path,$pancard);
        return response()->json(["success"=>true]);

    }

    public function passwordChangeMail()
    {
        $name = Auth::user();
        $email_id = "hr@thinktanker.in";
        $data = array('name'=>$name['fullname']);
        Mail::send('passwordnotify',$data, function($message) use ($email_id)
        {
           $message->from($email_id);
           $message->to($email_id)->subject("Password Change");
           // $message->text($body);
           // $body = "Your Password Has Been  Changed Successfully  ";
        });

    }

    public function userDeactive(Request $request){
        $this->validate($request,[
            'date' => 'required',
            'note' => 'required',
        ]);
        $data = $request->all();
        $user = User::find($data['id']);
        $user->is_verified = '0';
        $user->deactive_date = $data['date'];
        $user->note = $data['note'];
        $user->save();

        return response()->json('User Deactivated Successfully');
    }
    //for dashboard
    public function leavestore(Request $request)
    {
      
        $firststep_store = $request->formData;
        // dd($firststep_store);
        $date_of_leave = explode(',', $firststep_store['date']);
        $array_count_value = count($date_of_leave);
        $firstform_date = Session::put('firstform_date',$date_of_leave);  
        // $session_data = json($firstform_date);        
        $date = Session::put('date',$firststep_store['date']);
        $first_leavetype = Session::put('leave_type',$firststep_store['leave_type']);
        $first_leavepurpose = Session::put('leave_purpose',$firststep_store['leave_purpose']);
        $get_leavetype = Session::get('leave_type');
        $get_leavepurpose = Session::get('leave_purpose');
        // dd($get_leavetype);
        $content = View::make('dashboard.leave_form', compact('date_of_leave','get_leavetype','get_leavepurpose'))->render();

        return response()->json(['success'=>true,'html_content'=>$content,'firststep_store'=>$firststep_store,'date_of_leave'=>$date_of_leave]);
  
    }
    public function leavesecondstore(Request $request)
    {
        
        $requestAll = $request->all();
        $leave_detail = $request['leave_detail'];

        foreach ($leave_detail as $key => $value) {
            // dd($value);
            if(isset($value['day_type']))
            {
                $validator = Validator::make($value, [
                'leave_purpose' => 'required',
                 ]);
                if($validator->fails())
                {
                    return response()->json(['error'=>$validator->errors()->all()]);
                }
            }else
            {
                $validator = Validator::make($value, [
                'from_hour' => 'required',
                'to_hour' => 'required',    
                'leave_purpose' => 'required',
                 ]);
                if($validator->fails())
                {
                    return response()->json(['error'=>$validator->errors()->all()]);
                }
            } 
        }
        $session_put_data = Session::put('leave_detail',$leave_detail);
        
        return response()->json(['success'=>true,'leave_detail'=>$leave_detail]);

    }
    public function leavethirdstore(Request $request)
    {
        $step2_session = Session::get('leave_detail');
        if(!empty($step2_session))
        {
            $leave_confirmation = []; 
            foreach ($step2_session as $key => $value) {
        
                $leaves = new Leave;
                $leaves->user_id = Auth::id();
                $leaves->date = $key;
                $leaves->leave_type = $value['leave_type'];
                if($value['from_hour'] == "10:00 AM" && $value['to_hour'] == "07:00 PM")
                {
                    $leaves->leave_time = "full day";
                     $leaves->to_hour = $value['to_hour'];
                     $leaves->from_hour = $value['from_hour'];
                    
                }else
                {
                    $leaves->from_hour = $value['from_hour'];
                    $leaves->to_hour = $value['to_hour'];
                    $leaves->leave_time = "half day";
                }
                $leaves->leave_purpose = $value['leave_purpose'];
                
                $leaves->save();    
            }
             
        }else
        {
        $firstform_date =  Session::get('firstform_date');
        $first_leavetype = Session::get('leave_type');
        $first_leavepurpose = Session::get('leave_purpose');
             //dd($firstform_date);
        $leave_confirm = [];
        foreach ($firstform_date as $key => $value) {

                $leave_confirm[$key] = $value;
                $leaves = new Leave;
                $leaves->user_id = Auth::id();
                $leaves->date = $value;
                $leaves->leave_type = $first_leavetype;
                $leaves->leave_time = "full day";
                $leaves->leave_purpose = $first_leavepurpose;
                $leaves->save();    
            }
            

         } 
       
         return response()->json(['success'=>true,'request'=>$request->all()]);
    }
    public function checkIn(Request $request)
    {
    
        $current_time = date("H:i:s");

        $previous_checkin = Log::where('time','=',date('Y-m-d'))
                           ->where('user_id','=',Auth::id())
                           ->count();     
        $check_out =  Log::where('time','=',date('Y-m-d'))
                           ->where('user_id','=',Auth::id())
                           ->where('check_out','=',NULL)
                           ->count();
                                       
        if($previous_checkin == 0)
        {

        $checkin = new Log;
        $checkin->user_id = Auth::id();
        $checkin->time = date('Y-m-d');
        $checkin->check_in = $current_time;
        $checkin->save();
        $count_row = Log::where('time','=',date('Y-m-d'))
                                       ->where('user_id','=',Auth::id())
                                       ->count();
        return response()->json(['success'=>true,'message'=>'first time Check in successfully','check_in'=>$checkin,'count_row'=>$count_row]);
        }else if($previous_checkin > 0)
        {
            $checkin_count = Log::where('time','=',date('Y-m-d'))
                        ->where('user_id','=',Auth::id())
                        ->where('check_in','<>','')
                        ->where('check_out','<>','')
                        ->count();

           if($checkin_count > 0)
           {
                 $check_in_count = Log::where('time','=',date('Y-m-d'))
                                ->where('user_id','=',Auth::id())
                                ->where('check_in','<>','')
                                ->where('check_out','=',NULL)
                                ->count();

                if($check_in_count > 0)
                {
                    $checkin = Log::where('time','=',date('Y-m-d'))
                                ->where('user_id','=',Auth::id())
                                ->where('check_in','<>','')
                                ->where('check_out','=',NULL)->first();
                    
                       // dd($checkin);         
                    $check = Log::find($checkin['id']); 
                    $interval = \DB::select("select TIMEDIFF('".$current_time."','".$checkin['check_in']."') AS `interval`");
                    $check->check_out = $current_time;
                    $check->time_diff = $interval[0]->interval;
                    $check->save();
                    $count_row = Log::where('time','=',date('Y-m-d'))
                                       ->where('user_id','=',Auth::id())
                                       ->count();
                    $checkout_time_count =  Log::selectRaw("TIME_FORMAT(MIN(check_in), '%H:%i:%s') as check_in,TIME_FORMAT(MAX(check_out), '%H:%i:%s') as check_out,SEC_TO_TIME(SUM(TIME_TO_SEC(time_diff))) as time_difference")
                    ->where('time','=',date('Y-m-d'))
                    ->where('user_id','=',Auth::id())
                    ->get()
                    ->toArray();
                    $total_working_hours =  WorkingDays::selectRaw("SEC_TO_TIME(SUM(TIME_TO_SEC(working_hours))) as working_hours")->whereMonth('working_date','=',date('m'))
                                 ->where('year','=',date('Y'))
                                 ->get()
                                 ->toArray();
                    $exp_total_hours = explode(':',$total_working_hours[0]['working_hours']);             
                    $work_hours = Log::selectRaw("SEC_TO_TIME(SUM(TIME_TO_SEC(time_diff))) as work_hour")
                            ->whereMonth('time','=',date('m'))
                            ->whereYear('time','=',date('Y'))
                            ->where('user_id','=',Auth::id())
                            ->get()
                            ->toArray();
                    $exp_work_hours = explode(':',$work_hours[0]['work_hour']);           

                    return response()->json(['success'=>true,'message'=>'second Checkout in successfully','check_out'=>$check,'count_row'=>$count_row,'checkout_time_count'=>$checkout_time_count,'exp_total_hours'=>$exp_total_hours,'exp_work_hours'=>$exp_work_hours]);

                }else
                {
                 $checkin = new Log;
                 $checkin->user_id = Auth::id();
                 $checkin->time = date('Y-m-d');
                 $checkin->check_in = $current_time;
                 $checkin->save();
                 $count_row = Log::where('time','=',date('Y-m-d'))
                                       ->where('user_id','=',Auth::id())
                                       ->count();
                 $checkout_time_count =  Log::selectRaw("TIME_FORMAT(MIN(check_in), '%H:%i:%s') as check_in,TIME_FORMAT(MAX(check_out), '%H:%i:%s') as check_out,SEC_TO_TIME(SUM(TIME_TO_SEC(time_diff))) as time_difference")
                    ->where('time','=',date('Y-m-d'))
                    ->where('user_id','=',Auth::id())
                    ->get()
                    ->toArray();                      
                 return response()->json(['success'=>true,'message'=>'second time Check in successfully','check_in'=>$checkin,'count_row'=>$count_row,'checkout_time_count'=>$checkout_time_count]);        
                }    
            }else
            {
            

                $checkin = Log::where('time','=',date('Y-m-d'))
                                ->where('user_id','=',Auth::id())
                                ->where('check_in','<>','')
                                ->where('check_out','=',NULL)
                                ->first()
                                ->toArray();
                $check = Log::find($checkin['id']); 
                $interval = \DB::select("select TIMEDIFF('".$current_time."','".$checkin['check_in']."') AS `interval`");
                $check->check_out = $current_time;
                $check->time_diff = $interval[0]->interval;
                $check->save();
                $count_row = Log::where('time','=',date('Y-m-d'))
                                       ->where('user_id','=',Auth::id())
                                       ->count();
                $checkout_time_count =  Log::selectRaw("TIME_FORMAT(MIN(check_in), '%H:%i:%s') as check_in,TIME_FORMAT(MAX(check_out), '%h:%i:%s') as check_out,SEC_TO_TIME(SUM(TIME_TO_SEC(time_diff))) as time_difference")
                    ->where('time','=',date('Y-m-d'))
                    ->where('user_id','=',Auth::id())
                    ->get()
                    ->toArray();
                $total_working_hours =  WorkingDays::selectRaw("SEC_TO_TIME(SUM(TIME_TO_SEC(working_hours))) as working_hours")->whereMonth('working_date','=',date('m'))
                                 ->where('year','=',date('Y'))
                                 ->get()
                                 ->toArray();
                    $exp_total_hours = explode(':',$total_working_hours[0]['working_hours']);             
                    $work_hours = Log::selectRaw("SEC_TO_TIME(SUM(TIME_TO_SEC(time_diff))) as work_hour")
                            ->whereMonth('time','=',date('m'))
                            ->whereYear('time','=',date('Y'))
                            ->where('user_id','=',Auth::id())
                            ->get()
                            ->toArray();
                    $exp_work_hours = explode(':',$work_hours[0]['work_hour']); dd($exp_work_hours);                           
                return response()->json(['success'=>true,'message'=>'first Checkout in successfully','check_out'=>$check,'count_row'=>$count_row,'checkout_time_count'=>$checkout_time_count,'exp_total_hours'=>$exp_total_hours,'exp_work_hours'=>$work_hours]);
            }
        }
    }
}
