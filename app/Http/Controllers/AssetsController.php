<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Assets;
use App\Models\AssetsMaster;
use App\Models\AssetsService;
use App\Models\AssetLog;
use App\Events\AssetsEvent;
use Event,Log,Auth,Redirect,Mail;
use App\Models\User;
use DB;
use Illuminate\Support\Arr;

class AssetsController extends Controller
{
    public function index(Request $request)
    {

        if($request->ajax()){

            $where_str = '1 = ?';
            $where_params = [1];

            if ($request->has('sSearch')) {
                $search = $request->get('sSearch');
                 $where_str .= " and (title like \"%{$search}%\""
                            ." or device_type like \"%{$search}%\""
                            ." or serial_number like \"%{$search}%\""
                            .")";
            }
            $columns =['id','title','device_type','serial_number','notes','status','service_status','purchase_date'];

            //deviceType filter
            if (empty($request->get('deviceType'))) {
                $request->get('deviceType');
            }else{
                $deviceType = $request->get('deviceType'); 
                $where_str .= " AND (device_type = '$deviceType')";

            }

            $brand = Assets::select($columns)
                   ->whereRaw($where_str,$where_params)->where('status','!=','scrap');
                  
            $brand_count = Assets::select($columns)
                        ->whereRaw($where_str,$where_params)
                        ->count();

            if ($request->has('iDisplayStart') && $request->get('iDisplayLength') != '-1') {
                $brand = $brand->take($request->get('iDisplayLength'))->skip($request->get('iDisplayStart'));
            }

            if ($request->has('iSortCol_0')) {
                for ($i = 0; $i < $request->get('iSortingCols'); $i++) {
                    $column = $columns[$request->get('iSortCol_' . $i)];
                    if (false !== ($index = strpos($column, ' as '))) {
                        $column = substr($column, 0, $index);
                    }
                    $brand = $brand->orderBy($column, $request->get('sSortDir_' . $i));
                }
            }            
            $brand = $brand->get();
            $response['iTotalDisplayRecords'] = $brand_count;
            $response['iTotalRecords'] = $brand_count;
            $response['sEcho'] = intval($request->get('sEcho'));
            $response['aaData'] = $brand;
            return $response;

        }
            $all_device = Assets::distinct('device_type')->pluck('device_type','device_type')->toArray();
            $device_count = Assets::select('device_type')->where('status','!=','scrap')->count();

            $all_device_count = DB::table('assets')->selectRaw('COUNT(*) as count,device_type')->groupby('device_type')->where('status','!=','scrap')->get()->toArray();

            $total_device_count = array_column($all_device_count, 'count', 'device_type');

            return view('assets.index',compact('all_device','device_count','total_device_count'));
    }

    public function ShowUserAssets()
    {
        $id = Auth::id();
        // dd($id);
        $Assets_user = AssetsMaster::select('assets.title','assets.device_type','assets.serial_number')
                        ->where('user_id',$id)
                        ->join('assets','assets.id','=','assets_master.assets_id')
                        ->get();
        $device_type = AssetsMaster::select('assets.device_type')
                        ->where('user_id',$id)
                        ->join('assets','assets.id','=','assets_master.assets_id')
                        ->pluck('assets.device_type')->toArray();
        $serial_number = AssetsMaster::select('assets.serial_number')
                        ->where('user_id',$id)
                        ->join('assets','assets.id','=','assets_master.assets_id')
                        ->pluck('assets.serial_number')->toArray();
        $device_arr = [];
        for ($i=0; $i < count($device_type) ; $i++) { 
            $device_arr[$i] = $device_type[$i].' '. $serial_number[$i];
        }
        $device = [];
        foreach($device_arr as $key => $value)
        {   
            $trim = explode(" ", $value);
            $device[$trim[0]] = $value;
        }
        $count = 0;
        return view('assets.user_assets',compact('Assets_user','count','device'));
    }

    public function ShowService(Request $request,$id) 
    {
        // dd($request->ajax());
         $assets_service = AssetLog::select('assets_log.assets_master_id','assets_log.notes','assets_log.status','assets_log.created_at','assets.device_type')
                           ->join('assets','assets.id','=','assets_log.assets_master_id')
                           ->where('assets_master_id',$id)
                           ->orderBy('assets_log.id','DESC')->limit(5)
                           ->get()->toArray();
         // dd($assets_service);
         $count = 1;
         return view('assets.service_assets',compact('assets_service','count'));
        
    }

    public function CreateReport(Request $request)
    {
       
        $id = Auth::id();
        $asstes_id = Assets::select('assets.id')->join('assets_master','assets_master.assets_id','=','assets.id')->where('assets_master.user_id',$id)->where('assets.device_type',$request->device_type)->first()->toArray();
        $name = Auth::user();
        // dd($request->device_type);
        $assets_service = new AssetsService;
        $assets_service->user_id = $id;
        $assets_service->user_name = $name['fullname'];
        $assets_service->device_type = $request->device_type;
        $assets_service->assets_id = $asstes_id['id'];
        $assets_service->status = "Pending";
        $assets_service->notes = $request->notes;
        $assets_service->save();

        $this->reportMail($assets_service);
        return redirect()->back()->with('message','Record Added Successfully')->with('message_type','success');
    }

    public function reportMail($request)
    {

        $name = Auth::user();
        $email_id = 'hr@thinktanker.in';
        $device_type =  $request->device_type ;
        $notes = $request->notes;

        $data = array('name'=>$name['fullname'],'device_type'=>$device_type,'notes'=>$notes);
        Mail::send('assetnotify', $data, function($message) use ($email_id,$request)
        {
            $message->from(Auth::user()->email);
            $message->to($email_id)->subject('For Assets Service');
        });
        
    }
    public function ShowUserAssetsReport()
    {
        $id = Auth::id();
        $reported_assets = AssetsService::select('device_type','notes','status','created_at')->where('user_id',$id)->get()->toArray();
        $count = 0;
       return view('assets.reported_assets',compact('reported_assets','count'));
    }

    public function ShowAdminAssetsReport(Request $request)
    {
        if($request->ajax()){

            $where_str = '1 = ?';
            $where_params = [1];

            $columns =['assets_service.id','users.fullname as name','assets_service.device_type','assets_service.notes','assets_service.status'];

            //deviceType filter

            if ($request->has('sSearch')) {
                $search = $request->get('sSearch');
                 $where_str .= " and (device_type like \"%{$search}%\""
                            ." or users.fullname like \"%{$search}%\""
                            .")";
                // dd($where_str);
            }
            $brand = AssetsService::select($columns)
                    ->leftjoin('users','users.id','=','assets_service.user_id')
                    ->whereRaw($where_str,$where_params)->orderBy('id','DESC');
           
            $brand_count = AssetsService::select($columns)
                        ->leftjoin('users','users.id','=','assets_service.user_id')
                        ->whereRaw($where_str,$where_params)
                        ->count();

            if ($request->has('iDisplayStart') && $request->get('iDisplayLength') != '-1') {
                $brand = $brand->take($request->get('iDisplayLength'))->skip($request->get('iDisplayStart'));
            }

            if ($request->has('iSortCol_0')) {
                for ($i = 0; $i < $request->get('iSortingCols'); $i++) {
                    $column = $columns[$request->get('iSortCol_' . $i)];
                    if (false !== ($index = strpos($column, ' as '))) {
                        $column = substr($column, 0, $index);
                    }
                    $brand = $brand->orderBy($column, $request->get('sSortDir_' . $i));
                }
            }            
            $brand = $brand->get();
            $response['iTotalDisplayRecords'] = $brand_count;
            $response['iTotalRecords'] = $brand_count;
            $response['sEcho'] = intval($request->get('sEcho'));
            $response['aaData'] = $brand;
            return $response;

        }
         return view('assets.show_report_assets');
    }

    public function ChangeAdminAssetsReportStatus($id)
    {
        $assets_service = AssetsService::find($id);
        $assets_service->status = "InProcess";
        $assets_service->update();

        return response()->json(['message'=>true,'message_type'=>'success','msg'=>'Status Change successfully']);
    }
    
    public function ChangeAssetsReportStatus ($id)
    {
        $assets_service = AssetsService::find($id);
        $assets_service->status = "Resolved";
        // dd($assets_service->toArray());
        $assets_service->update();

        return response()->json(['message'=>true,'message_type'=>'success','msg'=>'Status Change successfully']);
    }
    
    public function CreateAdminAssetsReport()
    {
        
        $users = DB::table('users')
                ->selectRaw('id, fullname')
                ->orderBy('fullname')->get()->toArray(); 
        $user = array_column($users, 'fullname', 'id');
        $user = Arr::prepend($user, 'select user', '');
        return view('assets.create_report',['user'=>$user,]);
    }

    public function SaveAdminAssetsReport(Request $request)
    {   
        // dd($request->all());
        $id = $request->user;
        $asstes_id = Assets::select('assets.id')->join('assets_master','assets_master.assets_id','=','assets.id')->where('assets_master.user_id',$id)->where('assets.device_type',$request->device)->first()->toArray();
        $name = User::select('fullname')->where('id',$id)->first()->toArray();
        // dd($id);
        $assets_service = new AssetsService;
        $assets_service->user_id = $id;
        $assets_service->user_name = $name['fullname'];
        $assets_service->device_type = $request->device;
        $assets_service->assets_id = $asstes_id['id'];
        $assets_service->notes = $request->notes;
        $assets_service->status = "Pending";
        $assets_service->created_at =date('Y-m-d H:i:s');
        // dd($assets_service);
        $assets_service->save();
        return redirect('show_report_assets');
    }

    public function GetAssets(Request $request)
    {   
        // dd($request->id);
        $id = $request->id;
        $Assets_user = AssetsMaster::select('assets.id','assets.device_type','assets.serial_number')
                        ->where('user_id',$id)
                        ->where('assets.status','!=','scrap')
                        ->join('assets','assets.id','=','assets_master.assets_id')
                        ->get();
        $html='<option value="">Select Device</option>';
        foreach($Assets_user as $list){
            $html.='<option value="'.$list->device_type.'">'.$list->device_type.'-'.$list->serial_number.'</option>';
        }
        echo $html;
    }
    
     
    public function DeleteUserAssetsReport(Request $request)
    {
        // dd("hello");
        $id = $request->get('id');
        $laptop_assets = AssetsService::where('id', $id)->first();
        AssetsService::where('id', $id)->delete();
    }
        
    public function create()
    {  
        return view('assets.create');
    }
    public function store(Request $request)
    {
        $this->validate($request,[

            'title' => 'required',
            'device_type' => 'required',
            'serial_number' => 'required',
               
        ]);
        $laptop_assets = new Assets;
        $laptop_assets->fill($request->all());
        $laptop_assets->status = "Parchase";
        $laptop_assets->service_status = "inward";
        $laptop_assets->purchase_date = date('d-m-Y');  
        $laptop_assets->save();
        // $laptop_assets = $request->all();
        // Event::fire(new AssetsEvent($laptop_assets));
        if($laptop_assets['save_button'] == "save_new"){
            return redirect()->back()->with('message','Record Added Successfully')->with('message_type','success');
        }
        return redirect()->route('assets.index')
                         ->with('message','Record Added Successfully.')
                         ->with('message_type','success'); 
    }
    public function edit($id)
    {
        
        $laptop_assign = Assets::where('id',$id)->first();
        
        return view('assets.edit',compact('laptop_assign'));
    }
    public function update(Request $request)
    {
        $this->validate($request,[

            'title' => 'required',
            'device_type' => 'required',
            'serial_number' => 'required',
               
        ]);
        $laptop_assets = $request->all();
        event(new AssetsEvent($laptop_assets));
        if($laptop_assets['save_button'] == "save"){
            return redirect()->back()
                         ->with('message','Record Updated successfully')
                         ->with('message_type','success');    
        }
        return redirect()->route('assets.index')
                         ->with('message','Record Updated successfully')
                         ->with('message_type','success');
    }
    public function delete(Request $request)
    {
        $id = $request->get('id');
        $Assets = Assets::find($id);
        $Assets->status = "scrap";
        $Assets->update();
        // $laptop_assets = Assets::where('id', $id)->first();
        // Assets::where('id', $id)->delete();

        return back()->with('message', 'Record Deleted Successfully.')
            ->with('message_type', 'success');
    }


}