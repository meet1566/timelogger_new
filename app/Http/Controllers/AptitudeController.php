<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Aptitude;
use App\Models\Aptitudeoptions;
use Validator,PushNotification;
use DB;

class AptitudeController extends Controller
{
    //
    public function index(Request $request)
    {
        if($request->ajax()){

            $where_str = "1=?";
            $where_param = array('1');

             if ($request->get('search') ['value'] != "") {

                $search = $request->get('search') ['value'];

                $where_str .= " and (question like \"%{$search}%\""
                            ." or time like \"%{$search}%\""
                            .")";
            }

            $columns = array('id','question');

            $sort_columns = array('id','question');

            $quiz_count = Aptitude::select('id')
                                ->whereRaw($where_str,$where_param)
                                ->count();

            $quiz = Aptitude::select($columns)
                                ->whereRaw($where_str,$where_param)
                                ->orderBy('id','ASC');

            if($request->has('start') && $request->get('length') !='-1'){
                        $quiz = $quiz->take($request->get('length'))
                                    ->skip($request->get('start'));
            }
            if($request->has('order'))
            {
                for ( $i = 0; $i < $request->get('order.0.column'); $i++ )
                {
                    $column = $sort_columns[$i];
                    if(false !== ($index = strpos($column, ' as '))){
                        $column = substr($column, 0, $index);
                    }
                    $quiz = $quiz->orderBy($column,$request->get('order.'.$i.'.dir'));
                }
            }

            $quiz = $quiz->get();
            $response['iTotalDisplayRecords'] =$quiz_count;
            $response['iTotalRecords'] = $quiz_count;

            $response['sEcho'] = intval($request->get('sEcho'));

            $response['aaData'] = $quiz->toArray();

            return $response;
        }

        return view('aptitude.index');
    }

    public function create()
    {
        //$users = User::lists('fullname','id');
        $user = DB::table('users')
                ->selectRaw('id, fullname')
                ->orderBy('fullname')->get()->toArray(); 
        $users = array_column($user, 'fullname', 'id');

        return view('aptitude.create',compact('users'));
    }

    public function store(Request $request)
    {
        $aptitude_input = $request->all();
        $options = $aptitude_input['dynamic_form'];

        $rules = [
            'question' => 'required',
            'dynamic_form.dynamic_form.0.options' => 'required',
            'dynamic_form.dynamic_form.1.options' => 'required'
        ];

        $messages = [
            'dynamic_form.dynamic_form.0.options.required' => 'this field is required',
            'dynamic_form.dynamic_form.1.options.required' => 'this field is required'
        ];

        $a = 0;

        foreach ($aptitude_input['dynamic_form']['dynamic_form'] as $key => $value) {

            if ($a = 0) {

                if ( ! isset($value['is_true'])) {

                    $rules['dynamic_form.dynamic_form.0.is_true'] = 'required';
                    $messages['dynamic_form.dynamic_form.0.is_true.required'] = 'check atleast one true answer';

                } else {
                    $a = 1;
                    unset($rules['dynamic_form.dynamic_form.0.is_true']);
                    unset($messages['dynamic_form.dynamic_form.0.is_true']);
                }
            }
        }

        $count = count($options['dynamic_form']);

        for ($i=2; $i < $count; $i++) {
            if (isset($options['dynamic_form'][$i])) {
                $rules['dynamic_form.dynamic_form.'.$i.'.options'] = 'required';
            }
        }

        for ($i=2; $i < $count; $i++) {
            $messages['dynamic_form.dynamic_form.'.$i.'.options.required'] = 'this field is required';
        }

        $validator = Validator::make($aptitude_input,$rules,$messages);

        if ($validator->fails()) {
            return back()->withInput()
                           ->withErrors($validator->errors())
                           ->with('message', 'Unable to add details.')
                           ->with('message_type', 'danger');
        }

        $aptitude = new Aptitude();
        $aptitude->question = $aptitude_input['question'];
        $aptitude->save();

        foreach ($options as $option) {
            foreach ($option as $value) {
                $quizopt = new Aptitudeoptions();
                $quizopt->option = $value['options'];
                if (isset($value['is_true'])) {
                    $quizopt->is_true = '1';
                }
                $quizopt->aptitude_id = $aptitude->id;
                $quizopt->save();
            }
        }

        return redirect()->route('aptitude.index')->with('message', 'Quiz Successfully Added.')
                                              ->with('message_type', 'success');
    }

    public function edit($aptitude_id)
    {
        $aptitude_edit = Aptitude::find($aptitude_id);
        $aptitude_options = Aptitudeoptions::where('aptitude_id',$aptitude_id)->get()->toArray();

        $final = [];
        foreach ($aptitude_options as $aptitude_option) {
            $hello = [];
            $key = 'options';
            $key1 = 'is_true';
            $hello[$key] = $aptitude_option['option'];
            if ($aptitude_option['is_true']) {
                $hello[$key1] = $aptitude_option['is_true'];
            }
            $final[] = $hello;
        }
        
        return view('aptitude.edit',compact('aptitude_edit','final'));
    }

    public function update(Request $request,$aptitude_id)
    {
        // dd($request->all());
        $aptitude_input = $request->all();

        $options = $aptitude_input['dynamic_form'];

        $rules = [
            'question' => 'required',
            'dynamic_form.dynamic_form.0.options' => 'required',
            'dynamic_form.dynamic_form.1.options' => 'required'
        ];

        $messages = [
            'dynamic_form.dynamic_form.0.options.required' => 'this field is required',
            'dynamic_form.dynamic_form.1.options.required' => 'this field is required'
        ];

        $count = count($options['dynamic_form']);

        for ($i=2; $i < $count; $i++) {
            if (isset($options['dynamic_form'][$i])) {
                $rules['dynamic_form.dynamic_form.'.$i.'.options'] = 'required';
            }
        }

        for ($i=2; $i < $count; $i++) {
            $messages['dynamic_form.dynamic_form.'.$i.'.options.required'] = 'this field is required';
        }

        $validator = Validator::make($aptitude_input,$rules,$messages);

        if ($validator->fails()) {
            return back()->withInput()
                           ->withErrors($validator->errors())
                           ->with('message', 'Unable to add details.')
                           ->with('message_type', 'danger');
        }

        

            $quiz_update = Aptitude::where('id',$aptitude_id)->update([
                'question' => $aptitude_input['question'],
            ]);
        

        Aptitudeoptions::where('aptitude_id',$aptitude_id)->delete();

        foreach ($options as $option) {
            foreach ($option as $value) {
                $quizopt = new Aptitudeoptions();
                $quizopt->option = $value['options'];
                if (isset($value['is_true'])) {
                    $quizopt->is_true = '1';
                }
                $quizopt->aptitude_id = $aptitude_id;
                $quizopt->save();
            }
        }

        return redirect()->route('aptitude.index')->with('message', 'Aptitude Successfully Added.')
                                              ->with('message_type', 'success');

    }

    public function delete($aptitude_id)
    {
        Aptitude::where('id',$aptitude_id)->delete();

        return back()->with('message', 'Aptitude Delted Successfully.')
                     ->with('message_type', 'success');
    }
}
