<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\User;
use App\Models\Log;
use App\Models\Leave;
use App\Models\WorkingDays;
use DB;
use App\Models\AllowUserLog;
use Auth;

class WeeklyLogsController extends Controller
{
    public function weeklyLogs(Request $request){
    	$startdate = DB::select("SELECT q.working_date  FROM(
					SELECT working_date,id FROM `working_days`  WHERE working_date<'2019-01-01' ORDER BY `working_date` DESC LIMIT 5)q ORDER BY q.id LIMIT 1");
    	$startdate = json_decode(json_encode($startdate),true);
    	$startdates = $startdate[0]['working_date'];

    	$enddate = date('Y-m-d');
        $current_year = date('Y');
        $current_month = date('m');
        $currentWeekNumber = date('W');

        // $ts = strtotime(date('Y-m-d'));
        // $dow = date('w', $ts);
        // $offset = $dow - 1;
        // if ($offset < 0) {
        //     $offset = 6;
        // }
        // $ts = $ts - $offset*86400;
        // $date1 = date("Y-m-d", $ts) . "\n";
        // for ($i = 0; $i < 7; $i++, $ts += 86400){
        //     $date = date("Y-m-d", $ts) . "\n";
        // }
        ini_set('memory_limit', '-1');
                $list_logs = Log::selectRaw("users.is_verified,logs.user_id,TIME_FORMAT(check_out, '%h:%i:%s')  AS check_out,TIME_FORMAT(check_in, '%h:%i:%s') AS check_in,check_in as checkIN,check_out as checkOUT,users.fullname as fullname,logs.time AS Date,SEC_TO_TIME( SUM(TIME_TO_SEC(time_diff))) AS time_diff")
                    ->leftjoin('users', 'users.id', '=', 'logs.user_id')
                    ->where('users.is_admin',0)
                    ->where('users.is_verified',1)
                    ->whereMonth('logs.time','=',$current_month)
                    ->whereYear('logs.time','=',$current_year)
                    ->groupby('user_id','time')
                    //->orderBy('fullname')
                    ->get()
                    ->toArray();

        //$list_logs = DB::select("SELECT user_id,TIME_FORMAT(MAX(check_out),'%h:%i:%s') AS check_out,TIME_FORMAT(MIN(check_in),'%h:%i:%s') AS check_in,SEC_TO_TIME( SUM(TIME_TO_SEC(time_diff))) AS time_diff,TIME AS Date,users.fullname FROM `logs` LEFT JOIN `users` ON `users`.`id` = `logs`.`user_id`WHERE logs.time IN (SELECT working_days.`working_date` FROM `working_days`  WHERE working_date<'" .$enddate."' AND working_date >='".$startdates."')GROUP BY `time`, `user_id` order by `time` desc,users.fullname asc ");   

        // $list_logs = DB::select("SELECT user_id,TIME_FORMAT(MAX(check_out),'%h:%i:%s') AS check_out,TIME_FORMAT(MIN(check_in),'%h:%i:%s') AS check_in,SEC_TO_TIME( SUM(TIME_TO_SEC(time_diff))) AS time_diff,TIME AS Date,users.fullname FROM `logs` LEFT JOIN `users` ON `users`.`id` = `logs`.`user_id` where user_id=37 and logs.time IN (SELECT working_days.`working_date` FROM `working_days`  WHERE working_date<'" .$enddate."' AND working_date >='".$startdates."')GROUP BY `time`, `user_id` order by `time` desc,users.fullname asc ");
      
        $array_data = json_decode(json_encode($list_logs),true);
        $unique_date = [];
        $unique_name = [];
        foreach ($array_data as $key => $value) {
        	$unique_date[] = $value['Date'];
        	$unique_name[] = $value['fullname'];
        }

        $data = [];
        $unique_date = array_unique($unique_date);
        $unique_name = array_unique($unique_name);
        $index = 0;
        foreach ($unique_date as $date_key => $date_value) {
        	foreach ($unique_name as $name_key => $name_value) {
        		$data[$index]['date'] = $date_value;
        		$data[$index]['name'] = $name_value;
        		$index++;
        	}
        }

        $logs_data = [];
        $logs_data1 = [];
        $count = 0;
        $flag = true;
        foreach ($data as $data_key => $data_value) {
			foreach($array_data as $single_key => $single_value) {
	        	if(($data_value['date'] == $single_value['Date']) && ($data_value['name'] == $single_value['fullname'])){
	        		$logs_data[$count]['user_id'] = $single_value['user_id'];
	        		$logs_data[$count]['check_out'] = $single_value['check_out'];
	        		$logs_data[$count]['check_in'] = $single_value['check_in'];
                    $logs_data[$count]['checkOUT'] = $single_value['checkOUT'];
                    $logs_data[$count]['checkIN'] = $single_value['checkIN'];
                    if($single_value['check_in'] > "10:15:00"){
                        $logs_data[$count]['flag_allow'] = 'allow';

                    }else{
                        $logs_data[$count]['flag_allow'] = 'not a';
                    }
                    // \DB::enableQueryLog();
                    $allow_user_logs_count = AllowUserLog::where('user',$single_value['user_id'])->where(\DB::raw("DATE_FORMAT(`log_date`, '%Y-%m-%d')"),$single_value['Date'])->where('status','approved')->count();
                    // dd(\DB::getQueryLog());
                    if($allow_user_logs_count > 0){
                        $allow_user_logs_data = AllowUserLog::where('user',$single_value['user_id'])->where(\DB::raw("DATE_FORMAT(`log_date`, '%Y-%m-%d')"),$single_value['Date'])->where('status','approved')->first();
                        if($allow_user_logs_data['type'] == config('Constant.late_comming')){
                            if($single_value['check_out'] < "07:00:00"){
                                $log_data[$count]['flag_allow'] = 'not a';

                            }else{
                                $log_data[$count]['flag_allow'] = 'allow';
                            }
                            
                        }if($allow_user_logs_data['type'] == config('Constant.earlier_going')){
                            if($single_value['check_in'] > "10:15:00"){
                                $log_data[$count]['flag_allow'] = 'allow';

                            }else{
                                $log_data[$count]['flag_allow'] = 'not a';
                            }
                        }
                    }

	        		$logs_data[$count]['time_diff'] = $single_value['time_diff'];
	        		$logs_data[$count]['Date'] = $single_value['Date'];
	        		$logs_data[$count]['fullname'] = $single_value['fullname'];
                    // dd($log_data);
	        	}else{
	        		$flag = false;
	        	}
	        }
	        if($flag == false){
	        	$logs_data1[$count]['user_id'] = 1;
	    		$logs_data1[$count]['check_out'] = "";
	    		$logs_data1[$count]['check_in'] = "";
                $logs_data1[$count]['checkOUT'] = "";
                $logs_data1[$count]['checkIN'] = "";
	    		$logs_data1[$count]['time_diff'] = "";
                $logs_data1[$count]['Date'] = $data_value['date'];
	    		// $logs_data1[$count]['flag_allow'] = "";
	    		$logs_data1[$count]['fullname'] = $data_value['name'];
	    	}
	        	$count++;
        }
        $main_log_arr = array_replace_recursive($logs_data1, $logs_data);
        // echo"<pre>";
        // print_r($main_log_arr);
        // exit();
        
        return view('weeklylogs.index',['log_data'=>$main_log_arr]);
    }
}
