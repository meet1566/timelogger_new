<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Leave;
use App\Models\HolidayYear;
use App\Models\User;
use App\Models\leave_master;
use App\Models\Notification;
use Input,Response,Auth,Mail,Session,View,Validator;
use DB;

class LeavesController extends Controller{

    public function index(Request $request){
        if($request->ajax()){

            if(auth()->user()->is_admin){
                $where_str = "1 = ?";
                $where_param = array(1);
                if($request->has('sSearch')){

                    $search = $request->get('sSearch');

                    $where_str .= " and (DATE_FORMAT(leaves.date, '%d-%m-%Y') like \"%{$search}%\""
                                ." or leaves.leave_type like \"%{$search}%\""
                                ." or leaves.leave_time like \"%{$search}%\""
                                ." or leaves.status like \"%{$search}%\""
                                ." or leaves.leave_purpose like \"%{$search}%\""
                                // ." or users.fullname like \"%{$search}%\""
                                .")";
                }
                $user=null;
                if($request->has('user')){
                    $user = $request->get('user');
                    $where_str .= " and user_id = '$user'";
                }
                if($request->has('leavetype')){
                    $leavetype = $request->get('leavetype');
                    if($leavetype != null){
                        $where_str .= " and leave_type = '$leavetype'";
                    }
                }
                // dd($where_str);
                if($request->has('year')){
                    $year = $request->get('year');
                    $where_str .= " and YEAR(`date`) = $year";
                }

                //=========
                $user_id = $user;

                $sick_leave = Leave::where('user_id',$user_id)
                                    ->where('leave_type','Sick Leave')
                                    ->where('leave_time','full day')
                                    ->whereIn('status',array('a','u'))
                                    // ->where('status','a')
                                    ->whereRaw($where_str,$where_param)
                                    ->count();
                $sick_half_leaves = Leave::where('user_id',$user_id)
                                    // ->where('status','a')
                                    ->whereIn('status',array('a','u'))
                                    ->where('leave_type','Sick Leave')
                                    ->where('leave_time','half day')
                                    ->whereRaw($where_str,$where_param)
                                    ->count();

                $sick_leave = $sick_leave+($sick_half_leaves/2);
                            
                $privilege_leave = Leave::where('user_id',$user_id)
                                    // ->where('status','a')
                                    ->whereIn('status',array('a','u'))
                                    ->where('leave_type','Privilege Leave')
                                    ->where('leave_time','full day')
                                    ->whereRaw($where_str,$where_param)
                                    ->count();

                $priviledge_half_leaves = Leave::where('user_id',$user_id)
                                    // ->where('status','a')
                                    ->whereIn('status',array('a','u'))
                                    ->where('leave_type','Privilege Leave')
                                    ->where('leave_time','half day')
                                    ->whereRaw($where_str,$where_param)
                                    ->count();
                $privilege_leave = $privilege_leave+($priviledge_half_leaves/2);

                $casual_leave = Leave::where('user_id',$user_id)
                                    ->where('leave_type','Casual Leave')
                                    ->where('leave_time','full day')
                                    // ->where('status','a')
                                    ->whereIn('status',array('a','u'))
                                    ->whereRaw($where_str,$where_param)
                                    ->count();
                $casual_half_leaves = Leave::where('user_id',$user_id)
                                    // ->where('status','a')
                                    ->whereIn('status',array('a','u'))
                                    ->where('leave_type','Casual Leave')
                                    ->where('leave_time','half day')
                                    ->whereRaw($where_str,$where_param)
                                    ->count();

                $leave_master = leave_master::select('sick_leave','casual_leave','privilege_leave')->where('user_id',$user_id)->first(); 
                                   
                $casual_leave = $casual_leave+($casual_half_leaves/2);
                $res_html = view('leaves.leavelist',['sick_leave'=>$sick_leave,'privilege_leave'=>$privilege_leave,'casual_leave'=>$casual_leave,'leave_master'=>$leave_master])->render();
                //===========

                $columns = array('leaves.id','leaves.date','leaves.leave_type','leaves.leave_time','leaves.leave_purpose','leaves.status','leaves.pay_status','leaves.create_by','leaves.from_hour','leaves.to_hour','users.fullname as name');
                $leaves_count = Leave::select('leaves.user_id')
                            ->whereRaw($where_str,$where_param)
                            ->leftjoin('users','users.id','=','leaves.user_id')
                            ->count();

                $leaves = Leave::select($columns)
                        ->whereRaw($where_str,$where_param)
                        ->leftjoin('users','users.id','=','leaves.user_id');
                // dd($leaves->get());
            }else{
                $where_str = "user_id=?";

                $where_param =array( Auth::id() );
                if($request->has('sSearch')){

                    $search = $request->get('sSearch');

                    $where_str .= " and (DATE_FORMAT(leaves.date, '%d-%m-%Y') like \"%{$search}%\""
                            ." or leaves.leave_type like \"%{$search}%\""
                            ." or leaves.leave_time like \"%{$search}%\""
                            ." or leaves.status like \"%{$search}%\""
                            ." or leaves.leave_purpose like \"%{$search}%\""
                            .")";
                }
                $columns = array('id','date','date','leave_type','leave_time','leave_purpose','status','pay_status','from_hour','to_hour');
                $leaves_count = Leave::select('user_id')
                            ->whereRaw($where_str,$where_param)
                            ->count();

                $leaves = Leave::select($columns)
                        ->whereRaw($where_str,$where_param);

            }
            if ($request->has('iDisplayStart') && $request->get('iDisplayLength') != '-1') {
                $leaves = $leaves->take($request->get('iDisplayLength'))->skip($request->get('iDisplayStart'));
            }

            if ($request->has('iSortCol_0')) {
                for ($i = 0; $i < $request->get('iSortingCols'); $i++) {
                    $column = $columns[$request->get('iSortCol_' . $i)];
                    if (false !== ($index = strpos($column, ' as '))) {
                        $column = substr($column, 0, $index);
                    }
                    $leaves = $leaves->orderBy($column, $request->get('sSortDir_' . $i));
                }
            }
            $leaves = $leaves->get()->toArray();
            // dd($leaves);
            foreach($leaves as $key =>$value)
            {

                $leaves[$key]['day'] = date('l', strtotime($value['date']));

            }
            //dd($leaves);
            $response['iTotalDisplayRecords'] = $leaves_count;
            $response['iTotalRecords'] = $leaves_count;

            $response['sEcho'] = intval($request->get('sEcho'));

            $response['aaData'] = $leaves;
            if($request->has('user')){
                $response['res_html'] = $res_html;
            }
            return $response;
        }
        /*$user = ["" => "Select User"] + User::orderBy('fullname','asc')->pluck('fullname', 'id')->toArray();*/
        $users = User::selectRaw('id, fullname')
                ->orderBy('fullname')->where('is_verified','1')->get()->toArray();
        // dd($users);        
        $user = array_column($users, 'fullname', 'id');
        
		return view('leaves.index',['user'=>$user]);
	}

	public function create(){

		return view('leaves.create');
	}

	public function store(Request $request){
        $id = Auth::id();
        $data = $request->all();

        $token = str_random(40);

        $data['user_id'] = $id;
        $data['token'] = $token;

        $rules = [
            'date' => 'required',
            'leave_type' => 'required',
            'leave_time' => 'required',
            'leave_purpose' => 'required',
        ];

        if (isset($data['leave_time']) && $data['leave_time'] == 'half day') {
            $rules['from_hour'] = 'required';
            $rules['to_hour'] = 'required';
        }
        
		$this->validate($request,$rules);

        $leaves = new Leave($data);
		$leaves->save();

        $email_id = 'hr@thinktanker.in';

        if ($leaves->save())
        {
            $data = array(
                'token' => $token
            );

            Mail::send('notify', $data, function($message) use ($email_id,$request)
            {
                $message->from(Auth::user()->email);

                $message->to($email_id)->subject('Please Approve Leave');
            });
        }

        return back()->with('message','Your Leave Submitted Successfully')
                     ->with('message_type','success');
	}

	public function edit($id){

		$leaves = Leave::find($id);
        $leave = Leave::where('id',$id)->get()->toArray();
        $leaves_date = $leave[0]['date'];
        $date = date("Y-m-d", strtotime($leaves_date) );
        return view('leaves.edit',compact('leaves','date'));
	}

	public function update(Request $request,$id){

		$leaves = Leave::find($id);

        $rules = [
            'date' => 'required',
            'leave_type' => 'required',
            'leave_time' => 'required',
            'leave_purpose' => 'required',
        ];

        if (isset($leaves['leave_time']) && $leaves['leave_time'] == 'half day') {
            $rules['from_hour'] = 'required';
            $rules['to_hour'] = 'required';
        }

        $this->validate($request,$rules);

		$leaves->fill($request->all());
        $leaves->save();

        // $email_id = 'rajan@thinktanker.in';

        // if ($leaves->save())
        // {
        //     $data = array(
        //         'id' => $id
        //     );

        //     Mail::send('notify', $data, function($message) use ($email_id,$request)
        //     {
        //          $message->to($email_id)->subject('Please Approve The Editing Leave');
        //     });
        // }

        // return back()->with('message','Your Leave Updated Successfully')
        //              ->with('message_type','success');
        return redirect()->route('leaves.index')->with('Your Leave Updated Successfully')->with('message_type','success');

	}

	public function destroy(Request $request)
	{
        $leave=$request->get('id');

        if(!is_array($leave)){

            $leave = array($leave);
        }

        $leave = leave::whereIn('id',$leave)->delete();
        return response()->json(array('success' => true),200);

	}

    public function approveLeave($token)
    {
        $leaves = Leave::where('token',$token)->where('status','p')->first();

        if(! $leaves) {
            return 'wrong request';
        }

        $user = User::select('fullname')->where('id',$leaves->user_id)->first();

        return view('leaves.approveleave',compact('leaves','token','user'));
    }

    public function approvemail(Request $request, $id/*,$token*/)
    {
        //Leave::where('token', $token)->update(array('status' => 'a'));
        Leave::where('id', $id)->update(array('status' => 'a'));

        $msg = $request->get('message');
        

        $leaves = Leave::where('id',$id)->first();

        $email_id = User::where('id',$leaves->user_id)->pluck('email')[0];

        $user_name = User::where('id',$leaves->user_id)->pluck('fullname')[0];
        $data = array(
            'leaves' => $leaves,
            'user_name' =>$user_name,
        );

        Mail::send('approve_leave',$data,function($message) use ($email_id)
        {
           $message->from('hr@thinktanker.in');
           $message->to($email_id)->subject("Approve Leave");
        });

        //return response()->json(array('success' => true),200);
        return back()->with('message','Leave Approved Successfully')
                        ->with('message_type','success');

    }
    public function unpaidemail(Request $request, $id/*,$token*/)
    {
        Leave::where('id', $id)->update(array('status' => 'u'));
        $msg = $request->get('message');
        

        $leaves = Leave::where('id',$id)->first();

        $email_id = User::where('id',$leaves->user_id)->pluck('email')[0];

        $user_name = User::where('id',$leaves->user_id)->pluck('fullname')[0];
        $data = array(
            'leaves' => $leaves,
            'user_name' =>$user_name,
        );

        Mail::send('rejectleave',$data,function($message) use ($email_id)
        {
           $message->from('hr@thinktanker.in');
           $message->to($email_id)->subject("Disapproved Leave");
        });

        //return response()->json(array('success' => true),200);
        return back()->with('message','Leave Disapproved Successfully')
                        ->with('message_type','success');

    }

    public function approvunpaidemail(Request $request,$id/*,$token*/)
    {
        //Leave::where('token', $token)->update(array('status' => 'd'));

        Leave::where('id', $id)->update(array('status' => 'au'));
        
        $msg = $request->get('message');
        $leaves = Leave::where('id',$id)->first();
        $email_id = User::where('id',$leaves->user_id)->pluck('email')[0];
        $user_name = User::where('id',$leaves->user_id)->pluck('fullname')[0];

        $data = array(
            'leaves' => $leaves,
            'user_name' =>$user_name,
        );

        Mail::send('approveunpaid_leave',$data, function($message) use ($email_id,$request)
        {
            $message->from('hr@thinktanker.in');
            $message->to($email_id)->subject("Approve Unpaid  Leave");
        });

        return response()->json(array('success' => true),200);
    }

    public function userCreate($userId)
    {   
        // $id = $userId;
        $showUserName = user::where('id',$userId)->select('fullname')->first();
        return view('leaves.user-create',compact('userId','showUserName'));
    }


    public function userStore(Request $request, $userId)
    {
        $data = $request->all();
        // dd($request->all());
        $data['user_id'] = $userId;
        //$data['token'] = $token;
        Session::put("leave_user_id",$data['user_id']);
        $rules = [
            'date' => 'required',
            'leave_type' => 'required',
            'leave_time' => 'required',
            'leave_purpose' => 'required',
        ];

        if (isset($data['leave_time']) && $data['leave_time'] == 'half day') {
            $rules['from_hour'] = 'required';
            $rules['to_hour'] = 'required';
        }
        // dd($data['leave_time']);
        if ($data['leave_time'] == "full day") {
            unset($data['from_hour']);
            unset($data['to_hour']);
        }
        $this->validate($request,$rules);

        $leaves = new Leave($data);

        $leaves->status = 'p';

        if (isset($data['ungrant'])) {
            $leaves->is_granted = 1;
        }
        $leaves->create_by = 1;
        $leaves->save();
        return redirect()->route('leaves.index')->with('Leave added Successfully')->with('message_type','success');
        // return back()->with('message','leave added Successfully')
        //              ->with('message_type','success');
    }
    public function leavestore(Request $request)
    {
      
        $firststep_store = $request->formData;
        $date_of_leave = explode(',', $firststep_store['date']);
        $array_count_value = count($date_of_leave);
        $firstform_date = Session::put('firstform_date',$date_of_leave);  
        // $session_data = json($firstform_date);        
        $date = Session::put('date',$firststep_store['date']);
        $first_leavetype = Session::put('leave_type',$firststep_store['leave_type']);
        $first_leavepurpose = Session::put('leave_purpose',$firststep_store['leave_purpose']);
        $get_leavetype = Session::get('leave_type');
        $get_leavepurpose = Session::get('leave_purpose');
        $holiday_array = HolidayYear::select("holiday_date")->get()->pluck('holiday_date')->toArray();
        $content = View::make('dashboard.leave_form', compact('holiday_array','date_of_leave','get_leavetype','get_leavepurpose'))->render();

        return response()->json(['success'=>true,'html_content'=>$content,'firststep_store'=>$firststep_store,'date_of_leave'=>$date_of_leave]);
  
    }
    public function leavesecondstore(Request $request)
    {
        $requestAll = $request->all();
        $leave_detail = $request['leave_detail'];
        foreach ($leave_detail as $key => &$value) {
            if($value['day_type'] == "halfday")
            {

                if ($value['half'] == "first_half") {
                   $value['from_hour'] = "10:00 AM";
                   $value['to_hour'] = "02:00 PM";
                }
                else if($value['half'] == "second_half")
                {
                    $value['from_hour'] = "03:00 PM";
                   $value['to_hour'] = "07:00 PM";
                }
            }
            if(isset($value['day_type']))
            {
                $validator = Validator::make($value, [
                'leave_purpose' => 'required',
                 ]);
                if($validator->fails())
                {
                    return response()->json(['error'=>$validator->errors()->all()]);
                }
            }else
            {
                $validator = Validator::make($value, [
                'from_hour' => 'required',
                'to_hour' => 'required',    
                'leave_purpose' => 'required',
                 ]);
                if($validator->fails())
                {
                    return response()->json(['error'=>$validator->errors()->all()]);
                }
            } 
        }
        $session_put_data = Session::put('leave_detail',$leave_detail);
        
        return response()->json(['success'=>true,'leave_detail'=>$leave_detail]);

    }
    public function leavethirdstore(Request $request)
    {
        $step2_session = Session::get('leave_detail');
        if(!empty($step2_session))
        {
            $leave_confirmation = []; 
            foreach ($step2_session as $key => $value) {
        
                $leaves = new Leave;
                $leaves->user_id = Auth::id();
                $leaves->date = $key;
                $leaves->leave_type = $value['leave_type'];
                if($value['from_hour'] == "10:00 AM" && $value['to_hour'] == "07:00 PM")
                {
                     $leaves->leave_time = "full day";
                     // $leaves->to_hour = $value['to_hour'];
                     // $leaves->from_hour = $value['from_hour'];
                    
                }else
                {   
                   
                    $leaves->from_hour = $value['from_hour'];
                    $leaves->to_hour = $value['to_hour'];
                    $leaves->leave_time = "half day";
                    // dd($leaves);
                }
                $leaves->leave_purpose = $value['leave_purpose'];
                
                $leaves->save();    
            }
             
        }else
        {
        $firstform_date = Session::get('firstform_date');
        $first_leavetype = Session::get('leave_type');
        $first_leavepurpose = Session::get('leave_purpose');
             //dd($firstform_date);
        $leave_confirm = [];
        foreach ($firstform_date as $key => $value) {

                $leave_confirm[$key] = $value;
                $leaves = new Leave;
                $leaves->user_id = Auth::id();
                $leaves->date = $value;
                $leaves->leave_type = $first_leavetype;
                $leaves->leave_time = "full day";
                $leaves->leave_purpose = $first_leavepurpose;
                
                $leaves->save();    
            }
         } 
        if($leaves->save())
         {
            $user = User::find($leaves->user_id);    
            Mail::send('leave_request',['user' => $user,'step2_session' => $step2_session],function($message) use ($user)
            {
               $message->from($user->email);
               $message->to('leaverequest@thinktanker.in', 'rajan')->cc(['rajan@thinktanker.in','hr@thinktanker.in'])->subject('thinkTANKER Leave Request');
            });     
         }

         return response()->json(['success'=>true,'request'=>$request->all()]);
    }
    public function leaveStatus(Request $request){
        $leave_data = $request->all();
        // dd($leave_data);
        $id = $leave_data['id'];
        if (!is_array($id)) {

            $id = array($id);
        }
        // dd($id);
        $leaves = Leave::whereIn('id',$id)->first()->toArray();

        $notification = new Notification();

        if($leave_data['leaveapprove'] == "approve"){
            $leave = Leave::whereIn('id',$id)->update(['status'=>'a','pay_status'=>'p']);
            $notification->user_id = $leaves['user_id'];
            $notification->title = '<strong>Leave Approved as Paid</strong><br/>'.substr($leaves['leave_purpose'],0,20).'...';
            $notification->is_read = 0;
            $notification->is_view = 0;
            // dd("hello");
            $notification->save();
            return $this->approvemail($request,$id);
            // return back()->with('message','Leave Approved Successfully')
            //             ->with('message_type','success');
        }
        elseif($leave_data['leaveapprove'] == "approveunpaid"){
            // dd($notification);

            $leave = Leave::whereIn('id',$id)->update(['status'=>'a','pay_status'=>'u']);
            
            $notification->user_id = $leaves['user_id'];
            $notification->title = '<strong>Leave Approved as Unpaid</strong><br/>'.substr($leaves['leave_purpose'],0,20).'...';
            $notification->is_read = 0;
            $notification->is_view = 0;
            // dd("hello");
            $notification->save();
            return $this->approvemail($request,$id);
        }
        elseif($leave_data['leaveapprove'] == "approveee")
        {
            $leave = Leave::whereIn('id',$id)->update(['status'=>'u']);
            $notification->user_id = $leaves['user_id'];
            $notification->title = '<strong>Leave Disapproved</strong><br/>'.substr($leaves['leave_purpose'],0,20).'...';
            $notification->is_read = 0;
            $notification->is_view = 0;
            // dd("hyy");
            $notification->save();
            return $this->unpaidemail($request,$id);
            // return back()->with('message','Leave Approved Successfully')
            //             ->with('message_type','success');
        }else{
            $leave = Leave::whereIn('id',$id)->update(['status'=>'au']);
            $notification->user_id = $leaves['user_id'];
            $notification->title = '<strong>Leave Approved as Unpaid</strong><br/>'.substr($leaves['leave_purpose'],0,20).'...';
            $notification->is_read = 0;
            $notification->is_view = 0;
            $notification->save();
            return $this->approvunpaidemail($request,$id);
            // return back()->with('message','Leave Disapproved Successfully')
            //             ->with('message_type','success');
        }

    }
    public function notificationIsread(Request $request){
        // $notification = $notifications['notification'];
        $notification_data = $request->all();
        $notification = json_decode($notification_data['notification'],true);
        foreach ($notification as $key => $value) {
            $ids[]=$value['id'];
        }
        $notification = Notification::whereIn('id',$ids)->update(['is_read'=>'1']);
        
        return view("layout.layout",compact('notification'));
    }
    public function notificationIsview(Request $request){
        $notification_data = $request->all();
        $notification = Notification::where('id',$notification_data['id'])->update(['is_view'=>'1']);

        return view("layout.layout",compact('notification'));
        
    }
    public function checkboxtatus(Request $request){
        $data = $request->all();
        $id = $data['value'];
        $status = Leave::select('status')->where('id',$id)->first();
        return response()->json(['success'=>true,'data'=>$status]);
    }

    public function exportLeaves($user,$leavetype,$year){
        // dd($year);
        ini_set('memory_limit','523M');
        $where_str = "1 = ?";
        $where_param = array(1);
        
        $columns = array('leaves.id','leaves.date','leaves.date','leaves.leave_type','leaves.leave_time','leaves.leave_purpose','leaves.status','leaves.from_hour','leaves.to_hour','users.fullname as name');

        // \DB::enableQueryLog();                
        
            $where_str .= " and user_id = '$user'";
        
        if($leavetype != 'all'){
            $where_str .= " and leave_type = '$leavetype'";
        }
        if($year){
            $where_str .= " and YEAR(`date`) = $year";
        }
            // dd($user);
        $leaves = Leave::select($columns)
                        ->whereRaw($where_str,$where_param)
                        ->leftjoin('users','users.id','=','leaves.user_id')->get()->toArray();
        // dd(\DB::getQueryLog($leaves));
        // exit();
        $mpdf = new \Mpdf\Mpdf();
        //$mpdf = new \mPDF('c', 'A4','50','50','30','30','30','30');
        // $mpdf -> SetFont('eurostyle');
        $header= '<table width="100%"><tr>  
            <td width="80%">'.'<h4>Employee Leaves'." "." ".'</h4></td>
            <td width="20%"><h1>thinkTANKER</h1></td>
            </tr></table>';
        // $mpdf->setFooter("Page {PAGENO} of {nb}");
        // $mpdf->autoPageBreak = true;
        $mpdf->WriteHTML(\View::make('leaves.user-exportleaves',['leaves'=>$leaves])->render());
        $filename = $leaves[0]['name'].'-'.$year.'.pdf';
        $filepath = public_path() ."/uploads/" . $filename;
        $mpdf->output($filepath);
        
        return response()->download($filepath);
                     
    }
}   
