<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use App\Models\Permission;
use App\Models\User;
use App\Models\role;
use App\Models\UserPermission;
use App\Models\role_permission;

class userPermissionMaster extends Controller
{
    //
    public function index (Request $request)
    {
        if($request->ajax()){

            $where_str = '1 = ?';
            $where_params = [1];

            $columns =['id','name','description'];

            //deviceType filter
            $brand = role::select($columns)
                    ->whereRaw($where_str,$where_params);
           
            $brand_count = role::select($columns)
                        ->whereRaw($where_str,$where_params)
                        ->count();

            if ($request->has('iDisplayStart') && $request->get('iDisplayLength') != '-1') {
                $brand = $brand->take($request->get('iDisplayLength'))->skip($request->get('iDisplayStart'));
            }

            if ($request->has('iSortCol_0')) {
                for ($i = 0; $i < $request->get('iSortingCols'); $i++) {
                    $column = $columns[$request->get('iSortCol_' . $i)];
                    if (false !== ($index = strpos($column, ' as '))) {
                        $column = substr($column, 0, $index);
                    }
                    $brand = $brand->orderBy($column, $request->get('sSortDir_' . $i));
                }
            }            
            $brand = $brand->get();
            $response['iTotalDisplayRecords'] = $brand_count;
            $response['iTotalRecords'] = $brand_count;
            $response['sEcho'] = intval($request->get('sEcho'));
            $response['aaData'] = $brand;
            return $response;

        }
         return view('permission.index');
    }

    public function create()
    {
        //$users = User::select('fullname','id')->where('is_verified',1)->get()->pluck('fullname','id')->toArray();
         $users = role::select('name','id')->get()->pluck('name','id')->toArray();
        // dd($users);

        $permissions = Permission::select()->get()->toArray();

        $secPermissions = [];

        foreach($permissions as $key => $value) {
            $secPermissions[$value['section']][] = ['description' => $value['description'],'id' => $value['id']] ;
        }
        // dd($secPermissions);
        return view('permission.createMaster', compact('permissions','users','secPermissions'));
    }

    public function store(Request $request){

        //create the role 
        // dd($request->all());
        $this->validate($request,[
            'name'=>'required',
            'description'=>'required'
        ]);
            $id = $request->id;
        
            if ($id == null) {
                $role_save = new role();
                $role_save->name = $request->get('name');
                $role_save->description = $request->get('description');
                $role_save->save();
                $role_id  = $role_save->id;
                $count = count($request['permissions']);
                for ($i=0; $i < $count; $i++) { 
                        $role_permission_save = new role_permission();
                        $role_permission_save->role_id = $role_id;
                        $role_permission_save->permission_id = $request->get('permissions')[$i];
                        $role_permission_save->save();
                }
            }else{
                $role_save = role::find($id);
                $role_save->name = $request->name;
                $role_save->description = $request->description;
                $role_save->update();
                role_permission::where('role_id',$id)->delete();
                if ($request['permissions'] != null) {
                    $count = count($request['permissions']);
                    for ($i=0; $i < $count; $i++) { 
                            $role_permission_save = new role_permission();
                            $role_permission_save->role_id = $id;
                            $role_permission_save->permission_id = $request->get('permissions')[$i];
                            $role_permission_save->save();
                    }
                }
            }
       
          
            return redirect('showroles')->with('message', 'Role created/Changed Successfully')
                     ->with('message_type', 'success');
    }

    public function getRoleId(Request $request)
    {
        $role_permission = role_permission::select('role_id','permission_id')->where('role_id',$request['id'])->get()->toArray();
        return response()->json(['success' => true,'role_permission' => $role_permission],200);
    }

    public function edit ($id)
    {
        $role_permission = role_permission::where('role_id',$id)->pluck('permission_id')->toArray();
        $users = role::select('name','id')->where('id',$id)->first()->toArray();
        $roleselect = role::select('description','name','id')->where('id',$id)->get()->toArray();
        $permissions = Permission::select()->get()->toArray();
        
        $secPermissions = [];

        foreach($permissions as $key => $value) {
            $secPermissions[$value['section']][] = ['description' => $value['description'],'id' => $value['id']] ;
        }
        $rolename = $roleselect[0]['name'];
        
        return view('permission.editMaster',compact('role_permission','secPermissions','users','permissions','roleselect','rolename'));
        
    }

    public function ajaxResponse($id)
    {
        $editpermissions = role_permission::select('permission_id')->where('role_id',$id)->get()->pluck('permission_id')->toArray();

        return response()->json(['success' => true,'editpermissions' => $editpermissions],200);
    }

    public function delete(Request $request)
    {
        role_permission::where('role_id',$request->id)->delete();
        role::where('id',$request->id)->delete();

        return back()->with('message', 'Role deleted Successfully')
                     ->with('message_type', 'success');
    }
}
