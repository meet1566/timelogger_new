@extends('layout.layout')
@section('content')
<div class="page-title">
    <div>
    <h1>Aptitude</h1>
        <ul class="breadcrumb side">
            <li><i class="fa fa-home fa-lg"></i></li>
            <li>Create</li>
        </ul>
    </div>
</div>
<div class="clearix"></div>
<div class="col-md-12">
    
    <div class="card">
        <h3 class="card-title">Create</h3>
        <div class="card-body">
            <?= Form::open(array('url' => route('aptitude.store') ,'class' => 'form-horizontal','files' => true)) ?>
                <div class="form-group">
                    <label class="control-label col-sm-2">Question</label>
                    <div class="col-sm-8">
                        <?= Form::text('question', old('question'), ['class' => 'form-control', 'placeholder' => 'Aptitude Question']); ?>
                      	<?= $errors->first('question',"<span class='text-danger'>:message</span>");?>
                    </div>
             	</div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Options</label>
                    <div class="col-sm-8" style="border:1px solid #DDD;padding:20px;left: 14px;">
                        <div class="form-inline" id="dynamic_form">
                            <input type="radio" name="is_true" value="1" id="is_true" />&nbsp&nbsp
                            <input type="text" style="margin-bottom:10px;width:52.5%;" class="form-control" name="options" id="options" placeholder="enter options">
                            <a href="javascript:void(0)" style="vertical-align:3px;margin-left:5px;" class="btn btn-primary" id="plus5">Add More</a>
                            <a href="javascript:void(0)" style="vertical-align:3px;margin-left:5px;" class="btn btn-danger" id="minus5">Remove</a>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-8 col-md-offset-3">
                        <button name="quiz_post" value="exit" type="submit" class="btn btn-primary">Save & Exit</button>
                        <a href="<?= route('aptitude.index') ?>" class="btn btn-danger">Cancel</a>
                    </div>
                </div>
       	    <?= Form::close(); ?>
        </div>
    </div>
</div>
@stop
@section('script')
<?= Html::script('asset/js/jquery.js') ?>
<?= Html::script('js/jquery-dynamic-form.js') ?>
<?= Html::script('front/js/select2.min.js') ?>
<script type="text/javascript">

$(document).ready(function() {

    $("#user_ask").select2();

    $("#selectall").click(function(){
        if($("#selectall").is(':checked') ){
            $("#user_ask > option").prop("selected","selected");
            $("#user_ask").trigger("change");
        }else{
            $("#user_ask > option").removeAttr("selected");
             $("#user_ask").trigger("change");
         }
    });

    $('input[type="radio"]').click(function(){
        $('input[type="radio"]').not($(this)).prop('checked',false);
    });

    var dynamic_form =  $("#dynamic_form").dynamicForm("#plus5", "#minus5", {
        limit:100,
        normalizeFullForm : false,
    });

    @if(!count($errors))
        dynamic_form.inject([{"options":""},{"options":""}]);
    @endif

    @if(count($errors) >= 0)
        dynamic_form.inject( <?= json_encode(old('dynamic_form.dynamic_form')) ?> );

        var detail_Errors = <?= json_encode($errors->toArray()) ?>;

        $.each(detail_Errors, function(id, msg){
            var id_arr = id.split('.');
            $('#options' + id_arr[id_arr.length-2]).parent().append('</br><span class="help-inline text-danger">'+ msg[0] +'</span></br>');
        });
    @endif

});
</script>
@stop
