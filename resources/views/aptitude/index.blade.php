@extends('layout.layout')
@section('content')
<div class="page-title">
    <div>
    <h1>Aptitude</h1>
    <ul class="breadcrumb side">
        <li><i class="fa fa-home fa-lg"></i></li>
        <li>Questions List</li>
    </ul>
    </div>
    <div>
        <a href="<?=URL::route('aptitude.create')?>" class="btn btn-primary btn-flat"><i class="fa fa-lg fa-plus"></i></a>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
           <div class="card-body">
              <table id="employee" class="table table-hover table-bordered" border="1 px">
                 <thead>
                    <tr>
                        <th></th>
                        <th>Question</th>
                        <th>Action</th>
                    </tr>
                 </thead>
                 <tbody></tbody>
              </table>
           </div>
        </div>
    </div>
</div>
@stop
@section('script')
<?= Html::script('asset/DataTables/media/js/jquery.dataTables.js') ?>
<?= Html::script('asset/DataTables/media/js/dataTables.bootstrap.min.js') ?>

<script>
    $(document).ready(function()
    {
        $('#employee').DataTable(
        {
            "bProcessing" : true,
            "bServerSide" : true,
            "ajax" :"<?= URL::route('aptitude.index') ?>",
            "aaSorting": false,
            "aoColumns" :[{
                mData: "id",
                bSortable: false,
                bVisible: false
            },
            { mData: 'question',bSortable: false},
           
            {
                mData: null,
                bSortable : false,
                mRender:function(v,t,o)
                {
                    var extra_html = '';

                    var path_del = "<?= route('aptitude.delete',[':id']) ?>";
                    path_del = path_del.replace(':id',o['id']);

                    var path_edit = "<?= route('aptitude.edit',[':id']) ?>";
                    path_edit = path_edit.replace(':id',o['id']);

                        extra_html += '<a href="'+path_edit+'" title="edit" class="fa fa-fw fa-edit"></a>  ';
                        extra_html += '<a href="'+path_del+'" title="delete" class="fa fa-fw fa-trash-o"></a>  ';

                    return extra_html;
                }
            },
            ],
        });

    });
</script>
@stop
