@extends('partials.login-page')
@section('content')
<section class="material-half-bg">
    <div class="cover"></div>
</section>
<section class="login-content">
<div class="logo"><img src="/asset/images/logo.png" class="img-responsive"></div>
<div class="login-box">
    <?= Form::open(array('url' => route('password.update') ,'class' => 'login-form','method'=>'POST')) ?>
    <?= session('msg'); ?>
    {!! csrf_field() !!}
    <input type="hidden" name="token" value="{{ $request->route('token') }}">
 
    @if (count($errors) > 0)
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <div class="form-group">
        <label for="inputEmail">Email</label>
        <?= Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => 'Email']); ?>
        <?= $errors->first('email',"<span class='text-danger'>:message</span>");?>
    </div>
    <div class="form-group">
        <label for="inputEmail">New Password</label>
        <?= Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password']); ?>
        <?= $errors->first('password',"<span class='text-danger'>:message</span>");?>
    </div>
    <div class="form-group">
        <label for="inputEmail">Confirm New Password</label>
        <?= Form::password('password_confirmation',['class' => 'form-control', 'placeholder' => 'Confirm Password']); ?>
        <?= $errors->first('password_confirmation',"<span class='text-danger'>:message</span>");?>
    </div>
    <div class="form-group">
            <div class="btn-group">
            <button class="btn btn-primary">Submit</button>
            </div>
          </div>
    <?= Form::close()?>
</div>  
</section>

<script src="https://code.jquery.com/jquery-1.10.2.js"></script>

 <!-- <?= Html::script('js/jquery-dynamic-form.js') ?> -->

<script>
    $(document).ready(function() {
        $('form').submit(function(){
            $(this).find('button:submit').html('<span><i class="fa fa-spinner fa-spin">Please Wait..</span></i>').prop('disabled', true);
        });

    });
    
</script>