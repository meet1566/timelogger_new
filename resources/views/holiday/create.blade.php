@extends('layout.layout')
@section('content')
<div class="page-title">
    <div>
        <h1>Holidays</h1>
        <ul class="breadcrumb side">
            <li><i class="fa fa-home fa-lg"></i></li>
            <li>create</li>
        </ul>
    </div>
</div>
<div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))
    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
    @endif
    @endforeach
</div>
<div class="clearix"></div>
<div class="col-md-12">
    <div class="card">
        <h3 class="card-title">Create</h3>
        <div class="card-body">
            @include('partials.alert')
            <?= Form::open(array('url' => route('holiday.store') ,'class' => 'form-horizontal' ,'files' => true)) ?>
            {{ session('msg') }}
            <div class="form-group">
                <label class="control-label col-sm-2" for="inputFull Name">Day</label>
                <div class="col-sm-8">
                    <?= Form::select('day', [''=>'select','Sunday'=> 'Sunday','Monday' => 'Monday' ,'Tuesday' => 'Tuesday','Wednesday' => 'Wednesday','Thursday' => 'Thursday' ,'Friday' => 'Friday','Saturday' => 'Saturday'], null,['class' => 'form-control']); ?>
                    <?= $errors->first('day',"<span class='text-danger'>:message</span>");?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="inputFull Name">Date</label>
                <div class="col-sm-8">
                    <?= Form::date('date', null, ['class' => 'form-control', 'placeholder' => 'Date']); ?>
                    <?= $errors->first('date',"<span class='text-danger'>:message</span>");?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="inputFull Name">Holiday</label>
                <div class="col-sm-8">
                    <?= Form::text('holiday', null, ['class' => 'form-control', 'placeholder' => 'Holiday']); ?>
                    <?= $errors->first('holiday',"<span class='text-danger'>:message</span>");?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-8 col-md-offset-2">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <a href="<?= URL::route('holiday.index') ?>" class="btn btn-white btn-default"> Back</a>
                    
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@stop
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<script>
    jQuery(document).ready(function() {
        $('form').submit(function(){
            $(this).find('button:submit').html('<i class="fa fa-spinner fa-spin" value="Please Wait"></i>').prop('disabled', true);
        });

    });
</script>

