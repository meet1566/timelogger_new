@extends('layout.layout')
@section('content')
<style type="text/css">
  .graph{
    width: 440px;
    height: 318px !important;
    position: absolute;
    top: -30px !important;
    }
  
.fontsize {
    font-weight: bold;
    font-size: 18px;
  }
  .firststatus{
    display: none !important;
  }
  .datepicker-dropdown{
    /*position: absolute;
    top: 50% !important;
    left: 50% !important;
    transform: translate(-50%,-50%) !important;*/
    margin-top: 43px;
  } 

  .for_tool {
    position: relative;
    border-bottom: 1px dotted black;
  }

  .for_tool .for_tool_text {
    visibility: hidden;
    width: 120px;
    background-color: black;
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 5px 0;

    /* Position the tooltip */
    position: absolute;
    z-index: 1;
    bottom: 100%;
    left: 0;
  }
  .for-msg{
    display: block;
    padding: 14px;
    font-size: 14.5px;
    margin-bottom: 14px;
    border-radius: 3px;
    border: 1px solid rgb(180,180,180);
    background-color: #009688;
    color: #fff;
  }
  .for_tool:hover .for_tool_text {
    visibility: visible;
  }
  .assets_box{
    display: flex;
    width: 100%;
  }
  @media only screen and (max-width: 768px) {
    .datepicker-dropdown{
      margin-top: 90px;
    }
    .assets_box{
      display: block;
    }
  }

</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
  @foreach($holiday_array as $holi_date)
      <input type="hidden" name="" class="getholidays" value="{{$holi_date}}">
  @endforeach
    <div class="page-title">
      <div class="col-md-6">
        <h1><i class="fa fa-dashboard"></i> Dashboard </h1>
        <!-- <p>Leaves</p> -->
      </div>
      @if(Auth::user()->is_admin == 0)
        @if(Auth::user()->aadhar_img == null)
          <div class="col-md-6" style="display:flex;justify-content: end;color: #009688;"><h4><a href="<?= URL::route('show.user') ?>">Please upload your aadhar card and pan card Here</a></h4></div>
        @endif
      @endif
      <div>

      <!--   <ul class="breadcrumb">
          <li><i class="fa fa-home fa-lg"></i></li>
          <li><a href="{{route('dashboard.admin')}}">Dashboard</a></li>
        </ul> -->
      </div>
    </div>
    <div id="toastrmessage"></div>
    <div class="row mt-20">
      @if(!auth()->user()->is_admin)
        <div class="col-md-6">
            <div class="card" style="height: 354px;">
              <div style="display: flex;">
                <h3 class="card-title" style="margin-bottom: 0">Missing Logs</h3>
               <a style="margin-left: 160px;" href="<?= URL::route('show.logs',['id'=>Auth::user()->id]) ?>"><button type="button" class="btn btn-primary col-sm-offset-10 text-left"  style="display: inline-block;">Logs</button></a>
               </div>
               
                <div class="embed-responsive embed-responsive-16by9" style="height: 318px">
                    <canvas class="graph embed-responsive-item" id="barChartDemo"></canvas>
                </div>
            </div>
        </div>
        <div class="col-md-6 leaves">
            <div class="card" style="height: 354px;">
              <div style="display: flex;">
                <h3 class="card-title" style="margin-bottom: 0">Leaves Status</h3>
                 <a style="margin-left: 110px;" href="<?= URL::route('show.userleaves') ?>"><button type="button" class="btn btn-primary col-sm-offset-10 text-left"  style="display: inline-block;">Leaves</button></a>
                 </div>
                <div class="embed-responsive embed-responsive-16by9" style="height: 318px">
                    <canvas class="graph embed-responsive-item" id="barChartDemo_1"></canvas>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
              <div class="card-title-w-btn">
                <h3 class="title">Leave</h3>
                <p><a class="btn btn-primary" href="" data-toggle="modal" data-target="#leave-details">Leave Details</a>&nbsp;<a class="btn btn-primary " href="javascript:;" data-toggle="modal" data-target="#leave-request">Leave Request</a></p>
              </div>
              <!-- leave-details Modal-->
                <div class="modal fade" id="leave-details" role="dialog" tabindex="-1" aria-labelledby="leave-detailsLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="card">
                        <div class="card-title-w-btn">
                          <h3 class="title">leaves</h3>
                          <p>
                            <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          </p>
                        </div>
                        <div class="card-body">
                          <div class="bs-component">
                            <div class="list-group">
                                @foreach($total_leaves as $key => $leaves)
                                <a class="list-group-item">
                                <h4 class="list-group-item-heading date_leavetype"> <?= $leaves['date']?> (<?= $leaves['leave_type']?>)</h4>
                                <p class="list-group-item-text reason_leave"><?= $leaves['leave_purpose'];?></p>
                                <p class="list-group-item-text haft-full-day"><strong><?= $leaves['leave_time']?></strong></p></a>
                                @endforeach
                               </div>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
              <!-- leave-request Modal-->
                <div class="modal fade" id="leave-request" role="dialog" tabindex="-1" aria-labelledby="leave-requestLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="card">
                        <div class="card-title-w-btn">
                          <h3 class="title">Leave Request</h3>
                          <p>
                            <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          </p>
                        </div>
                        <div class="card-body">
                          <div class="bs-component well">
                            <ul class="nav nav-tabs activation-tabs">
                              <li class="active" id="previoustab"><a href="javascript:;" data-toggle="tab">Step 1</a></li>
                              <li class="step2" id="middletab"><a href="javascript:;" data-toggle="tab">Step 2</a></li>
                              <li class="step3" id="nexttab"><a href="javascript:;" data-toggle="tab">Step 3</a></li>
                            </ul>
                            <div class="tab-content mt-20" id="myTabContent">
                              <div class="tab-pane fade active in" id="step1">
                               
                                <?= Form::open(array('url' => route('dashboard.leaves.store') ,'class' => 'form-horizontal ajaxformsubmit','id'=>'firstajaxform','name'=>'firstajaxform')) ?>
                                 

                                  <div class="form-group">
                                    <label class="col-lg-3 control-label" for="leaveselecttype">Select Leave type</label>
                                    <div class="col-lg-9">
                                      <select class="form-control" id="leave_type" name="leave_type">
                                         <!-- <option value="">Select Type</option> -->
                                        <option value="Casual Leave">Casual Leave</option>
                                        <option value="Sick Leave">Sick/Emergency Leave</option>
                                        <option value="Privilege Leave">Privilege Leave</option>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="form-group for-hide">
                                     <label class="col-lg-3 control-label msg-label" for="leaveselecttype" style="display:block">Note</label>
                                    <div class="col-lg-9">
                                      <div class="for-msg" style="display:block">
                                      To get your Casual/Privilege leaves approved quickly, consider adding it before 5 days. Otherwise it may be subject to approval.
                                      </div>
                                    </div>
                                  </div>
                                  <div class="form-group" id="datesingle">
                                    <label class="col-lg-3 control-label" for="leaveselectdate">Select Date</label>
                                    <div class="col-lg-9">
                                      <?= Form::text('date',null,['class'=>'form-control pull-right selectdate','id'=>'datepicker','data-date-format'=>'yyyy-mm-dd','onkeydown'=>'return false','placeholder'=>'select date','autocomplete'=>'off'])?>
                                       <p class="help-block" style="color:red" id="date_error" ></p> 
                                       
                                    </div>
                                  </div>
                                  <input type="hidden" name="saveleave" id="save_leave" value="">
                                  <div class="form-group @if ($errors->has('leave_purpose')) has-error @endif">
                                    <label class="col-lg-3 control-label" for="leave-purpose">Leave-purpose</label>
                                    <div class="col-lg-9">
                                      <textarea class="form-control" type="text" value="" placeholder="leave-purpose" name="leave_purpose" rows="4"></textarea>
                                      <p class="help-block" style="color:red" id="error_f_leavepurpose" ></p> 
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <div class="col-lg-10 col-lg-offset-3">
                                      <button type="submit" class="btn btn-primary">Next</button>
                                    
                                      <!-- <a href="#step2" class="btn btn-primary" data-toggle="tab" id="firstpage">Next</a> -->
                                    </div>
                                  </div>
                                 {!! Form::close() !!}
                              </div>
                              <div class="tab-pane fade" id="step2">
                                <div class="bs-component">
                                  <div class="panel panel-primary" >
                                    <div class="panel-heading leave-app">
                                    <h3 class="panel-title modal-heading collapsed panellink-title" data-toggle="collapse" data-parent="#accordion" href="#collapse1" >Leave Request</h3>
                                      <!-- <button class="close" type="button">&times;</button> -->
                                    </div>
                                    <div class="panel-body collapse in custom_div" aria-expanded="true" id="collapse1">
                                    <div class="firststatus" style="color: #fff;min-height: 40px;padding: 0 10px;line-height: 40px;border-radius: 5px;margin: 20px 0;font-size: 15px;background-color: #009688;">                               
                                    </div>
                                    <?= Form::open(array('url' => route('dashboard.leaves.second.store') ,'class' => 'form-horizontal secondajaxform','id'=>'secondajaxform','name'=>'secondajaxform')) ?>
                                    <div id="error_msg" style="color: red;"></div>
                                    <div id="append_modal">
                                    </div>
                                    </div>
                                     </div>
                                  <div class="row">
                                    <div class="col-md-6">
                                      <a href="#step1" class="btn btn-primary" data-toggle="tab" id="tofirst">Previous</a>
                                    </div>
                                    <div class="col-md-6 text-right">
                                       {!! Form::submit('Save & Next',['class'=>'btn btn-primary'])!!}
                                       <!-- <a href="#step3" data-toggle="tab" class="btn btn-primary" id="tothird"></a> -->
                                    </div>

                                  </div>
                                  {!! Form::close() !!}
                                </div>
                              </div>
                           
                              <div class="tab-pane fade" id="step3">
                                <div class="bs-component">
                                  <h3 class="modal-heading" id="message"></h3>
                                  <div class="panel panel-primary" id="step3data">
                                    <div class="panel-heading">
                                      <h3 class="panel-title" id="date-panel-second"></h3>
                                    </div>
                                    <div class="panel-body"  >
                                      <div id="formwholeday">Whole Day</div>
                                      <div id="hours_data"></div>
                                      <div id="formpurpose">Purpose : </div>
                                      <div id="formleave_type">Casual leave</div>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-6">
                                      <a href="#step2" class="btn btn-primary" data-toggle="tab" id="tosecond">Previous</a>
                                    </div>
                                    <div class="col-md-6 text-right">
                                      <button class="btn btn-primary" id="submitleave" onclick="confirmLeave()">Confirm</button>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="card-leavebox row">
                      <div class="leavesubsection button-section col-xs-5 pl-0"><a class="fontsize" href="javascript:;">
                            Casual</a></div>
                      <div class="leavesubsection leave-used col-xs-3 text-center"><strong class="text-muted">USED</strong><br><span>{{ $casual_leaves}}</span></div>
                      <div class="leavesubsection avail col-xs-3 text-center"><strong class="text-muted">AVAILABLE</strong><br><span><?= 6-$casual_leaves ?></span></div>
                    </div>
                    <div class="card-leavebox row">
                      <div class="leavesubsection button-section col-xs-5 pl-0"><a class="fontsize" href="javascript:;"> Sick</a></div>
                      <div class="leavesubsection leave-used col-xs-3 text-center"><strong class="text-muted">USED</strong><br><span>{{ $sick_leaves}}</span></div>
                      <div class="leavesubsection avail col-xs-3 text-center"><strong class="text-muted">AVAILABLE</strong><br><span><?= 6-$sick_leaves ?></span></div>
                    </div>
                    <div class="card-leavebox row">
                      <div class="leavesubsection button-section col-xs-5 pl-0"><a class="fontsize" href="javascript:;">
                            Privilege</a></div>
                      <div class="leavesubsection leave-used col-xs-3 text-center"><strong class="text-muted">USED</strong><br><span>{{ $priviledge_leaves}}</span></div>
                      <div class="leavesubsection avail col-xs-3 text-center"><strong class="text-muted">AVAILABLE</strong><br><span><?= 6-$priviledge_leaves ?></span></div>
                    </div>
                </div>
            </div>
        </div>
      @endif    
      @if(!auth()->user()->is_admin)
      <div class="col-md-6" >
        <div class="card div-height" style="height: 278px;">
          <h3 class="card-title">Pending Leaves</h3>
           <div class="list-group">
            @if(!empty($pending_leave))
              @foreach($pending_leave as $key => $leaves)
              <h4 class="list-group-item-heading date_leavetype"> <?= $leaves['date']?> (<?= $leaves['leave_type']?>)</h4>
              <p class="list-group-item-text reason_leave"><?= $leaves['leave_purpose'];?></p>
              <p class="list-group-item-text haft-full-day"><strong><?= $leaves['leave_time']?></strong></p>
              @endforeach
            @elseif(empty($pending_leave))
            <h4>Bravo! You're working Hard :-)<i class="lar la-thumbs-up"></i></h4>
            @endif
          </div>
        </div>
      </div>
      @endif
    </div>
    @if(auth()->user()->is_admin)
    <div class="row mt-20">
        <div class="col-md-12">
            <div class="card" style="height: 520px;" id="firstlevel">
                <div class="card-title-w-btn">
                    <h3 class="title" id="graph_title">Annual Leaves Of Employees</h3>
                    <p><button id="back" type="button" class="btn btn-primary" onclick="back()" style="display: inline-block;">Back</button></p>
                </div>   
                <div class="embed-responsive embed-responsive-16by9">
                    <canvas class="embed-responsive-item" id="barChartDemo1"></canvas>
                </div>
            </div>
            <div class="card" style="height: 520px;" id="secondlevel">
                <div class="card-title-w-btn">
                    <h3 class="title" id="second_graph_title">Leaves</h3>
                    <p><button id="back" type="button" class="btn btn-primary" onclick="back()" style="display: inline-block;">Back Home</button></p>
                </div>   
                <div class="embed-responsive embed-responsive-16by9">
                    <canvas class="embed-responsive-item" id="barChartDemo2"></canvas>
                </div>
            </div>
            <div class="card" style="height: 520px;" id="thirdlevel">
                <div class="card-title-w-btn">
                    <h3 class="title" id="third_graph_title">Leaves</h3>
                    <p><button id="back1" type="button" class="btn btn-primary" onclick="back1()" style="display: inline-block;position: relative;z-index: 1">Back</button><button id="back" type="button" class="btn btn-primary" onclick="back()" style="display: inline-block;position: relative;z-index: 1;margin: 0px 7px;">Back Home</button></p>
                </div>   
                <div class="embed-responsive embed-responsive-16by9" style="position: absolute;top: 0;left: 0;right: 0;bottom: 0;display: block;">
                    <canvas class="embed-responsive-item" id="barChartDemo3" style="width: 949px; height: 520px;"></canvas>
                </div>
            </div>
        </div>
    </div>
    @endif
     <!--logs graph  -->
    @if(auth()->user()->is_admin)
    <div class="row mt-20">
        <div class="col-md-12">
            <div class="card" style="height: 520px;" id="firstlevel_logs">
                <div class="card-title-w-btn">
                    <h3 class="title" id="graph_title_logs">Missing Logs</h3>
                    <p><button id="backlog1" type="button" class="btn btn-primary" onclick="backLogs()" style="display: inline-block;">Back</button></p>
                </div>   
                <div class="embed-responsive embed-responsive-16by9">
                    <canvas class="embed-responsive-item" id="barChartDemo_logs1"></canvas>
                </div>
            </div>
            <div class="card" style="height: 520px;" id="secondlevel_logs">
                <div class="card-title-w-btn">
                    <h3 class="title" id="second_graph_title_logs">Logs</h3>
                    <p><button id="backlog" type="button" class="btn btn-primary" onclick="backLogs()" style="display: inline-block;">Back Home</button></p>
                </div>   
                <div class="embed-responsive embed-responsive-16by9">
                    <canvas class="embed-responsive-item" id="barChartDemo_logs2"></canvas>
                </div>
            </div>
        </div>
    </div>
    @endif
    <!--end logs graph  -->
    <div class="row mt-20">
        @if(!empty($birthday_members))
        <div class="col-md-6">
            <div class="card div-height">
              <h3 class="card-title">Upcoming Birthday</h3>
              <div class="product-list">
                @foreach($birthday_members as $key => $birthday)
                <div class="product">
                  @if(isset($birthday['profile_pic']))
                  <img class="img-circle" src="<?='/upload/'.$birthday['profile_pic'];  ?>" height="50" width="50">
                  @else
                    @if($birthday['gender'] == 'male')
                    <img class="img-circle" src="/images/user_male.png" height="50" width="50">
                    @else
                    <img class="img-circle" src="/images/user_female.png" height="50" width="50">
                    @endif
                  @endif
                  <div class="item-desc">
                    <h4> <a href="#"><?= ucwords($birthday['fullname'])?></a></h4>
                    <p><?= $birthday['birthdate'];?>
                    <?php
                     $day = $birthday['birthdate'];
                     $day = explode("-", $day);
                     $day[2] = date("Y");
                     $birth_day = implode("-", $day);
                     ?>
                    (<?= date('l',strtotime( $birth_day));?>)</p>
                  </div>
                </div>
                @endforeach
                
              </div>
            </div>
        </div>
      @endif
      @if(!empty($holiday))
        <div class="col-md-6">
          <div class="card div-height">
            <h3 class="card-title">Upcoming Holiday</h3>
            <div class="card-body">
              <div class="card-holidaysection">
                <ul class="list-unstyled">
                  @foreach($holiday as $key => $future_holiday)
                  <li><strong><?= ucwords($future_holiday['title']);?></strong><span class="date ml-10"> <?= $future_holiday['holiday_date'];?></span><span class="date ml-10">(<?= date('l',strtotime($future_holiday['holiday_date']));?>)</span></li><br/>
                  @endforeach
                </ul>
              </div>
            </div>
          </div>
        </div>
    @endif
  </div> 

<div class="row mt-20">
  @if(auth()->user()->is_admin)
    <div class="col-md-6" >
      <div class="card div-height" style="height: auto;">
      <div style="display: flex;">
      <h3 class="card-title">Pending Leaves</h3>
      <a href="<?= URL::route('leaves.index') ?>"><button type="button" class="btn btn-primary col-sm-offset-10 text-left"  style="display: inline-block; margin-left: 180px;">Leaves</button></a>
    </div>
        <div class="card-body">
              <div class="card-holidaysection">
          <ul class="list-unstyled">
            @foreach($all_pending_leaves as $key => $all_leaves)
            <li class="for_tool"><strong><?= $all_leaves['fullname'];?> : </strong>
               <span class="for_tool_text"><?= $all_leaves['leave_purpose'];?></span>
              <span class="date ml-10"> <?= date('d-m-Y',strtotime($all_leaves['date']));?></span>&nbsp&nbsp <span>(<?= date('l', strtotime($all_leaves['date']));?>) </span></li>
            @endforeach
          </ul>
        </div>
      </div>
      </div>
    </div>
  @endif

  @if(auth()->user()->is_admin)
    <div class="col-md-6" >
      <div class="card div-height" style="height: auto;">
      <div style="display: flex;">
      <h3 class="card-title">Today Leaves</h3>
      </div>
        <div class="card-body">
              <div class="card-holidaysection">
          <ul class="list-unstyled">
            @foreach($today_leaves as $key => $value)
            <li><?= $value['name'];?>  @if($value['day'] == "half day") <span>(<?=  $value['day'];?>)</span>@endif</li>
            @endforeach
          </ul>
        </div>
      </div>
      </div>
    </div>
  @endif
  <div class="assets_box">
  @if(auth()->user()->is_admin)
    <div class="col-md-6" >
      <div class="card div-height" style="height: auto;">
      <div style="display: flex;">
      <h3 class="card-title">Today Service Request</h3>
      <a href="<?= URL::route('show_report_assets') ?>"><button type="button" class="btn btn-primary col-sm-offset-10 text-left"  style="display: inline-block; margin-left: 180px;">Service</button></a>
      </div>
        <div class="card-body">
              <div class="card-holidaysection">
          <ul class="list-unstyled">
            @foreach($assets_service as $key => $value)
            <li>
              <span><b><?= $value['name'];?>:</b></span>&nbsp&nbsp&nbsp&nbsp
              <span><?= $value['device_type'];?></span>&nbsp&nbsp&nbsp&nbsp
              <span><?= $value['notes'];?></span>&nbsp&nbsp&nbsp&nbsp
            </li>
            @endforeach
          </ul>
        </div>
      </div>
      </div>
    </div>
  @endif
  @if(auth()->user()->is_admin)
    <div class="col-md-6" >
      <div class="card div-height" style="height: auto;">
      <div style="display: flex;">
      <h3 class="card-title">Outward Assets</h3>
      <a href="<?= URL::route('assets.index') ?>"><button type="button" class="btn btn-primary col-sm-offset-10 text-left"  style="display: inline-block; margin-left: 180px;">Assets</button></a>
      </div>
        <div class="card-body">
              <div class="card-holidaysection">
          <ul class="list-unstyled">
            @foreach($assets_master as $key => $value)
            <li>
              <span><b><?= $value['asset_history']['serial_number'];?>:</b></span>&nbsp&nbsp&nbsp&nbsp
              <span><?= $value['asset_user']['fullname'];?></span>&nbsp&nbsp&nbsp&nbsp
             <span><?= $value['status'];?></span>&nbsp&nbsp&nbsp&nbsp 
             <span><?=  substr($value['asset_log'][0]['created_at'],0,10);?></span>&nbsp&nbsp&nbsp&nbsp 
            </li>
            @endforeach
          </ul>
        </div>
      </div>
      </div>
    </div>
  @endif
</div>
</div>
    <div class="row mt-20"> 
      @if(!auth()->user()->is_admin)
        <!--<div class="col-md-4">-->
        <!--  <div class="card">-->
        <!--    <h3 class="card-title"><?php echo date('d-m-Y'); ?> Attendance</h3>-->
        <!--    <div class="product-list">-->
        <!--      <div class="form-group">-->
        <!--      @if($checkin_empty)-->
        <!--         <input type="button" name="" class="btn btn-success  btn btn-block checkintime" value="Checkout">-->
        <!--      @else-->
        <!--        <input type="button" name="" class="btn btn-success  btn btn-block checkintime" value="Checkin">-->
        <!--      @endif-->
        <!--    </div>-->
        <!--      <div class="form-group row">-->
        <!--       <label class="control-label col-md-3">CheckIn Time:</label>-->
        <!--       <div class="col-lg-9">-->
        <!--         <input type="text" class="form-control  " name="checkin" id="checkin_data" disabled="" value="@if(!empty($checkin_time['check_in'])){{ $checkin_time['check_in'] }}@endif">-->
        <!--       </div>-->
        <!--     </div>-->
        <!--     <div class="form-group row">-->
        <!--       <label class="control-label col-md-3">Chekout Time:</label>-->
        <!--       <div class="col-lg-9">-->
        <!--          <input type="text" name="checkin" class="form-control " id="checkout_data" disabled=""  value="@if(!empty($checkin_time['check_out'])){{ $checkin_time['check_out'] }}@endif">-->
        <!--        </div>-->
        <!--     </div>-->
        <!--    <div class="form-group row">-->
        <!--       <label class="control-label col-md-3">Total Hours:</label>-->
        <!--       <div class="col-lg-9">-->
        <!--          <input type="text" class="form-control " id="total_hours" disabled=""  value="@if(!empty($time_differance[0]['time_difference'])) {{ $time_differance[0]['time_difference']}} @endif">-->
        <!--        </div>-->
        <!--     </div>     -->
        <!--    <div class="form-group row">-->
        <!--       <label class="control-label col-md-3">Total Checkin</label>-->
        <!--       <div class="col-lg-9">-->
        <!--          <input type="text" class="form-control " id="total_time_checkin" disabled=""  value="{{$count_log_row}}">-->
        <!--        </div>-->
        <!--     </div>      -->
        <!--    </div>-->
        <!--  </div>-->
        <!--</div>    -->
      @endif  
      
<!--       @if(!auth()->user()->is_admin)
        <div class="col-md-4">
          <div class="card">
            <h3 class="card-title"><?php echo date("F",strtotime(date('Y-m-d')));echo "-"; echo substr(date('Y'),-2)  ?> Working Hours</h3>
            <div class="product-list">
                <div style="margin: 0;display:inline-block;font-size: 20px">Attended Hours:</div>
               <div style="margin: 0 0 0 10px;display:inline-block;font-size: 20px;font-weight: bold;">@if(empty($exp_work_hours[0])) 0 @else {{$exp_work_hours[0]}} /{{$count_hours}}@endif<small style="color: #333;font-weight: normal;">{{$exp_total_hours[0]}}</small></div>     
            </div>
          </div>
        </div>
      @endif -->
    </div>  
@stop
@section('style')
    <?= Html::style('asset/css/bootstrap-timepicker.min.css') ?>
    <?= Html::style('asset/css/new_dashboard.css') ?>
    <?= Html::style('asset/css/bootstrap-datepicker.css') ?>
    <?= Html::style('asset/css/teal.css') ?>
    <style type="text/css">
        #radioBtn .notActive{
            color: #3276b1;
            background-color: #fff;
        }
        
    </style>
@stop
@section('script') 
    <?= Html::script('asset/js/bootstrap-timepicker.min.js') ?>  
    <?= Html::script('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js') ?>
    <?= Html::script('asset/js/bootstrap-datepicker.min.js') ?> 
    <?= Html::script('asset/js/jquery.validate.min.js') ?>
    <?= Html::script('js/Chart.bundle.min.js') ?>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datejs/1.0/date.min.js"></script>      
    <script type="text/javascript">
      @if(!auth()->user()->is_admin)
        //Graph
        var month = [<?= $month ?>];
        var count_logs = [<?= $count_logs?>];
        var count_leaves = [<?= $count_leaves?>];
        var count_disapprove_leaves = [<?= $count_disapprove_leaves?>];
        var barChartData = {
            labels: month,
            datasets: [
            {
                label: 'Unpaid Leaves',
                backgroundColor: "<?= config('project.error_code.code')?>",
                data: count_disapprove_leaves
            },
            {
                label: 'Paid Leaves',
                backgroundColor: "#009688",
                data: count_leaves
                }
            ]
        }

        var context = $("#barChartDemo_1").get(0).getContext("2d");
        var myBar = new Chart(context, {
            type: 'bar',

            data: barChartData,
            options: {
                title: {
                    display: true,
                    text: ""
                },
                tooltips: {
                    mode: 'label'
                },
                scales: {
                    yAxes: [{
                            display: true,
                            ticks: {
                                beginAtZero: true,
                                steps: 1,
                                stepSize:1,
                                labelString: 'text'
                            }
                        }]
                },
                responsive: true,
            }
        });
      @endif
        //end leaves graph
        //log graph
        @if(!auth()->user()->is_admin)
        //Graph
        var month = [<?= $month ?>];
        var count_logs = [<?= $count_logs?>];
        var count_leaves = [<?= $count_leaves?>];
        var barChartData = {
            labels: month,
            datasets: [
            {
                label: 'Missing Logs',
                backgroundColor: "<?= config('project.error_code.code')?>",
                data: count_logs
            }
            // {
            //     label: 'Leaves',
            //     backgroundColor: "#009688",
            //     data: count_leaves
            //     }
            ]
        }

        var context = $("#barChartDemo").get(0).getContext("2d");
        var myBar = new Chart(context, {
            type: 'bar',

            data: barChartData,
            options: {
                title: {
                    display: true,
                    text: ""
                },
                tooltips: {
                    mode: 'label'
                },
                scales: {
                    yAxes: [{
                            display: true,
                            ticks: {
                                beginAtZero: true,
                                steps: 1,
                                stepSize:1,
                                labelString: 'text'
                            }
                        }]
                },
                responsive: true,
            }
        });
        @endif
      //end log graph
        $('#secondlevel').hide();
        $('#thirdlevel').hide();
        @if(auth()->user()->is_admin)
        var leaves_count_logs = [<?= $leaves_count_logs ?>];
        var username = [<?= $username?>];
        var fullnames = <?= json_encode($fullname)?>;
        $('#back').hide();
        $('#back1').hide();
        
        function back(){
            location.reload();
            $('#back').hide();
            $('#graph_title').text('Leaves');
        }
        //chart1
        var barChartData1 = {
            labels: username,
            datasets: [{
                label: 'Leaves',
                backgroundColor: "<?= config('project.error_code.code')?>",
                data: leaves_count_logs,
                
            }
            ]
        }
        var context = $("#barChartDemo1").get(0).getContext("2d");

        var myBar = new Chart(context, {
            type: 'bar',

            data: barChartData1,
            options: {
                title: {
                    display: true,
                    text: ""
                },
                tooltips: {
                    enabled:true,
                    mode: 'label',
                    callbacks: {
                    title: function(fullname, data) {
                        var index = fullname[0].index;
                        return fullnames[index];
                    },
                }

                },
                scales: {
                    yAxes: [{
                            display: true,
                            ticks: {
                                beginAtZero: true,
                                steps: 1,
                                stepSize:5,
                                labelString: 'text',
                            },   
                        }],
                        xAxes: [{
                            ticks: {
                                callback: function(value) {
                                    return value.substr(0, 2);//truncate
                                },
                            }
                        }],
                },
                responsive: true,
            }
        });

        //chart2
        var canvas = document.getElementById("barChartDemo1");
        var chartdepth = 0;
        var second_myBar;
        var barChartData2;
        canvas.onclick = function(evt) {
            var activePoints = myBar.getElementsAtEvent(evt);
            
            if(activePoints.length != 0){
                $('#back').show();
                var chartData = activePoints[0]['_chart'].config.data;
                var idx = activePoints[0]['_index'];
                var label = chartData.labels[idx];
                var number = label.match(/\d+/);
                if(number.index != 0){  
                    $('#secondlevel').show();
                    $('#firstlevel').hide();
                    sessionStorage.setItem("userid",number[0]);
                    $.ajax({
                        type: "POST",
                        url: '<?= URL::route('dashboard.leaves.chart') ?>',
                        data: { "_token" :'<?=csrf_token()?>','id': number[0]},

                        success: function(data) {
                            var count_leaves = [data.count_leaves];
                            var year = [data.year];
                            barChartData2 = {
                                labels: '',
                                datasets: [{
                                    label: 'Leaves',
                                    backgroundColor: "<?= config('project.error_code.code')?>",
                                    data: '',
                                    
                                }
                                ]
                            }

                            barChartData2.datasets[0].data = count_leaves[0];
                            barChartData2.labels = year[0];
                            var context1 = $("#barChartDemo2").get(0).getContext("2d");

                            second_myBar = new Chart(context1, {
                                type: 'bar',

                                data: barChartData2,
                                options: {
                                    title: {
                                        display: true,
                                        text: ""
                                    },
                                    tooltips: {
                                        enabled:true,
                                        mode: 'label',

                                    },
                                    scales: {
                                        yAxes: [{
                                                display: true,
                                                ticks: {
                                                    beginAtZero: true,
                                                    steps: 1,
                                                    stepSize:1,
                                                    labelString: 'text',

                                                },   
                                            }],
                                            xAxes: [{
                                                ticks: {
                                                    callback: function(value) {
                                                        return value;//truncate
                                                    },
                                                }
                                            }],
                                    },
                                    responsive: true,
                                }
                            });
                            if(data.fullname != null){
                                $('#second_graph_title').text('Leaves ' + data.fullname);
                            }
                        }
                    });
                }
            }
        }

        function back1(){
          $('#thirdlevel').hide();
          $('#firstlevel').hide();
          $('#secondlevel').show();
        }

        //chart 3
        var canvas1 = document.getElementById("barChartDemo2");
        var chartdepth = 0;
        canvas1.onclick = function(evt) {

            var activePoints = second_myBar.getElementsAtEvent(evt);
            // var year = evt;
            if(activePoints.length != 0){
              var year = activePoints[0]['_model']['label'];
            // }
            // sessionStorage.setItem("current_year",year);
            // year = sessionStorage.getItem("current_year");
            // console.log(activePoints);
                $('#back1').show();
                $('#back').show();
                $('#thirdlevel').show();
                $('#firstlevel').hide();
                $('#secondlevel').hide();
                // var year = activePoints[0]['_model']['label'];
                var user_id = sessionStorage.getItem("userid");
                $.ajax({
                    type: "POST",
                    url: '<?= URL::route('dashboard.leaves.secondchart') ?>',
                    data: { "_token" :'<?=csrf_token()?>','id': user_id,'year':year},

                    success: function(data) {
                      var count_leaves = [data.count_leaves];
                      var month = [data.month];
                      var fullname = data.fullname;
                      var barChartData3 = {
                          labels: '',
                          datasets: [{
                              label: 'Leaves',
                              backgroundColor: "<?= config('project.error_code.code')?>",
                              data: '',
                              
                          }
                          ]
                      }

                      barChartData3.datasets[0].data = count_leaves[0];
                      barChartData3.labels = month[0];
                      var context2 = $("#barChartDemo3").get(0).getContext("2d");

                      third_myBar = new Chart(context2, {
                          type: 'bar',

                          data: barChartData3,
                          options: {
                              title: {
                                  display: true,
                                  text: ""
                              },
                              tooltips: {
                                  enabled:true,
                                  mode: 'label',

                              },
                              scales: {
                                  yAxes: [{
                                          display: true,
                                          ticks: {
                                              beginAtZero: true,
                                              steps: 1,
                                              stepSize:1,
                                              labelString: 'text',
                                          },   
                                      }],
                                      xAxes: [{
                                          ticks: {
                                              callback: function(value) {
                                                  return value;//truncate
                                              },
                                          }
                                      }],
                              },
                              responsive: true,
                          }
                      });

                      //third_myBar.destroy();
                        $('#third_graph_title').text('Leaves ' + fullname +'-'+ year);
                    }
                });
            }
        }
        @endif

                //logs graph admin
        $('#secondlevel_logs').hide();
        @if(auth()->user()->is_admin)
        var users_count_logs = [<?= $users_count_logs ?>];
        var usernameLogs = [<?= $usernameLogs?>];
        var fullname_log = <?= json_encode($fullname_logs)?>;
        $('#backlog').hide();
        $('#backlog1').hide();
        
        function backLogs(){
            location.reload();
            $('#backlog').hide();
            $('#graph_title_logs').text('Logs');
        }
        //chart1

        var barChartDataLogs1 = {
            labels: usernameLogs,
            datasets: [{
                label: 'Logs',
                backgroundColor: "<?= config('project.error_code.code')?>",
                data: users_count_logs, 
            }
            ]
        }
        var contextlogs = $("#barChartDemo_logs1").get(0).getContext("2d");

        var myBarLogs = new Chart(contextlogs, {
            type: 'bar',

            data: barChartDataLogs1,
            options: {
                title: {
                    display: true,
                    text: ""
                },
                tooltips: {
                    enabled:true,
                    mode: 'label',
                    callbacks: {
                    title: function(fullname_logs, data) {

                        var index = fullname_logs[0].index;
                        return fullname_log[index];
                    },
                }

                },
                scales: {
                    yAxes: [{
                            display: true,
                            ticks: {
                                beginAtZero: true,
                                steps: 1,
                                stepSize:5,
                                labelString: 'text',
                            },   
                        }],
                        xAxes: [{
                            ticks: {
                                callback: function(value) {
                                    return value.substr(0, 2);//truncate
                                },
                            }
                        }],
                },
                responsive: true,
            }
        });

        //chart2

        var canvaslog = document.getElementById("barChartDemo_logs1");
        var chartdepth = 0;
        var second_myBar_logs;
        var barChartDataLogs2;
        canvaslog.onclick = function(evt) {
          var activePoints = myBarLogs.getElementsAtEvent(evt);
          if(activePoints.length != 0){
              $('#backlog').show();
              var chartData = activePoints[0]['_chart'].config.data;
              var idx = activePoints[0]['_index'];
              var label = chartData.labels[idx];
              var number = label.match(/\d+/);
              if(number.index != 0){  
                  $('#secondlevel_logs').show();
                  $('#firstlevel_logs').hide();
                  sessionStorage.setItem("userid",number[0]);

                  $.ajax({
                      type: "POST",
                      url: '<?= URL::route('dashboard.logs.chart') ?>',
                      data: { "_token" :'<?=csrf_token()?>','id': number[0]},

                      success: function(data) {
                          var count_leaves = [data.count_leaves];
                          var month_main = [data.month_main];
                          barChartDataLogs2 = {
                              labels: '',
                              datasets: [{
                                  label: 'Logs',
                                  backgroundColor: "<?= config('project.error_code.code')?>",
                                  data: '',
                                  
                              }
                              ]
                          }

                          barChartDataLogs2.datasets[0].data = count_leaves[0];
                          barChartDataLogs2.labels = month_main[0];
                          var contextlog1 = $("#barChartDemo_logs2").get(0).getContext("2d");

                          second_myBar_logs = new Chart(contextlog1, {
                              type: 'bar',

                              data: barChartDataLogs2,
                              options: {
                                  title: {
                                      display: true,
                                      text: ""
                                  },
                                  tooltips: {
                                      enabled:true,
                                      mode: 'label',

                                  },
                                  scales: {
                                      yAxes: [{
                                              display: true,
                                              ticks: {
                                                  beginAtZero: true,
                                                  steps: 1,
                                                  stepSize:1,
                                                  labelString: 'text',
                                              },   
                                          }],
                                          xAxes: [{
                                              ticks: {
                                                  callback: function(value) {
                                                      return value;//truncate
                                                  },
                                              }
                                          }],
                                  },
                                  responsive: true,
                              }
                          });
                          if(data.fullname != null){
                              $('#second_graph_title_logs').text('Logs ' + data.fullname);
                          }
                      }
                  });
              }
          }
      }
        function backlog1(){
          $('#firstlevel_logs').hide();
          $('#secondlevel_logs').show();
        }
      @endif
      //end logs graph admin

      //leaves status
       $(document).ready(function(){

          $(".firststatus").hide();
        })
        var casual_leaves = <?= $casual_leaves ?>;
        var sick_leaves = <?= $sick_leaves ?>;
        var priviledge_leaves = <?= $priviledge_leaves ?>;

        d = new Date();
        d.setDate(d.getDate()+5);
        var date = convert(d);

        function convert(str) {
            var date = new Date(str),
            mnth = ("0" + (date.getMonth() + 1)).slice(-2),
            day = ("0" + date.getDate()).slice(-2);
            return [date.getFullYear(),mnth,day].join("-");
        }
        
        var leaves;

        leaves = $("#leave_type").val();
   
        var selectdate;
        var leave_type;
        var dates;
        var selected_date;
        $(".selectdate").on('change', function(){
            dates = $("input[name=date]").val().split(',');
          // console.log(dates)
            selected_date = dates[dates.length - 1];

            selectdate = $("input[name=date]").val().split(",");
            leave_type = $('#leave_type').val();

            var dates = {};
            $.each(selectdate,function(i,value){
              leave_type = $('#leave_type').val();
              dates[value] =leave_type;
              // console.log(dates[value]);
            });
            var result = selectDate(dates);
            //  $(".firststatus").append("<h3>"+result+"</h3>");


        });
        $("#leave_type").on('change', function(){
             // $(".firststatus").show();
            $('.selectdate').val('');
            $('#datepicker').datepicker('setStartDate', selectedDate);
            dates = $("input[name=date]").val().split(',');
            selected_date = dates[dates.length - 1];
            selectdate = $("input[name=date]").val().split(",");
            leave_type = $('#leave_type').val();
            if (leave_type == "Sick Leave") {
              $('.for-hide').css("display","none");
            }else{
              $('.for-hide').css("display","block");
              $('.for-msg').text('');
              $('.for-msg').text("To get your Casual/Privilege leaves approved quickly, consider adding it before 5 days. Otherwise it may be subject to approval.");
            }
            var dates = {};
            $.each(selectdate,function(i,value){
              leave_type = $('#leave_type').val();
                dates[value] =leave_type;
            });
            var result = selectDate(dates);
            //$(".second_status").hide();
            var selectedDate = new Date();
            if (leave_type === "Sick Leave") {
              selectedDate = new Date();
              $('#datepicker').datepicker('setStartDate', selectedDate);
            }else{
              selectedDate = new Date(selectedDate.setDate(selectedDate.getDate() + 5));
              $('#datepicker').datepicker('setStartDate', selectedDate);
            } 
        });
        selectedDate = new Date();
        $('#datepicker').datepicker({
          
          beforeShowDay: function(date) {
            var day = date.getDay();
              if (day == 0) {
                  return false;
              } else {
                  return true;
              }
            //   var day = date.getDay();
            // var week = 0 | date.getDate() / 7;
            // if (week == 1 || week == 3) {
            //     if (day == 6)  { //check for satruday
            //         return false;
            //     }
            // }
            // if (day == 0)
            // {
            //     return false;
            // }
            },
            multidate:true,
            startDate:  new Date(selectedDate.setDate(selectedDate.getDate() + 5)),
            format: 'dd-mm-yyyy'
        });
        
        function selectDate(dates)
        {
          $(".firststatus").empty();
          $(".firststatus").show();
          var seleted_date_arr = {};
          var arr_count = Object.entries(dates);
          var paid = 0;
          $.each(dates,function(value,key){
            var value = value.split("-").reverse().join("-");
          if(key == "Casual Leave")
          {
              var casual_available = 6 - casual_leaves;

              var arr_count1 = arr_count.length;
              if(arr_count1 > casual_available)
              {
                  if(value > date)
                  {
                    if(paid < casual_available)
                    {
                       seleted_date_arr[value] = "Paid";
                       paid++;
                    }else{
                      seleted_date_arr[value] = "UnPaid";
                    }
                  }else{
                    if(value != ""){

                       seleted_date_arr[value] = "UnPaid";
                    }
                  }
              
              }else{

                  if(value > date)
                  {
                     if(paid < casual_available){
                         seleted_date_arr[value] = "Paid";
                         paid++;
                      }else{
                        seleted_date_arr[value] = "UnPaid";
                      }
                  }else{
                      seleted_date_arr[value] = "UnPaid";
                  }
              }
            }else if(key == "Privilege Leave")
            {
              var priviledge_available = 6 - priviledge_leaves;
              var arr_count1 = arr_count.length;
              if(arr_count1 > priviledge_available)
              {
                  if(value > date)
                  {
                    if(paid < priviledge_available)
                    {
                       seleted_date_arr[value] = "Paid";
                       paid++;
                    }else{
                      seleted_date_arr[value] = "UnPaid";
                    }
                  }else{
                       seleted_date_arr[value] = "UnPaid";
                  }
              
              }else{

                  if(value > date)
                  {
                     if(paid < priviledge_available){
                         seleted_date_arr[value] = "Paid";
                         paid++;
                      }else{
                        seleted_date_arr[value] = "UnPaid";
                      }
                  }else{
                      seleted_date_arr[value] = "UnPaid";
                  }
              }
            }else if(key == "Sick Leave")
            {
              var sick_available = 6 - sick_leaves;

              var arr_count1 = arr_count.length;
              
              if(arr_count1 > sick_available)
              {
                  
                    if(paid < sick_available)
                    {
                       seleted_date_arr[value] = "Paid";
                       paid++;
                    }else{
                      seleted_date_arr[value] = "UnPaid";
                    }
              }else{
                     if(paid < sick_available){
                         seleted_date_arr[value] = "Paid";
                         paid++;
                      }else{
                        seleted_date_arr[value] = "UnPaid";
                      }
                  } 
            }
          });
          $.each(seleted_date_arr,function(i,value){
            var i = i.split("-").reverse().join("-");
           $('.firststatus').append(" "+i+" : "+value+"  , ");
          });
          // return seleted_date_arr;
        }

        
      //end leaves status
       
         var token = "<?= csrf_token()?>";
        $('#firstajaxform').on('submit', function(event) {
          var type_leave = $('#leave_type').val();
          var array_of_date = $('#datepicker').val().split(",");
          $.each(array_of_date,function(i){

            var new_date_valid = new Date();
            var new_month_valid = new_date_valid.getMonth()+1;

            if (type_leave == "Sick Leave") {

              var day = new_date_valid.getDate();

            }else{
              var  dateAfter = new Date(new_date_valid.getTime()+(4*24*60*60*1000)); 
              day = dateAfter.getDay();

            }
            var a = array_of_date[i].split("-") ;
            var leave_month = a[1];
            var leave_day = a[0];
              if (leave_day < day) {
                 $('#date_error').text('Please Select valid date');
                  $('.selectdate').val('');
                if (leave_type == "Sick Leave") {
                  selectedDate = new Date();
                  $('#datepicker').datepicker('setStartDate', selectedDate);
                }else{
                  selectedDate = new Date(selectedDate.setDate(selectedDate.getDate())+ 5);
                  $('#datepicker').datepicker('setStartDate', selectedDate);
                } 

              }else{
                $('#date_error').text('');
              }
            
          });
          var check_date = $('#date_error').text();
          if (check_date=="") {
            $('#date_error').text('');
            if ($('input[name=date]').val() == "") {
                $('#date_error').text('Please Select Date');
                return false;
            } else {
                $('#date_error').text('');

            }
            if ($('textarea[name=leave_purpose]').val() == "") {
                $('#error_f_leavepurpose').text("plese fill leave purpose field");
                return false;
            } else {
                $('#error_f_leavepurpose').text('');

            }
            event.preventDefault();
            function dateFormate(value)
            {
              var yr      = value.getFullYear();
              var month   = String(value.getMonth()+1).padStart(2,'0');
              var day     = String(value.getDate()).padStart(2,'0');
              return  yr + '-' + month + '-' + day;
            }
            function previousDate(value){
              var dd = String(value.getDate()-1).padStart(2, '0');
              var mm = String(value.getMonth() + 1).padStart(2, '0'); 
              var yyyy = value.getFullYear();
              return yyyy +"-"+ mm +"-"+ dd;
            }
            function nextDate(value){
              var dd = String(value.getDate()+1).padStart(2, '0');
              var mm = String(value.getMonth() + 1).padStart(2, '0'); 
              var yyyy = value.getFullYear();
              return yyyy +"-"+ mm +"-"+ dd;
            }
            function unique(list) {
                var result = [];
                $.each(list, function(i, e) {
                  if ($.inArray(e, result) == -1) result.push(e);
                });
                return result;
            }
            function weekOfMonth(m) {
              return m.week() - moment(m).startOf('month').week() + 1;
            }
            var weekday = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
            var get_date_array = $('#datepicker').val().split(',');
            var date_arr = [];
            var sand_arr = [];
            $.each(get_date_array,function(k,v){
              var final = moment(v, "DD.MM.YYYY").format("YYYY-MM-DD");
              var date_value = new Date(final);
              var date_format = dateFormate(date_value);
              date_arr.push(date_format);
              var get_day = date_value.getDay();
              var week = 0 | date_value.getDate() / 7;
              var weekdays = weekday[get_day];
              if (week == 1 || week == 3) {
                if (weekdays == "Friday") {
                  sand_arr.push(date_format);
                }
              }else{

                if (weekdays == "Monday") {
                  sand_arr.push(date_format);
                }
              }
              if (week == 0 || week == 2 || week == 4) {

                if (weekdays == "Saturday") {
                  sand_arr.push(date_format);
                }
              }else{

                if (weekdays == "Monday") {
                  sand_arr.push(date_format);
                }
              }
              
            });
            if (sand_arr.length == 3) {

              var sorted = sand_arr.slice() 
              .sort(function(a, b) {
                return new Date(a) - new Date(b);
              });
              var mindate = sorted.shift();
              var fin_date = sand_arr.splice( $.inArray(mindate, sand_arr), 1 );
            
            }if (sand_arr.length == 2) {
              var date1 = new Date(sand_arr[0]);
              var date2 = new Date(sand_arr[1]);
              date_arr.push(dateFormate(date1),dateFormate(date2));
              if (date1<date2) {
                var sunday_date =  previousDate(date2);
              }else{
                sunday_date = previousDate(date1);
              }
              var saturday_date =  previousDate(new Date(sunday_date));
              var day1 = date1.getDay();
              var day2 = date2.getDay();
              var week1 = weekOfMonth(moment(dateFormate(date1)));
              var week2 = weekOfMonth(moment(dateFormate(date2)));
              var weekday1 = weekday[day1];
              var weekday2 = weekday[day2];
              if (week1<week2) {
                var week = week1;
              }else{
                week = week2;
              }
              if (week == 2 || week == 4 ) {
                if(week1 != week2){
                  if (weekday1 == "Friday" && weekday2 == "Monday" || weekday1 == "Monday" && weekday2 == "Friday") {
                    date_arr.push(sunday_date);
                    date_arr.push(saturday_date);
                  }

                }
              }else{
                if (week == 1 || week == 3 || week == 5) {
                  if(week1 != week2){
                    if (weekday1 == "Saturday" && weekday2 == "Monday" || weekday1 == "Monday" && weekday2 == "Saturday") {
                      date_arr.push(sunday_date);
                    }
                  }
                }
              }
            }else{
                  // alert(get_date_array);
              $.each(get_date_array,function(k,v){
                final = moment(v, "DD.MM.YYYY").format("YYYY-MM-DD");
                date_value = new Date(final);
                date_format = dateFormate(date_value);
                date_arr.push(date_format);
                 var holiday = <?=  json_encode($holiday_array);?>;
                $.each(holiday,function(k,v){
                  // alert(v);
                  var holi_date = moment(v, "DD.MM.YYYY").format("YYYY-MM-DD");
                  var new_holi = new Date(holi_date);
                  var prev_date = $.datepicker.formatDate( "dd-mm-yy", new Date(previousDate(new_holi)))
                  var next_date = $.datepicker.formatDate( "dd-mm-yy", new Date(nextDate(new_holi)))
                  
                  if (holi_date == previousDate(date_value) || holi_date == nextDate(date_value)) {
                    if ($.inArray(prev_date, get_date_array) != -1 && $.inArray(next_date, get_date_array) != -1) {
                      date_arr.push(holi_date);
                    }
                  }
                });
              });
            }
            var final_arr = unique(date_arr).sort();
            // console.log(final_arr);
            var formData = {
                date: final_arr.join(","),
                leave_type: $('select[name=leave_type]').val(),
                leave_purpose: $('textarea[name=leave_purpose').val(),
                sick_leaves : sick_leaves,
                casual_leaves : casual_leaves,
                priviledge_leaves : priviledge_leaves,

            }
            $.ajax({
                type: "POST",
                url: $(this).attr('action'),
                data: {
                    formData: formData,
                    _token: token
                },
                cache: false,

                success: function(data) {

                    $('#append_modal').empty();
                    // console.log(data.html_content);
                    $('#append_modal').html(data.html_content);
                    $('#step3data').empty();
                    $.each(data.date_of_leave, function(k, v) {
                        $('#step3data').append(
                            "<div class='panel-heading'>" +
                            "<h3 class='panel-title' id='date-panel-second'>" + v + "</h3>" +
                            "</div>" +
                            "<div class='panel-body'>" +
                            "<div id='formwholeday'>Whole Day</div>" +
                            "<div id='hours_data'></div>" +
                            "<div id='formpurpose'>" + data.firststep_store.leave_purpose + "</div>" +
                            "<div id='formleave_type'>" + data.firststep_store.leave_type + "</div>"
                        );

                    });
                    $('#previoustab a').attr('aria-expanded', false);
                    $('#previoustab').attr('class', '');
                    $('#middletab a').attr('aria-expanded', true);
                    $('#middletab').addClass('active');
                    $('#step1').removeClass('active in');
                    $('#step2').addClass('active in');
                },
                error: function(errors) {
                    console.log('hi');
                }
            });
          }else{
            $('#date_error').text('Please Select valid date');
                return false;
          }
        });
          

        $('.secondajaxform').on('submit', function(event) {
              $('.time_error').empty();
              if ($(".custom_time").prop('checked') == true) {

                const convertTime12to24 = (time12h) => {
                  const [time, modifier] = time12h.split(' ');
                  let [hours, minutes] = time.split(':');
                  if (hours === '12') {
                    hours = '00';
                  }
                  if (modifier === 'PM') {
                    hours = parseInt(hours, 10) + 12;
                  }
                  return `${hours}:${minutes}`;
                }
                 var from =  convertTime12to24($('.timepicker2').val()).split(':');
                 var to =  convertTime12to24($('.timepicker1').val()).split(':');
                 var diff = to[0]-from[0];
                 
                  
                if (diff > 4) {
                  $('.time_error').append("Please Select only 4 hours Difference")
                  return false;
                }
              }
              
            event.preventDefault();
            $.ajax({

                type: "POST",
                url: '<?= URL::route('dashboard.leaves.second.store') ?>',
                data: $(this).serialize(),
                dataType: 'json',

                success: function(data) {



                    if ($.isEmptyObject(data.error)) {
                        $('#middletab a').attr('aria-expanded', false);
                        $('#middletab').attr('class', '');
                        $('#nexttab a').attr('aria-expanded', true);
                        $('#nexttab').addClass('active');
                        $('#step2').removeClass('active in');
                        $('#step3').addClass('active in');

                        $('#step3data').empty();
                        $.each(data.leave_detail, function(k, v) {

                              if (v.from_hour == "10:00 AM" && v.to_hour == "07:00 PM") {
                      
                                  $('#step3data').append(
                                      "<div class='panel-heading'>" +
                                      "<h3 class='panel-title' id='date-panel-second'>" + k + "</h3>" +
                                      "</div>" +
                                      "<div class='panel-body'>" +
                                      "<div id='formwholeday'>Full Day</div>" +
                                      "<div id='formpurpose'>" + v.leave_purpose + "</div>" +
                                      "<div id='formleave_type'>" + v.leave_type + "</div>"
                                  );

                              } else {
                          
                                  $('#step3data').append(
                                      "<div class='panel-heading'>" +
                                      "<h3 class='panel-title' id='date-panel-second'>" + k + "</h3>" +
                                      "</div>" +
                                      "<div class='panel-body'>" +
                                      "<div id='formwholeday'>Half Day</div>" +

                                      "<div id='hours_data'>" + v.from_hour + "-" + v.to_hour + "</div>" +

                                      "<div id='formpurpose'>" + v.leave_purpose + "</div>" +
                                      "<div id='formleave_type'>" + v.leave_type + "</div>"
                                  );

                              }
                        });
                    } else {
                        printErrorMsg(data.error);
                    }

                }

            });
           
        });

        function printErrorMsg(msg) {
            $('#error_msg').empty();
            $.each(msg, function(key, value) {
                $('#error_msg').append('<li>' + value + '</li>');
            });
        }
        $('#tofirst').on('click', function() {
            $('#previoustab').addClass('active');
            $('#middletab').attr('class', '');

        });
        $('#tosecond').on('click', function() {
            $('#middletab').addClass('active');
            $('#nexttab').attr('class', '');

        });
        $('#submitleave').on('click', function() {
            $('#submitleave').attr('disabled', 'disabled');
        });
    </script>
    <script type="text/javascript">
        function confirmLeave() {
            $.ajax({
                type: "POST",
                url: "<?=URL::route('dashboard.leaves.third.store')?>",
                data: {
                    _token: token
                },
                cache: false,

                success: function(data) {

                    // message
                    $('#message').addClass('modaldate');
                    $("#message").html("Your leave request sent successfully");
                    $('#tosecond').attr('disabled', 'disabled');
                    $('#submitleave').attr('disabled', 'disabled');

                    window.setTimeout(function() {
                        location.reload()
                    }, 3000)
                }
            });
        }

        $(document).ready(function() {
            $(document).on('click', '.timepicker1', function() {
                $(this).timepicker();
            });
            $(document).on('click', '.timepicker2', function() {
                $(this).timepicker();
            });

            // $(document).on('change', 'input:checkbox[class=fulldaycheckboxclass]:checked', function() {
            //     var hide = $(this).parents('.parentdate').children().closest('#hour').hide();
            //     $(this).attr('value', 'fullday');
            // });
            // $(document).on('change', 'input:checkbox[class=fulldaycheckboxclass]:unchecked', function() {
            //     var show = $(this).parents('.parentdate').children().closest('#hour').show();
            //     $(this).attr('value', 'halfday');
            // });
            $(document).on('change', 'input:radio[class=halfdaycheckboxclass]:checked', function() {
                $(this).parents('.inner_div').children().closest('#hour').show();
                $(this).attr('value', 'halfday');

            })
            $(document).on('change', 'input:radio[class=fulldaycheckboxclass]:checked', function() {
                $(this).parents('.inner_div').children().closest('#hour').hide();
                $(this).attr('value', 'fullday');
                $('input:radio[class=custom_time]:checked').prop('checked', false);
                $('input:radio[class=second_half]:checked').prop('checked', false);
                $('input:radio[class=first_half]:checked').prop('checked', false);
            })
            $(document).on('change', 'input:radio[class=custom_time]:checked', function() {

                $('.for_hide_show').show();
            })
            $(document).on('change', 'input:radio[class=second_half]:checked', function() {
                $('.for_hide_show').hide();
                $('.time_error').empty();
            })
            $(document).on('change', 'input:radio[class=first_half]:checked', function() {
                $('.for_hide_show').hide();
                $('.time_error').empty();
            })
        });
    </script>
    <script type="text/javascript">
        var token = "<?= csrf_token()?>";
        $('.checkintime').click(function() {


            $.ajax({
                url: "<?= URL::route('user.checkin') ?>",
                type: "post",
                data: {
                    _token: token
                },
                dataType: 'JSON',

                success: function(resp) {
                    
                    if (resp.check_in) {
                        if (resp.checkout_time_count) {
                            $('#total_hours').attr('value', resp.checkout_time_count[0].time_difference)
                        }
                        $('.checkintime').attr('value', 'Checkout');
                        $('#checkin_data').attr('value', resp.check_in.check_in);
                        $('#total_time_checkin').attr('value', resp.count_row);

                    } else if (resp.check_out) {
                        $('#total_work_hours').attr('value',resp.exp_work_hours[0]);
                        $('#total_hours').attr('value', resp.checkout_time_count[0].time_difference);
                        $('.checkintime').attr('value', 'CheckIn');
                        $('#checkout_data').attr('value', resp.check_out.check_out)
                        $('#total_time_checkin').attr('value', resp.count_row);
                    }
                }
            });
        });
    </script>
    @stop 