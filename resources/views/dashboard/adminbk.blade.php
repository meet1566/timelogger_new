@extends('layout.layout')
@section('content')
@stop
<div class="content-wrapper">
        <div class="page-title">
            <div>
                <h1><i class="fa fa-dashboard"></i> Dashboard</h1>
            </div>
            
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-calendar-check-o fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                
                                <div><b>LOGS</b></div>
                                <!-- @if($logs['check_in'] == NULL AND $logs['check_in'] == '')
                                    Not Available
                                @else -->
                                    Date : <?= $logs['time'] ?>
                                    </br>
                                    @if($logs['check_in'])
                                    Check In : <?= $logs['check_in']; ?>
                                    @endif
                                    </br>
                                    @if($logs['check_out'])
                                    Check Out : <?= $logs['check_out'];?>
                                    @endif
                                <!-- @endif -->
                            </div>
                        </div>
                    </div>
                    <a href="<?= URL::route('logs.index') ?>">
                        <div class="panel-footer">
                            <span class="pull-left">View Details</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-green">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                            <i class="fa fa-plane fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                
                                <div><b>HOLIDAYS</b></div>
                                @if(!empty($holiday))
                                    {{ $holiday->holiday }}
                                @endif
                            </div>
                        </div>
                    </div>
                    <a href="<?= URL::route('holiday.index') ?>">
                        <div class="panel-footer">
                            <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
         <!--    <div class="col-lg-3 col-md-6">
                <div class="panel panel-yellow">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-user fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        
                                        <div><b>USER</b></div>
                                    </div>
                                </div>
                            </div>
                            <a href="<?= URL::route('show.user') ?>">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
            </div> -->
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-calendar-minus-o fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                
                                <div><b>LEAVES</b></div>
                                @if($leaves['date'] == NULL AND $leaves['date'] == '')
                                    No leave Taken
                                @else
                                Leave Date : <?= $leaves->date; ?>
                                </br>       
                                Leave Purpose : <?= substr($leaves['leave_purpose'],0,20); ?>...
                                @endif
                            </div>
                        </div>
                    </div>
                    <a href="<?= URL::route('leaves.index') ?>">
                        <div class="panel-footer">
                            <span class="pull-left">View Details</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></semptypan>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="card">
          <h3 class="card-title">Bar Chart</h3>
          <div id="chartContainer" style="height: 345px; width: 100%;">
          </div>
          <!-- <div class="embed-responsive embed-responsive-16by9">
            <canvas class="embed-responsive-item" id="barChartDemo"></canvas>
          </div> -->
        </div>
</div>empty
@section('script')
<?= Html::script('asset/js/canvasjs.min.js') ?>

<script type="text/javascript">
    $(document).ready(function(){
        window.onload = function () {
            var chart = new CanvasJS.Chart("chartContainer",
            {
              theme: "theme2",
              axisX: {
                title: "Month"
              },
              width: 800,
              height:350,
              axisY: [
               {
                 title: "1st Primary Y Axis",
                 lineColor: "#4F81BC",
                 tickColor: "#4F81BC",
                 labelFontColor: "#4F81BC",
                 titleFontColor: "#4F81BC",
                 lineThickness: 2,
                 interval: 2,
                 maximum: 30,
              },
              // {
              //    title: "2nd Primary Y Axis",
              //    lineColor: "#C0504E",
              //    tickColor: "#C0504E",
              //    labelFontColor: "#C0504E",
              //    titleFontColor: "#C0504E",
              //    lineThickness: 2
              // }
            ],
              
            data: [
              {
                type: "column",             
                dataPoints: [
                  { x: 1, y: 14,label: "Jan" },
                  { x: 2, y: 2 ,label: "Feb"},
                  { x: 3, y: 5 ,label: "Mar"},
                  { x: 4, y: 7 ,label: "Apr"},
                  { x: 5, y: 1 ,label: "May"},
                  { x: 6, y: 5 ,label: "Jun"},
                ]
             },
             // {
             //   type: "column",
             //   axisYIndex: 1,
             //   dataPoints: [      
             //     { x: 1, y: 70 },
             //     { x: 2, y: 45 },
             //     { x: 3, y: 56 },
             //     { x: 4, y: 77 },
             //     { x: 5, y: 58 },
             //     { x: 6, y: 72 },
             //     { x: 7, y: 58 },
             //     { x: 8, y: 26 },
             //     { x: 9, y: 18 }
             //   ]
             // }
            ]
            });

            chart.render();
          }
        
    });
</script>
@stop