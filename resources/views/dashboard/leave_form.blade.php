
@foreach($date_of_leave as $key => $form)
<div class="parentdate" >
  <div class="childdate">
    {{csrf_field()}}
    <a href="#" class="clicktohide">
    <div class="lable_div">
      
    <h3 class="modal-heading modaldate"><span class="fa fa-plus"></span>{{ $form}}  {{ date('l',strtotime($form))}}</h3>
    </div>
    </a>
    <div class="inner_div">
    <input type="hidden" value="{{$form}}" class="date_field" name="hidden_field">

    <div class="form-group">
      <div class="animated-checkbox col-md-12">
        <label>
          <input type="radio" id="fulldaycheckbox" name="leave_detail[{{$form}}][day_type]" value="fullday" class="fulldaycheckboxclass" checked ><span class="label-text">Full day</span>
        </label>
        @if(date('l',strtotime($form)) == 'Sunday')
        @elseif(date('l',strtotime($form)) == 'Saturday' && (ceil(date('d',strtotime($form))/7) == 2 || ceil(date('d',strtotime($form))/7) == 4))
        @elseif(in_array(date("d-m-Y", strtotime($form)),$holiday_array))
        @else 
        <label>
          <input type="radio" id="halfdaycheckbox" name="leave_detail[{{$form}}][day_type]" value="halfday" class="halfdaycheckboxclass"  ><span class="label-text">Half day</span>
        </label>
        @endif
      </div>
    </div>
    <div class="form-group form_hour" id="hour" style="display: none;">
        <label class="control-label col-lg-3" for="leaveselecttype">Select Time:</label>
     
      <div class="col-md-9">
            <div class="input-group col-md-9 bootstrap-timepicker timepicker">
              <label>FirstHalf</label>
              <input type="radio" class="first_half" name="leave_detail[{{$form}}][half]" value="first_half">
              <label>SecondHalf</label>
              <input type="radio" class="second_half" name="leave_detail[{{$form}}][half]" value="second_half">
              <label>Custom Time</label>
              <input type="radio" name="leave_detail[{{$form}}][half]" value="custom_time" class="custom_time">
            </div> 
            <div class="input-group col-md-6 bootstrap-timepicker timepicker for_hide_show" style="display:none;">
              <div style="display: flex;">
                <input class="form-control timepicker2" placeholder="To hour" name="leave_detail[{{$form}}][from_hour]" type="text" value="10:00 AM">
                <input class="form-control timepicker1"  placeholder="From hour" name="leave_detail[{{$form}}][to_hour]"  type="text" value="07:00 PM">
              </div>
            </div> 
                <span class="time_error" style="color:red"></span>
      </div>
     
      <p class="help-block" style="color:red" id="error_hours"></p>
    </div>
    <div class="form-group">
      <label class="col-lg-3 control-label" for="leaveselecttype">Select Leave type</label>
    
      <div class="col-lg-9">
          @if($get_leavetype == 'Sick Leave')
          
            <input type="text" class="form-control second_step_leave_type s_leave" name="leave_detail[{{$form}}][leave_type]" value="Sick Leave" onkeydown="return false"/>
          @else
        <select class="form-control second_step_leave_type" name="leave_detail[{{$form}}][leave_type]" id="field_leave_detail" data-date="{{ $form }}">

          <option value="Casual Leave" id="c_leave" class="c_leave" @if($get_leavetype == 'Casual Leave') selected="selected" @endif >Casual Leave</option>
          <option value="Sick Leave" class="s_leave" @if($get_leavetype == 'Sick Leave') selected="selected" @endif >Sick/Emergency Leave</option>
          <option value="Privilege Leave" class="p_leave" @if($get_leavetype == 'Privilege Leave') selected="selected" @endif>Privilege Leave</option>
          
          @endif
        </select>
        <p class="help-block" style="color:red" id="error_leave_detail{{$form}}"></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-lg-3 control-label" for="leave-purpose">Leave-purpose</label>
      <div class="col-lg-9">
        <textarea class="form-control leave_pupose_form2" name="leave_detail[{{$form}}][leave_purpose]" type="text" placeholder="leave-purpose" rows="4">{{$get_leavepurpose}}</textarea>
        <p class="help-block" style="color:red" id="error_s_leavepurpose"></p>
      </div>
    </div>
    </div>
    </div>
</div>     
@endforeach
<!-- leaves status -->
<script>
  var sick_leaves = sick_leaves;
  var casual_leaves = casual_leaves;
  var priviledge_leaves = priviledge_leaves;
  var selected_date = [];
  var second_step = $('.second_step_leave_type');
  var date_field = $('.date_field');
  
  $(document).ready(function(){
    $(".firststatus").show();
  })
  d = new Date();
  d.setDate(d.getDate()+5);
  var date = convert(d);
  function convert(str) {
      var date = new Date(str),
      mnth = ("0" + (date.getMonth() + 1)).slice(-2),
      day = ("0" + date.getDate()).slice(-2);
      return [date.getFullYear(),mnth,day].join("-");
  }

  $.each(date_field,function(k,v){
    var dateField = $(v).val();
    selected_date[k] = dateField;
  });

  var leaves_date = {};
  $.each(selected_date,function(i,value){
     var leave_type = $('.second_step_leave_type').val();
     leaves_date[value] =leave_type;
  });

// date disabled code start from here

  // var disable = $(".second_step_leave_type").val();
  // alert(disable);
  // if (disable == "Sick Leave") {
  //   $('.p_leave').attr('disabled','disabled');
  //   $('.c_leave').attr('disabled','disabled');
  //   var new_date1 = [];
  //   var date_field1 = $('.date_field');
  //       $.each(date_field1,function(k,v){
  //         var newdateField1 = $(v).val();
  //         new_date1[k] = newdateField1;
  //       });
  //       var dates1 = {};
  //       for(var i = 0; i < new_date1.length; i++){
  //           dates1[new_date1[i]] = disable;
  //       }
  //       selectDate(dates1);
  // }else{
    $(".second_step_leave_type").on("change",function(){
        // $(".firstatus").show();
        var new_date = [];
        var new_leave = [];
        var leaves_type1 = $('.second_step_leave_type').val();
        var element = $(this).attr("data-date");
        var second_step = $('.second_step_leave_type');
        $.each(second_step,function(k,v){
           var new_leaves_type = $(v).val();
           new_leave[k] = new_leaves_type;
        });

        var date_field = $('.date_field');
        $.each(date_field,function(k,v){
          var newdateField = $(v).val();
          new_date[k] = newdateField;
        });
        var dates = {};
        for(var i = 0; i < new_date.length; i++){
            dates[new_date[i]] = new_leave[i];
        }
        selectDate(dates)
    });
  // }
  function selectDate(dates)
  {
    $(".firststatus").empty();
    $(".firststatus").show();
    var casualleves = 0;
    var priviledgeleaves = 0;
    var sickleaves = 0;
    var seleted_date_arr = {};
    var arr_count = Object.entries(dates);
    var casualpaid = 0;
    var priviledgepaid = 0;
    var sickpaid = 0;

    $.each(dates,function(value,key){
    var value = value.split("-").reverse().join("-");
    if(key == "Casual Leave")
    {
        casualleves++;
        var casual_available = 6 - casual_leaves;
        if(casualleves > casual_available)
        {
            if(value > date)
            {
              if(casualpaid < casual_available)
              {
                 seleted_date_arr[value] = "Paid";
                 casualpaid++;
              }else{
                seleted_date_arr[value] = "UnPaid";
              }
            }else{
                 seleted_date_arr[value] = "UnPaid";
            }
        
        }else{
            if(value > date)
            {
               if(casualpaid < casual_available){
                   seleted_date_arr[value] = "Paid";
                   casualpaid++;
                }else{
                  seleted_date_arr[value] = "UnPaid";
                }
            }else{
                seleted_date_arr[value] = "UnPaid";
            }
        }
      }else if(key == "Privilege Leave")
      {
        priviledgeleaves++;

        var priviledge_available = 6 - priviledge_leaves;
        
        if(priviledgeleaves > priviledge_available)
        {
            if(value > date)
            {
              if(priviledgepaid < priviledge_available)
              {
                 seleted_date_arr[value] = "Paid";
                 priviledgepaid++;
              }else{
                seleted_date_arr[value] = "UnPaid";
              }
            }else{
                 seleted_date_arr[value] = "UnPaid";
            }
        }else{
            if(value > date)
            {
               if(priviledgepaid < priviledge_available)
               {
                   seleted_date_arr[value] = "Paid";
                   priviledgepaid++;
                }else{
                  seleted_date_arr[value] = "UnPaid";
                }
            }else{
                seleted_date_arr[value] = "UnPaid";
            }
        }
      }else if(key == "Sick Leave")
      {
        sickleaves++;
        var sick_available = 6 - sick_leaves;

        if(sickleaves > sick_available)
        {
            if(sickpaid < sick_available)
            {
               seleted_date_arr[value] = "Paid";
               sickpaid++;
            }else{
              seleted_date_arr[value] = "UnPaid";
            }
        }else{
           if(sickpaid < sick_available){
               seleted_date_arr[value] = "Paid";
               sickpaid++;
            }else{
              seleted_date_arr[value] = "UnPaid";
            }
        } 
       }
    });
    $.each(seleted_date_arr,function(i,value){
       var i = i.split("-").reverse().join("-");
       $('.firststatus').append(" "+i+" : "+value+" , ");
    });
  }
$(".clicktohide").next(".inner_div").removeClass("active").slideUp()
$(".clicktohide").click(function(){
      if($(this).next(".inner_div").hasClass("active")){
        $(this).next(".inner_div").removeClass("active").slideUp()
        $(this).children("span").removeClass("fa-minus").addClass("fa-plus")
      }
      else{
        // $(" .inner_div").removeClass("active").slideUp()
        $(".clicktohide span").removeClass("fa-minus").addClass("fa-plus");
        $(this).next(".inner_div").addClass("active").slideDown()
        $(this).children("span").removeClass("fa-plus").addClass("fa-minus")
      }
      });
</script>
<!-- end leaves status -->
<style>
  .form_hour .control-label{
    padding-top: 0;
  }
  .form_hour input, .animated-checkbox input{
    margin: 0 7px;
  }
</style>