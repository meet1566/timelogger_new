<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Timelogger | Thinktanker</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/adminlte.min.css">
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                <b>thinkTANKER</b>
            </div>
            <!-- /.login-logo -->
            <div class="login-box-body">
                @if(Session::get('message') && Session::has('message') != "")
                    @if(Session::get('message_type') && Session::has('message_type') != "" )
                    <div class="alert alert-dismissible alert-{{Session::get('message_type')}}" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{Session::get('message')}}
                    </div>
                    @endif
                @endif
                <p class="login-box-msg"><i class="fa fa-fw fa-user"></i> Change Password</p>
                <form method="post" action="/password/reset">
                    <input type="hidden" name="token" value="{{ $token }}">
                    <div class="form-group has-feedback">
                        <input type="email" name="email" placeholder="Email" class="form-control"/>
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        <?= $errors->first('email','<span class="help-inline text-danger">:message</span>'); ?>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" name="password" placeholder="Password" class="form-control"/>
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        <?= $errors->first('password','<span class="help-inline text-danger">:message</span>'); ?>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" name="password_confirmation" placeholder="Confirm Password" class="form-control"/>
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        <?= $errors->first('password_confirmation','<span class="help-inline text-danger">:message</span>'); ?>
                    </div>
                    <div class="row">
                        <!-- /.col -->
                        <div class="col-xs-4">
                            <button id="signin" type="submit" class="btn btn-primary btn-flat">Submit</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>
            </div>
            <!-- /.login-box-body -->
        </div>
        <script src="/assets/js/jquery.min.js"></script>
        <script src="/assets/js/bootstrap.min.js"></script>
        <script>
            $('form').submit(function() {
                $('#signin').html('<i class="fa fa-lg fa-pulse fa-fw fa-spinner"></i> Loading...').prop('disabled',true);
            });
        </script>
    </body>
</html>
