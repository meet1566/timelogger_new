<!doctype html>
<html>
<head>
	<title>Email-Box</title>
	<style type="text/css">
		/* all css reset here */
		body,div,aside,span,p,h1,h2,h3,h4,h5,h6{margin:0;padding:0;}
		nav{padding:10px;}
		.container{padding:0 !important;}
		.navbar{margin:0 !important;border:none !important;}
		.navbar-default{background-color:#222 !important;background-image: none !important;}
		.navbar-brand {color:#fff !important;font-weight:bold;font-size:20px !important;margin:0 !important;}
		.contant {
		    background-color: #f1c633;
		    float: left;
		    min-height: 100%;
		    position: absolute;
		    top: 0;
		    width: 100%;
		    padding:10px;
		}
		@media screen only and (max-width:1169px;) {
			.container{width:95% !important;}
		}
	</style>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top">
<div class="container">
	<div class="navbar-header page-scroll">
		<a class="navbar-brand page-scroll" href="#">thinkTANKER</a>
	</div>
</div>
</nav>

<!-- contant start here -->
<div class="contant">
	<div class="container">
		<h4 style="float:left;width:100%;font-size:22px;color:#fff;margin-top:100px;font-family: 'Montserrat', sans-serif;">Account Verified!</h4>
		<p style="color:#666;font-size:14px;font-family: 'Montserrat', sans-serif;">{{ $message }}</p>
		<span class="text-capitalize" style="float:left;width:100%;color:#666;font-size:14px;font-family: 'Montserrat', sans-serif;">thank you !</span>
	</div>
</div>
</body>
</html>
