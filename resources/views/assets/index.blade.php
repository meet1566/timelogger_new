@extends('layout.layout')
@section('content')
<style>
    .dataTables_filter {
   width: 50%;
   float: right;
   text-align: right;
}
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet.draw/0.4.2/leaflet.draw.css"/>
<div class="page-title">
    <div>
      <h1>Assets</h1>
        <div class="col-md-12">
              <ul class="nav nav-pills">
                <li class="leave-sum active">
                  <a href="javascript:;">Total Device 
                  <span class="badge bg-green">{{$device_count}}<strong></strong></span></a>
                </li>
                @foreach($total_device_count as $data =>$value)
                <li class="leave-sum active">
                  <a  href="javascript:;">{{$data}}
                  <span class="badge bg-green">{{$value}}<strong></strong></span></a>
                </li>
                @endforeach
              </ul>
        </div>
    </div>
    <div>
            <a href="<?= URL::route('assets.create') ?>"class="btn btn-primary btn-flat"><i class="fa fa-lg fa-plus"></i></a>
       <!--  <a href="<?=URL::route('assets.create')?>" class="btn btn-primary btn-flat"><i class="fa fa-lg fa-plus"></i></a> -->
        <!-- <a href="#" class="btn btn-info btn-flat"><i class="fa fa-lg fa-refresh"></i></a>
        <a id="delete" href="javascript:void(0)" class="btn btn-warning btn-flat"><i class="fa fa-lg fa-trash"></i></a> --> 
    </div>
</div>
<div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))
            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
    @endforeach
</div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                   <div class="row">
                    <div class="col-xs-12">
                        <form class="form-inline" role="form">
                            <div class="form-group">
                                   <label for="deviceType" >Select</label>
                                     <?= Form::select('deviceType',[null=>'Select'] + $all_device,null,['id' => 'deviceType']) ?>
                                    <label>Device</label>
                            </div>
                        </form>
                    </div>
                </div>
                <br>
                    <table id="employee" class="table table-hover table-bordered" border="1 px" style="width:100%;">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Company Name</th>
                                <th>Device Type</th>
                                <th>Barcode No</th>
                                <th>Purchase Date</th>
                                <th>Status</th>
                                <th>Service Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
<?= Html::script('asset/DataTables/media/js/jquery.dataTables.js') ?>
<?= Html::script('asset/DataTables/media/js/dataTables.bootstrap.min.js') ?>
<?= Html::script('asset/js/fnStandingRedraw.js') ?>
<?= Html::script('asset/js/delete_script.js') ?>
<script>
var token = "<?= csrf_token() ?>";
var title = "Are you sure to delete selected record(s)?";
var change_status = "Are you sure to change status of that record ?";
var text = "You will not be able to recover this record";
var type = "warning";
var token = "{{ csrf_token() }}";
var delete_path = "{{ route('assets.delete') }}";

$(document).ready(function(){

        var device_type = '';
        var master;

        $(function()
        {
            master = $('#employee').dataTable({
            "bProcessing": false,
            "bServerSide": true,
            "autoWidth": true,
            "aaSorting": [
                [1, "asc"]
            ],
            "sAjaxSource": "{{ URL::route('assets.index')}}",
            "fnServerParams": function ( aoData ) {
               var form_data = $('#deviceType').serializeArray();
                $.each(form_data, function(i, val) {
                    // console.log(val);
                    aoData.push(val);
                });
                server_params = aoData;
            },
            "aoColumns": [
            { mData: 'id',sWidth: "5%",bSortable:true, bVisible:false},
            { mData: 'title',sWidth: "15%",bSortable:true, },
            { mData: 'device_type',sWidth: "15%",bSortable:true, },
            { mData: 'serial_number',sWidth: "15%",bSortable:true, },
            { mData: 'purchase_date',sWidth: "10%",bSortable:true, },
            { mData: 'status',sWidth: "10%",bSortable:true, },
            { mData: 'service_status',sWidth: "10%",bSortable:true,},
            { mData: null,
                sWidth: "10%",
                bSortable : false,
                mRender:function(v,t,o) {

                    var path = "<?= URL::route('assets.edit',array(':id')) ?>";
                    var status = "<?= URL::route('assets_status',array(':id')) ?>";
                    var status_history = "<?= URL::route('assets.statusHistory',array(':id')) ?>";
                    var path_del = "<?= URL::route('assets.delete',array(':id')) ?>";
                    var id = o['id'];

                    path     = path.replace(':id',id);
                    status     = status.replace(':id',o['id']);
                    status_history    = status_history.replace(':id',o['id']);
                    path_del = path_del.replace(':id',o['id']);
                    console.log(o['assets_service']);
                    var extra_html  =   "<div class='btn-group pr5'>"
                                    +       "<a title='Edit' href='"+path+"'><i class='fa fa-edit'></i></a>"
                                    +   "&nbsp&nbsp&nbsp<a id='delete' href='javascript:void(0)' onclick=\"deleteRecord('"+delete_path+"','"+title+"','"+text+"','"+token+"','"+type+"',"+o['id']+")\" class='fa fa-fw fa-trash-o'></a>  "
                                    +   "<a onclick='statusChange("+id+")' title='Change Status'>&nbsp&nbsp&nbsp&nbsp<i class='fa fa-hourglass'></i></a>"
                                    +   "<a title='View Assets Log' data-toggle='modal' href='user_assets_service/"+id+"'>&nbsp&nbsp&nbsp&nbsp<i class='fa fa-eye show_history'></i></a>"
                                    +   "</div>";
                    return extra_html;
                }
            }
            ],
            
        });
            
        });
   
   $("#deviceType").on('change', function (){
        master.dataTable().fnFilter(this.value);
    });

});

</script>
<script>
    function statusChange(id)
    {   
        swal({
            title: change_status,
            type: type,
            showCancelButton: true,
            confirmButtonText: "Yes, Change it!",
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm) {
            if (isConfirm) {
               $.ajax({
                    url: 'assets_status/' +id,
                    type:'get',
                    dataType:'json',
                    data:{id:id},
                    success : function(data)
                    {
                        if (data.message == true) {
                            iziToast.success({
                              title:'Success!',
                                message: "Assets Status Change Successfully",
                            });
                            setTimeout(function() {
                                location.reload();
                            }, 3000);
                        }else{
                            iziToast.error({
                                title:'Please First Make Device In InProcess!',
                            });
                        }
                    }
               });
            }
        });
    }
    
</script>
@include('partials.alert')
@stop
