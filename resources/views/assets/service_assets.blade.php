@extends('layout.layout')
@section('content')
<style type="text/css">
    .flash-message{
          background: #009688;
          color: white;
          font-size: 18px;
          padding: 10px;
          margin: 10px;
          display: none;
          position: absolute;
          right: 15px;
          top: 700px;
    }
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet.draw/0.4.2/leaflet.draw.css"/>
<div class="page-title">
    <h2>Assets Service History</h2>
</div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                   <div class="row">
                    <div class="col-xs-12">
                        <form class="form-inline" role="form">
                            <div class="form-group">
                            </div>
                        </form>
                    </div>
                </div>
                <br>
                    <table id="employee" class="table table-hover table-bordered" border="1 px" style="width:100%;">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Device Type</th>
                                <th>Status</th>
                                <th>Notes</th>
                                <th>Day</th>
                                <th>Date & Time</th>
                            </tr>
                        </thead>
                        <tbody>
                                @foreach($assets_service as $item)
                            <tr>
                                <td>{{$count}}</td>
                                <td>{{$item['device_type']}}</td>
                                <td>{{$item['status']}}</td>
                                <td>{{$item['notes']}}</td>
                                <?php $timestamp = (strtotime($item['created_at']));
                                    $date = date('d-m-Y', $timestamp);
                                    $time = date('H:i:s', $timestamp);
                                    ?>
                                <?php $datetime = new DateTime($item['created_at']) ?>
                                <td><?php echo $datetime->format('l'); ?></td>
                                <td>{{ $date . " " . $time;}}</td>
                            </tr>
                                <?php $count++;?>
                                @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
 <div class="flash-message">  
 </div>
@stop
@section('script')
<?= Html::script('asset/DataTables/media/js/jquery.dataTables.js') ?>
<?= Html::script('asset/DataTables/media/js/dataTables.bootstrap.min.js') ?>
<?= Html::script('asset/js/fnStandingRedraw.js') ?>
<?= Html::script('asset/js/delete_script.js') ?>
@include('partials.alert')
@stop
