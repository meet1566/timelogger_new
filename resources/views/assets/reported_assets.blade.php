@extends('layout.layout')
@section('content')
<div class="page-title">
      <h2><span><i></i></span></span>My Reported Assets</h2>
</div>
 <div class="row">
        <div class="col-md-12">
            <div class="card">
                <br>
                    <table id="employee" class="table table-hover table-bordered" border="1 px" style="width:100%;">
                            <tr>
                                <th>No</th>
                                <th>Date</th>
                                <th>Day</th>
                                <th>Device Type</th>
                                <th>Status</th>
                                <th>Notes</th>
                            </tr>
                            
                            @if($reported_assets)
                            @foreach($reported_assets as $item )
                            <tr>
                                <?php $count++;?>
                                <td>{{$count}}</td>
                                <?php $timestamp = (strtotime($item['created_at']));
                                    $date = date('d-m-Y', $timestamp);
                                    $time = date('H:i:s', $timestamp);
                                    ?>
                                <?php $datetime = new DateTime($item['created_at']) ?>
                                <td>{{ $date . " " . $time;}}</td>
                                <td><?php echo $datetime->format('l'); ?></td>
                                <td>{{$item['device_type']}}</td>
                                <td>{{$item['status']}}</td>
                                <td>{{$item['notes']}}</td>
                            </tr>
                            @endforeach
                            @else
                            <tr class="dd">
                                <td colspan="6" align="center"><b>No Data Found</b></td>
                            </tr>
                            @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
@include('partials.alert')
@stop


<!-- <style type="text/css">
    .dd td{
        width: 100%;
    }
</style> -->