@extends('layout.layout')
@section('content')
<style type="text/css">
    .flash-message{
          background: #009688;
          color: white;
          font-size: 18px;
          padding: 10px;
          margin: 10px;
          display: none;
          position: absolute;
          right: 15px;
          top: 730px;
    }
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet.draw/0.4.2/leaflet.draw.css"/>
<div class="page-title">
    <div>
        <h3 style="display:flex; width:400px;">Assets Report Request</h3>
    </div>
    <div>
        <a href="<?= URL::route('assets.createAdminReport') ?>"class="btn btn-primary btn-flat" style="margin-left: 650px;" ><i class="fa fa-lg fa-plus"></i></a>
    </div>
</div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                   <div class="row">
                    <div class="col-xs-12">
                        <form class="form-inline" role="form">
                            <div class="form-group">
                            </div>
                        </form>
                    </div>
                </div>
                <br>
                    <table id="employee" class="table table-hover table-bordered" border="1 px" style="width:100%;">
                        <thead>
                            <tr>
                                <th></th>
                                <th>User Name</th>
                                <th>Device Type</th>
                                <th>Notes</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
 <div class="flash-message">  
 </div>
@stop
@section('script')
<?= Html::script('asset/DataTables/media/js/jquery.dataTables.js') ?>
<?= Html::script('asset/DataTables/media/js/dataTables.bootstrap.min.js') ?>
<?= Html::script('asset/js/fnStandingRedraw.js') ?>
<?= Html::script('asset/js/delete_script.js') ?>
<script>
var token = "<?= csrf_token() ?>";
var title = "Are you sure to Process Request?";
var title1 = "Are you sure to Resolved Issue?";
var title2 = "Are you sure to delete Request?";
var text = "You will not be able to recover this record";
var type = "warning";
var token = "{{ csrf_token() }}";
var delete_path = "{{ route('assets.deleteReport') }}";

$(document).ready(function(){

        var device_type = '';
        var master;

        $(function()
        {
            master = $('#employee').dataTable({
            "bProcessing": false,
            "bServerSide": true,
            "autoWidth": true,
            "aaSorting": [
                [1, "asc"]
            ],
            "sAjaxSource": "{{ URL::route('show_report_assets')}}",
            "fnServerParams": function ( aoData ) {
               var form_data = $('#deviceType').serializeArray();
                $.each(form_data, function(i, val) {
                    // console.log(val);
                    aoData.push(val);
                });
                server_params = aoData;
            },
            "aoColumns": [
            { mData: 'id',sWidth: "5%",bSortable:true, bVisible:false},
            { mData: 'name',sWidth: "20%",bSortable:true, },
            { mData: 'device_type',sWidth: "20%",bSortable:true, },
            { mData: 'notes',sWidth: "15%",bSortable:true, },
            { mData: 'status',sWidth: "10%",bSortable:true, },
            { mData: null,
                sWidth: "10%",
                bSortable : false,
                mRender:function(v,t,o) {
                    // console.log(v['status']);
                    var path = "<?= URL::route('admin_report_assets_status',array('id'=>':id')) ?>";
                    var id = o['id'];
                    path     = path.replace(':id',o['id']);
                    
                    if (v['status'] == "Pending") {

                        var extra_html  =   "<div class='btn-group pr5'>"
                                        +    "<a title='Resolved'  onclick=\"ResolveRecord('"+title1+"','"+token+"','"+type+"',"+id+")\" class='status' style='cursor:pointer;'><i class='fa fa-check'></i></a>"
                                        +    "<a title='Process' style='margin-left:20px;' onclick=\"AcceptRecord('"+title+"','"+token+"','"+type+"',"+id+")\" class='status' style='cursor:pointer;'><i class='fa fa-circle-o-notch'></i></a>"
                                        +   "<a id='delete' style='margin-left:20px;' href='javascript:void(0)' onclick=\"deleteRecord('"+delete_path+"','"+title2+"','"+text+"','"+token+"','"+type+"',"+o['id']+")\" class='fa fa-fw fa-trash-o'></a>  "
                                        +   "</div>";
                    }else if(v['status'] == "InProcess"){
                        extra_html  =   "<div class='btn-group pr5'>"
                                        +    "<a title='Resolved'  onclick=\"ResolveRecord('"+title1+"','"+token+"','"+type+"',"+id+")\" class='status' style='cursor:pointer;'><i class='fa fa-check'></i></a>"
                                        +   "<a id='delete' style='margin-left:20px;' href='javascript:void(0)' onclick=\"deleteRecord('"+delete_path+"','"+title2+"','"+text+"','"+token+"','"+type+"',"+o['id']+")\" class='fa fa-fw fa-trash-o'></a>  "
                                        +   "</div>";
                    }else{
                        extra_html  =   "<div class='btn-group pr5'>"
                                        +   "<a id='delete'  href='javascript:void(0)' onclick=\"deleteRecord('"+delete_path+"','"+title2+"','"+text+"','"+token+"','"+type+"',"+o['id']+")\" class='fa fa-fw fa-trash-o'></a>  "
                                        +   "</div>";
                    }
                    return extra_html;
                }
            }
            ],
            
        });
            
        });
   
   $("#deviceType").on('change', function (){
        master.dataTable().fnFilter(this.value);
    });
});

</script>
<script>
    function messageShow(id)
    {   
        $.ajax({
            url: 'admin_report_assets_status/' +id,
            type:'get',
            dataType:'json',
            data:{id:id},
            success : function(data)
            {
                if (data.message == true) {
                   
                    $('.flash-message').html(' Request Accept successfully').slideDown();
                    setTimeout(function() {
                        location.reload();
                        $('.flash-message').slideUp();
                    }, 2000);
                    
                }
            }
        });
        
    }
    function AcceptRecord(title,token,type,id)
    {
        swal({
            title: title,
            type: type,
            showCancelButton: true,
            confirmButtonText: "Yes, Process it!",
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm) {
            if (isConfirm) {
                messageShow(id);
            }
        });
    }
    function changeStatus(id)
    {   
        $.ajax({
            url: 'report_assets_status/' +id,
            type:'get',
            dataType:'json',
            data:{id:id},
            success : function(data)
            {
                if (data.message == true) {
                   
                    $('.flash-message').html(' Issue Resolved successfully').slideDown();
                    setTimeout(function() {
                        location.reload();
                        $('.flash-message').slideUp();
                    }, 2000);
                    
                }
            }
        });
        
    }
    function ResolveRecord(title,token,type,id)
    {
        swal({
            title: title1,
            type: type,
            showCancelButton: true,
            confirmButtonText: "Yes, Resolved it!",
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm) {
            if (isConfirm) {
                changeStatus(id);
            }
        });
    }
</script>

@include('partials.alert')
@stop
