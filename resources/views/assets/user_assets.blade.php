@extends('layout.layout')
@section('content')
<style type="text/css">
    .flash-message{
          background: #009688;
          color: white;
          font-size: 18px;
          padding: 10px;
          margin: 10px;
          display: none;
          position: absolute;
          right: 15px;
          top: 700px;
    }
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet.draw/0.4.2/leaflet.draw.css"/>
<div class="page-title">
      <h2><span><i></i></span></span>My Assets</h2>
      <a type="button" class="btn btn-primary" data-toggle="modal" data-target="#Report_modal" style="display:end">Report a Problem</a>
</div>


<!-- Modal -->
<div class="modal fade" id="Report_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <?= Form::open(array('url' => route('user_assets_report') ,'class' => 'form-horizontal form_submit','name' => 'form_submit','id' => 'form_submit')) ?>
            <div class="form-group">
                <label class="control-label col-sm-2" for="device_type Name">Device Type</label>
                <div class="col-sm-8">
                     <?=Form::select('device_type', ['' => 'Select Device type'] +  $device, old('device_type'), ['class' => 'form-control select2_1 iconSelect'])?>
                    <div id="error"></div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="inputFull Name">Problem Notes</label>
                <div class="col-sm-8">
                    <?= Form::textarea('notes', old('notes'), ['class' => 'form-control notes', 'placeholder' => 'Enter Description']); ?>
                   <div id="error_notes"></div>
                </div>
            </div>
         {!! Form::close() !!}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="submit_report">Report Problem</button>
      </div>
    </div>
  </div>
</div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <br>
                    <table id="employee" class="table table-hover table-bordered" border="1 px" style="width:100%;">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Company Name</th>
                                <th>Device Type</th>
                                <th>Barcode No</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($Assets_user as $item )
                            <tr>
                                <?php $count++;?>
                                <td>{{$count}}</td>
                                <td>{{$item['title']}}</td>
                                <td>{{$item['device_type']}}</td>
                                <td>{{$item['serial_number']}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="flash-message">  
    </div>
@stop
@section('script')
<?= Html::script('asset/DataTables/media/js/jquery.dataTables.js') ?>
<?= Html::script('asset/DataTables/media/js/dataTables.bootstrap.min.js') ?>
<?= Html::script('asset/js/fnStandingRedraw.js') ?>
<?= Html::script('asset/js/delete_script.js') ?>
<script>
    $(document).ready(function(){
        var csrf_token = '<?php echo csrf_token(); ?>'
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': csrf_token
            }
        });
        $('#submit_report').on('click',function(e){
                $('#error').empty();
                $('#error_notes').empty();
            if ($('.iconSelect').val()== "" || $('.notes').val()=="") {
                $('#error').append('<span class="text-danger">Please select device</span>');
                if ($('.iconSelect').val()!= "") {
                    $('#error').empty();
                }
                $('#error_notes').append('<span class="text-danger">Please enter notes</span>');
                if ($('.notes').val()!="") {
                    $('#error_notes').empty();
                }
            }else
            {
                $('#error').empty();
                $('#error_notes').empty();
                e.preventDefault();
                $.ajax({
                    type:'POST',
                    url: "/user_assets_report",
                    data: $('#form_submit').serialize(),
                    success: (data) => {
                        $('#submit_report').attr('disabled','disabled');
                        $('#Report_modal').modal('hide');
                        $('.flash-message').html('Your request sent successfully').slideDown();
                        setTimeout(function() {
                           $('.flash-message').slideUp();
                        }, 3000);
                    },
                    error: (data)=> {
                    }
                })
            }
        });
    });
</script>

@include('partials.alert')
@stop
