@extends('layout.layout')
@section('content')
<div class="page-title">
        <div>
          <h1>Permission</h1>
            <ul class="breadcrumb side">
              <li><i class="fa fa-home fa-lg"></i></li>
              <li>Create</li>
           </ul>
        </div>
</div>
<div class="clearix"></div>
    @include('partials.alert')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <h3 class="card-title">Edit</h3>
                <div class="card-body">
                    <?= Form::open(array('url' => route('userPermissionMasterEdit') ,'class' => 'form-horizontal' ,'files' => true)) ?>
                    <?= Form::hidden('id',$users['id'], ['class' => 'form-control getid', 'placeholder' => 'Enter role']); ?>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="inputFull Name">Name of Role</label>
                            <div class="col-sm-3">
                                   <?= Form::text('name',$users['name'], ['class' => 'form-control', 'placeholder' => 'Enter role']); ?>
                                <span id="name_error" class="help-inline text-danger"><?= $errors->first('roles') ?></span>
                            </div>
                            <label class="control-label col-sm-2" for="inputFull Name">Description</label>
                            <div class="col-sm-3">
                                <?= Form::text('description',$roleselect[0]['description'], ['class' => 'form-control', 'placeholder' => 'Enter Description']); ?>
                                <span id="name_error" class="help-inline text-danger"><?= $errors->first('description') ?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="inputFull Name">Permission</label>
                            <div class="animated-checkbox" style="margin-bottom:10px;">
                                <label style="margin: 11px 19px;"><input type="checkbox" id="selectall"><span class="label-text">Select All</span></label>
                                <span class="help-inline text-danger">{{ $errors->first('permissions') }}</span>
                            </div>
                            <div class="row">
                            <div class="col-sm-offset-2 col-sm-10">
                                    @foreach($secPermissions as $sectionName => $secByPermission)
                                    <div class="col-sm-12 permission-section">
                                        <div class="panel-group" id="accordion">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <a class="collapsed panellink-title" data-toggle="collapse" data-parent="#accordion" href="#<?= $sectionName ?>"><?= ucfirst($sectionName) ?></a>
                                                    <div class="animated-checkbox" style="margin-bottom:0px;float: right;">
                                                        <label><input type="checkbox" id="" class="section_selectall" data="<?= $sectionName?>"><span class="label-text">Select All</span></label>
                                                    </div>
                                                </div>
                                                <div class="panel-collapse collapse" id="<?= $sectionName ?>">
                                                    @foreach($secByPermission as $permission)
                                                        <div class="panel-body">
                                                            <div class="animated-checkbox">
                                                                <label>
                                                                    <input type="checkbox" name="permissions[]" id="<?= $permission['id']; ?>" value="<?= $permission['id']; ?>">
                                                                    <span class="label-text"><?= $permission['description']; ?></span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@stop
@section('style')
    <?= Html::style('asset/css/new_dashboard.css') ?>
    <?= Html::script('js/jquery-2.1.4.min.js') ?>
@stop
@section('script')
<!-- <script src="https://code.jquery.com/jquery-1.10.2.js"></script> -->
<!-- <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script> -->
<script>
    $(document).ready(function() {
        var id = $('.getid').val();
        // alert(id)
        $.ajax({
                url:"/editrole/" + id,
                type:"POST",
                data: 'id='+id+'&_token={{csrf_token()}}',
                 beforeSend: function(){
                    $('input:checkbox').removeAttr('checked');
                },
                complete: function(){

                },
                success: function(response){
                    // alert(response.editpermissions);
                    if( ! response.success ) {
                        return;
                    }
                    console.log(response.editpermissions);
                    for (var i = 0; i < response.editpermissions.length; i++) {
                        var seletedValue = response.editpermissions[i];
                        // console.log(response.editpermissions[i]);
                        $('#'+seletedValue).prop('checked',true);
                    }
                }

            });
        $('form').submit(function(){
            $(this).find('button:submit').html('<span><i class="fa fa-spinner fa-spin"></i>Please Wait....</span>').prop('disabled', true);
        });
        $('.section_selectall').on('change',function(){
            var isChecked = $(this).is(':checked');
            var id = $(this).attr('data');
            if (isChecked) {
                $('#'+id+' input:checkbox').prop('checked',true);
            } else {
                $('#'+id+' input:checkbox').prop('checked',false);
            }
        });
        $('#selectall').on('change',function(){
            var isChecked = $(this).is(':checked');
            console.log(isChecked);

            if (isChecked) {
                $('input:checkbox').prop('checked',true);
            } else {
                $('input:checkbox').prop('checked',false);
            }
        });

        $('#rolechange').on('change',function(){
           var role_id =  $('#rolechange').val();
           // alert(role_id);
           $.ajax({
                url:"/getroleid",
                type:"POST",
                data: 'id='+role_id+'&_token={{csrf_token()}}',
                 beforeSend: function(){
                    $('input:checkbox').removeAttr('checked');
                },
                complete: function(){

                },
                success: function(response){
                    if( ! response.success ) {
                        return;
                    }
                    // console.log(response)
                    for (var i = 0; i < response.role_permission.length; i++) {
                        var seletedValue = response.role_permission[i].permission_id;
                        $('#'+seletedValue).prop('checked',true);
                    }
                }

            });  
        });
        $('#userchange').on('change',function(){

            var userId = $(this).val();

            if (userId == null && userId == '') {
                return;
            }

            $.ajax({
                url: '<?= route('selectpermission.get') ?>',
                type: 'json',
                method: 'post',
                data: {'user_id': userId,'_token': '<?= csrf_token() ?>' },
                beforeSend: function(){
                    $('input:checkbox').removeAttr('checked');
                },
                complete: function(){

                },
                success: function(resp){
                    if( ! resp.success ) {
                        return;
                    }
                    for (var i = 0; i < resp.user_permissions.length; i++) {
                        var seletedValue = resp.user_permissions[i].permission_id;
                        $('#'+seletedValue).prop('checked',true);
                    }
                }
            })
        });

    });
</script>
@include('partials.alert')
@stop
