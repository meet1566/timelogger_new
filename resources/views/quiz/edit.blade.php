@extends('layout.layout')
@section('content')
<div class="page-title">
    <div>
    <h1>Quiz</h1>
        <ul class="breadcrumb side">
            <li><i class="fa fa-home fa-lg"></i></li>
            <li>Edit</li>
        </ul>
    </div>
</div>
<div class="clearix"></div>
<div class="col-md-12">
    @include('partials.alert')
    <div class="card">
        <h3 class="card-title">Edit</h3>
        <div class="card-body">
            <?= Form::model($quiz_edit,array('route' => ['quiz.update',$quiz_edit->id],'class' => 'form-horizontal','method' => 'put','files' => true)) ?>
                <div class="form-group">
                    <label class="control-label col-sm-2">Question</label>
                    <div class="col-sm-8">
                        <?= Form::text('question', old('question'), ['class' => 'form-control', 'placeholder' => 'Quiz Question']); ?>
                      	<?= $errors->first('question',"<span class='text-danger'>:message</span>");?>
                    </div>
             	</div>
                @if($quiz_edit['image'] != NULL)
                <div class="form-group">
                    <label class="control-label col-sm-2">Question</label>
                    <div class="col-sm-8">
                        <img src="{{ $quiz_edit['image'] }}" height="150" width="150">
                    </div>
                </div>
                @endif
                <div class="form-group">
                    <label class="col-sm-2 control-label">Quiz Image</label>
                    <div class="col-sm-8">
                        <?= Form::file('image',['class' => 'form-control']) ?>
                        <?= $errors->first('image','<span class="help-inline text-danger">:message</span>') ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2">Time of Question</label>
                    <div class="col-sm-8">
                        <?= Form::number('time', old('time'), ['class' => 'form-control', 'placeholder' => 'Time In Seconds Per Question']); ?>
                      	<?= $errors->first('time',"<span class='text-danger'>:message</span>");?>
                    </div>
             	</div>
                <div class="form-group">
                    <label class="control-label col-sm-2">Select Users</label>
                    <div class="col-sm-8">
                        <?= Form::select('user[]',$users,old('user',$getSelectedUsers),['class' => 'form-control','multiple' => true,'id' => 'user_ask','style' => 'width:100%;']) ?>
                      	<?= $errors->first('user',"<span class='text-danger'>:message</span>");?>
                    </div>
             	</div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Options</label>
                    <div class="col-sm-8" style="border:1px solid #DDD;padding:20px;left: 14px;">
                        <div class="form-inline" id="dynamic_form">
                            <input type="text" style="margin-bottom:10px;width:52.5%;" class="form-control" name="options" id="options" placeholder="enter options">
                            <input type="radio" name="is_true" value="1" id="is_true" />
                            <a href="javascript:void(0)" style="vertical-align:3px;margin-left:5px;" class="btn btn-primary" id="plus5">Add More</a>
                            <a href="javascript:void(0)" style="vertical-align:3px;margin-left:5px;" class="btn btn-danger" id="minus5">Remove</a>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-8 col-md-offset-3">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <a href="<?= route('quiz.index') ?>" class="btn btn-danger">Cancel</a>
                    </div>
                </div>
       	    <?= Form::close(); ?>
        </div>
    </div>
</div>
@stop
@section('script')
<?= Html::script('asset/js/jquery.js') ?>
<?= Html::script('js/jquery-dynamic-form.js') ?>
<?= Html::script('front/js/select2.min.js') ?>

<script type="text/javascript">
jQuery(document).ready(function() {

    $('form').submit(function(){
        $(this).find('button:submit').html('<i class="fa fa-spinner fa-spin"></i>').prop('disabled', true);
    });

    $("#user_ask").select2();

    $('input[type="radio"]').click(function(){
        $('input[type="radio"]').not($(this)).prop('checked',false);
    });

    var dynamic_form =  $("#dynamic_form").dynamicForm("#plus5", "#minus5", {
        limit:100,
        normalizeFullForm : false,
    });

    dynamic_form.inject( <?= json_encode($final) ?> );

    @if($errors)

        var detail_Errors = <?= json_encode($errors->toArray()) ?>;

        $.each(detail_Errors, function(id, msg){
            var id_arr = id.split('.');
            $('#options' + id_arr[id_arr.length-2]).parent().append('<span class="help-inline text-danger">'+ msg[0] +'</span>');
        });
    @endif

});
</script>
@stop
