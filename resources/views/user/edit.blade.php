@extends('layout.layout')
@section('content')
<style>
    .back_button{
        display: flex;
    }
    .padding_space{
        margin-right: 10px;
    }
</style>
<div class="page-title">
    <div style="float: left;width: 100%">
        <div style="float: left" class="back_button">
            <div class="padding_space">

         <a href="<?= route('user.index')?>" class="btn btn-primary"><i class="fa fa-arrow-left"></i>BACK</a>
        </div>
            <p style="font-size:26px;">User<p>
            <span>
            <ul class="breadcrumb side">
                <li><i class="fa fa-home fa-lg"></i></li>
                <li>Update</li>
            </ul>
            </span>
        </div>
        @if(!empty($last_logs_date['time']))
            <div style="float: right;background-color: #009688;padding: 10px;color: #fff;font-size: 15px;"><strong>Last Log Date: </strong>{{$last_logs_date['time']}}</div>
       @endif
    </div>
</div>
<div class="clearix"></div>
<div class="row">
    <div class="col-md-3" style="padding: 0">
        <div class="card">
            <div class="card-body">
                @if(isset($users['profile_pic']))
                    <div class=" avtar text-center">
                        <img src="/upload/<?= $users['profile_pic'] ?>" alt="" style="width:150px;height:150px;margin-bottom: 20px">
                        <a href="http://timeloggerapp.thinktanker.in/user/profile/upload-photo/{{$users['id']}}" class=""></a>
                    </div>
                @else
                    @if($users['gender'] === 'male')
                        <div class=" avtar text-center">
                            <img src="/images/user_male.png" alt="" style="width:150px;height:150px;margin-bottom: 20px">
                            <a href="http://timeloggerapp.thinktanker.in/user/profile/upload-photo/{{$users['id']}}" class=""></a>
                        </div>
                    @else
                        <div class=" avtar text-center">
                            <img src="/images/user_female.png" alt="" style="width:150px;height:150px;margin-bottom: 20px">
                            <a href="http://timeloggerapp.thinktanker.in/user/profile/upload-photo/{{$users['id']}}" class=""></a>
                        </div>
                    @endif
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-9">
        <div class="card">
            <h3 class="card-title">Update</h3>
            <div class="card-body">
                <?= Form::model($users,['route'=>['user.update',$users->id],'files'=> true , 'class' => 'form-horizontal','method'=>'put']) ?>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="inputGender">Gender</label>
                    <div class="col-sm-9">
                        <?= Form::radio('gender', 'male',old('gender')); ?> Male
                        <?= Form::radio('gender', 'female',old('gender')); ?> Female
                        <?= $errors->first('gender',"<span class='text-danger'>:message</span>");?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="input Fullname">FullName</label>
                    <div class="col-sm-9">
                        <?= Form::text('fullname',old('fullname'),['class'=>'form-control' , 'placeholder' => 'FullName' ]); ?>
                        <?= $errors->first('fullname', "<span class='text-danger'>:message</span>"); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="inputPosition Name">Position</label>
                    <div class="col-sm-9">
                    <?= Form::select('position',$position,old('position_name') ,['class' => 'form-control select2','id'=>'position']); ?>
                        <?= $errors->first('position',"<span class='text-danger'>:message</span>");?>
                        <?= $errors->first('position',"<span class='text-danger'>:message</span>");?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="inputDate of Birth">Date of Birth</label>
                    <div class="col-sm-9">
                        <?= Form::text('birthdate', old('birthdate'), ['class' => 'form-control','id'=>'birth_Date','placeholder' => 'Date of Birth']); ?>
                        <?= $errors->first('birthdate',"<span class='text-danger'>:message</span>");?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="inputEmail">Email</label>
                    <div class="col-sm-9">
                        <?= Form::text('personal_email', old('personal_email'), ['class' => 'form-control', 'placeholder' => 'Email']); ?>
                        <?= $errors->first('personal_email',"<span class='text-danger'>:message</span>");?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="inputContact No">Contact No</label>
                    <div class="col-sm-9">
                        <?= Form::text('contact', old('contact'), ['class' => 'form-control', 'placeholder' => 'Contact No']); ?>
                        <?= $errors->first('contact',"<span class='text-danger'>:message</span>");?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="inputEmergency No">Emergency Contact No</label>
                    <div class="col-sm-9">
                        <?= Form::text('emergency_contact',old('emergency_contact'), ['class' => 'form-control', 'placeholder' => 'Emergency Contact No']); ?>
                        <?= $errors->first('emergency_contact',"<span class='text-danger'>:message</span>");?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="inputEmergency No">Address</label>
                    <div class="col-sm-9">
                        <?= Form::text('address', old('address'), ['class' => 'form-control', 'placeholder' => 'Address']); ?>
                        <?= $errors->first('address',"<span class='text-danger'>:message</span>");?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="inputEmail">Pay Code</label>
                    <div class="col-sm-9">
                        <?= Form::text('pay_code', old('pay_code'), ['class' => 'form-control', 'placeholder' => 'Pay Code']); ?>
                        <?= $errors->first('pay_code',"<span class='text-danger'>:message</span>");?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="inputJoiningDate">Joining Date</label>
                    <div class="col-sm-9">
                        @if($users['joining_date'] == null)
                        <?= Form::text('joining_date',$todayDate, ['class' => 'form-control', 'placeholder' => 'JoiningDate','id'=>'joining_date']); ?>
                        @elseif($users['joining_date'] == "01-01-1970")
                        <?= Form::text('joining_date',$todayDate, ['class' => 'form-control', 'placeholder' => 'JoiningDate','id'=>'joining_date']); ?>
                        @else
                        <?= Form::text('joining_date',old('joining_date'), ['class' => 'form-control', 'placeholder' => 'JoiningDate','id'=>'joining_date']); ?>
                        @endif
                        <?= $errors->first('joining_date',"<span class='text-danger'>:message</span>");?>
                    </div>
                </div>
                <div class="form-group">
                    <label title="Employee Resign Date" class="control-label col-sm-3" for="inputJoiningDate">Resign Date</label>
                    <div class="col-sm-9">
                        <?= Form::text('resign_date',old('resign_date'), ['class' => 'form-control', 'placeholder' => 'Resign Date','id'=>'resign_date']); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label title="Last Date Serving After Resign" class="control-label col-sm-3" for="inputJoiningDate">Last Serving Date</label>
                    <div class="col-sm-9">
                        <?= Form::text('serving_date',old('serving_date'), ['class' => 'form-control', 'placeholder' => 'Last Serving Date','id'=>'serving_date']); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label title="for remarks" class="control-label col-sm-3" for="inputJoiningDate">Remarks</label>
                    <div class="col-sm-9">
                        <?= Form::text('remarks',old('remarks'), ['class' => 'form-control', 'placeholder' => 'Enter Remarks','id'=>'remarks']); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label title="for Bank" class="control-label col-sm-3" for="inputJoiningDate">Name As Bank</label>
                    <div class="col-sm-9">
                        <?= Form::text('passbook_username',old('passbook_username'), ['class' => 'form-control', 'placeholder' => 'Enter passbook name','id'=>'passbook_username']); ?>
                    </div>
                </div>
                 <div class="form-group">
                    <label title="for Bank" class="control-label col-sm-3" for="inputJoiningDate">Bank Name</label>
                    <div class="col-sm-9">
                        <?= Form::text('bank_name',old('bank_name'), ['class' => 'form-control', 'placeholder' => 'Enter Bank Name','id'=>'bank_name']); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label title="for Bank" class="control-label col-sm-3" for="inputJoiningDate">Account No</label>
                    <div class="col-sm-9">
                        <?= Form::text('account_no',old('account_no'), ['class' => 'form-control', 'placeholder' => 'Enter Account No','id'=>'account_no']); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label title="for Bank" class="control-label col-sm-3" for="inputJoiningDate">IFS Code</label>
                    <div class="col-sm-9">
                        <?= Form::text('ifs_code',old('ifs_code'), ['class' => 'form-control', 'placeholder' => 'Enter IFS Code','id'=>'ifs_code']); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label title="for Document" class="control-label col-sm-3" for="inputJoiningDate">Addhar No</label>
                    <div class="col-sm-9">
                        <?= Form::text('aadhar_no',old('aadhar_no'), ['class' => 'form-control', 'placeholder' => 'Enter Aadhar No','id'=>'aadhar_no']); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label title="for Document" class="control-label col-sm-3" for="inputJoiningDate">Pancard No</label>
                    <div class="col-sm-9">
                        <?= Form::text('pancard_no',old('pancard_no'), ['class' => 'form-control', 'placeholder' => 'Enter PanCard No','id'=>'pancard_no']); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label title="for Document" class="control-label col-sm-3" for="inputJoiningDate">Aadhar Card</label>
                    <div class="col-sm-9">
                        <?= Form::file('aadhar_img[]',array('class' => 'form-control','id'=>'aadhar_img','multiple' => true)); ?>
                        <!-- <input type="file" name="aadhar_img[]" id="aadhar_img" multiple> -->
                    </div>
                </div>
                <div class="form-group">
                    <label title="for Document" class="control-label col-sm-3" for="inputJoiningDate">Pan Card</label>
                    <div class="col-sm-9">
                        <?= Form::file('pancard_img',old('pancard_img'), ['class' => 'form-control','id'=>'pancard_img']); ?>
                    </div>
                </div>
                @if($split_img[0] != null)
                <div class="form-group">
                    <label title="for Document" class="control-label col-sm-3 " for="inputJoiningDate">Addhar </label>
                    
                    <div class="col-sm-4">
                        <a href="/document/{{ $split_img[0] }}" download rel="noopener noreferrer" class="btn btn-primary" target="_blank">Download Aadhar1</a>
                    </div>
                    
                    @if(count($split_img)== 2)
                    <div class="col-sm-4">
                        <a href="/upload/Documents/{{ $split_img[1] }}" download rel="noopener noreferrer" class="btn btn-primary" target="_blank">Download Aadhar2</a>
                    </div>
                    @endif
                </div>
                @endif
                @if($users['pancard_img'] != null)
                <div class="form-group">
                    <label title="for Document" class="control-label col-sm-3 " for="inputJoiningDate">PanCard </label>
                    
                    <div class="col-sm-9">
                        <a href="/document/{{ $users['pancard_img'] }}" download rel="noopener noreferrer" class="btn btn-primary" target="_blank">Download PanCard</a>
                    </div>
                </div>
                @endif
                <div class="form-group">
                    <div class="col-md-9 col-md-offset-3">
                    <button type="submit" class="btn btn-primary" value="save_exit" name="save_button">Save & Exit</button>
                    <button type="submit" class="btn btn-primary" value="save" name="save_button">Save</button>
                   <a href="<?=URL::route('user.index',['type'=>'currentemp'])?>">
                    <!-- <button type="submit" class="btn btn-white btn-default" value="cancel" id="" name="cancel">Cancel</button> -->
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    </a>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('style')
<?= Html::style('css/colorbox.css') ?>
<?= Html::style('asset/css/bootstrap-datepicker.css') ?> 
@stop
@section('script')
<?= Html::script('asset/js/bootstrap-datepicker.min.js') ?>
<?= Html::script('js/jquery.colorbox-min.js') ?>
<?= Html::script('asset/js/select2.min.js') ?>
<script type="text/javascript">
    $( document ).ready(function(){
        var currentDate = new Date();  
        //birth date
        $('#birth_Date').datepicker({
            format: "dd-mm-yyyy",
            autoclose: true,
            todayHighlight: true,
            endDate:currentDate,
        });
        
        //joining date

        var joining_date = "{{ old('joining_date',$users['joining_date'])}}";
        console.log(joining_date);
        $('#joining_date').datepicker({
            format: "dd-mm-yyyy",
            autoclose: true,
            todayHighlight: true,
            endDate:currentDate,
        });

        // resign date
        var resign_date = "{{ old('resign_date',$users['resign_date'])}}";
        
        $('#resign_date').datepicker({
            format: "dd-mm-yyyy",
            autoclose: true,
            todayHighlight: true,
        });
        $("#resign_date").datepicker("setDate",resign_date);

        //serving date
        var serving_date = "{{ old('serving_date',$users['serving_date'])}}";
        $('#serving_date').datepicker({
            format: "dd-mm-yyyy",
            autoclose: true,
            todayHighlight: true,
        });
        $("#serving_date").datepicker("setDate",serving_date);

        parent.$.fn.colorbox.close();

        $(".iframe").colorbox({
            iframe   : true,
            width    : "70%",
            height   : "80%",
            onClosed : function(){ location.reload(true); }
        });
    });  

    //position
    $("#position")
    .select2({
        placeholder: 'Select type',
        width: '100%',
        minimumResultsForSearch: Infinity
      })
    .on('select2:close', function() {
        var el = $(this);
        if(el.val()==="NEW") {
          var newval = prompt("Enter New Position: ");
          if(newval !== null) {
            $.ajax({
                url     : "{{URL::route('position.store')}}",
                type    : 'post',
                dataType: 'html',
                data    :{
                            'newval': newval,
                            '_token':'{{csrf_token()}}'
                        },
                success    : function(resp) 
                {
                    el.append('<option value='+resp+'>'+newval+'</option>')
                      .val(newval);
                }
            });
          }
        }
    });  
</script>
<?= Html::script('asset/js/form.demo.min.js') ?>
@include('partials.alert')
@stop