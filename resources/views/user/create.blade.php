@extends('layout.layout')
@section('content')
<div class="page-title">
    <div>
    <h1>Empolyee</h1>
        <ul class="breadcrumb side">
            <li><i class="fa fa-home fa-lg"></i></li>
            <li>Create Employee</li>
        </ul>
    </div>
</div>
<div class="clearix"></div>
<div class="row">
    <div class="col-md-3" style="padding: 0">
        <div class="card">
            <div class="card-body">
                <div class=" avtar text-center">

                    @if(\Session::has('image_name'))
                        <img src="/temp_image/<?=\trim(Session::get('image_name'))?>" alt="" style="width:150px;height:150px;margin-bottom: 20px">
                        <!-- <a href="http://timeloggerapp.thinktanker.in/user/profile/upload-photo" class="btn btn-primary iframe cboxElement">Change Photo</a>  -->
                        <a href="<?= URL::route('user.profile.upload.get') ?>" class="btn btn-primary iframe">Change Photo</a>  
                    @else
                        <img src="/images/user-icon.jpg" alt="" style="width:150px;height:150px;margin-bottom: 20px">
                        <!-- <a href="http://timeloggerapp.thinktanker.in/user/profile/upload-photo" class="btn btn-primary iframe cboxElement">Upload Photo</a> -->
                        <a href="<?= URL::route('user.profile.upload.get') ?>" class="btn btn-primary iframe">Add Photo</a>
                    @endif
                </div>
                <span class='text-danger' id="image_error"></span>
            </div>
        </div>
    </div>
    <div class="col-md-9">
        <div class="card">
        <h3 class="card-title">Create</h3>
        <div class="card-body">
            <?= Form::open(array('class' => 'form-horizontal' ,'files' => true,'id'=>'usercreate')) ?>
            {{ session('msg') }}
     	<div class="form-group">
            <label class="control-label col-sm-3" for="inputGender">Gender</label>
            <div class="col-sm-9">
                <?= Form::radio('gender', 'male','true'); ?> Male
             	<?= Form::radio('gender', 'female'); ?> Female
            </div>
        </div>         
        <div class="form-group">
            <label class="control-label col-sm-3" for="inputFull Name">Full Name</label>
            <div class="col-sm-9">
                <?= Form::text('fullname',old('fullname'), ['class' => 'form-control', 'placeholder' => 'Full Name']); ?>
                <span class='text-danger' id="fullname_error"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="inputposition Name">Position</label>
            <div class="col-sm-9">
               <?= Form::select('position',$position,old('position') ,['class' => 'form-control select2','id'=>'position']); ?>
               <span class='text-danger' id="position_error"></span>
            </div>
        </div>
        <div class="form-group">
         	<label class="control-label col-sm-3" for="inputDate of Birth">Date of Birth</label>
            <div class="col-sm-9">
                <?= Form::text('birthdate',old('birthdate'), ['class' => 'form-control', 'id'=>'birth_Date','placeholder' => 'Date of Birth' , 'autocomplete'=>'off']); ?>
                <span class='text-danger' id="birthdate_error"></span>
            </div>
        </div> 
        <div class="form-group">
            <label class="control-label col-sm-3" for="inputEmail">Email</label>
            <div class="col-sm-9">
                <?= Form::email('email',old('email'), ['class' => 'form-control', 'placeholder' => 'Email']); ?>
                <span class='text-danger' id="email_error"></span>
            </div>
        </div>    
        <div class="form-group">
            <label class="control-label col-sm-3" for="inputPassword">Password</label>
            <div class="col-sm-9">
                <?= Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password']); ?>
                <span class='text-danger' id="password_error"></span>
            </div>
        </div>  
        <div class="form-group">
            <label class="control-label col-sm-3" for="inputConfirmPassword">Confirm Password</label>
            <div class="col-sm-9">
                <?= Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Password']); ?>
                <span class='text-danger' id="password_confirmation_error"></span>
            </div>
        </div> 
        <div class="form-group">
            <label class="control-label col-sm-3" for="inputContact No">Contact No</label>
            <div class="col-sm-9">
                <?= Form::text('contact',old('contact'), ['class' => 'form-control', 'placeholder' => 'Contact No']); ?>
                <span class='text-danger' id="contact_error"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="inputEmergency No">Emergency Contact No</label>
            <div class="col-sm-9">
                <?= Form::text('emergency_contact',old('emergency_contact'), ['class' => 'form-control', 'placeholder' => 'Emergency Contact No']); ?>
                <span class='text-danger' id="emergency_contact_error"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="inputAddress">Address</label>
            <div class="col-sm-9">
                <?= Form::text('address',old('address'), ['class' => 'form-control', 'placeholder' => 'Address']); ?>
                <span class='text-danger' id="address_error"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="inputJoiningDate">Joining Date</label>
            <div class="col-sm-9">
                <?= Form::text('joining_date',old('joining_date'), ['class' => 'form-control', 'placeholder' => 'JoiningDate','id'=>'joining_date','autocomplete'=>'off']); ?>
                <span class='text-danger' id="joining_date_error"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="inputHobby">Hobby</label>
            <div class="col-sm-9">
                <?= Form::text('hobby',old('hobby'), ['class' => 'form-control', 'placeholder' => 'Hobby']); ?>
                <span class='text-danger' id="hobby_error"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="inputJoiningDate">Time Machine Code</label>
            <div class="col-sm-9">
                <?= Form::text('code',old('code'), ['class' => 'form-control', 'placeholder' => 'Time Machine Code','id'=>'code']); ?>
                <span class='text-danger' id="code_error"></span>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-9 col-md-offset-3">
                <!-- <button type="button" class="btn btn-primary submit" id="submit" name="save_button" value="save_new">Save & New</button> -->
                <button type="button" class="btn btn-primary submit" id="submit1" name="save_button" value="save_exit">Save & Exit</button>
                <a href="<?=URL::route('user.index',['type'=>'currentemp'])?>">
                    <button type="button" class="btn btn-white btn-default" value="cancel" id="" name="cancel">Cancel</button>
                </a>
                {!! Form::close() !!}
            </div>
        </div>
        </div>
        </div>
    </div>
</div>    
@stop
@section('style')
<?= Html::style('asset/css/bootstrap-datepicker.css') ?> 
<?= Html::style('css/colorbox.css') ?>
@stop
@section('script')
<!-- <script src="https://code.jquery.com/jquery-1.10.2.js"></script> -->
<?= Html::script('asset/js/bootstrap-datepicker.min.js') ?>
<?= Html::script('js/jquery.colorbox-min.js') ?>
<!-- <script src="http://malsup.github.com/jquery.form.js"></script>  -->
<!-- <script src="http://malsup.github.io/jquery.form.js"></script>  -->
<?= Html::script('asset/js/malsup.github.io.js') ?>
<?= Html::script('asset/js/select2.min.js') ?>

<script type="text/javascript">
    jQuery(document).ready(function() {

        var birthdate = "{{ old('birthdate')}}";
        var currentDate = new Date();  
        $('#birth_Date').datepicker({
            format: "dd-mm-yyyy",
            autoclose: true,
            todayHighlight: true,
            endDate:currentDate,
        });

        var joining_date = "{{ old('joining_date')}}";
        var currentDate = new Date();  
        $('#joining_date').datepicker({
            format: "dd-mm-yyyy",
            autoclose: true,
            todayHighlight: true,
            endDate:currentDate,
        });

        if('<?= Session::has('image_name')?>'){
            arr = [''];
        }else{
            arr = ['Image field is required'];
        }
        $('.submit').click(function(){
            button_id = $(this).attr('id');
            token = '<?= csrf_token()?>';
            $('#usercreate').ajaxSubmit({
                url: "<?=route('user.store')?>",
                type: 'POST',
                data: { "_token" : token,'id':button_id},
                dataType: 'json',
                beforeSubmit:function(){
                    $('.pace-inactive').css('display','block');
                },
                success : function(resp){
                    if(resp == "new"){
                        window.setTimeout(function(){window.location.href = '<?=URL::route('user.create',['type'=>'currentemp'])?>';},3000)
                    }else{
                        window.location.href = '<?=URL::route('user.index',['type'=>'currentemp'])?>';
                    }
                    $('.pace-inactive').css('display','none');

                    iziToast.success({
                        title:'Ok',
                        message: "User created Successfully",
                    });
                },
                error : function(respObj){
                    iziToast.error({
                        title:'Ok',
                        message: "There were some error!",
                    });
                    $("[id$='_error']").empty();
                    respObj.responseJSON['image'] = arr;
                    $.each(respObj.responseJSON, function(k,v){
                        $('#'+k+'_error').text(v);
                    });
                    $('.pace-inactive').css('display','none');
                }

            });
        });
        parent.$.fn.colorbox.close();
        $(".iframe").colorbox({
            iframe   : true,
            width    : "70%",
            height   : "80%",
            onClosed : function(){ parent.location.reload(true); }
        });
    });

    //position
    $('#position').prepend('<option value="">Select Positions</option>');
    $("#position").val($(".select2 option:first").val());
    $("#position")
    .select2({
        placeholder: 'Select type',
        width: '100%',
        minimumResultsForSearch: Infinity
      })
    .on('select2:close', function() {
        var el = $(this);
        if(el.val()==="NEW") {
          var newval = prompt("Enter New Positions: ");
          if(newval !== null) {
            $.ajax({
                url     : "{{URL::route('position.store')}}",
                type    : 'post',
                dataType: 'html',
                data    :{
                            'newval': newval,
                            '_token':'{{csrf_token()}}'
                        },
                success    : function(resp) 
                {
                    el.append('<option value='+resp+'>'+newval+'</option>')
                      .val(newval);
                }
            });
          }
        }
    });

</script>
<?= Html::script('asset/js/form.demo.min.js') ?>
@include('partials.alert')
@stop