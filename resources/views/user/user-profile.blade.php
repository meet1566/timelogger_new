@extends('layout.layout')
@section('content')
<!-- <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))
    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
    @endif
    @endforeach
    </div> -->
<style type="text/css">
    .avtar{
    background-color: #fff;
    padding:20px 0;
    }
    .avtar button.btn-primary {
    float: none;
    width: 80%;
    margin: 20px auto;
    }
    .avtar form {
    float: none;
    width: 90%;
    margin: auto;
    }
    .profile-detail{
    width:100%;
    background-color: #fff;
    padding:20px 0;
    }
    .profile-detail .dl-horizontal, .profile-detail .dd-horizontal {
    margin: 10px 0;
    }
    .profile-detail label {
    float: right;
    font-size: 16px;
    margin: 19px 0;
    }
    .profile-detail input {
    float: left;
    width: 98%;
    margin: 10px 0;
    }
    .profile-detail textarea {
    width: 98%;
    }
    .profile-detail button {
    float: left;
    }
    .checkbox {
    float: left;
    width: 98%;
    text-align: left;
    }
    .checkbox label {
    float: left;
    width: auto;
    margin: 0;
    }
    .checkbox input[type="checkbox"] {
    float: left;
    width: auto;
    margin: 4px 0;
    }
    .dl-horizontal.dd-horizontal > dd {
    width: 100%;
    text-align: left;
    }
    .leave{
    align-content: center;
    margin-top: 20px;
    width: 990px;
    margin-left: 2px;
    margin-bottom: -40px;
    }

    .page-title{
        position: fixed;
        width: 100%;
        z-index: 1;
        margin-bottom: 74px;
    }
    .page-title1{
       /* position: fixed;*/
        width: 100%;
        z-index: 1;
        margin-bottom: 74px;
    }
    .row1{
        margin-top: 64px;
    }
     .row2{
        padding-top: 0;
    }
     .row3{
         width: 100%;
    }
    #employee_filter{
      display: none;
    }
    .bankdetails_container{
        display: flex;
        padding: 30px 0;
    }
    .left_bankdetails, .right_documnent{
        width: 50%;
        background: #fff;
    }
    .left_bankdetails{
        margin-right: 15px;
    }
    
    .left_bankdetails .row .col-sm-3,.right_documnent .row .col-sm-3{
        margin-left:20px ;
    }
</style>

<div class="page-title">
    <div>
        <h1> {{ucwords($user_details['fullname'])}}</h1>
    </div>
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">Leaves</button>
</div>
<div class="row row1">
    <div class="col-md-12" style="display: flex;">
        <div class="profile-detail">
                <h3 align="center">Basic Details</h3>
            <div class="row">
                <div class="col-sm-3">
                    <dl class="dl-horizontal ">
                        <dt>Name:</dt>
                    </dl>
                </div>
                <div class="col-sm-9">
                    <dd class="dd-horizontal">
                        {{ ucwords($user_details['fullname']) }}
                    </dd>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <dl class="dl-horizontal">
                        <dt>Gender:</dt>
                    </dl>
                </div>
                <div class="col-sm-9">
                    <dd class="dd-horizontal">
                    {{ ucwords($user_details['gender']) }}
                    </dd>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <dl class="dl-horizontal">
                        <dt>Position:</dt>
                    </dl>
                </div>
                <div class="col-sm-9">
                    <dd class="dd-horizontal">
                        {{ ucwords($user_details['position']) }}
                    </dd>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <dl class="dl-horizontal">
                        <dt>BirthDate:</dt>
                    </dl>
                </div>
                <div class="col-sm-9">
                    <dd class="dd-horizontal">
                        {{$user_details['birthdate']}}
                    </dd>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <dl class="dl-horizontal dd-horizontal">
                        <dt>Email:</dt>
                    </dl>
                </div>
                <div class="col-sm-9">
                    <dd class="dd-horizontal">
                       <a style="color:black;" href="mailto:{{$user_details['email']}}">{{$user_details['email']}}</a>
                    </dd>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <dl class="dl-horizontal">
                        <dt>Contact:</dt>
                    </dl>
                </div>
                <div class="col-sm-9">
                    <dd class="dd-horizontal">
                       <a style="color:black;" href="tel:{{$user_details['contact']}}">{{$user_details['contact']}}</a>
                    </dd>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <dl class="dl-horizontal">
                        <dt>Emergency Contact:</dt>
                    </dl>
                </div>
                <div class="col-sm-9">
                    <dd class="dd-horizontal"></dd>
                    <a style="color:black;" href="tel:{{$user_details['emergency_contact']}}">
                    {{$user_details['emergency_contact']}}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <dl class="dl-horizontal">
                        <dt>Address:</dt>
                    </dl>
                </div>
                <div class="col-sm-9">
                    <dd class="dd-horizontal"></dd>
                     {{ ucwords($user_details['address']) }}
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-sm-offset-3 text-left">
                    <a href="<?= URL::route('dashboard.admin') ?>" class="btn btn-white btn-default">Back</a>
                </div>
            </div>
        </div>
        <div class="avtar text-center" style="background-color: white;">
            <br>
            <br>
        <img src="/upload/{{$user_details['profile_pic']}}" alt="" style="width:200px;height:200px; margin: 19px 75px;">
        </div>
    </div>
</div>
<div class="bankdetails_container">
    <div class="left_bankdetails" style="font-size: 16px;">
        <h3 align="center">Bank Details</h3>
        <div class="row" style="padding-top:30px;">
            <div class="col-sm-3">
                <dl class="dl-horizontal dd-horizontal">
                    <label>Name as per Bank Passbook:</label>
                </dl>
            </div>
            <div class="col-sm-8">
                <dd class="dd-horizontal">
                   {{ $user_details['passbook_username'] }}
               </dd>
           </div>
       </div>
       <div class="row">
            <div class="col-sm-3">
                <dl class="dl-horizontal dd-horizontal">
                    <label>Bank Name:</label>
                </dl>
            </div>
            <div class="col-sm-8">
                <dd class="dd-horizontal">
                   {{ $user_details['bank_name'] }}
               </dd>
           </div>
       </div>
       <div class="row">
            <div class="col-sm-3">
                <dl class="dl-horizontal dd-horizontal">
                    <label>Account No:</label>
                </dl>
            </div>
            <div class="col-sm-8">
                <dd class="dd-horizontal">
                   {{ $user_details['account_no'] }}
               </dd>
           </div>
       </div>
       <div class="row">
            <div class="col-sm-3">
                <dl class="dl-horizontal dd-horizontal">
                    <label>IFSC Code:</label>
                </dl>
            </div>
            <div class="col-sm-8">
                <dd class="dd-horizontal">
                   {{ $user_details['ifs_code'] }}
               </dd>
           </div>
       </div>
    </div>
    <div class="right_documnent">
        <h3 align="center">Document</h3>
        <div class="row">
            <div class="col-sm-6" style="display:flex; justify-content: space-evenly;">
                <dl class="dl-horizontal dd-horizontal">
                    <label>Aadhar No:</label>
                </dl>
                <dd class="dd-horizontal">
                   {{ $user_details['aadhar_no'] }}
               </dd>
            </div>
           <div class="col-sm-6" style="display:flex; justify-content: space-evenly;">
                <dd class="dd-horizontal">
                    <label>Pancard No:</label>
               </dd>
                <dd class="dd-horizontal">
                   {{ $user_details['pancard_no'] }}
               </dd>
           </div>
          
        </div>
        
        <div class="row">
            <div class="col-sm-6">
                <dd class="dd-horizontal" style="text-align: center;">
                @if($user_details['aadhar_img'] != NULL)
                  @foreach($split_img as $image)
                    <div style="margin-bottom:20px;"> 
                    <img src="/document/{{ $image }}" alt="" style="width:195px;height:120px;">
                    </div>
                    @endforeach
                @endif
               </dd>
           </div>
           <div class="col-sm-6" >
                <dd class="dd-horizontal" style="text-align: center;">
                  @if($user_details['pancard_img'] != NULL)
                    <img src="/document/{{$user_details['pancard_img']}}" alt="" style="width:195px;height:120px;">
                 @endif
               </dd>
           </div>
       </div>
        <br>
    </div>
</div>
<!--start model -->
<div class="modal fade modal-dialog modal-dialog-centered" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Laves Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <ul class="nav nav-pills">
            <li class="leave-sum active">
              <a href="javascript:;">Total Leaves 
              <span class="badge bg-green">{{$total_leaves}}<strong></strong></span></a>
            </li>                   
            <li class="leave-sum active">
              <a  href="javascript:;">Current Month Leaves
              <span class="badge bg-green">{{$current_month_leaves}}<strong></strong></span></a>
            </li>
          </ul>
      </div>
    </div>
  </div>
</div>
<!-- end model -->
<!-- logs table -->
<div class="row leave">
    <div>
        <ul class="breadcrumb side row3">
        <h3>Log History</h3>
          <!--   <li><i class="fa fa-home fa-lg"></i></li> -->
            <li class="active"><a href="<?= URL::route('show.logs',['id'=>':id']) ?>">Employee Logs</a></li>
            <li>
            <!-- <?= Form::selectMonth('month',date('m'),['id'=>'month','class' => 'form-control breadcrumb-select input-sm']); ?>
            <?= Form::selectRange('year','2013',date('Y'),date('Y'),['id'=>'year','class' => 'form-control breadcrumb-select input-sm']); ?> -->
            <select id="month" name="month" class="form-control breadcrumb-select input-sm month"></select>
            <select id="year" name="year" class="form-control breadcrumb-select input-sm"></select>
            <input type="hidden" name="month" value="<?= date('m')?>">
            </li>
        </ul>
    </div>
</div>
<div class="row leave">
    <div class="col-md-12" style="padding: 0;">
        <div class="card row2">
            <div class="card-body">
                <table id="employee" class="table table-hover table-bordered" border="1 px">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Day</th>
                            <th>Date</th>
                            <th>TimeIn</th>
                            <th>TimeOut</th>
                            <th>Hours</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>
<!-- / logs table -->

<!-- leaves chart -->
 <div class="row mt-20">
        <div class="col-md-12">
            <!-- <button id="showchart" type="button" class="btn btn-primary"  onclick="showchart()">Show Graph</button> -->
            <div class="card" style="height: 520px;" id="firstlevel">
                <div class="card-title-w-btn">
                    <h3 class="title" id="graph_title">Leaves</h3>
                    <p><button id="back" type="button" class="btn btn-primary" onclick="back()" style="display: inline-block;">Closed</button></p>
                </div>   
                <div class="embed-responsive embed-responsive-16by9">
                    <canvas class="embed-responsive-item" id="barChartDemo1"></canvas>
                </div>
            </div>
            <div class="card" style="height: 520px;" id="secondlevel">
                <div class="card-title-w-btn">
                    <h3 class="title" id="second_graph_title">Leaves</h3>
                    <p><button id="back" type="button" class="btn btn-primary" onclick="back()" style="display: inline-block;">Back Home</button></p>
                </div>   
                <div class="embed-responsive embed-responsive-16by9">
                    <canvas class="embed-responsive-item" id="barChartDemo2"></canvas>
                </div>
            </div>
            <div class="card" style="height: 520px;" id="thirdlevel">
                <div class="card-title-w-btn">
                    <h3 class="title" id="third_graph_title">Leaves</h3>
                    <p><button id="back1" type="button" class="btn btn-primary" onclick="back1()" style="display: inline-block;position: relative;z-index: 1">Back</button></p>
                </div>   
                <div class="embed-responsive embed-responsive-16by9" style="position: absolute;top: 0;left: 0;right: 0;bottom: 0;display: block;">
                    <canvas class="embed-responsive-item" id="barChartDemo3" style="width: 949px; height: 520px;"></canvas>
                </div>
            </div>
        </div>
    </div>
<!-- leaves --> 
<!-- logs table -->

<div class="row">
  <div class="col-md-12">
    <div class="card">
      <h3>Leave History</h3>
      <div class="card-body">
      <table id="employee1" class="table table-hover table-bordered" border="1 px">
      <thead>
        <tr>
        <th>Date</th>
        <th>Leave Type</th>
        <th>Leave Time</th>
        <th>Leave Purpose</th>
        <th>From Hour</th>
        <th>To Hour</th>
        <th>Status</th>
        </tr>
      </thead> 
      </table>
      </div>
    </div>
  </div>
</div>

<!--/ leaves -->   
<!-- / leaves chart -->
@stop
@section('script')
<?= Html::script('asset/DataTables/media/js/jquery.dataTables.js') ?>
<?= Html::script('asset/DataTables/media/js/dataTables.bootstrap.min.js') ?>
<?= Html::script('asset/js/plugins/sweetalert.min.js') ?>
<?= Html::script('asset/js/plugins/select2.min.js') ?>
<?= Html::script('asset/js/fnStandingRedraw.js') ?>
<?= Html::script('js/Chart.bundle.min.js') ?> 

<script type="text/javascript">
var token = "<?= csrf_token() ?>";
var path = '<?= URL::route('show.user-profile',['id'=>':id']) ?>';
path = path.replace(':id','<?= $id ?>');

var token1 = "<?= csrf_token() ?>";
var path1 = '<?= URL::route('show.leaves',['id'=>':id']) ?>';
path1 = path1.replace(':id','<?= $id ?>');

$(document).ready(function(){
    $('#employee').DataTable(
    {   
        "bProcessing" : true,
        "bServerSide" : true,  
        'bPaginate'   : false,
        "pageLength"  : 50,
        "ajax": {
            'url':path,
            'data':function(d){
                d.month=$('#month').val();
                d.year=$('#year').val();
            }
        },
        "aaSorting"   : false,
        "aoColumns"   :[ 
       
            { mData : 'current_date',bSortable:false},
            { mData : 'day',bSortable:false},
            { mData : 'time',bVisible:false,bSortable:false},
            { mData : 'check_in',bSortable:false},
            { mData : 'check_out',bSortable:false},
            { mData : 'time_diff',bSortable:false},
        ],
        rowCallback: function( row, data, index ,col) {
            console.log(data)
            var  $node = this.api().row(row).nodes().to$();
            var holidayDate = data.holidayDate;
            var weekday = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];

            var current_date = data.current_date;
                current_date = current_date.split("-").reverse().join("-");
            var format_date = formatDate(current_date)
            var date = new Date(format_date);
            var day = weekday[date.getDay()];
            var d = new Date(date);
            var getTot = daysInMonth(d.getMonth(),d.getFullYear()); //Get total days in a month
            var saturday_num = new Array();
            var sun = new Array();   
            for(var i=1;i<=getTot;i++)
            {   
                var newDate = new Date(d.getFullYear(),d.getMonth(),i)
                if(newDate.getDay()==0){   //if Sunday
                    sun.push(i);
                }
                if(newDate.getDay()==6){   //if Saturday
                 saturday_num.push(i);
                }
            }
            function daysInMonth(month,year)
            {
                return new Date(year, month, 0).getDate();
            }
            function convert(str) 
            {
                var date = new Date(str),
                mnth = ("0" + (date.getMonth() + 1)).slice(-2),
                day = ("0" + date.getDate()).slice(-2);
                return day;
            }

            function formatDate(date) {
                var d = new Date(date),
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();
                if (month.length < 2) 
                    month = '0' + month;
                if (day.length < 2) 
                    day = '0' + day;
                return [year, month, day].join('-');
            }

            var date2 = new Date(current_date);
            var n1 =convert(date2)
            
            if (data.time_diff < '09')
            {
                if(data.userleave ==  data.current_date && data.status == "a")
                {
                    $node.find("td:eq(4)").text('09:00:00');
                    $node.find("td:eq(2)").text('Paid Leaves').css('text-align','center');
                    $node.find("td:eq(2)").attr('colspan',2);
                    $node.find("td:eq(3)").remove();
                    $node.css('background-color','{{config('project.sat_code.code')}}');
                }else if(data.userleave ==  data.current_date && data.status == "d")
                {
                    $node.find("td:eq(4)").text('00:00:00');
                    $node.find("td:eq(2)").text('UnPaid Leaves').css('text-align','center');
                    $node.find("td:eq(2)").attr('colspan',2);
                    $node.find("td:eq(3)").remove();
                    $node.css('background-color','{{config('project.sat_code.code')}}');
                }
                else if(data.userleave ==  data.current_date && data.status == "p")
                {
                    //$node.find("td:eq(4)").text('09:00:00');
                    $node.find("td:eq(2)").text('Pending Leaves').css('text-align','center');
                    $node.find("td:eq(2)").attr('colspan',2);
                    $node.find("td:eq(3)").remove();
                    $node.css('background-color','{{config('project.sat_code.code')}}');
                }else
                {
                    $node.css('background-color','rgb(248 209 200)');
                }
            }

            var date3 = (new Date());
            date4 = convert1(date3)
            
            function convert1(str) {
              var date = new Date(str),
                  mnth = ("0" + (date.getMonth() + 1)).slice(-2),
                  day = ("0" + date.getDate()).slice(-2);
              return [day, mnth, date.getFullYear()].join("-");
            }

            if(date4 == data.current_date)
            {
                $node.css('background-color','white');
            }

            if(holidayDate != "" && day !=  "Sunday")
            {
                $(holidayDate).each(function(k,v)
                {    
                    if(data.current_date == v)
                    {
                        $node.css('background-color','#64dba7');
                        $node.find("td:eq(2)").text('Holiday');
                        $node.find("td:eq(2)").css('text-align','center');
                        $node.find("td:eq(2)").attr('colspan',3);
                        $node.find("td:eq(3)").remove();
                        $node.find("td:eq(1)").remove();
                    }   

                });
            }
            
            if (data.time_diff == "00:00:00")
            {
                if(data.userleave ==  data.current_date && data.status == "a")
                {
                   $node.find("td:eq(2)").text('Paid Leaves').css('text-align','center');
                   $node.find("td:eq(2)").attr('colspan',2);
                   $node.find("td:eq(4)").text('09:00:00');
                   $node.css('background-color','{{config('project.sat_code.code')}}');
                }else if(data.userleave ==  data.current_date && data.status == "d")
                {
                   $node.find("td:eq(2)").text('UnPaid Leaves').css('text-align','center');
                   $node.find("td:eq(2)").attr('colspan',2);
                   $node.find("td:eq(4)").text('00:00:00');
                   $node.css('background-color','{{config('project.sat_code.code')}}');
                }
                else if(data.userleave ==  data.current_date && data.status == "p")
                {
                   //$node.find("td:eq(4)").text('09:00:00');
                   $node.find("td:eq(2)").text('Pending Leaves').css('text-align','center');
                   $node.find("td:eq(2)").attr('colspan',2);
                   $node.find("td:eq(3)").remove();
                   $node.css('background-color','{{config('project.sat_code.code')}}');
                }else
                {
                    $node.css('background-color','rgb(248 209 200)');
                }
            }

            if(new Date() <= date){
                   $node.css('background-color','white');
            }
            
            if (day ==  "Sunday")
            {
               $node.css('background-color','#64dba7');
               $node.find("td:eq(2)").text('Sunday');
               $node.find("td:eq(2)").css('text-align','center');
               $node.find("td:eq(2)").attr('colspan',3);
               $node.find("td:eq(3)").remove();
               $node.find("td:eq(1)").remove();              
            }
            if (day ==  "Saturday" && saturday_num[1] == n1)
            {
               $node.css('background-color','#64dba7');
               $node.find("td:eq(2)").text('Saturday');
               $node.find("td:eq(2)").css('text-align','center');
               $node.find("td:eq(2)").attr('colspan',3);
               $node.find("td:eq(3)").remove();
               $node.find("td:eq(1)").remove();
            }

            if (day ==  "Saturday" && saturday_num[3] == n1)
            {
               $node.css('background-color','#64dba7');
               $node.find("td:eq(2)").text('Saturday');
               $node.find("td:eq(2)").css('text-align','center');
               $node.find("td:eq(2)").attr('colspan',3);
               $node.find("td:eq(3)").remove();
               $node.find("td:eq(1)").remove();
            }

        }
    });

    $('#month').change(function(){
        $('#month').on('click',function(){
           $('#monthtimetotal').val('time_diff');
        }); 
        $('.dataTable').each(function() {
            dt = $(this).dataTable();
            dt.fnDraw();
        });
    });
    $('#year').change(function(){
        $('.dataTable').each(function() {
            dt = $(this).dataTable();
            dt.fnDraw();
        });
    });
});




/*leaves*/
  $('#employee1').DataTable(
  {
    "bProcessing" : true,
    "bServerside" : true,
    "ajax"        :{
        'url': path1
    },
    "aaSorting": false,
    "aoColumns"   :[
        { mData: 'date',sWidth: "10%",bSortable:false},
        { mData: 'leave_type',sWidth: "10%",bSortable:false},
        { mData: 'leave_time',sWidth: "5%",bSortable:false},
        { mData: 'leave_purpose',sWidth: "35%",bSortable:false},
        { mData: 'from_hour',sWidth: "10%",bSortable:false},
        { mData: 'to_hour',sWidth: "10%",bSortable:false},
        {
            "mData": "status",
            bSortable:false,
            sClass: "text-center",
            sWidth: "10%",
            mRender:function(v,t,o)
            {
                if (v == 'a') {
                    return '<span class="badge bg-green">Approve</span>';
                } else if(v == 'd'){
                    return '<span class="badge bg-red" style="background-color: #fc5c3c;">UnPaid</span>';
                } else {
                    return '<span class="badge bg-yellow">Pending</span>';
                }
            }
        },
                
    ],
});

/*/leaves*/
    /*chart*/
    $('#firstlevel').hide();
    $('#secondlevel').hide();
    $('#thirdlevel').hide();
    //all user missing logs
    var leaves_count_logs = [<?= $leaves_count_logs ?>];
    var username = [<?= $username?>];
    var fullnames = <?= json_encode($fullname)?>;
    $('#back').hide();
    $('#back1').hide();

    function back(){
        //location.reload();
         $('#firstlevel').hide();
        $('#back').hide();
        $('#graph_title').text('Leaves');
    }
    /*first chart*/
    var number = [];
    number[0] = <?= $user_details['id']; ?>;
    var canvas = document.getElementById("barChartDemo1");
    var chartdepth = 0;
    var myBar;
    var barChartData2;
    $.ajax({
        type: "POST",
        url: '<?= URL::route('dashboard.leaves.chart') ?>',
        data: { "_token" :'<?=csrf_token()?>','id': number[0]},

    success: function(data) {
        var count_leaves = [data.count_leaves];
        var year = [data.year];
        barChartData2 = {
            labels: '',
            datasets: [{
                label: 'Leaves',
                backgroundColor: "<?= config('project.error_code.code')?>",
                data: '',
            }]
        }
        barChartData2.datasets[0].data = count_leaves[0];
        barChartData2.labels = year[0];
        var context = $("#barChartDemo1").get(0).getContext("2d");
        myBar = new Chart(context, {
            type: 'bar',
            data: barChartData2,
            options: {
                title: {
                    display: true,
                    text: ""
                },
                tooltips: {
                    enabled:true,
                    mode: 'label',
                },
                scales: {
                    yAxes: [{
                        display: true,
                        ticks: {
                            beginAtZero: true,
                            steps: 1,
                            stepSize:1,
                            labelString: 'text',
                        },   
                    }],
                    xAxes: [{
                        ticks: {
                            callback: function(value) {
                                return value;//truncate
                            },
                        }
                    }],
                },
                responsive: true,
            }
        });
        if(data.fullname != null){
            $('#second_graph_title').text('Leaves ' + data.fullname);
        }
        }
    });

    function back1(){
      $('#thirdlevel').hide();
      $('#firstlevel').show();
      $('#secondlevel').hide();
    }
    function showchart(){
      $('#thirdlevel').hide();
      $('#firstlevel').toggle();
      $('#secondlevel').hide();
    }
    /*second chart*/
        var canvas1 = document.getElementById("barChartDemo1");
        var chartdepth = 0;
        canvas1.onclick = function(evt) {
        var activePoints = myBar.getElementsAtEvent(evt);
        if(activePoints.length != 0){
        var year = activePoints[0]['_model']['label'];

        $('#back1').show();
        $('#back').show();
        $('#thirdlevel').show();
        $('#firstlevel').hide();
        $('#secondlevel').hide();
        var user_id = <?= $user_details['id']; ?>;
        $.ajax({
            type: "POST",
            url: '<?= URL::route('dashboard.leaves.secondchart') ?>',
            data: { "_token" :'<?=csrf_token()?>','id': user_id,'year':year},
            success: function(data) {
              var count_leaves = [data.count_leaves];
              var month = [data.month];
              var fullname = data.fullname;
              var barChartData3 = {
                  labels: '',
                  datasets: [{
                      label: 'Leaves',
                      backgroundColor: "<?= config('project.error_code.code')?>",
                      data: '',
                  }
                  ]
              }
              barChartData3.datasets[0].data = count_leaves[0];
              barChartData3.labels = month[0];
              var context2 = $("#barChartDemo3").get(0).getContext("2d");
              third_myBar = new Chart(context2, {
                  type: 'bar',

                  data: barChartData3,
                  options: {
                      title: {
                          display: true,
                          text: ""
                      },
                      tooltips: {
                          enabled:true,
                          mode: 'label',

                      },
                      scales: {
                          yAxes: [{
                                  display: true,
                                  ticks: {
                                      beginAtZero: true,
                                      steps: 1,
                                      stepSize:1,
                                      labelString: 'text',
                                  },   
                              }],
                              xAxes: [{
                                  ticks: {
                                      callback: function(value) {
                                          return value;//truncate
                                      },
                                  }
                              }],
                      },
                      responsive: true,
                  }
              });
              third_myBar.destroy();
                $('#third_graph_title').text('Leaves ' + fullname +'-'+ year);
            }
        });
    }
}

$(document).ready(function() 
{
  const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
  ];
  let qntYears = 9;
  let selectYear = $("#year");
  let selectMonth = $("#month");
  console.log(selectMonth)
  let currentYear = new Date().getFullYear();
  for (var y = 0; y < qntYears; y++)
  {
    let date = new Date(currentYear);
    let yearElem = document.createElement("option");
    yearElem.value = currentYear
    yearElem.textContent = currentYear;
    selectYear.append(yearElem);
    currentYear--;
  }
         
  var d = new Date();
  var getyears = d.getFullYear();
  var getmonth = d.getMonth();
  getmonth =  getmonth+1;

  var d = new Date();
  var month = d.getMonth();
  var year = d.getFullYear();
  
  AdjustMonth();
  selectMonth.val(year);

  selectYear.val(year);
  selectYear.on("change", AdjustMonth);
  selectMonth.val(month);

  function AdjustMonth() 
  {
    var setmonth =$(".month").val();
    console.log(setmonth)
    var year = selectYear.val();
    var month = parseInt(selectMonth.val()) + 1;
    selectMonth.empty();
    if(year == getyears)
    {
      for (var m = 0; m < getmonth; m++) 
      {
        let month1 = monthNames[m];
        let monthElem1 = document.createElement("option");
        monthElem1.value = m;
        monthElem1.textContent = month1;
        selectMonth.append(monthElem1);
        selectMonth.val(setmonth);
      }
    }else
    {
      for (var m = 0; m < 12; m++){
        let month1 = monthNames[m];
        let monthElem1 = document.createElement("option");
        monthElem1.value = m;
        monthElem1.textContent = month1;
        selectMonth.append(monthElem1);
        selectMonth.val(setmonth);
      }
    }
  }
});
</script>  
@stop