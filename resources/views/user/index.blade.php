@extends('layout.layout')
@section('content')
<style type="text/css">
    .btn_color{
        font-size: 11px !important;
        padding: 5px 10px !important;
        background-color: #3aaf98;
        color: white;
        margin-right: 5px;
    }
    .btn_color_dlt{
        font-size: 11px !important;
        padding: 5px 10px !important;
        background-color: #fc5c3c;;
        color: white;
        margin-right: 5px;
    }
    .card-body .nav .active a{
        background-color: white !important;
        color:white;
    }
    .card-body .nav .inactive a{
        background-color: #e5e5e5 !important;
        color:#009688;
    }
   .nav-tabs.custom-tab>li a {
      background-color: white !important;
    }
   
</style>
<div class="page-title">
    <div>
        <h1>Employees</h1>
        <ul class="breadcrumb side">
            <!-- <li><i class="fa fa-home fa-lg"></i></li> -->
        <!--     <li class="active"><a href="#">User Table</a></li> -->
            <li class="active"><a href="<?= URL::route('user.recyclebin')?>">Inactive Employees</a></li>
        </ul>
    </div>
    <div>
        <a href="<?=URL::route('user.create')?>" class="btn btn-primary btn-flat createuser"><i class="fa fa-lg fa-plus"></i></a>
    </div>
</div>
<div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))
            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
    @endforeach
</div>
<div class="row">
    <div class="col-md-12">
        <div class="modal fade" id="deactiveuser" role="dialog" tabindex="-1" aria-labelledby="leave-detailsLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close" onclick="closeModel()"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Deactive Employees</h3>
                </div>
                <div class="modal-body">
                    <?= Form::open(array('url' => route('user.deactive'),'class' => 'form-horizontal' ,'files' => true,'id'=>'user_deactive')) ?>
                        <input type="hidden" name="id" id="id" value=''>
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="inputDate of Birth">Date</label>
                            <div class="col-sm-9">
                                <?= Form::text('date', old('date'), ['class' => 'form-control','id'=>'date','placeholder' => 'Date','autocomplete' => 'off']); ?>
                                <span class="text-danger" id="date_error"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="inputDate of Birth">Note</label>
                            <div class="col-sm-9">
                                <?= Form::textarea('note', old('note'), ['class' => 'form-control','rows'=>'5','id'=>'reason','placeholder' => 'Note']); ?>
                                <span class="text-danger" id="note_error"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    <?Form::close()?>
                </div>
              </div>
            </div>
        </div>
        <div class="card p-0">
           <div class="card-body">
                <ul class="nav nav-tabs activation-tabs custom-tab">
                    <li @if(Request::get('type') == 'currentemp') class="active" @endif class="inactive" id="current_emp"><a href="<?= URL::route('user.index',['type'=>'currentemp']) ?>">Current Employee</a></li>
                    <li @if(Request::get('type') == 'pastemp') class="active" @endif class="inactive" id="past_emp"><a href="<?= URL::route('user.index',['type'=>'pastemp']) ?>">Past Employee</a></li>
                </ul>
                <div class="p-20">
                  <table id="employee" class="table table-hover table-bordered" border="1 px" style="width: 100%;">
                    <thead>
                        <tr>
                           <th>Id</th>
                           <th>Fullname</th>
                           <th>Position</th>
                           <th>Email</th>
                           <th>Action</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                  </table>
                </div>
           </div>
        </div>
    </div>
</div>
@stop
@section('style')
<?= Html::style('asset/css/bootstrap-datepicker.css') ?> 
@stop
@section('script')
<?= Html::script('asset/DataTables/media/js/jquery.dataTables.js') ?>
<?= Html::script('asset/js/bootstrap-datepicker.min.js') ?>
<?= Html::script('asset/DataTables/media/js/dataTables.bootstrap.min.js') ?>
<!-- <script src="http://malsup.github.com/jquery.form.js"></script>  -->
<!-- <script src="http://malsup.github.io/jquery.form.js"></script>  -->
<?= Html::script('asset/js/malsup.github.io.js') ?>
<?= Html::script('asset/js/fnStandingRedraw.js') ?>
<?= Html::script('asset/js/delete_script.js') ?>
<script>
    function closeModel()
    {
        $("[id$='_error']").empty();
        $('#user_deactive')[0].reset();
    }
    function deactive(id){
        $('.linkdeactive').attr('data-target','#deactiveuser');
        $('.linkdeactive').attr('data-toggle','modal');
        $("#id").attr('value',id);
        
           
        
    }
    $(document).ready(function()
    {

        var date = "{{ old('date') }}";
        var currentDate = new Date();  
        var type = "<?= Request::get('type')?>";
        var title = "Are you sure to delete selected record(s)?";
        var text = "You will not be able to recover this record";
        var delete_type = "warning";
        var token = "{{ csrf_token() }}";
        var delete_path = "{{ route('user.trash') }}";
        if(type == "currentemp"){
            url = "<?= URL::route('user.index',['type'=>'currentemp']) ?>";
        }else{
            url = "<?= URL::route('user.index',['type'=>'pastemp']) ?>";
        }
       
        $('#date').datepicker({
            format: "dd-mm-yyyy",
            autoclose: true,
            todayHighlight: true,
        });
        // $('#date').on("click",function(){
        //     if ($('.datepicker-dropdown').hasClass("datepicker-orient-top")) {
        //         $('.datepicker-dropdown').removeClass("datepicker-orient-top");
        //         $('.datepicker-dropdown').addClass("datepicker-orient-bottom");
        //     }
        // })
        $("#date").datepicker("setDate",date);
        $('#submit').click(function(e,ele)
        {
            url = "<?=URL::route('user.deactive')?>";
            var method_type = 'POST';

            $('#user_deactive').ajaxSubmit({
                url: url,
                type: method_type,
                data: { "_token" : '<?= csrf_token() ?>' },
                dataType: 'json',

                beforeSubmit : function()
                {
                   $("[id$='_error']").empty();
                },

                success : function(resp)
                {
                    $('#deactiveuser').modal('hide');
                    $('#user_deactive')[0].reset();

                    iziToast.success({title:'Success!',message: resp});
                    window.setTimeout(function(){location.reload()},3000)
                },
                error : function(respObj){
                    console.log(respObj);
                    $.each(respObj.responseJSON, function(k,v){
                        $('#'+k+'_error').text(v);
                    });
                    iziToast.error({title:'Error!',message:"There were some errors.!"});

                }
            });
            return false;
        });
        $('.createuser').click(function(){
            <?= \Session::forget("image_name")?>;
        });
        $(function()
        {
            var master = $('#employee').dataTable({
            "bProcessing": false,
            "bServerSide": true,
            "autoWidth": true,
            lengthMenu: [
                [ 100,200,500,50,25,10],
                [ '100','200','500','50','25','10']
            ],
            "aaSorting": [
               
            ],
            "sAjaxSource":url,
            "fnServerParams": function ( aoData ) {
                aoData.push({ "name": "act", "value": "fetch" });
                server_params = aoData;
            },
            "aoColumns": [
            { mData: 'id',sWidth: "20%",bSortable:true,bVisible:false},
            
            { mData: "fullname",
                bSortable : false,
                mRender:function(v,t,o)
                {
                    console.log(v)
                    var user_profile   = "<?= URL::route('show.user-profile',['id'=>':id'])?>";
                    user_profile = user_profile.replace(':id',o['id']);
                    var extra_html1 = '<a href="'+user_profile+'" class="" style="color: unset;" style="font-size: 13px" title="Profile">'+v +'  ('+ o['pay_code']+')'+'</a>';
                    return extra_html1;
                }
            },
            { mData: 'position',bSortable: false,},
            { mData: 'email',bSortable:false, },
            {
                mData: null,
                bSortable : false,
                mRender:function(v,t,o)
                {
                    console.log(o['fullname']);
                    var path     = "<?= URL::route('user.edit',array(':id')) ?>";
                    var path_del = "<?= URL::route('user.trash',['id'=>':id']) ?>";
                    var logs     = "<?= URL::route('show.logs',['id'=>':id'])?>";
                    var leaves   = "<?= URL::route('show.leaves',['id'=>':id'])?>";
                    

                    path = path.replace(':id',o['id']);
                    path_del = path_del.replace(':id',o['id']);
                    logs = logs.replace(':id',o['id']);
                    leaves = leaves.replace(':id',o['id']);
                    
                    var type = '<?= Request::get("type")?>';
                    if(type == 'currentemp'){
                        var extra_html = '<a href="'+path+'" class="badge btn_color" style="font-size: 18px" title="Edit">Edit  </a>'+'<a href="'+logs+'" class="badge btn_color" style="font-size: 18px" title="Logs">Logs  </a>'+'<a href="'+leaves+'" class="badge btn_color" style="font-size: 18px" title="Leaves">Leaves  </a>'+'<a href="javascript:;" style="font-size: 18px" title="Deactive" id="linkdeactive" class="linkdeactive badge btn_color" onclick=\"deactive('+o['id']+')\">Deactive  </a>'+'<a href="javascript:void(0)" onclick=\'deleteRecord("'+delete_path+'","'+title+'","'+text+'","'+token+'","'+delete_type+'","'+o['id']+'")\' class="badge btn_color_dlt" style="font-size: 18px" title="Delete">Delete  </a>';
                    }else{
                        var extra_html = '<a href="'+path+'" class="badge btn_color" style="font-size: 18px" title="Edit">Edit </a>'+'<a href="'+logs+'" class="badge btn_color" style="font-size: 18px" title="Logs">Logs </a>'+'<a href="'+leaves+'" class="badge btn_color" style="font-size: 18px" title="Leaves">Leaves </a>'+'<a href="javascript:void(0)" onclick=\'deleteRecord("'+delete_path+'","'+title+'","'+text+'","'+token+'","'+delete_type+'","'+o['id']+'")\' class="badge btn_color_dlt  " style="font-size: 18px" title="Delete">Delete </a>';
                    }
                    return extra_html;
                }
            },
            ],
            
        });

        
        });
    });
</script>
@include('partials.alert')
@stop
