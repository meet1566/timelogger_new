@extends('layout.layout')
@section('content')
<div class="page-title">
    <div>
        <h1>User Leave</h1>
    </div>
</div>
<div class="clearix"></div>
    <div class="col-md-12">
        <div class="card">
            <h3 class="card-title">Create</h3>
            <div class="card-body">
            @include('partials.alert')
                <?= Form::open(array('url' => route('user.leaves.store',$userId) ,'class' => 'form-horizontal')) ?>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Date</label>
                        <div class="col-sm-8">
                            <?= Form::date('date', old('date'), ['class' => 'form-control', 'placeholder' => 'From_Date']); ?>
                            <?= $errors->first('date',"<span class='text-danger'>:message</span>");?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">LeavePurpose</label>
                        <div class="col-sm-8">
                            <?= Form::time('leave_purpose', old('leave_purpose'), ['class' => 'form-control', 'placeholder' => 'LeavePurpose']); ?>
                            <?= $errors->first('leave_purpose',"<span class='text-danger'>:message</span>");?>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<script>

$(document).ready(function() {
    $('form').submit(function(){
        $(this).find('button:submit').html('<i class="fa fa-spinner fa-spin"></i>').prop('disabled', true);
    });
});

</script>
