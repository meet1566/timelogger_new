@foreach($users as $key=>$value)

	<tr>
		<td rowspan="{{count($value)}}">{{current($users[$key])['fullname']}}</td>
		<td rowspan="{{count($value)}}">{{current($users[$key])['time']}}</td>
		<td>{{current($users[$key])['check_in']}}</td>
		<td>{{current($users[$key])['check_out']}}</td>
		<td>{{current($users[$key])['time_diff']}}</td>
		@if(auth()->user()->is_admin)
		<!-- <td>{{current($users[$key])['checkin_device_id']}}@if(current($users[$key])['checkin_user'] != null) ({{current($users[$key])['checkin_user']}}) @endif</td>
		<td>{{current($users[$key])['checkout_device_id']}}@if(current($users[$key])['checkout_user'] != null) ({{current($users[$key])['checkout_user']}}) @endif</td> -->
		{{-- rowspan="{{count($value)}}" style="border-left:1px solid #ddd;" --}}
		<td  
		@if($users_times[$key]['total'] == '00:00:00') 
        	style="background-color:yellow"{{$users_times[$key]['total']}}
      	@elseif(current($users[$key])['log_user'] != 74 && current($users[$key])['log_user'] != 69 && current($users[$key])['log_user'] != 113)
      		@if($users_times[$key]['total'] < '09:00:00' &&  '06:30:00'< $users_times[$key]['total'])
      		style="background-color:red"{{$users_times[$key]['total']}}
      		@endif
      	@elseif(current($users[$key])['log_user'] == 74 || current($users[$key])['log_user'] == 69 || current($users[$key])['log_user'] == 113 )
      	style="background-color:white"{{$users_times[$key]['total']}}
      	@endif
      	>
			{{$users_times[$key]['total']}}	
		</td>
		
		@endif
	</tr>
	@if(count($value) > 1)
		<?php $c = 0;?>
		@foreach($value as $v_key =>$v_value)
			@if($c != 0)
				<tr>
					<td>{{$v_value['check_in']}}</td>
					<td>{{$v_value['check_out']}}</td>
					<td>{{$v_value['time_diff']}}</td>
					<td>{{$v_value['checkin_device_id']}}@if($v_value['checkin_user'] != null) ({{$v_value['checkin_user']}}) @endif</td>
					<td>{{$v_value['checkout_device_id']}}@if($v_value['checkout_user'] != null) ({{$v_value['checkout_user']}}) @endif</td>
				</tr>
			@endif
			<?php $c++;?>
		@endforeach
	@endif
@endforeach