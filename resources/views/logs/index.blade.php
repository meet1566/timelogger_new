@extends('layout.layout')
@section('content')
<div class="page-title">
    <div>
        <h1>Employee Logs</h1>
    </div>
    <div>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
          Import User Logs
        </button>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-xs-12">
                        <form class="form-inline" role="form">
                            <div class="form-group">
                                
                                @if(auth()->user()->is_admin)
                                    <label for="email">Select Date</label>
                                    <input id="log_Date" type="text" placeholder="Select Date" class="form-control input-sm">
                                    <label for="user" style="margin-left: 10px;">Select User</label>
                                    <?= Form::select('user_id',[''=>'select'] + $all_users,old('user_id'),['class' => 'form-control input-sm','id' => 'users']) ?>
                                @else
                                    <b>Month: </b><?= Form::selectMonth('month',date('m'),['id'=>'month','class' => 'form-control breadcrumb-select input-sm']); ?>
                                    <b>Year: </b><?= Form::selectRange('year','2016','2025',date('Y'),['id'=>'year','class' => 'form-control breadcrumb-select input-sm']); ?>
                                    <input type="hidden" name="month" value="<?= date('m')?>">    
                                @endif
                            </div>
                        </form>
                    </div>
                </div>
                <br/>
                <table id="employee" class="table table-hover table-bordered" border="1 px" style="width:100%;">
                    <thead>
                        <tr>
                            <th width="10%">Employee Name</th>
                            <th width="10%">Date</th>
                            <th width="10%">TimeIn</th>
                            <th width="10%">TimeOut</th>
                            <th width="10%">Hours</th>
                            @if(auth()->user()->is_admin)
                            <!-- <th width="20%">CheckIn Device</th>
                            <th width="20%">CheckOut Device</th> -->
                            <th width="10%">Total Hours</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody id="logs_body">
                        @include('logs.index_partial')
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">    
    <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
             <h5 class="modal-title" id="exampleModalLabel">Choose File</h5>
            <button type="button" class="close closed" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <input type="file" name="file" id="file">
          </div>
          <div class="modal-footer">
            <button type="button" class="closed btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" data-dismiss="modal" id="save_file" class="btn btn-primary closed">Upload</button>
          </div>
        </div>
    </div>    
</div>

@stop
@section('style')
<?= Html::style('css/colorbox.css') ?>
<?= Html::style('admin/globals/plugins/selectize/dist/css/selectize.bootstrap3.css') ?>
<?= Html::style('front/css/user.css') ?>
<?= Html::style('asset/css/bootstrap-datepicker.css') ?>   
@stop
@section('script')
<?= Html::script('asset/DataTables/media/js/jquery.dataTables.js') ?>
<?= Html::script('asset/DataTables/media/js/dataTables.bootstrap.min.js') ?>
<?= Html::script('asset/js/plugins/sweetalert.min.js') ?>
<?= Html::script('asset/js/plugins/select2.min.js') ?>
<?= Html::script('asset/js/fnStandingRedraw.js') ?>
<?= Html::script('asset/js/bootstrap-datepicker.min.js') ?>


<script>
function getLogs(val,flag){
    $.ajax({
        url        : '{{ URL::route('getlogs.index')}}',
        type       : 'post',
        data       : { "log_date":  $('#log_Date').val(),'user':$('#users').val(), '_token' : "{{csrf_token()}}"},
        dataType   : 'html',
        success    : function(response) {
            var response = jQuery.parseJSON(response);
            $('#logs_body').html('');
            if(response.count == 0){
                $('#logs_body').html('<tr><td colspan="8"><center>There were no record Found!</center></td></tr>');  
            }else{
                $('#logs_body').html(response.response_html);
            }
        }
    }); 
}
var token = "<?= csrf_token() ?>";
$( document ).ready(function(){

    var currentDate = new Date();  
    $('#log_Date').datepicker({
        format: "dd-mm-yyyy",
        // multidate:true,
        autoclose: true,
        todayHighlight: true,
        // endDate:currentDate,
    });

    $("#log_Date").datepicker("setDate",currentDate);
        
    // $('#employee').dataTable({
    //     "bProcessing" : true,
    //     "bServerSide" : true,
    //     "searching"   : false,
    //     'bPaginate'   : false,
        // "pageLength"  : 50, 
        
    // });
    $('#log_Date').change(function(){
        getLogs($(this).val(),'log_date');
    });

    $('#users').change(function(){
        getLogs($(this).val(),'user');

        // $('.dataTable').each(function() {
        //     dt = $(this).dataTable();
        //     dt.fnDraw();
        // });
    });

    $('#month').change(function(){
        // $('.dataTable').each(function() {
        //     dt = $(this).dataTable();
        //     dt.fnDraw();
        // });
    });

    $('#year').change(function(){
        // $('.dataTable').each(function() {
        //     dt = $(this).dataTable();
        //     dt.fnDraw();
        // });
    });

    $('#save_file').click(function(){
        var data = $('#file').prop('files')[0];
        var token="{{csrf_token()}}";
        var form_data = new FormData();
        form_data.append('excel', data);

        $.ajax({
            url : "<?=URL::route('logs.logsimport') ?>",
            headers: {'X-CSRF-TOKEN':token},
            type : "post",
            cache : false,
            contentType : false,
            processData : false,
            data: form_data,
            dataType : 'JSON',
            success: function (data) {
                iziToast.success({
                title:'Ok',
                message: data.message,
                });          
            },
        });
    });

    $(".closed").click(function(){
        $("#file").val('');
    });
});
</script>
@stop
