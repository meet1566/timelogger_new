<!DOCTYPE html>
<html>
<head>
    <title>Leave Request</title>
    <style type="text/css">
    *{
        margin:0;
        padding:0;
    }
    body{
        font-size:1em;
    }
    .container{
        width:95%;
        margin:0 auto;
    }
    table{
        width:100%;
        border-bottom:1px solid #ddd;
    }
    tbody tr {
        float: left;
        width: 100%;
    }
    tbody tr td{
        padding:10px;
        width:auto;
        float:none;
        display: inline-block;
    }
    tbody tr.flex{
        display: flex;
        flex-direction: row;
    }
    tbody tr td:first-child{
        text-align:right;
        width:100px;
    }
    tbody tr td.purpose {
        width: 45%
    }
    </style>
</head>
<body>
<section class="container">
    @foreach($leaveRequests as $leaveRequest)
    <table>
        <tbody>
            <tr>
                <?php $leaveDate = date("d F Y", strtotime($leaveRequest->senddate)); ?>
                <td><?= $leaveDate; ?></td>
            </tr>
            <tr>
                <td>Whole Day:</td>
                @if($leaveRequest->iswholeday)
                    <td>Yes</td>
                @else
                    <td>No</td>
                @endif
            </tr>
            @if( ! $leaveRequest->iswholeday)
            <tr>
                <td>Hours:</td>
                <td><?=$leaveRequest->fromhour; ?></td>
                <td>to</td>
                <td><?= $leaveRequest->tohour; ?></td>
            </tr>
            @endif
            <tr class="flex">
                <td>Purpose:</td>
                <td class="purpose"><?= $leaveRequest->purpose; ?></td>
            </tr>
            <tr>
                <td>Type:</td>
                <td>
                    <?= $leaveRequest->type; ?>
                </td>
            </tr>
        </tbody>
    </table>
    @endforeach

    <p>Thanks</p>
    <p><?= $user->fullname ?></p>
    <a href="mailto:<?= $user->email; ?>"><?= $user->email ?></a>
</section>
</body>
</html>
