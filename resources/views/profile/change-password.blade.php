@extends('layout.layout')
@section('content')
<style>
    .profile-detail{
        font-size: 14px !important;
    width:100%;
    background-color: #fff;
    padding:20px 0;
    }
    .profile-detail .dl-horizontal, .profile-detail .dd-horizontal {
    margin: 10px 0;
    }
    .profile-detail label {
    float: right;
    font-size: 16px;
    margin: 19px 0;
    }
    .profile-detail input {
    float: left;
    width: 98%;
    margin: 10px 0;
    }
    .col-sm-3{
        width: 22%;
    }
    .row_align{
    margin-left: 0px;
    margin-right: 0px;
    }
    .row_1{
        display: flex;
        align-items: center;
        justify-content: space-between;
    }
    .col_3{
        width: 20%;
        display: flex;
        justify-content: end;
    }
    .col_9{
        width: 78%;
        display: flex;
        flex-direction: column;
        justify-content: start;
    }
    .pass_btn{
        width: 120px;
    }
    .text-danger{
        padding: 0px;
    }
</style>
<div class="page-title">
    <div>
        <h1> Change Password</h1>
    </div>
</div>
<div class="row ">
    <form class="form-horizontal" method="POST">
        <div class="col-md-9 " style="float:center; background-color: #fff; padding: 40px;">
            <div class="profile-detail">
                
                <div class="row_1 ">
                    <div class="col_3">
                            <dt>Old Password:</dt>
                    </div>
                    <div class="col_9">
                        <input type="password" name="old_password" id="old_password" class="form-control"  placeholder="Enter Your Old Password">
                    <span class='col-sm-9 text-danger' id="old_error" style="float:right;"></span>
                    </div>  
                </div>
                <div class="row_1 ">
                    <div class="col_3">
                            <dt>New Password:</dt>
                    </div>
                    <div class="col_9">
                        <input type="password" name="new_password" id="new_password" class="form-control"  placeholder="Enter Your New Password">
                    <span class='col-sm-9 text-danger' id="new_error" style="float:right;"></span>
                    </div>  
                </div>
                <div class="row_1 ">
                    <div class="col_3">
                            <dt>Confirm New Password:</dt>
                    </div>
                    <div class="col_9">
                        <input type="password" name="confirm_password" id="confirm_password" class="form-control"  placeholder="Confirm Password">
                    <span class='col-sm-9 text-danger' id="confirm_error" style="float:right;"></span>
                    </div>  
                </div>
                <div class="row_1 ">
                    <div class="col_3">
                    </div>
                    <div class="col_9">
                        <button  class="btn btn-primary pass_btn" style=" margin-top: 14px;">Submit</button>
                    </div>  
                </div>
               
            </div>
        </div>
    </form>
</div>
@stop
@section('style')
<?= Html::style('asset/css/bootstrap-datepicker.css') ?> 
<?= Html::style('css/colorbox.css') ?>
@stop
@section('script')
<!-- <script src="https://code.jquery.com/jquery-1.10.2.js"></script> -->
<?= Html::script('asset/js/bootstrap-datepicker.min.js') ?>
<?= Html::script('js/jquery.colorbox-min.js') ?>
<!-- <script src="http://malsup.github.com/jquery.form.js"></script>  -->
<!-- <script src="http://malsup.github.io/jquery.form.js"></script>  -->
<?= Html::script('asset/js/malsup.github.io.js') ?>
<?= Html::script('asset/js/select2.min.js') ?>

<script type="text/javascript">
    $(document).ready(function(){
        $('.pass_btn').click(function(e){
            e.preventDefault();
            $('#old_error').text("");
            $('#new_error').text("");
            $('#confirm_error').text("");
            var token = '<?= csrf_token()?>';
            var old_password = $('#old_password').val();
            var new_password = $('#new_password').val();
            var confirm_password = $('#confirm_password').val();
            $.ajax({
                url: "<?=route('change.password')?>",
                type: 'POST',
                data: { "_token" : token,"old_password":old_password,"new_password":new_password,"confirm_password":confirm_password},
                dataType: 'json',
                success : function(res)
                {
                   if (res.success == true) {
                        iziToast.success({
                            title:'Success!',
                            message: "Password Change successfully",
                        });
                       setTimeout(function() {
                        window.location.href = "/logout";
                        }, 3000);

                   }else{

                        iziToast.error({
                            title:'error!',
                            message: "Old Password is not Matched",
                        });
                   }
                },
                error : function(res){
                    $('#old_error').text(res.responseJSON.old_password);
                    $('#new_error').text(res.responseJSON.new_password);
                    $('#confirm_error').text(res.responseJSON.confirm_password);
                    // console.log();
                }
            }); 
        })
    });
</script>
@stop