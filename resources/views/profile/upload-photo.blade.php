<!DOCTYPE html>
<html>
<head>
	<title>Upload Profile Photo</title>
	<?= Html::style('front/css/style.css') ?>
	<?= Html::style('plugins/krajee/css/fileinput.min.css') ?>
	<?= Html::script('front/js/jquery.min.js') ?>
	<?= Html::script('plugins/krajee/js/fileinput.min.js') ?>
	<style>
		.form-control {border-radius: 0; background: none; border:1px solid #EEE;  outline: none; -webkit-box-shadow: none !important; -moz-box-shadow: none !important; box-shadow: none !important; height:32px;}
		/*.btn-file {height:40px; background:#000 !important; padding:10px;}*/
		.btn-file:hover {height:32px; background:#CCC !important; color:#000; }
		/*button[class^="fileinput-"], button[class*=" fileinput-"]{ padding: 10px !important }*/
	</style>
	
<script type="text/javascript">
	$(document).ready(function(){
		$("#profile_pic").fileinput({ 
			showPreview: false,
            allowedFileExtensions: ["jpg", "gif", "png", "jpeg"],
            // previewFileType: "image",
            showUpload : true,
            uploadClass: 'btn btn-primary',
			uploadTitle: 'Please upload file'
		});
	});
</script>
</head>
<body>
	<div class="text-center" align="center">

		<?= Form::open(array('route' => array('profile.upload.post'), 'id' => 'frm_profile_photo', 'class' => 'pt60 pb60', 'method' => 'POST', 'files' => true)) ?>
			<input type="hidden" name="_token" value="{!! csrf_token() !!}">
			<b>only jpg, and png images are allowed</b><br/>
			<b>image size should be less than 4 mb</b>
			
			<div class="col-md-2">
				<?= Form::file('profile_pic', array('class' => 'file', 'id' => 'profile_pic')) ?><br/>
			</div>
			<span class="text-danger">{{ $errors->first('profile_pic') }}</span>
		
		<?= Form::close() ?>
	</div>

</body>
</html>