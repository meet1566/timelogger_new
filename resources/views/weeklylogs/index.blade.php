@extends('layout.layout')
@section('content')
<style type="text/css">
	.table-box{
		width: 100%;
		height: 600px;
		overflow-y:scroll;
		overflow-x: scroll;
		background: :#fff;
		box-shadow: 0 10px 100px rgba(0,0,0,0.5); 
	}	
	table{
		width: 100%;
	}
	table,th{
		border:1px solid #005277;
		border-collapse: collapse;
		text-align: left;
	}
	th{
		position: sticky;
		top: 0;
		left: 0;
		background: #00bcd4;
		color: #fff; 	
		text-align: center; 
		/*box-sizing: border-box;*/
	}
	tr th:nth-child(1){
		/*width: 60px;*/
		text-align: center;
		background: #03707d;
	}
	table .namecol{
		color: #fff;
		background: #00bcd4;
		text-align: center;
		position: sticky;
		left: 0;
		/*top: 50;*/
	}
	td{
	    text-align: center;
	    background-color: antiquewhite
	}
	.employee tr(even){
		background-color: #fff;
	}
</style>
<div class="page-title">
    <div>
        <h1>Weekly Logs</h1>
    </div>
</div>
<?php $date_arr=[]; $name_arr=[];?>

@foreach ($log_data as $product=>$value)
	<?php $date_arr[] = $value['Date'];
		$name_arr[]	= $value['fullname'];
	?>
@endforeach
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                	<div class="table-box">
	                    <table id="employee" cellpedding="10" border="1">
	                        <thead>
						      	<tr>
							      <th width="15%" style="padding: 5px;z-index: 1;">Name</th>
							      <th width="10%">In/Out</th>
							      @foreach (array_unique($date_arr) as $key=>$value)
							      	<?php $dayname = date('D', strtotime($value));
							      	$value = date('d-m-Y', strtotime($value));?>
							      	<th style="width: 10px;padding-left:3px;padding-right: 3px">{{$dayname}}<br/>{{$value}}</th>
							      @endforeach
						      	</tr>
						    </thead>
						    <tbody>
							    @foreach (array_unique($name_arr) as $key=>$value)
							    	
								    	<tr>
											<td rowspan="3" style="padding: 5px;width: 30px" class="namecol">{{ucwords($value)}}
											</td>
										    <td style="width: 10px">In</td>
										    <?php $c=0;?>
									    	@foreach ($log_data as $data_product=>$data_value)
													@if($data_value['fullname'] == $value)
														<td style="width: 10px;padding-left:3px;padding-right: 3px">{{substr($data_value['check_in'],0,5)}}
														@if($data_value['checkIN'] < '12:00:00')	
														AM
														@else
														PM
														@endif
														 </td>
														<?php $c++;?>
													@endif
											@endforeach
											<?php $count=count(array_unique($date_arr))-$c;?>
											@for($i=0;$i<$count;$i++)
												<td></td>
											@endfor
								      	</tr>
								      	<tr>
									    	<td style="width: 10px">Out</td>
									    	@foreach ($log_data as $data_product=>$data_value)
												@if($data_value['fullname'] == $value)
													<td style="width:10px;padding-left:3px;padding-right: 3px">{{substr($data_value['check_out'],0,5)}} 
													@if($data_value['checkOUT'] < '12:00:00')	
													AM
													@else
													PM
													@endif
													</td>
												@endif
											@endforeach	
											<?php $count=count(array_unique($date_arr))-$c;?>
											@for($i=0;$i<$count;$i++)
												<td></td>
											@endfor
								      	</tr>
								      	<tr>
									    	<td style="width: 10px">Hours</td>
									    	@foreach ($log_data as $data_product=>$data_value)

												@if($data_value['fullname'] == $value)
													<td @if(($data_value['time_diff']) < '09:00:00')style="background-color:{{config('project.error_code.code')}};width:10px;padding-left:3px;padding-right:3px;" @else style="width:10px;padding-left:3px;padding-right: 3px;"@endif>{{substr($data_value['time_diff'],0,5)}} HRS</td>
												@endif
											@endforeach	
											<?php $count=count(array_unique($date_arr))-$c;?>
											@for($i=0;$i<$count;$i++)
												<td></td>
											@endfor

								      	</tr>
							    @endforeach
						    </tbody>
	                    </table>
                	</div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
<script>
	$(document).ready(function(){
		$('#employee tr:even').css('background-color','#fff');
	});
</script>
@stop
