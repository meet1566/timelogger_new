<!DOCTYPE html>
<html>

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="no-cache">
    <meta http-equiv="Expires" content="-1">
    <meta http-equiv="Cache-Control" content="no-cache"> -->
    <!-- CSS-->

    <?= Html::style('/main1.css') ?>
    <?= Html::style('/asset/css/sb-admin.css') ?>
    <?= Html::style('/asset/css/iziToast.min.css') ?>
    <?= Html::style('/asset/css/sweetalert.css') ?>
    <?= Html::style('/css/custom.css') ?>
    @yield('style')

    <title>Think Admin</title>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries-->
    <!--if lt IE 9
    script(src='https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js')
    script(src='https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js')

    -->
  </head>
  <body class="sidebar-mini fixed">

      <!-- Navbar-->
     @include('layout.header')
      <!-- Side-Nav-->
      @include('layout.sidebar')
      <div class="content-wrapper">
       @yield('content')
      </div>

    <!-- Javascripts-->
    @include('layout.footer')
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    {{--
    <script type="text/javascript">
      var data1 = '<?= addslashes(json_encode($notification["notification"])) ?>';
       var count = '<?= $notification['notification_count']?>';
       var token = "<?= csrf_token()?>";
       function view(id){
        $.ajax({
          type: "POST",
          url: '<?= route('notification.isview')?>',
          data: {
              id:id,
              _token: token
          },
          success: function(resp) {
            window.location.href = '<?=URL::route('leaves.index')?>';
          }
        });
       }
        $('.countsection').click(function(){
          if(count > 0){
          $.ajax({
            type: "POST",
            url: '<?= route('notification.isread')?>',
            data: {
                notification:data1,
                _token: token
            },
            success: function(resp) {
              count = 0;
              $('.count-section').css('display','none');
            }                                                                                                        
          });
        } 
        });
        $.ajaxSetup({
            statusCode: {
                401: function() {
                  location.reload();
                }
            }
        });

        $('.alert').hide(2000);
    </script> --}}
    @yield('script')

  </body>
</html>
