<html>
<div>
    <!-- Side-Nav-->
    <aside class="main-sidebar">
        <section class="sidebar">
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="http://timelogger.thinktanker.in/uploads/<?=Auth::user()->profile_pic ?>" alt="User Image" class="img-circle">
                </div>
                <div class="pull-left info">
                    <p><?= Auth::user()->fullname ?></p>
                    <p class="designation"><?= Auth::user()->position ?></p>
                </div>
            </div>
            <!-- Sidebar Menu-->
            <ul class="sidebar-menu">
                <li class="active"><a href="<?= url('dashboard')?>"><i class="fa fa-dashboard"></i><span>Dashboard</span></a></li>
                @if(CheckPermission::isPermitted('user.index'))
                    <li class="@if( 'index' == Request::segment(2)) active @endif"><a href="<?= URL::route('user.index') ?>"><i class="fa fa-user"></i>User</a></li>
                @endif
                <!-- <li><a href="<?= URL::route('leaves.index') ?>"><i class="fa fa-calendar-minus-o"></i> Leaves</a></li>
                <li><a href="<?= URL::route('holiday.index') ?>"><i class="fa fa-plane"></i> Holidays</a></li>
                <li><a href="<?= URL::route('logs.index') ?>"><i class="fa fa-calendar-check-o"></i>Logs</a></li> -->
                @if(CheckPermission::isPermitted('userpermission.create'))
                    <li class="@if( 'user' == Request::segment(2)) active @endif"><a href="<?= URL::route('userpermission.create') ?>"><i class="fa fa-circle-o"></i> User Permission</a></li>
                @endif
                    
            </ul>
        </section>
    </aside>
</div>
</html>