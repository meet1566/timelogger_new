<div class="modal fade" id="Report_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="staticBackdropLabel">Asset History</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="font-size:16px">
          <div class="row">
            <div class="col-md-4">
              <div>Name:<b><span class="username"></span></b></div>
            </div>  
            <div class="col-md-4">
              <div>Device Type:<b><span class="device_type"></span></b></div>
            </div>  
            <div class="col-md-4">
              <div>Serial Number:<b><span class="serial_number"></span></b></div>
            </div>  
          </div> 
          <br>
        <table class="table" style="font-size:16px" border="1px">
          <thead style="background-color:black; color: white;">
            <tr>
              <th scope="col" class="col-md-3">#</th>
              <th scope="col" class="col-md-4">Status</th>
              <th scope="col" class="col-md-5">Date</th>
            </tr>
          </thead>
          <tbody class="data_user" style="background-color: lightgray ; color: black;">
           
          </tbody>
      </table>
      </div>

    </div>
  </div>
</div>