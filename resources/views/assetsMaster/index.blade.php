@extends('layout.layout')
@section('content')
<div class="page-title">
    <div>
        <h1>Assets Master</h1>
    </div>
    <div>
           <a href="<?= URL::route('assets.master.create') ?>"class="btn btn-primary btn-flat"><i class="fa fa-lg fa-plus"></i></a>
    </div>
</div>
<div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))
            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
    @endforeach
</div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <table id="employee" class="table table-hover table-bordered" border="1 px" style="width:100%;">
                        <thead>
                            <tr>
                                <th>Company Name</th>
                                <th>Device Type</th>
                                <th>UserName</th>
                                <th>StartDate</th>
                                <th>status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

@include('assetsMaster.modal_index')

@stop
@section('script')

<?= Html::script('asset/DataTables/media/js/jquery.dataTables.js') ?>
<?= Html::script('asset/DataTables/media/js/dataTables.bootstrap.min.js') ?>
<?= Html::script('asset/js/fnStandingRedraw.js') ?>
<?= Html::script('asset/js/delete_script.js') ?>

<script>
var token = "<?= csrf_token() ?>";
var title = "Are you sure to delete selected record(s)?";
var text = "You will not be able to recover this record";
var type = "warning";
var token = "{{ csrf_token() }}";
var delete_path = "{{ route('assets.master.delete') }}";

$(document).ready(function(){

    $(function()
        {
            var master = $('#employee').dataTable({
            "bProcessing": false,
            "bServerSide": true,
            "autoWidth": true,
            "aaSorting": [
                [1, "asc"]
            ],
            "sAjaxSource": "{{ URL::route('assets.master.index')}}",
            "fnServerParams": function ( aoData ) {
                aoData.push({ "name": "act", "value": "fetch" });
                server_params = aoData;
            },
            "aoColumns": [
            // { mData: 'id',sWidth: "20%",bSortable:true,},
            { mData: 'title',sWidth: "20%",bSortable:true, },
            { mData: 'device_type',sWidth: "20%",bSortable:true, },
            { mData: 'fullname',sWidth: "20%",bSortable:true,},
            { mData: 'startdate',sWidth: "15%",bSortable:true, },
            { mData: 'status',sWidth: "12%",bSortable:true,},
            { mData: null,
                sWidth: "10%",
                bSortable : false,
                mRender:function(v,t,o) {
                    var id = btoa(o['id']);
                    
                    var path_del = "<?= URL::route('assets.master.delete',array(':id')) ?>";
                    
                    path_del = path_del.replace(':id',o['id']);

                    var extra_html  =   "<div class='btn-group pr5'>"
                                    +       "<a title='Edit' href='{{ url('assets/master/edit')}}/"+id+"'><i class='fa fa-edit'></i></a>"
                                    +   "&nbsp&nbsp&nbsp<a id='delete' href='javascript:void(0)' onclick=\"deleteRecord('"+delete_path+"','"+title+"','"+text+"','"+token+"','"+type+"',"+o['id']+")\" class='fa fa-fw fa-trash-o'></a>"
                                    +   "</div>";
                    return extra_html;


                }

            }
            ],
            
        });

        
        });
    $.ajaxSetup({
        statusCode: {
          401: function() {
            location.reload();
          }
        }
    });

});
</script>
<script >
    $('.show_history').click(function(){

       ShowHistroyDetails();

    });
    function ShowHistroyDetails(id)
    {
       
        $.get('assets_status_history/'+id,function(data){
            $('.username').text(data.asset_user.fullname)
            $('.device_type').text(data.asset_history.device_type)
            $('.serial_number').text(data.asset_history.serial_number)
            var count = 1;
            var html = "";

            $.each(data.asset_log, function(k,v){
                html += "<tr>";
                html += "<td>"+count+"</td>";
                html += "<td>"+v.status+"</td>";
                html += "<td>"+v.created_at+"</td>";
                html += "</tr>";
                count++;
            });

            $('.data_user').html(html);
            $('#Report_modal').modal('show');
        });
    }
    
    // function statusChange(id)
    // {
    //     swal({
    //         title: change_status,
    //         type: type,
    //         showCancelButton: true,
    //         confirmButtonText: "Yes, Change it!",
    //         cancelButtonText: "No, cancel it!",
    //         closeOnConfirm: true,
    //         closeOnCancel: true
    //     }, function(isConfirm) {
    //         if (isConfirm) {
    //            $.ajax({
    //                 url: 'assets_status/' +id,
    //                 type:'get',
    //                 dataType:'json',
    //                 data:{id:id},
    //                 success : function(data)
    //                 {
    //                     if (data.message == true) {
                            
    //                         location.reload();
    //                     }
    //                 }
    //            });
    //         }
    //     });
    // }
    
</script>
@include('partials.alert')

@stop
