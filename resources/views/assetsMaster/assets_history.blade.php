@extends('layout.layout')
@section('content')
<div class="page-title">
    <div>
        <h1>Assets Master History</h1>

    </div>
    
</div>
<div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))
            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
    @endforeach
</div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <table id="employee" class="table table-hover table-bordered" border="1 px" style="width:100%;">
                        <thead>
                            <tr>
                                <th>Company Name</th>
                                <th>Device Type</th>
                                <th>UserName</th>
                                <th>StartDate</th>
                                <th>EndDate</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')

<?= Html::script('asset/DataTables/media/js/jquery.dataTables.js') ?>
<?= Html::script('asset/DataTables/media/js/dataTables.bootstrap.min.js') ?>
<?= Html::script('asset/js/fnStandingRedraw.js') ?>
<?= Html::script('asset/js/delete_script.js') ?>

<script>
var token = "<?= csrf_token() ?>";
var title = "Are you sure to delete selected record(s)?";
var text = "You will not be able to recover this record";
var type = "warning";
var token = "{{ csrf_token() }}";
var delete_path = "{{ route('assets.master.delete') }}";

$(document).ready(function(){

    $(function()
        {
            var master = $('#employee').dataTable({
            "bProcessing": false,
            "bServerSide": true,
            "autoWidth": true,
            "aaSorting": [
                [1, "asc"]
            ],
            "sAjaxSource": "{{ URL::route('assetsMaster.history')}}",
            "fnServerParams": function ( aoData ) {
                aoData.push({ "name": "act", "value": "fetch" });
                server_params = aoData;
            },
            "aoColumns": [
            // { mData: 'id',sWidth: "20%",bSortable:true,},
            { mData: 'title',sWidth: "40%",bSortable:true, },
            { mData: 'device_type',sWidth: "20%",bSortable:true, },
            { mData: 'fullname',sWidth: "20%",bSortable:true,},
            { mData: 'startdate',sWidth: "20%",bSortable:true, },
            { mData: 'enddate',sWidth: "20%",bSortable:true, },
            
            ],
            
        });

        
        });
    $.ajaxSetup({
        statusCode: {
          401: function() {
            location.reload();
          }
        }
    });

});
</script>
@include('partials.alert')
@stop
