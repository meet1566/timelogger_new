@extends('layout.layout')
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card">
        <div class="card-body">
          <ul class="breadcrumb side">
            <div class="row">
                <div class="col-sm-offset-10 text-left">
                    <a data-dismiss="modal" href="<?= URL::route('show.logs',array('id'=>$id)) ?>" class="btn btn-primary btn-flat">Back</a>
                </div>
            </div>
              </li>
          </ul>
        	<table id="employee" class="table table-hover table-bordered" border="1 px">
          <thead>
              <tr>
                <th>Date</th>	
                <th>Check In</th>
                <th>Check Out</th>
                <th>Working Hours</th>	
              </tr>
          </thead>
           <tbody id="date_body">
              @foreach($logs as $data =>$value)
            	<tr>
                <td>{{ $value['time']}}</td>
                <td>{{ $value['check_in']}}</td>
                <td>{{ $value['check_out']}}</td>
              <?php if(isset($leaves_dates))
                { 
              ?>
                <td 
              @foreach($leaves_dates as $key =>$value1) 
                @if($value1 == $value['time']) 
                  style="background-color:rgb(253, 203, 110)"{{ $value['time_diff']}}
                  @endif                   
                @endforeach
                 >
                 {{ $value['time_diff']}} 
                </td>
              <?php 
                 } 
              ?>
              </tr>
              @endforeach
             <!--<tr>
              	<td>Working Hours</td>
              	<td>{{$working_hr}}</td>                	 
              </tr>
               <tr>
              	<td>Approve Leaves Hours</td>
                  <td>{{$leave_hours}}</td>
              </tr> -->
          </tbody>
          </table>
     	</div>
  	</div>
  </div>
</div>
@stop
@section('style')