@extends('layout.layout')
@section('content')

<style type="text/css">
    .avtar{
    background-color: #fff;
    padding:20px 0;
    }
    .avtar button.btn-primary {
    float: none;
    width: 80%;
    margin: 20px auto;
    }
    .avtar form {
    float: none;
    width: 90%;
    margin: auto;
    }
    .profile-detail{
        font-size: 14px !important;
    width:100%;
    background-color: #fff;
    padding:20px 0;
    }
    .profile-detail .dl-horizontal, .profile-detail .dd-horizontal {
    margin: 10px 0;
    }
    .profile-detail label {
    float: right;
    font-size: 16px;
    margin: 19px 0;
    }
    .profile-detail input {
    float: left;
    width: 98%;
    margin: 10px 0;
    }
    .profile-detail textarea {
    width: 98%;
    }
    .profile-detail button {
    float: left;
    }
    .checkbox {
    float: left;
    width: 98%;
    text-align: left;
    }
    .checkbox label {
    float: left;
    width: auto;
    margin: 0;
    }
    .checkbox input[type="checkbox"] {
    float: left;
    width: auto;
    margin: 4px 0;
    }
    .dl-horizontal.dd-horizontal > dd {
    width: 100%;
    text-align: left;
    }
     .bankdetails_container{
        display: flex;
        padding: 30px 0;
    }
    .left_bankdetails, .right_documnent{
        width: 50%;
        background: #fff;
    }
    .left_bankdetails{
        margin-right: 15px;
    }
    .left_bankdetails .row,.right_documnent .row{
        padding-top:11px ;
    }
    .left_bankdetails .row .col-sm-3,.right_documnent .row .col-sm-3{
        margin-left:20px ;
    }
</style>
<div class="page-title">
    <div>
        <h1> Employee Profile</h1>
    </div>
</div>
<div class="row">
    @if(Auth::user()->profile_pic != '' AND Auth::user()->profile_pic != NULL)
    <div class="col-md-3 avtar text-center">
        <img src="/upload/<?= Auth::user()->profile_pic ?>" alt="" style="width:150px;height:150px;">
    </div>
    @endif
    <div class="col-md-9">
        <div class="profile-detail">
            <div class="row">
                <div class="col-sm-3">
                    <dl class="dl-horizontal ">
                        <dt>Name:</dt>
                    </dl>
                </div>
                <div class="col-sm-9">
                    <dd class="dd-horizontal">
                         {{ ucwords(Auth::user()->fullname) }}
                    </dd>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <dl class="dl-horizontal">
                        <dt>Gender:</dt>
                    </dl>
                </div>
                <div class="col-sm-9">
                    <dd class="dd-horizontal">
                        {{ ucfirst(Auth::user()->gender) }}
                    </dd>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <dl class="dl-horizontal">
                        <dt>Position:</dt>
                    </dl>
                </div>
                <div class="col-sm-9">
                    <dd class="dd-horizontal">
                        {{ ucwords($profile_details->position) }}
                    </dd>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <dl class="dl-horizontal">
                        <dt>JoiningDate:</dt>
                    </dl>
                </div>
                <div class="col-sm-9">
                    <dd class="dd-horizontal">
                        <?= date('d-M-Y', strtotime($profile_details->joining_date)) ?>
                    </dd>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <dl class="dl-horizontal">
                        <dt>BirthDate:</dt>
                    </dl>
                </div>
                <div class="col-sm-9">
                    <dd class="dd-horizontal">
                        <?= date('d-M-Y', strtotime(Auth::user()->birthdate)) ?>
                    </dd>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <dl class="dl-horizontal dd-horizontal">
                        <dt>Company Email:</dt>
                    </dl>
                </div>
                <div class="col-sm-9">
                    <dd class="dd-horizontal">
                        <a style="color:black;" href="mailto:<?= Auth::user()->email; ?>"><?= Auth::user()->email ?></a>
                    </dd>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <dl class="dl-horizontal">
                        <dt>Personal Email:</dt>
                    </dl>
                </div>
                <div class="col-sm-9">
                    <dd class="dd-horizontal">
                        <a style="color:black;" href="tel:<?= Auth::user()->personal_email; ?>"><?= Auth::user()->personal_email ?></a>
                    </dd>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <dl class="dl-horizontal">
                        <dt>Contact:</dt>
                    </dl>
                </div>
                <div class="col-sm-9">
                    <dd class="dd-horizontal">
                        <a style="color:black;" href="tel:<?= Auth::user()->contact; ?>"><?= Auth::user()->contact ?></a>
                    </dd>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <dl class="dl-horizontal">
                        <dt>Emergency Contact:</dt>
                    </dl>
                </div>
                <div class="col-sm-9">
                    <dd class="dd-horizontal"><a style="color:black;" href="tel:<?= Auth::user()->emergency_contact; ?>"><?= Auth::user()->emergency_contact ?></a></dd>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <dl class="dl-horizontal">
                        <dt>Address:</dt>
                    </dl>
                </div>
                <div class="col-sm-9">
                    <dd class="dd-horizontal">{{ ucwords(Auth::user()->address) }}   </dd>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <dl class="dl-horizontal dd-horizontal">
                        <dt>Hobby:</dt>
                    </dl>
                </div>
                <div class="col-sm-9">
                    <dd class="dd-horizontal">
                         {{ ucwords(Auth::user()->hobby) }}
                    </dd>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-offset-3 text-left">
                    <a href="<?= URL::current().'/edit' ?>" class="btn btn-primary">Edit</a> 
                    <a href="<?= URL::route('dashboard.admin') ?>" class="btn btn-white btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- modal -->
<div class="modal fade" id="Report_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title">Bank Details</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <?= Form::open(array('class' => 'form-horizontal form_submit','name' => 'form_submit','id' => 'form_submit')) ?>
            <div class="form-group">
                <label class="control-label col-sm-3" for="device_type Name">Name as Passbook</label>
                <div class="col-sm-8">
                    <input type="text" name="passbook_username" class="form-control" @if(Auth::user()['passbook_username']) value="{{Auth::user()['passbook_username']}}" @endif>
                    <div id="username_error" class="text-danger"></div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3" for="device_type Name">Bank Name</label>
                <div class="col-sm-8">
                    <input type="text" name="bank_name" class="form-control" @if(Auth::user()['bank_name']) value="{{Auth::user()['bank_name']}}" @endif>
                    <div id="bankname_error" class="text-danger"></div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3" for="device_type Name">Account  No</label>
                <div class="col-sm-8">
                    <input type="text" name="account_no" class="form-control" @if(Auth::user()['account_no']) value="{{Auth::user()['account_no']}}" @endif>
                    <div id="accountno_error" class="text-danger"></div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3" for="device_type Name">IFS Code</label>
                <div class="col-sm-8">
                    <input type="text" name="ifs_code" class="form-control" @if(Auth::user()['ifs_code']) value="{{Auth::user()['ifs_code']}}" @endif>
                    <div id="ifscode_error" class="text-danger"></div>
                </div>
            </div>

         {!! Form::close() !!}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="submit_details">Add Details</button>
      </div>
    </div>
  </div>
</div>
<!-- end Modal -->

<!-- this is for bank section -->
<div class="bankdetails_container" id="scroll_down">
    <div class="left_bankdetails">
        <h3 align="center">Bank Details</h3>
        <div class="row">
            <div class="col-sm-3">
                <dl class="dl-horizontal dd-horizontal">
                    <label>Name as per Bank Passbook:</label>
                </dl>
            </div>
            <div class="col-sm-8">
                <dd class="dd-horizontal">
                   {{ ucwords(Auth::user()->passbook_username) }}
               </dd>
           </div>
       </div>
       <div class="row">
            <div class="col-sm-3">
                <dl class="dl-horizontal dd-horizontal">
                    <label>Bank Name:</label>
                </dl>
            </div>
            <div class="col-sm-8">
                <dd class="dd-horizontal">
                   {{ ucwords(Auth::user()['bank_name']) }}
               </dd>
           </div>
       </div>
       <div class="row">
            <div class="col-sm-3">
                <dl class="dl-horizontal dd-horizontal">
                    <label>Account No:</label>
                </dl>
            </div>
            <div class="col-sm-8">
                <dd class="dd-horizontal">
                   {{ ucwords(Auth::user()->account_no) }}
               </dd>
           </div>
       </div>
       <div class="row">
            <div class="col-sm-3">
                <dl class="dl-horizontal dd-horizontal">
                    <label>IFS Code:</label>
                </dl>
            </div>
            <div class="col-sm-8">
                <dd class="dd-horizontal">
                   {{ ucwords(Auth::user()->ifs_code) }}
               </dd>
           </div>
       </div>
       
        <div class="row">
            <div class="col-sm-3 text-left">
                <button class="btn btn-primary bank_add" data-toggle="modal" data-target="#Report_modal">@if(Auth::user()->passbook_username != NULL) Edit @else Add @endif</button> 
            </div>
        </div>
       
        <br>
    </div>
    <!-- modal -->
<div class="modal fade" id="document_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title">Bank Details</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form-horizontal form_submit" id="form_document" enctype="multipart/form-data">

            <input type="hidden" name="check" class="form-control" value="1">
            <div class="form-group">
                <label class="control-label col-sm-3" for="device_type Name">AadharCard No:</label>
                <div class="col-sm-8">
                    <input type="text" name="aadhar_no" class="form-control" @if(Auth::user()['aadhar_no']) value="{{Auth::user()['aadhar_no']}}" @endif>
                    <div id="aadhar_error" class="text-danger"></div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3" for="device_type Name">Aadhar Image</label>
                <div class="col-sm-8">
                    <input type="file" name="aadhar_img[]" class="form-control" id="aadhar_img" multiple>
                    <div id="aadharimg_error" class="text-danger"></div>
                    
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-3" for="device_type Name">PanCard No:</label>
                <div class="col-sm-8">
                    <input type="text" name="pancard_no" class="form-control" @if(Auth::user()['pancard_no']) value="{{Auth::user()['pancard_no']}}" @endif>
                    <div id="pancard_error" class="text-danger"></div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3" for="device_type Name">PanCard Image</label>
                <div class="col-sm-8">
                    <input type="file" name="pancard_img" class="form-control">
                    <div id="pancardimg_error" class="text-danger"></div>
                </div>
            </div>

         
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <input type="submit" class="btn btn-primary" id="submit_document" value="Add">
      </div>
    </div>
    </form>
  </div>
</div>
<!-- end Modal -->
    <div class="right_documnent">
        <h3 align="center">Document</h3>
        <div class="row">
            <div class="col-sm-6" style="display:flex; justify-content: space-evenly;">
                <dl class="dl-horizontal dd-horizontal">
                    <label>Aadhar No:</label>
                </dl>
                <dd class="dd-horizontal">
                   {{ ucwords(Auth::user()->aadhar_no) }}
               </dd>
            </div>
           <div class="col-sm-6" style="display:flex; justify-content: space-evenly;">
                <dd class="dd-horizontal">
                    <label>Pancard No:</label>
               </dd>
                <dd class="dd-horizontal">
                   {{ ucwords(Auth::user()->pancard_no) }}
               </dd>
           </div>
          
        </div>
        
        <div class="row">
            <div class="col-sm-6">
                <dd class="dd-horizontal" style="text-align: center;">
                @if(Auth::user()->aadhar_img != NULL)
                    @foreach($split_img as $image)
                        <div style="margin-bottom:20px;"> 
                            <img src="/document/{{ $image }}" alt="" style="width:210px;height:120px;">
                        </div>
                    @endforeach
                @endif
               </dd>
           </div>
           
           <div class="col-sm-6" >
                <dd class="dd-horizontal" style="text-align: center;">
                  @if(Auth::user()->pancard_img != NULL)
                    <img src="{{url('storage/app/document/Patel Meet _107349946.jpeg')}}" alt="" style="width:195px;height:120px;">
                 @endif
               </dd>
           </div>
       </div>
      
        @if(Auth::user()->aadhar_no == NULL)
       <div class="row">
            <div class="col-sm-3 text-left">
                <button class="btn btn-primary bank_add" data-toggle="modal" data-target="#document_modal">  Add</button> 
            </div>
        </div>
        @endif
        <br>
    </div>
    @if(Auth::user()->aadhar_img == NULL)
    <input type="hidden" name="for_check" class="check_val" value="1">
    @else
    <input type="hidden" name="for_check" class="check_val" value="0">
    @endif
</div>
@stop
@section('style')
<?= Html::style('asset/css/bootstrap-datepicker.css') ?> 
<?= Html::style('css/colorbox.css') ?>
@stop
@section('script')

<?= Html::script('asset/js/bootstrap-datepicker.min.js') ?>
<?= Html::script('js/jquery.colorbox-min.js') ?>
<!-- <script src="http://malsup.github.com/jquery.form.js"></script>  -->
<!-- <script src="http://malsup.github.io/jquery.form.js"></script>  -->
<?= Html::script('asset/js/malsup.github.io.js') ?>
<?= Html::script('asset/js/select2.min.js') ?>

<script>
$(document).ready(function(){
        if ($('.check_val').val() == 1) {
            $("html, body").animate({scrollTop: $('html, body').get(0).scrollHeight}, 1000);
        }
    $('#submit_details').click(function(){
        $('#username_error').text("");

        $('#bankname_error').text("");
        $('#accountno_error').text("");
        $('#ifscode_error').text("");
        $.ajax({
            type:'POST',
            url: "/bank_detail",
            data: $('#form_submit').serialize(),
            success: (data) => {
                if (data.success == true) {
                        iziToast.success({
                            title:'Success!',
                            message: "Bank Details Add/Change successfully",
                        });
                       setTimeout(function() {
                        $('#Report_modal').hide();
                        location.reload();
                        }, 3000);
                   }
            },
            error : function(res){
                $('#username_error').text(res.responseJSON.passbook_username);
                $('#bankname_error').text(res.responseJSON.bank_name);
                $('#accountno_error').text(res.responseJSON.account_no);
                $('#ifscode_error').text(res.responseJSON.ifs_code);
            }
        })
    });
      
    $('#form_document').submit(function(e){
        e.preventDefault();
        $('#aadhar_error').text("");
        $('#aadharimg_error').text("");
        $('#pancard_error').text("");
        $('#pancardimg_error').text("");
        var formData = new FormData(this);
        var token="{{csrf_token()}}";
        $.ajax({
            type:'POST',
            url: "/document_detail",
            data: formData,
            headers: {'X-CSRF-TOKEN':token},
            contentType: false,
            processData: false,
            success: (data) => {
                if (data.success == true) {
                        iziToast.success({
                            title:'Success!',
                            message: "Bank Details Add/Change successfully",
                        });
                       setTimeout(function() {
                        $('#Report_modal').hide();
                        location.reload();
                        }, 3000);
                   }
            },
            error : function(res){
                console.log(res.responseJSON)
                $('#aadhar_error').text(res.responseJSON.aadhar_no);
                $('#aadharimg_error').text(res.responseJSON.aadhar_img);
                $('#pancard_error').text(res.responseJSON.pancard_no);
                $('#pancardimg_error').text(res.responseJSON.pancard_img);
            }
        })
    });
      
    
});
</script>
@stop