<!DOCTYPE html>
<html>
<head>
		<?= Html::style('/main.css')?>
</head>
	<body style="background-color: #fff;margin-bottom: 30px;">
		<table>
			<tr>
				<td width="80%"><b>{{$name}} - {{$month_name}} {{$year}}</b>Summary</td>
				<td width="20%"><h1>THINK TANKER</h1></td>
			</tr>
		</table>
		<div class="container">
		  <div style="border: 1px solid #ddd;margin-bottom: 30px;float: left;width: 100%;">
		  	<table style="width: 100%;float: left;">
		  		<tbody>
		  			<tr>
		  				<th style="float: left;width: 22%;padding: 10px"><b>Improper Logs</b></th>
		  				<th style="float: left;width: 20%;padding: 10px"><b>Approve Leaves</b></th>
		  				<th style="float: left;width: 20%;padding: 10px"><b>Disapprove Leaves</b></th>
		  				<th style="float: left;width: 18%;padding: 10px"><b>Work Hours</b></th>
		  			</tr>
		  			<tr>
		  				<td style="float: left;width: 20%;padding: 10px" align="center">
		  						{{$improper_count}}
		  				</td>
		  				<td align="right" style="width: 25%;padding: 10px;" align="center">
		  						{{$leave_count_approve}}
		  				</td>
		  				<td align="right" style="width: 30%;padding: 10px;" align="center">
		  						{{$leave_count_disapprove}}
		  				</td>
		  				<td align="right" style="width: 25%;padding: 10px;" align="center">
		  						{{$works_hours}}
		  				</td>
		  			</tr>	
		  		</tbody>
		  	</table>
		  </div>
		  <div class="table-responsive" style="float: left;width: 100%">         
			  <table class="table table table-bordered" style="font-size: 12px;" cellpadding="7">
			    <thead>
			      <tr>
			        <th style="width:150px;">Date</th>
			        <th style="width:150px;">Day</th>
			        <th style="width:150px;">Time In</th>
			        <th style="width:150px;">Time Out</th>
			        <th style="width:150px;">Hours</th>
			      </tr>
			    </thead>
			    <tbody>
			        @foreach ($pdf_data as $product=>$value)
			        <tr>
			            <td style="width:150px;" align="center" @if(($value['time_diff']) < '09:00:00' && '00:00:00' < ($value['check_in']))style="background-color:{{config('project.error_code.code')}}" @endif @if(date('l', strtotime($value['time'])) == 'Sunday')style="background-color:rgb(100, 219, 167);" @endif @if(date('l', strtotime($value['time'])) == 'Saturday' && ($value['check_in']) < '09:00:00')style="background-color:{{config('project.sat_code.code')}}" @endif @if(($value['check_in']) == '12:00:00')style="background-color:rgb(100, 219, 167);"@endif>{{ $value['time'] }}</td>
			            @if(($value['check_in']) == '12:00:00')
			            <td colspan="4" style="background-color:rgb(100, 219, 167);" align="center">User Leave</td>
			            @else
			            <td style="width:150px;" align="center" @if(($value['time_diff']) < '09:00:00' && '00:00:00' < ($value['check_in']))style="background-color:{{config('project.error_code.code')}}" @endif @if(date('l', strtotime($value['time'])) == 'Sunday')style="background-color:rgb(100, 219, 167);" colspan="4"  @endif @if(date('l', strtotime($value['time'])) == 'Saturday' && ($value['check_in']) < '09:00:00')style="background-color:{{config('project.sat_code.code')}}" colspan="4" @endif>{{date('l', strtotime($value['time']))}}</td>
			            <td  style="width:150px;" align="center" @if(($value['time_diff']) < '09:00:00' && '00:00:00' < ($value['check_in']))style="background-color:{{config('project.error_code.code')}}" @endif @if(date('l', strtotime($value['time'])) == 'Sunday')style="background-color:rgb(100, 219, 167);" style="display: none;" @endif @if(date('l', strtotime($value['time'])) == 'Saturday' && ($value['check_in']) < '09:00:00')style="background-color:{{config('project.sat_code.code')}}" style="display: none;" @endif>@if(date('l', strtotime($value['time'])) == 'Saturday' && ($value['check_in']) < '09:00:00') @elseif(date('l', strtotime($value['time'])) == 'Sunday') @else{{ $value['check_in'] }} @endif</td>
			            <td style="width:150px;" align="center" @if(($value['time_diff']) < '09:00:00' && '00:00:00' < ($value['check_in']))style="background-color:{{config('project.error_code.code')}}" @endif @if(date('l', strtotime($value['time'])) == 'Sunday')style="background-color:rgb(100, 219, 167);" style="display: none;" @endif @if(date('l', strtotime($value['time'])) == 'Saturday' && ($value['check_in']) < '09:00:00')style="background-color:{{config('project.sat_code.code')}}" style="display: none;" @endif>@if(date('l', strtotime($value['time'])) == 'Saturday' && ($value['check_in']) < '09:00:00') @elseif(date('l', strtotime($value['time'])) == 'Sunday') @else{{ $value['check_out'] }}@endif</td>
			            <td style="width:150px;" align="center" @if(($value['time_diff']) < '09:00:00' && '00:00:00' < ($value['check_in']))style="background-color:{{config('project.error_code.code')}}" @endif @if(date('l', strtotime($value['time'])) == 'Sunday')style="background-color:rgb(100, 219, 167);" style="display: none;" @endif @if(date('l', strtotime($value['time'])) == 'Saturday' && ($value['check_in']) < '09:00:00')style="background-color:{{config('project.sat_code.code')}}" style="display: none;" @endif>@if(date('l', strtotime($value['time'])) == 'Saturday' && ($value['check_in']) < '09:00:00') @elseif(date('l', strtotime($value['time'])) == 'Sunday') @else{{ $value['time_diff'] }}@endif</td>
			            @endif
			        </tr>
			        @endforeach
			    </tbody>
			  </table>
			</div>  
		</div>
	</body>
</html>