@extends('layout.layout')
@section('content')
<style type="text/css">
    .page-title{
        position: fixed;
        width: 100%;
        z-index: 1;
        margin-bottom: 74px;
    }
    .row1{
        margin-top: 114px;
    }
    .row2{
        display: flex;
        margin-right: 240px;
    }
    .row3{
        display: flex;
        flex-direction: column;
        margin-right: 20px;
    }
    .btn-color{
        font-size: 11px !important;
        padding: 5px 10px !important;
        background-color: #397E71;
    }
    .dataTables_filter{
      display: none;
    }
    .back_button{
        display: flex;
    }
    .padding_space{
        margin-right: 10px;
    }
</style>
<div class="page-title">
    <div class="back_button">
        <div class="padding_space">
            @if(auth()->user()->is_admin)  
            <a href="<?= route('user.index')?>" class="btn btn-primary"><i class="fa fa-arrow-left"></i>BACK</a>
            @endif
        </div>
        <div class="emplyee_logs">
        <h1>{{ ucwords($name['fullname'])}} Logs </h1>
        <ul class="breadcrumb side">
           <!--  <li><i class=""></i></li> -->
            <li class="active"><a href="">Employee Logs</a></li>
            <li>
            <!-- <?= Form::selectMonth('month',date('m'),['id'=>'month','class' => 'form-control breadcrumb-select input-sm']); ?>
            <?= Form::selectRange('year','2013',date('Y'),date('Y'),['id'=>'year','class' => 'form-control breadcrumb-select input-sm']); ?> -->
            <select id="month" name="month" class="form-control breadcrumb-select input-sm month"></select>
              <select id="year" name="year" class="form-control breadcrumb-select input-sm"></select>
            <input type="hidden" name="month" value="<?= date('m')?>">
            </li>
            <!-- @if(auth()->user()->is_admin)
            <a style="margin-bottom: 10px;" href="<?= url('leaves/'.$id) ?>" taget="_self" id="" class="badge btn-color">Leaves
            </a>
            @endif -->
        </ul>
        </div>
    </div>
    <div class="row2">
            <a style="display: flex;align-self: end;padding: 10px;"><h5 class="btn btn-primary">Improper Logs:<span id="count"></span></h5></a>
            <a style="display: flex;align-self: end;padding: 10px;"><h5 class="btn btn-primary">Late Coming Logs:<span id="late_count"></span></h5></a>
     @if(auth()->user()->is_admin)
        <div class="row3">
            <!--  <a href="" style="margin-bottom: 10px;"class="currentyear currentmonth 
               btn btn-primary btn-flat col-2" id="total_hours"></a> -->

            <a href="<?= url('logs/logs-user-workinghours/'.$id.'/'.date("m").'/'.date("Y")) ?>" style="margin-bottom: 10px;"class="getyear getmonth 
               btn btn-primary btn-flat col-2" id="working_hr"></a>
               <a style="margin-bottom: 10px;" href="<?= url('leaves/'.$id) ?>" taget="_self" id="" class="btn btn-primary btn-flat">Leaves
              </a>
               
        </div>
        <div class="row3">
            <a style="margin-bottom: 10px;" href="<?= url('logs/alluserexportlogs/'.$id.'/'.date("m").'/'.date("Y")) ?>" taget="_self" id="allexportlogs" class="btn btn-primary btn-flat">Export All User Logs
            </a>
            <a href="<?= url('logs/exportlogs/'.$id.'/'.date("m").'/'.date("Y")) ?>" taget="_self" id="exportlogs" class="btn btn-primary btn-flat">Export Logs
            </a>
        </div>
    @endif
    </div>
</div>
<div class="row1">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
            	<table id="employee" class="table table-hover table-bordered" border="1 px">
               		<thead>
                  		<tr>
                            <th>Date</th>
    		                <th>Day</th>
                            <th>Date</th>
    		                <th>TimeIn</th>
                            <th>TimeOut</th>
    		                <th>Hours</th>
		                </tr>
               		  </thead>
                  <tbody>    
                </tbody>
              </table>
         	</div>
      	</div>
   	</div>
</div>
<div class="row d-flex">
<div class="col-6" id="monthtimetotal"></div>
</div>
@stop

@section('script')
<?= Html::script('asset/DataTables/media/js/jquery.dataTables.js') ?>
<?= Html::script('asset/DataTables/media/js/dataTables.bootstrap.min.js') ?>
<?= Html::script('asset/js/plugins/sweetalert.min.js') ?>
<?= Html::script('asset/js/plugins/select2.min.js') ?>
<?= Html::script('asset/js/fnStandingRedraw.js') ?>

<script type="text/javascript">
var token = "<?= csrf_token() ?>";
var path = '<?= URL::route('show.logs',['id'=>':id']) ?>';
path = path.replace(':id','<?= $id ?>');
var user_id = path.split('/').pop();
// alert();
$( document ).ready(function(){
    var url_id = '<?= $id ?>';
    var logged_id = "<?=$logged_user_id?>"

    if(logged_id != url_id && logged_id != ''){
        var path = '<?= URL::route('show.logs',['id'=>':id']) ?>';
        path = path.replace(':id','<?= $logged_user_id ?>');
        window.location.href = path;

    }
    $('#employee').DataTable(
    {   
        "bProcessing" : true,
        "bServerSide" : true,  
        'bPaginate'   : false,
        "pageLength"  : 50,
        "ajax": {
            'url':path,
            'data':function(d){
                if ($('#month').val() === null) {
                    var a = new Date();
                    d.month= a.getMonth();
                    d.year=a.getFullYear();
                }else{
                    d.month=$('#month').val();
                    d.year=$('#year').val();
               }
            }
        },
        "aaSorting"   : false,
        "aoColumns"   :[
            { mData : 'current_date',bSortable:false},
            { mData : 'day',bSortable:false},
            { mData : 'time',bVisible:false},
            { mData : 'check_in',bSortable:false},
            { mData : 'check_out',bSortable:false},
            { mData : 'time_diff',bSortable:false}
        ],

        fnDrawCallback: function (oSettings) {
            if(oSettings.aoData == "")
            {
                $('#working_hr').html("Working Hours: "+"00:00:00");
                // $('#total_hours').html("Total Hours: "+"00:00:00");
            }else{
               
            }    
        },

        rowCallback: function( row, data, index ,col) {
            $("#count").text(data.count_test);
            $("#late_count").text(data.latecoming_count);
            var  $node = this.api().row(row).nodes().to$();
            var holidayDate = data.holidayDate;
            // console.log(data);
            
            var weekday = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];

            var current_date = data.current_date;
                current_date = current_date.split("-").reverse().join("-");
            var format_date = formatDate(current_date)
            var date = new Date(format_date);
            var day = weekday[date.getDay()];

            var d = new Date(date);

            var getTot = daysInMonth(d.getMonth(),d.getFullYear()); //Get total days in a month
            var saturday_num = new Array();
            var sun = new Array();   
            for(var i=1;i<=getTot;i++)
            {   
                var newDate = new Date(d.getFullYear(),d.getMonth(),i)
                if(newDate.getDay()==0){   //if Sunday
                    sun.push(i);
                }
                if(newDate.getDay()==6){   //if Saturday
                 saturday_num.push(i);
                }
            }

            var working_hr = "Working Hours: "+(data.working_hr)+"/"+data.count_working_hours;
            // var total_hours = "Total Hours: "+(data.count_working_hours);
            var today_date = new Date();
            //$('#total_hours').html(total_hours);
            $('#working_hr').html(working_hr);
            function daysInMonth(month,year)
            {
                return new Date(year, month, 0).getDate();
            }

            function convert(str) 
            {
                var date = new Date(str),
                mnth = ("0" + (date.getMonth() + 1)).slice(-2),
                day = ("0" + date.getDate()).slice(-2);
                return day;
            }

            function formatDate(date) {
                var d = new Date(date),
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();
                if (month.length < 2) 
                    month = '0' + month;
                if (day.length < 2) 
                    day = '0' + day;
                return [year, month, day].join('-');
            }

            var currentDate = new Date(current_date);
            var n1 =convert(currentDate)
            function capitalize_Words(str)
            {
             return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
            }
           

            if (data.time_diff < '09')
            {
                if(data.userleave ==  data.current_date && data.status == "a")
                {   
                    // alert(data.time_diff)
                    if (data.leave_time == "half day") {
                        $node.find("td:eq(4)").text(data.time_diff);
                    }else{
                        $node.find("td:eq(4)").text('09:00:00');
                    }
                    $node.find("td:eq(2)").text(capitalize_Words(data.leave_time) +'- Approved - Paid Leaves').css('text-align','center');
                    $node.find("td:eq(2)").attr('colspan',2);
                    $node.find("td:eq(3)").remove();
                    $node.css('background-color','{{config('project.sat_code.code')}}');
                }else if(data.userleave ==  data.current_date && data.status == "d")
                {
                    if (data.leave_time == "half day") {
                        $node.find("td:eq(4)").text(data.time_diff);
                    }else{
                        $node.find("td:eq(4)").text('09:00:00');
                    }
                    $node.find("td:eq(2)").text(capitalize_Words(data.leave_time) +'- Disapproved - UnPaid Leaves').css('text-align','center');
                    $node.find("td:eq(2)").attr('colspan',2);
                    $node.find("td:eq(3)").remove();
                    $node.css('background-color','{{config('project.sat_code.code')}}');
                }
                else if(data.userleave ==  data.current_date && data.status == "p")
                {
                    $node.find("td:eq(2)").text('Pending Leaves').css('text-align','center');
                    $node.find("td:eq(2)").attr('colspan',2);
                    $node.find("td:eq(3)").remove();
                    $node.css('background-color','{{config('project.sat_code.code')}}');
                }else if(user_id != 74 && user_id != 69 &&  user_id != 116 )
                {
                    $node.css('background-color','rgb(248 209 200)');
                }else if((user_id == 74 || user_id == 69 || user_id == 116) && data.time_diff < "08"){
                    $node.css('background-color','rgb(248 209 200)');
                }
            }
            

            var date3 = (new Date());
            date4 = convert1(date3)
            
            function convert1(str) {
              var date = new Date(str),
                  mnth = ("0" + (date.getMonth() + 1)).slice(-2),
                  day = ("0" + date.getDate()).slice(-2);
              return [day, mnth, date.getFullYear()].join("-");
            }

            if(date4 == data.current_date)
            {
                $node.css('background-color','white');
            }
            if(holidayDate != "" && day !=  "Sunday")
            {
                $(holidayDate).each(function(k,v)
                {    
                    if(data.current_date == v)
                    {
                        $node.css('background-color','#64dba7');
                        $node.find("td:eq(2)").text('Holiday');
                        $node.find("td:eq(2)").css('text-align','center');
                        $node.find("td:eq(2)").attr('colspan',3);
                        $node.find("td:eq(3)").remove();
                        $node.find("td:eq(1)").remove();
                    }   

                });
            }
            
            if (data.time_diff == "00:00:00")
            {
                if(data.userleave ==  data.current_date && data.status == "a")
                {
                    if (data.leave_time == "half day") {
                        $node.find("td:eq(4)").text(data.time_diff);
                    }else{
                        $node.find("td:eq(4)").text('09:00:00');
                    }
                    $node.find("td:eq(2)").text(capitalize_Words(data.leave_time) +'- Approved - Paid Leaves').css('text-align','center');
                   $node.find("td:eq(2)").attr('colspan',2);
                   $node.css('background-color','{{config('project.sat_code.code')}}');
                }else if(data.userleave ==  data.current_date && data.status == "d")
                {
                    if (data.leave_time == "half day") {
                        $node.find("td:eq(4)").text(data.time_diff);
                    }else{
                        $node.find("td:eq(4)").text('09:00:00');
                    }
                   $node.find("td:eq(2)").text(capitalize_Words(data.leave_time) +'- Disapproved - UnPaid Leaves').css('text-align','center');
                   $node.find("td:eq(2)").attr('colspan',2);
                   $node.css('background-color','{{config('project.sat_code.code')}}');
                }
                else if(data.userleave ==  data.current_date && data.status == "p")
                {
                  // $node.find("td:eq(4)").text('00:00:00');
                   $node.find("td:eq(2)").text('Pending Leaves').css('text-align','center');
                   $node.find("td:eq(2)").attr('colspan',2);
                   $node.find("td:eq(3)").remove();
                   $node.css('background-color','{{config('project.sat_code.code')}}');
                }else
                {
                    $node.css('background-color','rgb(248 209 200)');
                }
            }
            if (day ==  "Sunday")
            {
               $node.css('background-color','#64dba7');
               $node.find("td:eq(2)").text('Sunday');
               $node.find("td:eq(2)").css('text-align','center');
               $node.find("td:eq(2)").attr('colspan',3);
               $node.find("td:eq(3)").remove();
               $node.find("td:eq(1)").remove(); 
            }

            if (day ==  "Saturday" && saturday_num[1] == n1)
            {
               $node.css('background-color','#64dba7)');
               $node.find("td:eq(2)").text('Saturday');
               $node.find("td:eq(2)").css('text-align','center');
               $node.find("td:eq(2)").attr('colspan',3);
               $node.find("td:eq(3)").remove();
               $node.find("td:eq(1)").remove();
            }

            if (day ==  "Saturday" && saturday_num[3] == n1)
            {
               $node.css('background-color','#64dba7');
               $node.find("td:eq(2)").text('Saturday');
               $node.find("td:eq(2)").css('text-align','center');
               $node.find("td:eq(2)").attr('colspan',3);
               $node.find("td:eq(3)").remove();
               $node.find("td:eq(1)").remove();
            }
        }
    });

    $('#month').change(function(){
        //for export logs
        var months = parseInt($('#month').val());
        months=months+1;
        @if(auth()->user()->is_admin)
        url = $('#exportlogs').attr('href');
        main_url = url.split('/');
        main_url[6] = ("0" + months).slice(-2);
        month_url = main_url.join('/');
        $('#exportlogs').attr('href',month_url);
    
        //for total working hours
        user_url1 = $('.getmonth').attr('href');
        user_main_url1 = user_url1.split('/');
        user_main_url1[6] = ("0" + months).slice(-2);
        user_month_url1 = user_main_url1.join('/');
        $('.getmonth').attr('href',user_month_url1);

        //for all user export logs
        user_url = $('#allexportlogs').attr('href');
        user_main_url = user_url.split('/');
        user_main_url[6] = ("0" + months).slice(-2);
        user_month_url = user_main_url.join('/');
        $('#allexportlogs').attr('href',user_month_url);
        @endif
        
        $('#month').on('click',function(){
           $('#monthtimetotal').val('time_diff');
        }); 
        $('.dataTable').each(function() {
            dt = $(this).dataTable();
            dt.fnDraw();
        });
    });

    $('#year').change(function(){
        //for export logs
        @if(auth()->user()->is_admin)
        url = $('#exportlogs').attr('href');
        main_url = url.split('/');
        main_url[7] = $('#year').val();
        year_url = main_url.join('/');
        $('#exportlogs').attr('href',year_url);
     
        //for total working hours
        user_url2 = $('.getyear').attr('href');
        user_main_url2 = user_url2.split('/');
        user_main_url2[7] = $('#year').val();
        user_year_url2 = user_main_url2.join('/');
        $('.getyear').attr('href',user_year_url2);

        //for all user export logs
        user_url = $('#allexportlogs').attr('href');
        user_main_url = user_url.split('/');
        user_main_url[7] = $('#year').val();
        user_year_url = user_main_url.join('/');
        $('#allexportlogs').attr('href',user_year_url);
        @endif
        
        $('.dataTable').each(function() {
            dt = $(this).dataTable();
            dt.fnDraw();
        });
    });

});

$(document).ready(function() 
{
  const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
  ];
  let qntYears = 9;
  let selectYear = $("#year");
  let selectMonth = $("#month");
  console.log(selectMonth)
  let currentYear = new Date().getFullYear();
  for (var y = 0; y < qntYears; y++)
  {
    let date = new Date(currentYear);
    let yearElem = document.createElement("option");
    yearElem.value = currentYear
    yearElem.textContent = currentYear;
    selectYear.append(yearElem);
    currentYear--;
  }
         
  var d = new Date();
  var getyears = d.getFullYear();
  var getmonth = d.getMonth();
  getmonth =  getmonth+1;

  var d = new Date();
  var month = d.getMonth();
  var year = d.getFullYear();
  
  AdjustMonth();
  selectMonth.val(year);

  selectYear.val(year);
  selectYear.on("change", AdjustMonth);
  selectMonth.val(month);

  function AdjustMonth() 
  {
    var setmonth =$(".month").val();
    console.log(setmonth)
    var year = selectYear.val();
    var month = parseInt(selectMonth.val()) + 1;
    selectMonth.empty();
    if(year == getyears)
    {
      for (var m = 0; m < getmonth; m++) 
      {
        let month1 = monthNames[m];
        let monthElem1 = document.createElement("option");
        monthElem1.value = m;
        monthElem1.textContent = month1;
        selectMonth.append(monthElem1);
        selectMonth.val(setmonth);
      }
    }else
    {
      for (var m = 0; m < 12; m++){
        let month1 = monthNames[m];
        let monthElem1 = document.createElement("option");
        monthElem1.value = m;
        monthElem1.textContent = month1;
        selectMonth.append(monthElem1);
        selectMonth.val(setmonth);
      }
    }
  }
});

</script>
@stop
