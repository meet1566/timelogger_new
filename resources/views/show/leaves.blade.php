@extends('layout.layout')
@section('content')
<style>
  .btn-color{
        font-size: 16px !important;
        padding: 5px 10px !important;
        background-color: #397E71;
        margin-bottom: -1px !important;
        margin-left: 789px;
        margin-top: -30px;
    }
    .back_button{
        display: flex;
        align-items: center;
    }
    .padding_space{
        margin-right: 10px;
    }

  }
</style>
<div class="page-title">
      <div class="back_button">
        <div class="padding_space">

         <a href="<?= route('user.index')?>" class="btn btn-primary"><i class="fa fa-arrow-left"></i>BACK</a>
        </div>
        <h1>{{ucwords($username['fullname'])}} Leaves</h1>
          
      </div>
      <div style="display: flex;align-items: center;">
        <ul class="breadcrumb side">
            <!-- <li><i class="fa fa-home fa-lg"></i></li> -->
            <li class="active">
              @if(auth()->user()->is_admin)
              <a style="margin-bottom: 10px;" href="<?= url('logs/'.$id) ?>" taget="_self" id="" class="btn btn-primary btn-flat" > Logs
            </a>@endif
          </li>
          </ul>
        <a style="margin-left: 10px;" href="<?= route('user.leaves.create',$id)?>" class="btn btn-primary btn-flat"><i class="fa fa-lg fa-plus"></i></a>
    </div>
</div>
<div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))

      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
    @endforeach
</div>
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-body">
              <table id="employee" class="table table-hover table-bordered" border="1 px">
              <thead>
                <tr>
                <th>Date</th>
                <th>Day</th>
                <th>Leave Type</th>
                <th>Half Or Full</th>
                <th>Leave Purpose</th>
                <th>From Hour</th>
        		<th>To Hour</th>
        		<th>Status</th>
                <th>Action</th>
                </tr>
              </thead> 
              </table>
              </div>
            </div>
          </div>
        </div>
@stop
@section('script')

<?= Html::script('asset/DataTables/media/js/jquery.dataTables.js') ?>
<?= Html::script('asset/DataTables/media/js/dataTables.bootstrap.min.js') ?>
<script>
var token = "<?= csrf_token() ?>";
var path = '<?= URL::route('show.leaves',['id'=>':id']) ?>';
path = path.replace(':id','<?= $id ?>');

function btn_delete(id){
    //alert('hi');
    swal({
        title: "Are you sure to delete Request?",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: true,
        closeOnCancel: true
    }, function(isConfirm) {
        if (isConfirm) {
            $.ajax(
                {
                url : "<?=URL::route('leaves.delete') ?>",
                type : "post",
                datatype : "JSON",
                data : { id: id,_token: token },
                success : function(resp){
                      console.log(resp);
                      iziToast.success({
                      title:'Success!',
                        message: "Record Successfully Deleted",
                      });
                      if(resp)
                      {
                         window.setTimeout(function(){location.reload()},4000)
                      }
                      $('#delete input:checkbox[name="selectall"]').prop('checked', false);
                  }
                })
        }
    });
} 
$(document).ready(function()
{
  $('#employee').DataTable(
  {
    "bProcessing" : true,
    "bServerside" : true,
    "ajax"        :{
        'url': path
    },
    "aaSorting": [
                   // [2, "asc"]
               ],
    "aoColumns"   :[
        { mData: 'date',sWidth: "8%",bSortable:false},
        { mData: 'day',sWidth: "8%",bSortable:false},
        { mData: 'leave_type',sWidth: "10%",bSortable:false},
        { mData: 'leave_time',sWidth: "8%",bSortable:false},
        { mData: 'leave_purpose',sWidth: "30%",bSortable:false},
        { mData: 'from_hour',sWidth: "7%",bSortable:false},
        { mData: 'to_hour',sWidth: "7%",bSortable:false},
        {
            "mData": "status",
            bSortable:false,
            sClass: "text-center",
            sWidth: "11%",
            mRender:function(v,t,o)
            {
                if (v == 'a') {
                  return '<span class="badge bg-green" style="background-color: #006400;">Approved</span>'
              } else if(v == 'au' ){
                  return '<span class="badge bg-red" style="background-color: #006400;">Approve</span><span class="badge bg-red" style="background-color: #fc5c3c;">UnPaid</span>'
              } else if( v == 'u' || v == 'd' ){
                  return '<span class="badge bg-red" style="background-color: #fc5c3c;">UnPaid</span>'
              } else {
                  return '<span class="badge bg-yellow">Pending</span>'
              }
            }
        },
        { mData: null,
          bSortable:false,
          sWidth: "10%",
          mRender:function(v,t,o)
          {
            var path     = "<?= URL::route('leaves.edit',array(':id')) ?>";
              
            path     = path.replace(':id',o['id']);
            
             
            var extra_html = '<a href="'+path+'" class="fa fa-fw fa-edit" style="font-size: 18px"></a>' +
                             '<a id="delete" class="fa fa-fw fa-trash-o" href="javascript:void(0)" style="font-size: 18px" onclick="btn_delete('+o['id']+')"></a>' 
            
            return extra_html;
            }

          },
                
    ],
});
  
    $('#delete').click(function(){
        //alert('hi');
        var deleted_id = $('#employee tbody input[type=checkbox]:checked');

        if(deleted_id.length > 0)   
        {
            var id = [];
                $.each(deleted_id,function(i,ele){
                    id.push($(ele).val());
                });
        }
    });
     $.ajaxSetup({
              statusCode: {
                401: function() {
                  location.reload();
                }
              }
        });      

});

</script>

@stop