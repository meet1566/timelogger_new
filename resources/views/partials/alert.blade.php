@if (count($errors) > 0)
<script>
    $(document).ready(function(){
        iziToast.error({
        title:'There were some error!',
        message: "{{ Session::get('message') }}",
        });
    });
</script>
@endif

@if (Session::has('message'))
	@if (Session::has('message_type') && 'success' == Session::get('message_type'))
	<script>
        $(document).ready(function(){
            iziToast.success({
            title:'Ok',
            message: "{{ Session::get('message') }}",
            });
        });
    </script>
	@elseif (Session::has('message_type') && 'danger' == Session::get('message_type'))
		<script>
	        $(document).ready(function(){
	            iziToast.error({
	            title:'Ok',
	            message: "{{ Session::get('message') }}",
	            });
	        });
	    </script>
	@elseif (Session::has('message_type') && 'warning' == Session::get('message_type'))
	<div class="alert alert-warning" role="alert">
		<script>
	        $(document).ready(function(){
	            iziToast.warning
	            ("{{ Session::get('message') }}");
	        });
	    </script>
	</div>
	@elseif (Session::has('message_type') && 'info' == Session::get('message_type'))
		<script>
	        $(document).ready(function(){
	            iziToast.info
	            ("{{ Session::get('message') }}");
	        });
	    </script>
	@endif
@endif