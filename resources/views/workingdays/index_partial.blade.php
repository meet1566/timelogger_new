@foreach($dates as $key=>$value)
    <tr 
      @if($value['dayname'] == "Sunday") 
        style="background-color:{{config('project.sun_code.code')}}" 
      @elseif(($value['dayname'] == "Saturday")) 
        style="background-color:{{config('project.sat_code.code')}}" 
      @endif>
      <td style="width: 45px;text-align: center;">
        <div class="animated-checkbox">
          <label>
            <input type="checkbox" name="dates" id="dates" value="{{$key}}" @if($working_hrs != null) @foreach($working_hrs as $hrs_key=>$hrs_value) @if($hrs_value == $key) checked @endif @endforeach @endif>
              <span class="label-text"></span>
            </label>
          </div>  
      </td>
      <td>{{$key}}</td>
      <td>
        @foreach($week_name as $week_key=>$week_value)
          @foreach($week_value as $weekname_key=>$weekname_value)
            @if(date("d-m-Y",strtotime($weekname_value)) == $key)
              {{$week_key}}
            @endif
          @endforeach
        @endforeach
      </td>
      <td>{{$value['dayname']}} 
        @if(isset($value['odd_sat_date'])) 
          ({{$value['odd_sat_date']}} - Odd) 
        @elseif(isset($value['even_sat_date']))
          ({{$value['even_sat_date']}} - Even) 
        @elseif(isset($value['odd_sun_date'])) 
          ({{$value['odd_sun_date']}} - Odd) 
        @elseif(isset($value['even_sun_date']))
          ({{$value['even_sun_date']}} - Even) 
        @endif
      </td>
    </tr>
@endforeach