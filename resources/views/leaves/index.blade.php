@extends('layout.layout')
@section('content')
<div class="page-title">
        <div>
          <h1 style="float: left;width: auto;margin-right: 10px;line-height: 40px;">Leaves</h1>
          @if(auth()->user()->is_admin)
            <?=Form::select('user', $user, Session::get("leave_user_id"), array('class' => 'form-control select', 'id' => 'user','style'=>'float: left;width: 170px;'))?>
            <?=Form::select('leavetype',config('project.leave_type'), null, array('class' => 'form-control select', 'id' => 'leavetype','style'=>'float: left;width: 170px;margin-left: 10px;display: inline-block;'))?>
            <?= Form::selectRange('year','2016','2022',date('Y'),['id'=>'year','class' => 'form-control breadcrumb-select input','style'=>'float: left;width: 170px;margin-left: 10px;display: inline-block;']); ?>
            <input type="hidden" name="month" value="<?= date('m')?>">
          @endif
          </div>
          @if(auth()->user()->is_admin)
            <div>
             
              <!-- <a href="javascript:;" class="btn btn-primary btn-flat export_leaves" style="margin-top: 5px;">Export</a> -->
              <a href="<?= url('leaves/{leaves_id}/create') ?>" id="leaves_create" class="fa fa-lg fa-plus btn btn-primary btn-flat" style="margin-top: 6px;">
              </a>
              <a href="<?= url('user/leaves/export/41/all/'.date("Y")) ?>" taget="_self" id="exportleaves" class="btn btn-primary btn-flat" style="margin-top: 5px; margin-left: 4px;">Export Leaves
              </a>

            </div>
          @endif
          <!-- <div><a href="<?=URL::route('leaves.create')?>" class="btn btn-primary btn-flat"><i class="fa fa-lg fa-plus"></i></a> --><!-- <a href="#" class="btn btn-warning btn-flat"><i class="fa fa-lg fa-trash"></i></a> </div>-->
</div>
<div style="display: flex; justify-content: space-between;">
  <div class="row" id="leavesummary" >
  </div>
  <div class="row" id="button_leave" style="margin: 0;">
      <a href="javascript:void(0)" class="btn btn-primary btn-flat approve_leaves">Approved</a>
      <a href="javascript:void(0)" class="btn btn-primary btn-flat approve_unpaid_leaves">Approved Unpaid</a>
      <a href="javascript:void(0)" class="btn btn-primary btn-flat disapprove_leaves">Disapproved</a>
      <br/>
  </div>
</div>
<div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))

      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
    @endforeach
</div>
<div class="row">
  <div class="col-md-12">
    <div class="modal fade" id="leavestatus" role="dialog" tabindex="-1" aria-labelledby="leave-detailsLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
              <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h3 class="modal-title">Leave Approve</h3>
          </div>
          <div class="modal-body" id="leave_body">
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-body">
      <table id="employee" class="table table-hover table-bordered" border="1 px" style="width:100%;">
      <thead>
        <tr>
        <th style="width: 45px;text-align: center;"><div class="animated-checkbox"><label><input type="checkbox" id="selectall" class="select_check_box mt-1" ><span class="label-text"></span></label></div></th>
        <th>Date</th>
        <th>Day</th>
        @if(auth()->user()->is_admin)
        <th>Employee</th>
        @endif
        <th>Leave Type</th>
        <th>Half Or Full</th>
        <th>Leave Purpose</th>
        <th>From Hours</th>
        <th>To Hours</th>
        <th>Status</th>
        <th>Action</th>
        </tr>
      </thead> 
      </table>
      </div>
    </div>
  </div>
</div>
        
@stop
@section('script')

<?= Html::script('asset/DataTables/media/js/jquery.dataTables.js') ?>
<?= Html::script('asset/DataTables/media/js/dataTables.bootstrap.min.js') ?>
<?= Html::script('asset/js/delete_script.js') ?>
<?= Html::script('asset/js/leaveapproval.js') ?>
<?= Html::script('asset/js/fnStandingRedraw.js') ?>
<?= Html::script('asset/js/sweetalert.min.js') ?>
<!-- <script src="sweetalert2.all.min.js"></script> -->
<!-- <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script> -->

<script>
var title = "Are you sure to delete selected record(s)?";
var text = "You will not be able to recover this record";
var type = "warning";
var delete_path = "<?=URL::route('leaves.delete') ?>";
var token = "{{ csrf_token() }}";

//approve
var approve_title = "Are you sure to approve selected record(s)?";
var unpaid_title = "Are you sure to Unpaid selected record(s)?";
var approve_text = "You will not be able to recover this record";
var approve_type = "warning";
var approve_delete_path = "<?=URL::route('leave.status') ?>";
var approve_confirmButtonText = "Yes, approve it!";
var unpaid_confirmButtonText = "Yes, Unpaid it!";

//disapprove
var disapprove_title = "Are you sure to disapprove selected record(s)?";
var disapprove_text = "You will not be able to recover this record";
var disapprove_type = "warning";
var disapprove_delete_path = "<?=URL::route('leave.status') ?>";
var disapprove_confirmButtonText = "Yes, disapprove it!";

  var list = [];
  var status='';
 
function handlecheckbox(this_val,value){
  if(this_val.checked == true){
    $.ajax({
      url: '<?=URL::route('leave.checkboxtatus')?>',
      type:'post',
      data:{'value':value,'_token' : '<?= csrf_token()?>'},
      success:function(resp){
        list.push(resp.data.status);
        
      },
    }); 
  }else{
   
    $.ajax({
      url: '<?=URL::route('leave.checkboxtatus')?>',
      type:'post',
      data:{'value':value,'_token' : '<?= csrf_token()?>'},
      success:function(resp){
        list.splice( list.indexOf(resp.data.status), 1 );
        
      },
    }); 
  }
}
$(document).ready(function()
{
   
  $('.approve_leaves').click(function(){
      var leave_id = $('#employee tbody input[type=checkbox]:checked');
      approve_leave_checkLength(leave_id,approve_title,approve_text,approve_type,approve_delete_path,approve_confirmButtonText);
  });
  $('.approve_unpaid_leaves').click(function(){
      var leave_id = $('#employee tbody input[type=checkbox]:checked');
      approveunpaid_leave_checkLength(leave_id,approve_title,approve_text,approve_type,approve_delete_path,approve_confirmButtonText);
  });
  $('.disapprove_leaves').click(function(){
      var leave_id = $('#employee tbody input[type=checkbox]:checked');
      disapprove_leave_checkLength(leave_id,disapprove_title,disapprove_text,disapprove_type,disapprove_delete_path,disapprove_confirmButtonText);
  });
  // $('.chk_id')
  $(function()
  {
    var master = $('#employee').dataTable({
    "bProcessing": false,
    "bServerSide": true,
    "autoWidth": true,
    "aaSorting": [
        [1, "desc"]
    ],
    "sAjaxSource": "{{ URL::route('leaves.index')}}",
    "fnServerParams": function ( aoData ) {
        aoData.push({ "name": "act", "value": "fetch" });
        aoData.push({ "name": "user", "value": $('#user').val() });
        aoData.push({ "name": "leavetype", "value": $('#leavetype').val() });
        aoData.push({ "name": "year", "value": $('#year').val()});
        server_params = aoData;
    },
    "aoColumns"   :[
    {
        mData: "id",
        bSortable: false,
        bVisible: true,
        sWidth: "2%",
        mRender: function(v, t, o) 
        {
            return '<div class="animated-checkbox">'
            +'<label><input type="checkbox" class="chk_id mt-1 checked_chk" id="chk_'+v+'" name="id[]" value="'+v+'" onchange="handlecheckbox(this,'+v+')"/><span class="label-text"></span></label>'
            +'</div>';
        }
    },
      { mData: 'date',sWidth: "7%",},
      { mData: 'day',sWidth: "7%",bSortable: false},
      @if(auth()->user()->is_admin)
        { mData: 'name',sWidth: "10%",bSortable: false},
      @endif
      { mData: 'leave_type',sWidth: "7%",bSortable: false},
      { mData: 'leave_time',sWidth: "8%",bSortable: false},
      { mData: 'leave_purpose',sWidth: "20%",bSortable: false},
      { mData: 'from_hour',sWidth: "7%",bSortable: false},
      { mData: 'to_hour',sWidth: "7%",bSortable: false},
      {
          "mData": "status",
          bSortable: false,
          sClass: "text-center",
          sWidth: "11%",
          mRender:function(v,t,o)
          {
              // echo(o);
              if (v == 'a') {
                  return '<span class="badge bg-green" style="background-color: #006400;">Approved</span>'
              } else if(v == 'au' ){
                  return '<span class="badge bg-red" style="background-color: #006400;">Approve</span><span class="badge bg-red" style="background-color: #fc5c3c;">UnPaid</span>'
              } else if( v == 'u' || v == 'd' ){
                  return '<span class="badge bg-red" style="background-color: #fc5c3c;">UnPaid</span>'
              } else {
                  return '<span class="badge bg-yellow">Pending</span>'
              }
          }
      },
      
      { mData: null,
        bSortable:false,
        sWidth: "10%",
        mRender:function(v,t,o) {

          if (o['status'] == 'p' || o['status'] == 'd' || o['status'] == 'u') {
                // console.log(o['create_by'] == '1');
            @if(auth()->user()->is_admin)
              if(o['status'] == 'p'){
                var extra_html = "<a title='Delete' href='javascript:void(0)' onclick=\"deleteRecord('"+delete_path+"','"+title+"','"+text+"','"+token+"','"+type+"',"+o['id']+")\" style='font-size: 18px' class='fa fa-fw fa-trash'></a>\
                <a title='Approve' href='javascript:void(0)' onclick=\"leaveApproval('"+approve_delete_path+"','"+approve_title+"','"+approve_text+"','"+token+"','"+approve_type+"',"+o['id']+",'"+approve_confirmButtonText+"','approve')\" style='font-size: 18px' class='fa fa-fw fa-check'></a>\
                <a title='Approve Unpaid' href='javascript:void(0)' onclick=\"leaveApproval('"+disapprove_delete_path+"','"+approve_title+"','"+approve_text+"','"+token+"','"+disapprove_type+"','"+o['id']+"','"+approve_confirmButtonText+"','disapprove')\" style='font-size: 18px' class='fa fa-check-circle'></a>\
                <a title='Disapprove' href='javascript:void(0)' onclick=\"leaveApproval('"+approve_delete_path+"','"+unpaid_title+"','"+approve_text+"','"+token+"','"+approve_type+"',"+o['id']+",'"+unpaid_confirmButtonText+"','approveee')\" style='font-size: 18px' class='fa fa-fw fa-close'></a>";
                
              }else{
                  var extra_html = "<a href='javascript:void(0)' onclick=\"deleteRecord('"+delete_path+"','"+title+"','"+text+"','"+token+"','"+type+"',"+o['id']+")\" style='font-size: 18px' class='fa fa-fw fa-trash'></a><a href='javascript:void(0)' onclick=\"leaveApproval('"+approve_delete_path+"','"+approve_title+"','"+approve_text+"','"+token+"','"+approve_type+"',"+o['id']+",'"+approve_confirmButtonText+"','approve')\" style='font-size: 18px' class='fa fa-fw fa-check'></a>";
              }
            @else
              if(o['status'] == 'p'){
                var extra_html = "<a href='javascript:void(0)' onclick=\"deleteRecord('"+delete_path+"','"+title+"','"+text+"','"+token+"','"+type+"',"+o['id']+"','"+approve_confirmButtonText+"')\" style='font-size: 18px' class='fa fa-fw fa-trash'></a>";
              }else{
                return '';
              }
            @endif
            return extra_html;
          }else{
            return '';
            
          }
       }
      },
    ],
    createdRow: function ( row, data, index ) {
      if ( data['leave_time'] == "full day" ) {
          $('td', row).eq(5).css('background-color','#fff');
      } else {
          $('td', row).eq(5).css('background-color','#009688');
          $('td', row).eq(5).css('color','#fff');
      }
    },
      fnDrawCallback: function(oSettings) {
        res_html = oSettings.json.res_html;
        $('#leavesummary').html(res_html);
      }
    });     
  });
$(document).ready(function()
{
  var id = $('#user').val();
  url2 = $('#leaves_create').attr('href');
  main_url2 = url2.split('/');
  main_url2[4] = id;
  user_main_url2 = main_url2.join('/');
  $('#leaves_create').attr('href',user_main_url2);
});

  $('#user').change(function(){
    id = $(this).val();
    <?php session::forget("leave_user_id");?>
    url1 = $('#leaves_create').attr('href');
    main_url1 = url1.split('/');
    main_url1[4] = $(this).val();
    user_main_url1 = main_url1.join('/');
    $('#leaves_create').attr('href',user_main_url1);
    //for export leaves
    url = $('#exportleaves').attr('href');
    main_url = url.split('/');
    main_url[6] = $(this).val();
    user_main_url = main_url.join('/');
    $('#exportleaves').attr('href',user_main_url);

    $('.dataTable').each(function() {
        dt = $(this).dataTable();
        dt.fnDraw();
    });
  });
  $('#leavetype').change(function(){
    //for export leaves
    url = $('#exportleaves').attr('href');
    main_url = url.split('/');
    main_url[7] = $(this).val();
    user_main_url = main_url.join('/');
    $('#exportleaves').attr('href',user_main_url);

    $('.dataTable').each(function() {
        dt = $(this).dataTable();
        dt.fnDraw();
    });
  });
  $('#year').change(function(){
    //for export leaves
    url = $('#exportleaves').attr('href');
    main_url = url.split('/');
    main_url[8] = $(this).val();
    user_main_url = main_url.join('/');
    $('#exportleaves').attr('href',user_main_url);

    $('.dataTable').each(function() {
        dt = $(this).dataTable();
        dt.fnDraw();
    });
  });
  $(".select_check_box").on('click', function(){
    console.log('hi');
    var is_checked = $(this).is(':checked');
    $(this).closest('table').find('tbody tr td:first-child input[type=checkbox]').prop('checked',is_checked);
    $(".select_check_box").prop('checked',is_checked);
  });
});

</script>
@include('partials.alert')
@stop