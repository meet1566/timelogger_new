@extends('layout.layouts')
@section('style')
    <style type="text/css">
     .profile-detail textarea {
        width: 100%;
        }
         .profile-detail .dl-horizontal, .profile-detail .dd-horizontal {
        margin: 10px 0;
        }

    </style>
@stop
@section('content')


<h1> Approve Leave</h1>  
<div class="row">
    <div class="col-md-9">
        <div class="profile-detail">
            
            <div class="row">
                <div class="col-sm-3">
                    <dl class="dl-horizontal dd-horizontal">
                        <dt>Username</dt>
                    </dl>
                </div>
                <div class="col-sm-9">
                    <dd class="dd-horizontal">
                       <?= $user->fullname ?>
                    </dd>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-3">
                    <dl class="dl-horizontal dd-horizontal">
                        <dt>Leave Date</dt>
                    </dl>
                </div>
                <div class="col-sm-9">
                    <dd class="dd-horizontal">
                       <?= $leaves->date ?>
                    </dd>
                </div>
            </div>
           
            <div class="row">
                <div class="col-sm-3">
                    <dl class="dl-horizontal">
                        <dt>Leave Type</dt>
                    </dl>
                </div>
                <div class="col-sm-9">
                    <dd class="dd-horizontal">
                        <?= $leaves->leave_type ?>  
                    </dd>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <dl class="dl-horizontal">
                        <dt>Leave Time</dt>
                    </dl>
                </div>
                <div class="col-sm-9">
                    <dd class="dd-horizontal">
                        <?= $leaves->leave_time ?>  
                    </dd>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <dl class="dl-horizontal dd-horizontal">
                        <dt>Leave Purpose</dt>
                    </dl>
                </div>
                <div class="col-sm-9">
                    <dd class="dd-horizontal">
                        <?= $leaves->leave_purpose ?> 
                    </dd>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <dl class="dl-horizontal">
                        <dt>From Hours</dt>
                    </dl>
                </div>
                <div class="col-sm-9">
                    <dd class="dd-horizontal">
                      <?= $leaves->from_hour ?> 
                    </dd>
                </div>
            </div>
             <div class="row">
                <div class="col-sm-3">
                    <dl class="dl-horizontal">
                        <dt>To Hours</dt>
                    </dl>
                </div>
                <div class="col-sm-9">
                    <dd class="dd-horizontal">
                      <?= $leaves->to_hour ?> 
                    </dd>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <dl class="dl-horizontal">
                        <dt>Message</dt>
                    </dl>
                </div>
                <div class="col-sm-9">
                    <textarea class="form-control" name='message' id="message" placeholder="Message"></textarea>
                </div>
            </div></br>
            <div class="row">
                <div class="col-sm-offset-3 col-sm-4">
                    <button type="button" class="btn btn-primary" id="approve">Approve</button>
                    <button type="button" class="btn btn-danger" id="reject">Reject</button>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
<?= Html::script('js/jquery-2.1.4.min.js') ?>
<script>
    var token = "<?= csrf_token() ?>";
        $(document).ready(function(){
            $('#approve').click(function(){

                var id =$(this).attr('id');
                var ele  = $(this);
                var eleHtml = ele.html();
                var message = $('#message').val();
                
                $.ajax({
                    url : "<?= URL::route('sendmail.approve',$token) ?>",
                    type : "post",
                    data : { id : id, message : message , _token: token },
                    dataType : 'json',
                    beforeSend : function() {
                        ele.prop('disabled', true).html('<i class="fa fa-spin fa-spinner"></i>');
                    },
                    complete : function(){
                        ele.prop('disabled',false).html(eleHtml);
                    },
                    success : function(resp) {
                        $('#message').val('');
                    }
                });
            });

            $('#reject').click(function(){

                var id =$(this).attr('id');
                var ele  = $(this);
                var eleHtml = ele.html();
                var message = $('#message').val();
                
                $.ajax({
                    url : "<?= URL::route('sendmail.reject',$token) ?>",
                    type : "post" ,
                    data : { id : id, message : message , _token: token },
                    dataType : 'json',
                    beforeSend : function() {
                        ele.prop('disabled', true).html('<i class="fa fa-spin fa-spinner"></i>');
                    },
                    complete : function(){
                        ele.prop('disabled',false).html(eleHtml);
                    },
                    success : function(resp) {
                        $('#message').prop('value', false);
                    }
                });
            });
        });
</script>