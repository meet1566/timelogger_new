<!DOCTYPE html>
<html>
<head>
</head>
	<body style="background-color: #fff">
		<div class="container" style="margin-top:-1cm; ">
		  <div style="border: 1px solid #ddd;margin-bottom: 15px;float: left;width: 100%">
		  	<table style="width: 100%;float: left;" border="1">
		  		<tbody>
		  			<tr>
		  				<td style="float: left;width: 50%;padding: 10px;font-weight: bold;">Date</td>
		  				<td style="float: left;width: 50%;padding: 10px;font-weight: bold;">Employee</td>
		  				<td style="float: left;width: 50%;padding: 10px;font-weight: bold;">Leave Type</td>
		  				<td style="float: left;width: 50%;padding: 10px;font-weight: bold;">Leave purpose</td>
		  				<td style="float: left;width: 50%;padding: 10px;font-weight: bold;">Leave Time</td>
		  				<td style="float: left;width: 50%;padding: 10px;font-weight: bold;">Status</td>
		  			</tr>	
		  			@foreach($leaves as $key=>$value)
		  				<tr>
			  				<td style="float: left;width: 50%;padding: 10px">{{$value['date']}}</td>
			  				<td style="float: left;width: 50%;padding: 10px">{{$value['name']}}</td>
			  				<td style="float: left;width: 50%;padding: 10px">{{$value['leave_type']}}</td>
			  				<td style="float: left;width: 50%;padding: 10px">{{$value['leave_purpose']}}</td>
			  				<td style="float: left;width: 50%;padding: 10px">{{$value['leave_time']}}</td>
			  				@if ($value['status'] == 'a') 
				                  <td style="float: left;width: 50%;padding: 10px"><span class="badge bg-green">Approve</span></td>
				            @elseif($value['status'] == 'd')
				                  <td style="float: left;width: 50%;padding: 10px"><span class="badge bg-green">Disapprove</span></td>
				            @else      
				                <td style="float: left;width: 50%;padding: 10px"><span class="badge bg-green">Pending</span></td>
				            @endif    
			  			</tr>	
		  			@endforeach
		  		</tbody>
		  	</table>
		</div>
	</div>
</body>
</html>		  	