<div class="col-md-12">
  <ul class="nav nav-pills">
    <li class="leave-sum active">
      <a href="javascript:;">CASUAL
      <span class="badge bg-green">{{$casual_leave}}/<strong>6</strong></span></a>
    </li>
    <li class="leave-sum active">
      <a  href="javascript:;">SICK
      <span class="badge bg-green">{{$sick_leave}}/<strong>6</strong></span></a>
    </li>
    <li class="leave-sum active">
      <a href="javascript:;">PREVILEGE
      <span class="badge bg-green">{{$privilege_leave}}/<strong>6</strong></span></a>
    </li>
  </ul>
</div>