@extends('layout.layout')
@section('content')
<style>
    .manage
    {   
        /*align: left;*/
        display: flex;
        flex-direction: column;
        align-items: flex-start;
    }
</style>
<div class="page-title manage">
    <div>
        <h1>Manage Leave</h1>
    </div>
    <div style="font-size: 20px;margin-top: 10px">{{$showUserName->fullname}}</div>
</div>
<div class="clearix"></div>
    <div class="col-md-12">
        <div class="card">
            <h3 class="card-title">Add Leaves</h3>
            <div class="card-body">
            @include('partials.alert')
                <?= Form::open(array('url' => route('user.leaves.store',$userId) ,'class' => 'form-horizontal')) ?>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Date</label>
                        <div class="col-sm-8">
                            <?= Form::date('date', old('date'), ['class' => 'form-control', 'placeholder' => 'From_Date']); ?>
                            <?= $errors->first('date',"<span class='text-danger'>:message</span>");?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">LeaveType</label>
                        <div class="col-sm-8">
                            <?= Form::select('leave_type',[''=>'select','Sick Leave'=>'Sick Leave', 'Casual Leave'=>'Casual Leave','Privilege Leave'=>'Privilege Leave'] ,old('leave_type'), ['class' => 'form-control']); ?>
                            <?= $errors->first('leave_type',"<span class='text-danger'>:message</span>");?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">LeaveTime</label>
                        <div class="col-sm-8">
                            <?= Form::select('leave_time', [''=>'select','full day'=>'Full Day', 'half day'=>'Half Day'], old('leave_time'),['class' => 'form-control','id' => 'leave_time']); ?>
                            <?= $errors->first('leave_time',"<span class='text-danger'>:message</span>");?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">LeavePurpose</label>
                        <div class="col-sm-8">
                            <?= Form::text('leave_purpose', old('leave_purpose'), ['class' => 'form-control', 'placeholder' => 'LeavePurpose']); ?>
                            <?= $errors->first('leave_purpose',"<span class='text-danger'>:message</span>");?>
                        </div>
                    </div>
                    <div class="form-group" id="from_hour_div">
                        <label class="control-label col-sm-2">From Hours</label>
                        <div class="col-sm-8">
                            <?= Form::text('from_hour', '10:00 AM', ['class' => 'form-control timepicker1', 'placeholder' => 'From Hours']); ?>
                            <?= $errors->first('from_hour',"<span class='text-danger'>:message</span>");?>
                        </div>
                    </div>
        			<div class="form-group" id="to_hour_div">
                        <label class="control-label col-sm-2">To Hours</label>
                        <div class="col-sm-8">
                            <?= Form::text('to_hour', '07:00 PM', ['class' => 'form-control timepicker2', 'placeholder' => 'To Hours']); ?>
                            <?= $errors->first('to_hour',"<span class='text-danger'>:message</span>");?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Ungranted Leave</label>
                        <div class="col-sm-8">
                            <?= Form::checkbox('ungrant', 1, (old('ungrant') == 1) ? true : false); ?>
                            <?= $errors->first('ungrant',"<span class='text-danger'>:message</span>");?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-2">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <a href="<?= URL::route('show.leaves',$userId) ?>" class="btn btn-white btn-default"> Back</a>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop
@section('style')
    <?= Html::style('asset/css/bootstrap-timepicker.min.css') ?>
    <?= Html::style('asset/css/new_dashboard.css') ?>
    <?= Html::style('asset/css/bootstrap-datepicker.css') ?>
    <?= Html::style('asset/css/teal.css') ?>
@stop
@section('script') 
    <?= Html::script('asset/js/bootstrap-timepicker.min.js') ?>  
    <?= Html::script('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js') ?>
    <?= Html::script('asset/js/bootstrap-datepicker.min.js') ?> 
    <?= Html::script('asset/js/jquery.validate.min.js') ?>
    <?= Html::script('js/Chart.bundle.min.js') ?>  

<script>
$(document).ready(function() {
    // $(document).on('click', '.timepicker1', function() {
    //     $(this).timepicker();
    // });
    // $(document).on('click', '.timepicker2', function() {
    //     $(this).timepicker();
    // });
    $('form').submit(function(){
        $(this).find('button:submit').html('<i class="fa fa-spinner fa-spin"></i>').prop('disabled', true);
    });

    var leaveTime = $('#leave_time');

    fromAndToHoursShowHide();

    leaveTime.on('change',function(){
        fromAndToHoursShowHide();
    });

    function fromAndToHoursShowHide() {

        leaveTimeVal = leaveTime.val();

        if (leaveTimeVal == 'half day') {
            $('#from_hour_div').show();
            $('#to_hour_div').show();
        } else {
            $('#from_hour_div').hide();
            $('#to_hour_div').hide();
        }
    }
});

</script>
@stop