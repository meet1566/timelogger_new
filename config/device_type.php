<?php

return [
'mouse' => 'Mouse',
'charger' => 'Charger',
'laptop' => 'Laptop',
'pc' => 'Pc',
'battery' => 'Battery',
'power-cable' => 'Power Cable',
'phone' => 'Phone',
'key-board' => 'Key Board',
'MacBook' => 'MACBook',
'MacBook charger' => 'MacBook Charger',
'Pc Cabinet' => 'Pc Cabinet'
];

?>