<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\Auth\PasswordController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AssetsController;
use App\Http\Controllers\AllowUserLogsController;
use App\Http\Controllers\AssetsMasterController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\HolidaysController;
use App\Http\Controllers\HolidaysMasterController;
use App\Http\Controllers\HolidaysYearController;
use App\Http\Controllers\AptitudeController;
use App\Http\Controllers\LeavesController;
use App\Http\Controllers\LogController;
use App\Http\Controllers\QuizController;
use App\Http\Controllers\ServicesController;
use App\Http\Controllers\ShowController;
use App\Http\Controllers\TimeloggerController;
use App\Http\Controllers\UserPermissionController;
use App\Http\Controllers\userPermissionMaster;
use App\Http\Controllers\WeeklyLogsController;
use App\Http\Controllers\WorkingDaysController;
use App\Http\Controllers\Auth\AuthenticatedSessionController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/dashboard',[TimeloggerController::class,'admin'])->middleware(['auth'])->name('dashboard.admin');
Route::get('/logout',[AuthenticatedSessionController::class,'destroy'])->name('Logout');

Route::group(['middleware' => 'web'], function() {


    Route::post('/trash',[TimeloggerController::class, 'trash'])->name('user.trash');
    Route::get('/recyclebin',[TimeloggerController::class, 'recyclebin'])->name('user.recyclebin');

    Route::get('/restore/{id}',[TimeloggerController::class, 'restore'])->name('user.restore');

    Route::get('/user/delete/{id}',[TimeloggerController::class, 'destroy'])->name('user.destroy');

    Route::get('denied',function(){
            return 'denied';
        })->name('user.denied');
     

    /*User authinticate*/
    Route::group(array('middleware' => ['auth']), function(){

            // Route::get('dashboard',[TimeloggerController::class,'admin'])->name('dashboard.admin');

            Route::post('get-selected-pemission',[UserPermissionController::class,'getSelectedPermissions'])->name('selectpermission.get');
           
           
        //karishma  
            Route::post('leaves/delete',[LeavesController::class,'destroy'])->name('leaves.delete');
            Route::post('leave/status',[LeavesController::class,'leaveStatus'])->name('leave.status');

            Route::post('notification/isread',[LeavesController::class,'notificationIsread'])->name('notification.isread');

            Route::post('notification/isview',[LeavesController::class,'notificationIsview'])->name('notification.isview');

            Route::post('leave/checkbox_status',[LeavesController::class,'checkboxtatus'])->name('leave.checkboxtatus');

            Route::get('logs/exportlogs/{id}/{month}/{year}',[ShowController::class,'exportlogs'])->name('logs.exportlogs');

            Route::get('logs/alluserexportlogs/{id}/{month}/{year}',[ShowController::class,'allUserExportLogs'])->name('logs.alluserexportlogs');

            Route::get('workingdays',[WorkingDaysController::class,'index'])->name('admin.workingdays');

            // Route::post('workingdays',[WorkingDaysController::class,'workingdays'])->name('admin.workingdays');

            Route::post('store/workingdays',[WorkingDaysController::class,'storeworkingdays'])->name('admin.store.workingdays');

            Route::get('user/profile/upload-photo/{id?}',[TimeloggerController::class,'getUploadProfilePhoto'])->name('user.profile.upload.get');

            Route::post('user/profile/upload-photo',[TimeloggerController::class,'postUploadProfilePhoto'])->name('user.profile.upload.post');

            Route::get('/user/profile/crop-photo',[TimeloggerController::class,'getCropProfilePhoto'])->name('user.profile.photo.crop.get');

            Route::post('/user/profile/crop-photo',[TimeloggerController::class,'postCropProfilePhoto'])->name('user.profile.photo.crop.post');

            Route::post('/user/deactive',[TimeloggerController::class,'userDeactive'])->name('user.deactive');

            Route::post('position/store',[TimeloggerController::class,'positionStore'])->name('position.store');

            Route::post('/leaves/chart',[TimeloggerController::class,'getChart'])->name('dashboard.leaves.chart');

            Route::post('/leaves/secondchart',[TimeloggerController::class,'getSecondChart'])->name('dashboard.leaves.secondchart');

            Route::post('/leaves/logs-chart',[TimeloggerController::class,'getLogsChart'])->name('dashboard.logs.chart');

            Route::post('workingdays/hours',[TimeloggerController::class,'workingdaysHours'])->name('admin.workingdays.hours');


            Route::resource('services',ServicesController::class,['except'=>['delete','show']]);
        
            Route::post('services/delete',[ServicesController::class,'delete'])->name('services.delete');
            Route::post('category/store',[ServicesController::class,'categoryStore'])->name('category.store');
            Route::get('weeklylogs',[WeeklyLogsController::class,'weeklyLogs'])->name('admin.weeklylogs');
            //end karishma
            //rushik
            Route::resource('/holiday-master',HolidaysMasterController::class,['except'=>['delete','show']]);
             
            Route::post('/holiday-master/delete',[HolidaysMasterController::class,'delete'])->name('holiday-master.delete');

            Route::resource('/holiday-year',HolidaysYearController::class,['except'=>['delete','show']]);
        
            Route::post('/holiday-year/delete',[HolidaysYearController::class,'delete'])->name('holiday-year.delete');

            Route::get('holiday/upload-photo',[HolidaysMasterController::class,'getUploadProfilePhoto'])->name('holiday.upload.get');

            Route::post('holiday/upload-photo',[HolidaysMasterController::class,'postUploadProfilePhoto'])->name('holiday.upload.post');
        
            Route::get('/holiday/crop-photo',[HolidaysMasterController::class,'getCropProfilePhoto'])->name('holiday.photo.crop.get');
       
            Route::post('/holiday/crop-photo',[HolidaysMasterController::class,'postCropProfilePhoto'])->name('holiday.photo.crop.post');

            Route::post('/dashboard/leaves/store',[LeavesController::class,'leavestore'])->name('dashboard.leaves.store');

            Route::post('/dashboard/leaves/second/store',[LeavesController::class,'leavesecondstore'])->name('dashboard.leaves.second.store');

            Route::post('/dashboard/leaves/third/store',[LeavesController::class,'leavethirdstore'])->name('dashboard.leaves.third.store');

            Route::post('/user/checkin',[TimeloggerController::class,'checkIn'])->name('user.checkin');

            Route::resource('assets',AssetsController::class,['except'=>['delete','show']]);

            Route::post('assets/delete',[AssetsController::class,'delete'])->name('assets.delete');

            Route::resource('assets/master',AssetsMasterController::class,['except'=>['delete','show']]);

            Route::get('assets/master/create',[AssetsMasterController::class,'create'])->name('assets.master.create');

            Route::get('assets/master/index',[AssetsMasterController::class,'index'])->name('assets.master.index');

            Route::post('assets/master/store',[AssetsMasterController::class,'store'])->name('assets.master.store');

            Route::get('assets/master/edit/{id}',[AssetsMasterController::class,'edit'])->name('assets.master.edit');

            Route::post('assets/master/delete',[AssetsMasterController::class,'delete'])->name('assets.master.delete'); 

            Route::post('assets/master/update',[AssetsMasterController::class,'update'])->name('assets.master.update');  
 

            Route::post('getDevice',[AssetsController::class,'getDevice'])->name('getDevice.index');  
            //end rushik

            Route::post('getlogs',[LogController::class,'getLogs'])->name('getlogs.index');  

            Route::post('logs/import/daily',[LogController::class,'importlogs'])->name('logs.logsimport');  
            Route::get('user/leaves/export/{user}/{type}/{year}',[LeavesController::class,'exportLeaves'])->name('leaves.export');  



        Route::group(array('middleware'=> ['acl']),function(){

                Route::resource('/user',TimeloggerController::class,['except'=>['destroy']]);
                Route::resource('/holiday',HolidaysController::class,['except'=>['destroy']]);
                Route::resource('/leaves',LeavesController::class,['except'=>['destroy','show']]);
                Route::resource('/allowuserlogs',AllowUserLogsController::class,['except'=>['destroy','show']]);


                Route::get('holiday/delete/{id}',[HolidaysController::class,'destroy'])->name('holidays.delete');  

                Route::get('leaves/{user_id}/create',[LeavesController::class,'userCreate'])->name('user.leaves.create');  

                Route::post('leaves/{user_id}/store',[LeavesController::class,'userStore'])->name('user.leaves.store');  

                Route::get('quiz-delete/{quiz_id}',[QuizController::class,'delete'])->name('quiz.delete');  
                Route::get('quiz-send/{quiz_id}',[QuizController::class,'send'])->name('quiz.send');  

                Route::resource('quiz',QuizController::class,['except'=>['destroy','show']]);
                
                Route::get('logs',[LogController::class,'index'])->name('logs.index');  

                Route::resource('userpermission',UserPermissionController::class);

                Route::get('profile',[ShowController::class,'profile'])->name('show.user'); 

                Route::get('logs/{id}',[ShowController::class,'log'])->name('show.logs');  

                Route::get('logs/logs-user-workinghours/{id}/{month}/{year}',[ShowController::class,'userWorkingHours'])->name('show.logs.userworkinghours');  

                Route::get('leaves/{id}',[ShowController::class,'leave'])->name('show.leaves');  

                // Route::post('remaning_leave',[
                //  'as'=>'remaning_leave',
                //  'uses'=>'ShowController@remaning_leave'
                // ]);

                Route::get('user-leaves',[ShowController::class,'userLeaves'])->name('show.userleaves'); 

                Route::get('Leave/{id}/approve',[LeavesController::class,'approveLeave'])->name('leaves.approve');  

                Route::post('Leave/{id}/Approve',[LeavesController::class,'approvemail'])->name('sendmail.approve');  

                Route::post('Leave/{id}/Reject',[LeavesController::class,'rejectemail'])->name('sendmail.reject');  

                // Route::post('leaves/delete',[
                //  'as'   => 'leaves.destroy',
                //  'uses' => 'LeavesController@destroy'
                // ]);

                Route::get('profile/upload-photo',[ShowController::class,'getUploadProfilePhoto'])->name('profile.upload.get');  

                Route::post('profile/upload-photo',[ShowController::class,'postUploadProfilePhoto'])->name('profile.upload.post');  

                Route::get('/profile/crop-photo',[ShowController::class,'getCropProfilePhoto'])->name('profile.photo.crop.get'); 

                Route::post('/profile/crop-photo',[ShowController::class,'postCropProfilePhoto'])->name('profile.photo.crop.post'); 

                Route::get('profile/edit',[ShowController::class,'edit'])->name('show.edit'); 

                Route::post('update',[ShowController::class,'update'])->name('show.update'); 

                Route::get('user-profile/{id}',[ShowController::class,'user_profile'])->name('show.user-profile'); 

                Route::get('create_report_assets',[AssetsController::class,'CreateAdminAssetsReport'])->name('assets.createAdminReport');

                Route::get('assets_master_history',[AssetsMasterController::class,'ShowHistory'])->name('assetsMaster.history'); 

                Route::get('user_assets',[AssetsController::class,'ShowUserAssets'])->name('user.assets');

                Route::get('reported_assets',[AssetsController::class,'ShowUserAssetsReport'])->name('assets.reportedAssets'); 

                Route::get('show_report_assets',[AssetsController::class,'ShowAdminAssetsReport'])->name('show_report_assets'); 

        });
    });

    // Route::controllers([
    //         'password' => 'Auth\PasswordController',
    // ]);

    // Route::get('remaning_leave',[
    //  'as'=>'remaning_leave',
    //  'uses'=>'ShowController@remaningleave'
    // ]);
    // Route::get('pay_count',[
    //  'as'=>'pay_count',
    //  'uses'=>'ShowController@paycount'

    // ]);


    Route::get('assets_status/{id}',[AssetsMasterController::class,'AssetsStatus'])->name('assets_status'); 
    Route::post('user_assets_report',[AssetsController::class,'CreateReport'])->name('user_assets_report'); 
    Route::get('assets/assets_status_history/{id}',[AssetsMasterController::class,'ShowAssetsHistory'])->name('assets.statusHistory'); 

    Route::post('delete_reported_assets',[AssetsController::class,'DeleteUserAssetsReport'])->name('assets.deleteReport'); 

    Route::post('delete_reported_assets',[AssetsController::class,'DeleteUserAssetsReport'])->name('assets.deleteReport'); 

    Route::post('save_report_assets',[AssetsController::class,'SaveAdminAssetsReport'])->name('assets.saveReport'); 

    Route::post('getassets',[AssetsController::class,'GetAssets'])->name('getassets'); 

    Route::get('admin_report_assets_status/{id}',[AssetsController::class,'ChangeAdminAssetsReportStatus'])->name('admin_report_assets_status'); 

    Route::get('report_assets_status/{id}',[AssetsController::class,'ChangeAssetsReportStatus'])->name('report_assets_status'); 

    Route::get('user_assets_service/{id}',[AssetsController::class,'ShowService'])->name('user_assets_service'); 

    Route::get('userPermissionMaster',[userPermissionMaster::class,'create'])->name('userPermissionMaster'); 

    Route::post('userPermissionMasterEdit',[userPermissionMaster::class,'store'])->name('userPermissionMasterEdit'); 

    Route::post('getroleid',[userPermissionMaster::class,'getRoleId'])->name('getroleid'); 

    Route::get('showroles',[userPermissionMaster::class,'index'])->name('showroles'); 

    Route::get('editroles/{id}',[userPermissionMaster::class,'edit'])->name('editroles'); 

    Route::post('delete_role',[userPermissionMaster::class,'delete'])->name('delete_role'); 

    Route::post('editrole/{id}',[userPermissionMaster::class,'ajaxResponse'])->name('editrole'); 

    Route::post('change_password',[TimeloggerController::class,'changePassword'])->name('change.password');

    Route::get('password',[TimeloggerController::class,'changePasswordIndex'])->name('change.password.index'); 

    Route::post('bank_detail',[TimeloggerController::class,'addBankDetails'])->name('bank.detail');

    Route::post('document_detail',[TimeloggerController::class,'addDocumnetDetails'])->name('document.detail');

    Route::get('aptitude',[AptitudeController::class,'index'])->name('aptitude.index');
    Route::get('aptitude_create',[AptitudeController::class,'create'])->name('aptitude.create');
    Route::post('aptitude_store',[AptitudeController::class,'store'])->name('aptitude.store');
    Route::get('aptitude_edit/{id}',[AptitudeController::class,'edit'])->name('aptitude.edit');
    Route::put('aptitude_update/{id}',[AptitudeController::class,'update'])->name('aptitude.update');
    Route::any('aptitude_delete/{id}',[AptitudeController::class,'delete'])->name('aptitude.delete');
});



require __DIR__.'/auth.php';
