$(document).ready(function() {
         
        $('#slider').slideReveal({
            trigger: $("#trigger"),
            push: false,
            autoEscape: true
        });

        $(window).scroll(function () {
            if ($(this).scrollTop() > 200) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });

        // scroll body to 0px on click
        $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
        
        //$('#back-to-top').tooltip('show');

        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('.scroll-btn').fadeOut();
            } else {
                $('.scroll-btn').fadeIn();
            }
        });

        // scroll body to 0px on click
        $('.scroll-btn').click(function () {
            $('.scroll-btn').tooltip('hide');
            $('body,html').animate({
                scrollTop: $("#home-tabs").offset().top
            }, 800);
            return false;
        });

        // Homepage Tabs
        $('#home-tabs a').click(function(e) {
            e.preventDefault()
            $(this).tab('show')
        })

        // Bind normal buttons
        Ladda.bind('button[type=submit]', {timeout: 2000});

        $('input[type=checkbox]').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            increaseArea: '20%' // optional
        });

        $('input[type=radio]').iCheck({
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });   

        /* Common Search */
        var $select = $('#smartsearch').selectize({

            valueField  : 'url',
            labelField  : 'name',
            searchField : ['name'],
            maxOptions  : 10,
            options     : [],
            create      : false,
            render      : {
                option  : function(item, escape) {

                    is_logo = (item.logo) ? '<img src="'+ item.logo +'">' : '';
                    return '<div>' + 
                                is_logo +
                                escape(item.name) +
                            '</div>';
                }
            },
            optgroups : [
                { value : 'job', label : 'Jobs' },
                { value : 'company', label : 'Companies' },
                { value : 'keyword', label : 'Keywords' }
            ],
            optgroupField : 'class',
            optgroupOrder : ['job','company', 'keyword'],
            load          : function(query, callback) {

                if (!query.length) return callback();
                $.ajax({
                    url      : root+'/smartsearch/common',
                    type     : 'GET',
                    dataType : 'json',
                    data     : {
                        q : query
                    },
                    error    : function() {
                        callback();
                    },
                    success  : function(res) {
                        callback(res.data);
                    }
                });
            },
            onChange: function() {
                window.location = this.items[0];
            },
            onDropdownClose: function($dropdown) {
                this.clearOptions();
                this.removeOption(1)
            } 
        });

        /* Job Search */
        var $select = $('#jobsearch').selectize({
            valueField: 'url',
            labelField: 'name',
            searchField: ['name'],
            maxOptions: 10,
            options: [],
            create: false,
            render: {
                option: function(item, escape) {

                    return '<div>'+ escape(item.name) + '</div>';
                }
            },
            optgroups: [
                {value: 'job', label: 'Jobs'}
            ],
            optgroupField: 'class',
            optgroupOrder: ['job'],
            load: function(query, callback) {
                if (!query.length) return callback();
                $.ajax({
                    url: root+'/smartsearch/job',
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        q: query
                    },
                    error: function() {
                        callback();
                    },
                    success: function(res) {
                        callback(res.data);
                    }
                });
            },
            onChange: function(){
                window.location = this.items[0];
            },
            onDropdownClose: function($dropdown) {
                this.clearOptions();
                this.removeOption(1)
            }
        });  

        /* Company Search */
        var $select = $('#companysearch').selectize({
            valueField: 'url',
            labelField: 'name',
            searchField: ['name'],
            maxOptions: 10,
            options: [],
            create: false,
            render: {
                option: function(item, escape) {

                    is_logo = (item.logo) ? '<img src="'+ item.logo +'">' : '';
                    return '<div>'+ 
                                is_logo+
                                escape(item.name)+
                            '</div>';
                }
            },
            optgroups: [
                {value: 'company', label: 'Companies'},
            ],
            optgroupField: 'class',
            optgroupOrder: ['company'],
            load: function(query, callback) {
                if (!query.length) return callback();
                $.ajax({
                    url: root+'/smartsearch/company',
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        q: query
                    },
                    error: function() {
                        callback();
                    },
                    success: function(res) {
                        callback(res.data);
                    }
                });
            },
            onChange: function(){
                window.location = this.items[0];
            },
            onDropdownClose: function($dropdown) {
                this.clearOptions();
                this.removeOption(1)
            }
        });

        /* Location Search */
        var $select = $('.locationsearch').selectize({
            valueField: 'url',
            labelField: 'name',
            searchField: ['name'],
            maxOptions: 10,
            options: [],
            create: false,
            render: {
                option: function(item, escape) {

                    return '<div>'+ escape(item.name) + '</div>';
                }
            },
            optgroups: [
                {value: 'location', label: 'Locations'}
            ],
            optgroupField: 'class',
            optgroupOrder: ['keyword'],
            load: function(query, callback) {
                if (!query.length) return callback();
                $.ajax({
                    url: root+'/smartsearch/location',
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        q: query
                    },
                    error: function() {
                        callback();
                    },
                    success: function(res) {
                        callback(res.data);
                    }
                });
            },
            onChange: function(){
                window.location = this.items[0];
            },
            onDropdownClose: function($dropdown) {
                this.clearOptions();
                this.removeOption(1)
            }
        });
        
});